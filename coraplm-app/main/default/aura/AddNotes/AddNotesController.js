/*
*   Name           : AddNotesController.js
*   Author         : Tejas Patel
*   Description    : This is a notes component for add new note.
*   JIRA ID        : PUX-340
*/
({	
    //To initialised component data
    doInit : function(component, event, helper) {
        var action = component.get("c.getUserName");
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (state === "SUCCESS") {
                component.set("v.EM.Subject", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    //To close addnote editor | Added by Tejas Patel | PUX-340 | 23-Oct-2017
    close : function(component, event, helper) {
        component.set("v.showNotesScreen",!component.get("v.showNotesScreen"));  
    },
    //To save note as interaction | Added by Tejas Patel | PUX-340 | 23-Oct-2017
    save:function(component, event, helper) {
        var Subject = component.get("v.EM.Subject");
        var HtmlBody = component.get("v.EM.HtmlBody");
        var np = component.getConcreteComponent().getDef().getDescriptor().getNamespace();
        var addNoteTitle = (np == 'c') ? $A.get("$Label.c.Add_Note") : $A.get("$Label.CoraPLM.Add_Note");
        
        if(Subject == undefined || Subject == ''){
            var errorSubject = (np == 'c') ? $A.get("$Label.c.Error_Message_Subject") : $A.get("$Label.CoraPLM.Error_Message_Subject");
            helper.showToast(component, addNoteTitle, errorSubject, 'error');
        }else if(HtmlBody == undefined || HtmlBody == ''){
            var errorBody = (np == 'c') ? $A.get("$Label.c.Error_Message_HtmlBody") : $A.get("$Label.CoraPLM.Error_Message_HtmlBody");
            helper.showToast(component, addNoteTitle, errorBody, 'error');
        }else{
            var caseid = component.get("v.Case.Id"); 
            var eMsg = component.get("v.EM");
            eMsg.RelatedToId = caseid;
            var action = component.get("c.addNoteInfo");
            action.setParams({ EM : eMsg});
            action.setCallback(helper, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                   
                    var callback=component.get("v.callback");
                    if(typeof callback ==='function'){
                        
                        callback(response);
                       
                    } 	
                    
                    var successMessage = (np == 'c') ? $A.get("$Label.c.Success_Message_AddNote") : $A.get("$Label.CoraPLM.Success_Message_AddNote");
                    helper.showToast(component, addNoteTitle, successMessage, 'success');
                    component.set("v.showNotesScreen",!component.get("v.showNotesScreen"));  
                }else if (state === "INCOMPLETE") {
                    alert('Incomplete State');
                }else if (state === "ERROR") {
                    alert('ERROR');
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
                
            });
            $A.enqueueAction(action);
        }            
    },
})