/*
*   Name           : AddNotesHelper.js
*   Author         : Tejas Patel
*   Description    : This is a notes component for add new note.
*   JIRA ID        : PUX-340
*/
({
	  showToast: function(component,title,message,msgtype){
        
          var  type = msgtype;
          var  css = 'toast-top-center';
          var  msg = message;
    
        toastr.options.positionClass = 'toast-top-full-width';
        toastr.options.extendedTimeOut = 0; //1000;
        toastr.options.timeOut = 3000;
        toastr.options.fadeOut = 250;
        toastr.options.fadeIn = 250
        toastr.options.positionClass = css;
        toastr[type](msg);
    }
})