({
    show: function (cmp, event) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
    },
    toggleSpinner: function (cmp) {
        var spinner = cmp.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
    },
    
    hide:function (cmp, event) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-show");
        $A.util.addClass(spinner, "slds-hide");
    },
    getAttachments :function(component,helper){
        var action = component.get("c.getAttachments");
        
        action.setParams({ caseId :component.get("v.Case.Id")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.Attachments',helper.addIcon(response.getReturnValue().attachments));
                helper.toggleSpinner(component);
           }else if (state === "INCOMPLETE") {
                alert('e');
            }else if (state === "ERROR") {
                alert('e');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    deleteAttachments :function(component,Id,helper){
        helper.toggleSpinner(component);
        var action = component.get("c.deleteAttachments");
        action.setParams({ docId :Id,caseId:component.get("v.Case.Id")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.Attachments',helper.addIcon(response.getReturnValue().attachments));
                helper.toggleSpinner(component);
            }else if (state === "INCOMPLETE") {
                alert('e');
            }else if (state === "ERROR") {
                alert('e');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    upload: function(component, file, base64Data, callback) {
        var action = component.get("c.uploadFile");
        this.uploadJs(component, file, base64Data, callback);
        action.setParams({
            fileName: file.name,
            base64Data: base64Data,
            contentType: file.type,
            ct:component.get("v.Case"),
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // this.getAttachments(component);
                component.set('v.Attachments',this.addIcon(response.getReturnValue().attachments));
                var appEvent = $A.get("e.c:ShowToast");
                appEvent.setParams({ "title" : "Success !","message":" File is uploded successfully!" });
                appEvent.fire();
                this.toggleSpinner(component);
                //callback(response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    uploadJs: function(component, file, base64Data, callback) {
         sforce.connection.sessionId = component.get("v.SessionId");
        var attachment         = new sforce.SObject('Attachment');
        attachment.Name        = file.name;
        attachment.IsPrivate   = false;
        attachment.ContentType = file.type;
        attachment.Body        = base64Data;
        attachment.Description = 'xxx';
        attachment.ParentId    = component.get("v.Case.Id");
        sforce.connection.create([attachment]);
    },
    addIcon :function(attachments) {
        for(var i=0;i<attachments.length;i++){
            var extension =attachments[i].ContentDocument.FileExtension;
            attachments[i]['isPreviewAvailable']=false;
            var size=attachments[i].ContentDocument.ContentSize;
            if((size/1000).toFixed(2)<1){
                size=size+' BYTES';
            }else if(((size/1000)/1024).toFixed(2)<1){
                size=(size/1000).toFixed(2)+' KB';
            }else{
                size=((size/1000)/1024).toFixed(2)+' MB';
            }
            attachments[i].Size=size;
            if(attachments[i].ContentDocument.LatestPublishedVersion.Flag__c=='Archive'){
                attachments[i]['display']=false;
            }else{
                attachments[i]['display']=true;
            }
            if(extension=='pdf'){
                attachments[i]['icon']='doctype:pdf';
                attachments[i]['isPreviewAvailable']=true;
            }else if(extension=='xlsx'){
                attachments[i]['icon']='doctype:excel';
                
            }else if(extension=='jpg' || extension=='png' || extension=='gif' || extension=='jpg'){
                attachments[i]['icon']='doctype:image';
                attachments[i]['isPreviewAvailable']=true;
            }else{
                attachments[i]['icon']='doctype:attachment';
            }
            
        }
        return attachments;
    },
    filterAttachment :function(attachments,filterValue) {
        for(var i=0;i<attachments.length;i++){
            if(attachments[i].ContentDocument.LatestPublishedVersion.Flag__c==filterValue){
                attachments[i]['display']=true;
            }else{
                attachments[i]['display']=false;
            }
        }   
        return attachments;
    },

})