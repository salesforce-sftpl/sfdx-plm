/*
* Authors	   :  Atul Hinge
* Date created :  14/11/2017
* Purpose      :  [PUX-30,PUX-27,PUX-270] This component Display attachment.
* Dependencies :  CaseDetailView.cmp,AttachmentListView.cmp,SubTabViewer.cmp
* JIRA ID      :  PUX-30,PUX-27,PUX-270
* -----------------------------------------------------------
* Modifications: 
*        Date:  
*        Purpose of modification:  
*        Method/Code segment modified:    
*/
({
    /* Authors: Atul Hinge || Purpose: To initialized component data  */
    init : function(component, event, helper) {
        var nameSpace =component.getConcreteComponent().getDef().getDescriptor().getNamespace();
        nameSpace=(nameSpace=='c')?'':nameSpace+'__';
        component.set("v.attachmentURL","/apex/"+nameSpace+"FileViewer?fileId="+component.get("v.attachmentId"));
        
        
        component.set("v.doctype",component.get("v.subTab.label").split('.')[1]);
        if(component.get("v.subTab.label").split('.')[1]!='pdf'){
            helper.toggleSpinner(component, event);
        }
    },
    /* Authors: Atul Hinge || Purpose: To close attachment viewer */
    onLoadImage : function(component, event, helper) {
      helper.toggleSpinner(component, event);
    },
    
})