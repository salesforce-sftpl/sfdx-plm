/*
Authors: Tej Pal Kumawat
Date created: 4-Dec-2017
Purpose: PUX-459: Comment entered by user should store and display in historical maner.
Dependencies: CaseAdditionalDetails.cmp, CaseAdditionalDetailsController.js, CaseAdditionalDetails.cls             
*/
({
	/*
	Authors: Tej Pal Kumawat
	Purpose: PUX-459: Comment entered by user should store and display in historical maner.
	Dependencies: None
    */
    getRecord: function(component, event) {
		var action = component.get("c.getCaseComments");
		action.setParams({
            childObjectId: component.get("v.childObjectId")
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var storeResponse = response.getReturnValue();
				//To set internal attributes value 
				component.set('v.caseCommentsList', storeResponse);				
				if (storeResponse.length != 0) {
					component.set('v.hasData', true);
				}
				component.set('v.resReceived', true);
			} else if (state === "INCOMPLETE") {
				//Logic for incomplete
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);
	}
})