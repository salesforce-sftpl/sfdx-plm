/*
Authors: Atul Hinge,Niraj Prajapati
Date created: 14/08/2017
Purpose: This controller will get the fields as per field set configured and update case record.
Dependencies: 
-------------------------------------------------
Modifications:
                Date: 15/09/2017
                Purpose of modification: [PUX-124,129] Fields Display from fieldset and Validation on fields
                Method/Code segment modified: populateRelatedField function and UpdateCase function
                
*/ 

({
	/*
		Authors: Niraj Prajapati
		Purpose: It will be called at very first time when component render,To fetch field set wise fields
		Dependencies: CaseDetailSection.cmp
                
    */

	doInit: function(component, event, helper) {
        var ct=component.get("v.ct");
        component.set("v.caseHolder",ct.UserAction__c);
		var conEle = component.find("MLDiv");
        if ($A.util.hasClass(conEle, 'slds-is-open')){ 
			$A.util.removeClass(conEle, 'slds-is-open');
		}
		helper.getPredictionDetail(component, event);
	/*
		helper.toggleSpinner(component, event);
		//Below code will identify which fieldset is required for particular case record. 
		var process = component.get("v.ct.Process__c") == undefined ? '' : component.get("v.ct.Process__c");
		var workType = (component.get("v.ct.WorkType__c") == undefined) ? '' : component.get("v.ct.WorkType__c");
		var cstatus = component.get("v.ct.Status__c") == undefined ? '' : component.get("v.ct.Status__c");
		var action = component.get("c.getFieldSetByParam");
		action.setParams({
			caseType: process,
			subType: workType,
			stage: cstatus,
			subStage: '',
			fieldLocation: 'Editable',
			userProfile: ''
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var responceVar = response.getReturnValue();
				var createCmpEvent = $A.get("e.c:GetFieldSetComponent");
                console.log(createCmpEvent);
				createCmpEvent.setParams({
					"objectType": "CaseManager__c",
					"fsName": responceVar,
					"paramName": "ct",
					"component": component,
					"size": 6,
					"callback": function(response) {
						component.set("v.processingfields", response);
						helper.toggleSpinner(component, event);
						if (component.get("v.ct.OwnerId") == component.get("v.loginUserId")) {
							if (response.toString().indexOf('UserAction__c') > 0) {
								component.set("v.ct.UserAction__c", '');
							}
						}
					},
				});
				createCmpEvent.fire();
			} else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);
		var action1 = component.get("c.getAllFieldSet");
		action1.setParams({
			objType: 'CaseManager__c'
		});
		action1.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var responceVar = response.getReturnValue();
				component.set("v.fieldSetList", responceVar);
				helper.populateRelatedField(component, event);
			} else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action1);*/
        component.set("v.isOpen",true);
         $A.util.addClass(component.find("openCaseDetailsContainer"), "slds-hide");
	},
	/*
		Authors: Niraj Prajapati
		Purpose: toggle function will used to open/close sidepanel
		Dependencies: CaseDetailSection.cmp
                
    */
	onmouseover: function(component, event, helper) {
    		$A.util.addClass(component.find("openCaseDetails"), "Animation");
    },
    onmouseout: function(component, event, helper) {
    		$A.util.removeClass(component.find("openCaseDetails"), "Animation");
    },
    toggleDoc: function(component, event, helper) {
        helper.openCaseDetails(component, event);
    },
	/*
		Authors: Atul Hinge
		Purpose: functions will used to open/maximize casedetail section
	*/
    expand: function(component, event, helper) {
        var toggleCaseDetail = component.find("docContainer");
		$A.util.toggleClass(toggleCaseDetail, "slds-modal slds-fade-in-open slds-docked-composer-modal");
        $A.util.toggleClass(component.find("backdrop"), "slds-backdrop_open");
        $A.util.toggleClass(component.find("dialog"), "slds-modal slds-fade-in-open slds-docked-composer-modal");
        $A.util.toggleClass(component.find("container"), "slds-modal__container");
        $A.util.toggleClass(component.find("content"), "slds-modal__content");
        if( component.get("v.size")==6){
            component.set("v.size","12");
        }else{
            component.set("v.size","6");
        }
        
	},
    /*
		Authors: Atul Hinge
		Purpose: functions will used to maximize casedetail section
	*/
    maximize: function(component, event, helper) {
         helper.openCaseDetails(component, event);
         helper.maximizeCaseDetails(component, event);
   	},
    /*
		Authors: Atul Hinge
		Purpose: functions will used to minimize casedetail section
	*/
    minimize: function(component, event, helper) {
        helper.openCaseDetails(component, event);
        helper.minimizeCaseDetails(component, event);
   	},
     /*
		Authors: Atul Hinge
		Purpose: functions will used to close casedetail section
	*/
     close: function(component, event, helper) {
        helper.closeCaseDetails(component, event);
    },
	/*
		Authors: Niraj Prajapati
		Purpose: populateRelatedField function will used to populate mandatory field based on selected user action Value
		Dependencies: CaseDetailSection.cmp
                
    */
	
	populateRelatedField: function(component, event, helper) {
        if(component.get("v.caseHolder") != component.get("v.ct.UserAction__c") && component.get("v.ct.UserAction__c") !=''){
            component.set("v.enableSave",false);
        }
		//helper.populateRelatedField(component, event);
	},
    
	/*
		Authors: Niraj Prajapati
		Purpose: updateCase function will used to validate mandatory fields and update Case record.
		Dependencies: CaseDetailSection.cmp
                
    */
	
	updateCase: function(component, event, helper) {
        component.find('caseLayout').validate();
        if(!Boolean(component.get("v.ct")['hasError'])){
             helper.updateCase(component, event);
        }else{
           
        }
		/*var appEvent = $A.get("e.c:ValidateComponentEvent");
		var test = component.get("v.processingfields");
        if (component.get("v.relatedFields") != '') {
			test = test.concat(component.get("v.relatedFields"));
		}
		appEvent.setParams({
			"components": test,
			"callback": function(response) {
				if (response.success == true) {
					helper.updateCase(component, event);
				}
			}
		});
		appEvent.fire();*/
	},
    
	/* 
    	Authors: Tushar Moradiya.
    	Purpose: To open PredictedValue popup
		Dependencies : CaseTrackerHelper.js
	*/
	expandMLDiv: function(component, event, helper) {     
        $A.util.toggleClass(component.find('MLmodals'), 'slds-hide');
        
	},
	/* 
    	Authors: Tushar Moradiya.
    	Purpose: To close PredictedValue popup
		Dependencies : CaseTrackerHelper.js
	*/
	collepsMLDiv: function(component, event, helper) {     
		$A.util.toggleClass(component.find('MLmodals'), 'slds-hide');
	},
	/* 
    	Authors: Tushar Moradiya.
    	Purpose: To update PredictedValue status of Indexing Field
		Dependencies : CaseTrackerHelper.js
	*/
    acceptDiscardPrediction:function(component, event, helper) {    
        $A.util.toggleClass(component.find('MLmodals'), 'slds-hide');
        helper.acceptDiscardPrediction(component, event);		
	},
})