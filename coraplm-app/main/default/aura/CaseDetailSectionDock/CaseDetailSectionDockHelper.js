/*
Authors: Atul Hinge,Niraj Prajapati
Date created: 14/08/2017
Purpose: This Helper will call from controller and update case record.
Dependencies: 
-------------------------------------------------
Modifications:
                Date: 15/09/2017
                Purpose of modification: [PUX-124,129] Fields Display from fieldset and Validation on fields
                Method/Code segment modified: populateRelatedField function and UpdateCase function
                
*/ 
({
    /*
		Authors: Niraj Prajapati
		Purpose: To update case detail record
		Dependencies: CaseDetailSection.cmp
                
    */
    
    
    updateCase: function(component, event) {
        var caseId = component.get("v.ct.Id");
        var compEvent = component.getEvent("caseAction");
        compEvent.setParams({
            "Action": "update",
            "Case": component.get("v.ct"),
            "callback": function(response) {
                //component.set("v.ct", response.getReturnValue()[0]);
				console.log(response.returnValue.ErrorMessage);
				console.log(response.returnValue.FieldNames);
				//alert();
				if(response.returnValue.ErrorMessage!=null){
					var appEvent = $A.get("e.c:ShowToast");
					appEvent.setParams({
						"title": "Error !",
						"message": response.returnValue.ErrorMessage,
						"type":"error"
					});
					appEvent.fire();


                }
				else{
					var appEvent = $A.get("e.c:ShowToast");
                    var auraId = event.getSource().getLocalId();
                    if(auraId=='save'){
                        
                        appEvent.setParams({
                            "title": "Success !",
                            "message": " Case successfully saved!"
                        });
                    }
                    if(auraId=='submit'){
                        appEvent.setParams({
                            "title": "Success !",
                            "message": " Case successfully Submited!"
                        });
                    }
					appEvent.fire();
					
					if(auraId=='submit'){
						var compEvent = component.getEvent("caseAction");
						compEvent.setParams({
							"Action": "redirect",
							"Case": component.get("v.ct")
						});
						compEvent.fire();
					}
				}
                
            }
        });
        compEvent.fire();
    },
    /*
		Authors: Niraj Prajapati
		Purpose: Spinner symbol will display till all fields value will fetch
		Dependencies: CaseDetailSection.cmp
                
    */
    toggleSpinner: function (cmp, event) {
        var spinner = cmp.find("mySpinner1");
        $A.util.toggleClass(spinner, "slds-hide");
    },
    /*
		Authors: Atul Hinge
		Purpose: maximizeCaseDetails,minimizeCaseDetails,openCaseDetails,closeCaseDetails
        		 This functions are utlity function for maximize,minimize,open,and close CaseDetailSection
		Dependencies: CaseDetailSection.cmp
                
    */
    maximizeCaseDetails: function (component, event) {
        var toggleCaseDetail = component.find("docContainer");
        $A.util.addClass(toggleCaseDetail, "slds-modal slds-fade-in-open slds-docked-composer-modal");
        //$A.util.toggleClass(toggleCaseDetail, "slds-is-open");
        $A.util.addClass(component.find("backdrop"), "slds-backdrop_open");
        $A.util.addClass(component.find("dialog"), "slds-modal slds-fade-in-open slds-docked-composer-modal");
        $A.util.addClass(component.find("container"), "slds-modal__container");
        $A.util.addClass(component.find("content"), "slds-modal__content");
        $A.util.addClass(component.find("CloseBtn"), "slds-hide");
        component.set("v.size","4");
        component.set("v.isFullScreen",true);
        
    },
    minimizeCaseDetails: function (component, event) {
        var toggleCaseDetail = component.find("docContainer");
        $A.util.removeClass(toggleCaseDetail, "slds-modal slds-fade-in-open slds-docked-composer-modal");
        //$A.util.toggleClass(toggleCaseDetail, "slds-is-open");
        $A.util.removeClass(component.find("backdrop"), "slds-backdrop_open");
        $A.util.removeClass(component.find("dialog"), "slds-modal slds-fade-in-open slds-docked-composer-modal");
        $A.util.removeClass(component.find("container"), "slds-modal__container");
        $A.util.removeClass(component.find("content"), "slds-modal__content");
        $A.util.removeClass(component.find("CloseBtn"), "slds-hide");
        component.set("v.size","12");
        component.set("v.isFullScreen",false);
    },
    openCaseDetails: function (component, event) {
        $A.util.removeClass(component.find("caseDetails"), "CaseDetailAnimation");
        $A.util.removeClass(component.find("caseDetails"), "slds-hide");
          
        $A.util.removeClass(component.find("CloseBtn"), "slds-hide");
        $A.util.addClass(component.find("openCaseDetailsContainer"), "slds-hide");
        component.set("v.isOpen",true);
    },
    closeCaseDetails: function (component, event) {
        $A.util.addClass(component.find("caseDetails"), "slds-hide");
        $A.util.removeClass(component.find("openCaseDetailsContainer"), "slds-hide");
        $A.util.addClass(component.find("CloseBtn"), "slds-hide");
        
        component.set("v.isOpen",false);
    },
    /*
		Authors: Niraj Prajapati
		Purpose: populateRelatedField function will used to populate mandatory field based on selected user action Value
		Dependencies: CaseDetailSection.cmp
                
    */
    
    populateRelatedField: function(component, event) {
        var fsname = component.get("v.ct.UserAction__c") == undefined ? 'Blank' : component.get(
            "v.ct.UserAction__c");
        fsname = fsname.replace(/\s/g, '');
        var fsList = component.get("v.fieldSetList");
        if (fsList.indexOf(fsname.toLowerCase()) == -1) {
            component.set("v.relatedFields", '');
        } else {
            var createCmpEvent = $A.get("e.c:GetFieldSetComponent");
            createCmpEvent.setParams({
                "objectType": "CaseManager__c",
                "fsName": fsname,
                "paramName": "ct",
                "component": component,
                "size": 6,
                "callback": function(response) {
                    component.set("v.relatedFields", response);
                },
            });
            createCmpEvent.fire();
        }
    },
	/* 
    	Authors: Tushar Moradiya.
    	Purpose: To get PredictedValue of Indexing Field
		Dependencies : CaseTrackerHelper.js
	*/
	getPredictionDetail: function(component, event) {
		var action = component.get("c.getPredictedValue");
		action.setParams({
			caseId: component.get("v.ct.Id")
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {	               
                if(response.getReturnValue().AWSResponseJson__c != undefined){
                    component.set("v.predictedField", JSON.parse(response.getReturnValue().AWSResponseJson__c));				
					component.set("v.predictedFieldStatus", response.getReturnValue().Predicted_Field_Status__c);				
                }
                
            } else if (state === "INCOMPLETE") {
				//Logic
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);
	},
	/* 
    	Authors: Tushar Moradiya.
    	Purpose: To update PredictedValue status of Indexing Field
		Dependencies : CaseTrackerHelper.js
	*/
     acceptDiscardPrediction:function(component, event) {     
         
		var status = event.getSource().getLocalId();
        var action = component.get("c.updateCaseTracker");
		var casObje=component.get("v.ct");		

		if(status=="Accept"){
			var delId = [];
			var getAllId = component.find("chkBox");
			if(! Array.isArray(getAllId)){			
				if (getAllId.get("v.value") == true) {
				delId.push(getAllId.get("v.text"));
				}
			}else{
				for (var i = 0; i < getAllId.length; i++) {
					if (getAllId[i].get("v.value") == true) {
						delId.push(getAllId[i].get("v.text"));
					}
				}
			}
            
			var jsonObj=component.get("v.predictedField").Indexing;
			for (var property in jsonObj) {
				var apiName = jsonObj[property].APIName;
				var val = jsonObj[property].Value;                           
				if (jsonObj.hasOwnProperty(property) && delId.includes(apiName)) {		
                    component.find("caseLayout").getFieldDetails(apiName);
                    var fieldDetails = component.find("caseLayout").get("v.methodResponse");
					if(fieldDetails.fieldType == "PICKLIST"){
						console.log(fieldDetails.dependentValues[casObje[fieldDetails.controlField]]);
						var arr = fieldDetails.dependentValues[casObje[fieldDetails.controlField]]
						console.log("0"+arr.some(item => item.value == val));
						if(arr.some(item => item.value == val)){
						console.log("1"+arr.some(item => item.value == val));
							casObje[apiName]=val;
						}else{
							var appEvent = $A.get("e.c:ShowToast");
							appEvent.setParams({ "title" : "Error !","message":"Value misMatch" });
							appEvent.fire();
							break;
						}
					}
					else{
						casObje[apiName]=val;
					}                    
					component.set("v.ct",casObje);
				}                                           
			}				  
         }

		action.setParams({
			caseObj: casObje,
            status: status
		});

		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {			
				component.set("v.predictedFieldStatus",status);					
               if(status=="Accept"){
					component.set("v.ct",casObje);		
			   }				
            } else if (state === "INCOMPLETE") {
				//Logic
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);		
	}, 
})