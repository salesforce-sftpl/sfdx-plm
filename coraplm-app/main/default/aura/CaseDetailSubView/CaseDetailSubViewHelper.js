/*
    Name           : CaseDetailSubViewHelper.js
    Author         : Parth Lukhi
    Description    : CaseDetailSubViewHelper JS used for Case Detail Subview part
    JIRA ID        : PUX-15 
*/
({
	/* Start : makeMeOwner  function  is used for Accept button event*/
	makeMeOwner: function(component, event, isRedirect) {
		var ct = component.get("v.Case");
        var owner = component.get("v.Case.OwnerId");
		ct.OwnerId = component.get("v.loginUserId");
		component.set("v.Case", ct);
		if (component.get("v.Case.Status__c") == "Ready For Processing") {
			ct.OriginalProcessor__c = component.get("v.loginUserId");
		}
		var compEvent = component.getEvent("caseAction");
		var myCompEvent = component.getEvent("caseAction");
		var caseObj = component.get("v.Case");
		compEvent.setParams({
			"Action": "update",
			"Case": caseObj,
			"callback": function(response) {
				var state = response.getState();
				if (state === "SUCCESS") {
					var message = '';
					component.set("v.Case", response.getReturnValue()[0]);
					if (!isRedirect) {
						message = "Case-" + caseObj.Name + " is accepted!";
					} else {
						message = "Case-" + caseObj.Name + " is accepted and navigated to detail page!";
					}
					var appEvent = $A.get("e.c:ShowToast");
					appEvent.setParams({
						"title": "Success !",
						"message": message
					});
					appEvent.fire();
					//component.destroy(); 
					//component.set("v.templates",response.getReturnValue().templates);
					//component.set("v.body",component.get("v.templates")[0].Body);
				} else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
					var np = component.toString().split('":"')[1].split('"}')[0];
					
					ct.OwnerId = owner;
                    component.set("v.Case", ct);
					var errorMessage = (np == 'c') ? $A.get("$Label.c.Error_Message") : $A.get("$Label.CoraPLM.Error_Message");
					var appEvent = $A.get("e.c:ShowToast");
					appEvent.setParams({
						"title": "Success !",
						"message": errorMessage
									
					});
					appEvent.fire();
                    var errors = response.getError();
					if (errors) {
						if (errors[0] && errors[0].message) {
							console.log("Error message: " + errors[0].message);
						}
					} else {
						console.log("Unknown error");
					}
				}
			}
		});
		compEvent.fire();
	},
	/* End : makeMeOwner  function  is used for Accept button event*/
})