/*
  * Authors		: Parth Lukhi
  * Date created : 10/08/2017 
  * Purpose      : Controller JS used for getting Case Detail View 
  * Dependencies : CaseDetailViewHelper.js
  * -------------------------------------------------
  * Modifications:
                Date: 
                Purpose of modification:  
                Method/Code segment modified: 
*/
({	/*
	 * Authors: Parth Lukhi
	 * Purpose:  Init  function  for Case Detail View  component
	 * Dependencies:  CaseDetailViewHelper.js
	 * Start : Init  function  for Case Detail View  component*/
     init :function(component, event, helper) {
       console.log('##############'+window.SessionId);
       helper.getCaseDetail(component, event);
       //Code start for PUX-512
       helper.getQCAvailable(component, event);
       //Code End for PUX-512
 	},
	/* PUX-150 & PUX-146*/
	/*
	 * Authors: Parth Lukhi
	 * Purpose:  This method is used for setting active tab style and setting breadcrumb 
	 * Dependencies:  CaseDetailViewHelper.js
	 * Start : This method is used for setting active tab style and setting breadcrumb */
	handleActive: function(cmp, event, helper) {		
		var tab = event.getSource();
		var tabName = tab.get('v.id');
		if(tabName == 'Case History'){
			var comVar = cmp.get("v.caseHistroyRefCounter");
			cmp.set("v.caseHistroyRefCounter", comVar+1);
			console.log(cmp.get("v.caseHistroyRefCounter"));
		}
       
		cmp.set("v.selectedTabId", tab.get('v.id'));
	},
    /*
	 * Authors: Parth Lukhi
	 * Purpose: This method is used to display Processing field Icon only on Case Details and Attachment Tab 
	 * Dependencies:  CaseDetailViewHelper.js
	 * Start : This method is used to display Processing field Icon only on Case Details and Attachment Tab */
    selTabIdChange: function(cmp, event, helper) {		
	     var selTab=cmp.get("v.selTabId");
        
        if(selTab=='Attachments' || selTab=='Case Detail'){
            
            $A.util.removeClass(cmp.find("processingField"), 'slds-hide');
        }else{
            $A.util.addClass(cmp.find("processingField"), 'slds-hide');
            
        }
		//cmp.set("v.selectedTabId", tab.get('v.id'));
	},
	/* PUX-150 & PUX-146*/
     /* Start : Init  function  for Case Detail View  component*/
    doChange: function(cmp, event, helper) {
        var nameSpace =cmp.getConcreteComponent().getDef().getDescriptor().getNamespace();
        nameSpace=(nameSpace=='c')?'':nameSpace+'__';
        var caseObject=cmp.get("v.case");
        if(caseObject[nameSpace+'UserAction__c'] !='QC Reject')
        {
			cmp.set("v.userAction",caseObject[nameSpace+'UserAction__c']);            
        }
        if(caseObject[nameSpace+'UserAction__c']=='QC Reject' && cmp.get("v.userAction") != 'QC Reject' && cmp.get("v.isQCAvailable")==true){
            cmp.set("v.userAction",'QC Reject');
            cmp.find("tabs").set("v.selectedTabId","Quality Control");
            cmp.set("v.selectedTabId", "Quality Control");
            
          var appEvent = $A.get("e.c:ShowToast");
                appEvent.setParams({
                    "title": "Success !",
                    "message":  $A.get("$Label.c.QC_Require_Message")
                });
                appEvent.fire();
        }
    }
})