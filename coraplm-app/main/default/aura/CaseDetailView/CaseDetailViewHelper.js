/*
  * Authors		: Parth Lukhi
  * Date created : 10/08/2017 
  * Purpose      : Helper JS used for getting Case Detail View 
  * Dependencies : CaseDetailViewController.apxc
  * ------------------------------------------------------
  * Modifications:
                Date: 
                Purpose of modification:  
                Method/Code segment modified: 

*/
({
	/*
	 * Authors: Parth Lukhi
	 * Purpose: For geeting Case Detail using CaseID
	 * Dependencies:   CaseDetailViewController.apxc
	 * 
	 * Start : Init  function  for New Case Gen component*/
    getCaseDetail: function(component, event) {
		var action = component.get("c.getCaseDetail");
		action.setParams({
			caseId: component.get("v.case.Id")
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.case", response.getReturnValue());
                component.set("v.userAction",response.getReturnValue().UserAction__c);
                
			} else if (state === "INCOMPLETE") {
				//Logic
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);
	},
    /*
	 * Authors: Niraj Prajapati
	 * Purpose: For getting Quality Field set Name
	 * Dependencies:   CaseDetailViewController.apxc
	 * Code start for PUX-512
	 */
     
     getQCAvailable: function(component, event) {
		var action = component.get("c.getQCFieldSetName");
		action.setParams({
			caseId: component.get("v.case.Id")
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
                //alert(1);
                //console.log(JSON.stringify(response.getReturnValue().qcInfo.qcFormFieldsetName));
                if(response.getReturnValue().qcInfo.qcFormFieldsetName != null && response.getReturnValue().qcInfo.qcFormFieldsetName !='')
                {
					component.set("v.isQCAvailable", true);
                    component.set("v.qcInfo",response.getReturnValue().qcInfo);}
                else{
                    //alert(2);
                    component.set("v.isQCAvailable", false);}
			} else if (state === "INCOMPLETE") {
				//Logic
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);
	}
    //Code End for PUX-512
})