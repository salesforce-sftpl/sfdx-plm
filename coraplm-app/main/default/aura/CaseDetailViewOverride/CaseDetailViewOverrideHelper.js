/*
    Name           : CaseDetailViewOverrideHelper.js
    Author         : Ashish Kr.
    Description    : Helper JS used for getting Case Detail View 
    JIRA ID        : 
*/
({
    getCaseDetail: function(component, event) {
        var action = component.get("c.getCaseDetail");
        action.setParams({
            caseId: component.get("v.caseid")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.Case", response.getReturnValue());
                component.set("v.renderComponents", true);

            } else if (state === "INCOMPLETE") {
                //Logic
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    getLoginUser: function(component, event) {
        console.log('getLoginUser');
        //Start Code for PUX-221
        var action = component.get("c.getUserInfo");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //console.log('123'+response.getReturnValue());
                component.set("v.loginUserId", response.getReturnValue().UserId);
                window.SessionId = response.getReturnValue().UserSessionId;
                window.InstanceName=response.getReturnValue().InstanceName;
                //End Code for PUX-221
            } else if (state === "INCOMPLETE") {
				console.log('incomplete');
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        //console.log("Error message: " + 
                        //       errors[0].message);
                    }
                } else {
                    //console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
})