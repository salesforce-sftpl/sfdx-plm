/*
  Authors	   :   Rahul Pastagiya
  Date created :   05/10/2017
  Purpose      :   To display Approval Detail and History records for given Case.
	               It will display fields in Tabular form as per given Field Set value.
  Dependencies :   CaseHistoryHelper.js, CaseHistoryController.cls
  __________________________________________________________________________
  Modifications:
		    Date:  
		    Purpose of modification:  
		    Method/Code segment modified: 
 */
({
	/*
		Authors: Rahul Pastagiya
		Purpose: To fetch Case History records with the help of Helper
	*/
	doInit: function(component, event, helper) {
		helper.getRecord(component, event);
	},
	/*
		Authors: Rahul Pastagiya
		Purpose: It will be called when HTTP Req is being initiated
	*/
	waiting: function(component, event, helper) {
		document.getElementById("Accspinner").style.display = "block";
	},
	/*
		Authors: Rahul Pastagiya
		Purpose: It will be called when HTTP Req is completed
	*/
	doneWaiting: function(component, event, helper) {
		if (document.getElementById("Accspinner") != null || document.getElementById("Accspinner") != undefined) {
			document.getElementById("Accspinner").style.display = "none";
		}
	},
})