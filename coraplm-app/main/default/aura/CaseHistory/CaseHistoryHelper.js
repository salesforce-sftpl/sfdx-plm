/*
  Authors	   :   Rahul Pastagiya
  Date created :   05/10/2017
  Purpose      :   To display Approval Detail and History records for given Case.
	               It will display fields in Tabular form as per given Field Set value.
  Dependencies :   CaseHistoryController.js, CaseHistoryController.cls
  __________________________________________________________________________
  Modifications:
		    Date:  
		    Purpose of modification:  
		    Method/Code segment modified: 
 */
({
	/*
		Authors: Rahul Pastagiya
		Purpose: To fetch Case History Data from server side by passing parameters Field Set, Case
	*/
	getRecord: function(component, event) {
		var action = component.get("c.getCaseHistoryData");
		action.setParams({
			childobjectname: component.get("v.childobjectname"),
			childObjectId: component.get("v.childObjectId")
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var storeResponse = response.getReturnValue();
				//To set internal attributes value 
				component.set('v.caseHistoryList', storeResponse);
				if (storeResponse.length != 0) {
					component.set('v.hasData', true);
				}
				component.set('v.resReceived', true);
			} else if (state === "INCOMPLETE") {
				//Logic
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);
	}
})