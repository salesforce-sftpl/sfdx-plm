/*
 *	Name 		   : CaseListController.js
 *   Author         : Niraj Prajapati
 *   Date   		   : 09-Aug-2017
 *   Description    : To display all Cases tab wise with the help of CaseListItem Component.
 *   JIRA ID        : PUX-23
 *
 *	____________________________________________________________
 *	Modifications: 1
 *			Authors:  Tushar Moradiya
 *			Date:  18-Sep-2017
 *			Purpose of modification:  Implement filters on list page.
 *			Method/Code segment modified: PUX-305.
 *	____________________________________________________________
 *	Modifications: 2
 *			Authors:  Tushar Moradiya
 *			Date:  11-Dec-2017
 *			Purpose of modification: Filter removed while we go bake on case list view
 *			Method/Code segment modified: PUX-421
 	Modifications: 3
                   Date: 09/02/2018
   				   Changed by : Niraj Prajapati
                   Purpose of modification: [PUX-625] Buttons visibility based on profile level access

 */
({
    /*
		Authors: Tushar Moradiya
		Purpose: To assign default value when page is load.
				 It will be called at very first time when component render	
				 To fetch all cases as per selected tab and prepare filter dropdown according	
	*/
    doInit: function(component, event, helper) {
        /* Start : PUX-304 *///component.toString().split('":"')[1].split('"}')[0]
        // Start : PUX-501
        var tempPageNo = component.get("v.tempPageNumber");
        component.set("v.pageNumber", tempPageNo);
        // End : PUX-501
        var conMainEle = component.find("filterMainDiv");
        if (component.get('v.condition') == 'Unassigned') {
            if ($A.util.hasClass(conMainEle, 'slds-hide')) {
                $A.util.removeClass(conMainEle, 'slds-hide');
            }
        } else {
            $A.util.removeClass(conMainEle, 'slds-show');
            $A.util.addClass(conMainEle, 'slds-hide');
        }
        var emptyList = [];
        component.set('v.caseList', emptyList);
        //PUX-207 
        if (component.get('v.resetPagination')) {
            component.set('v.pageNumber', 1);
            //component.set('v.pageNo',1);
        }
        component.set('v.maxPage', 1);
        //component.set('v.direction','first');
        //PUX-207
        component.set('v.caseList', emptyList);
        var noRecordfoundMsgDiv = component.find("noRecordfoundMsg");
        $A.util.addClass(noRecordfoundMsgDiv, 'toggleDiv');
        /* End : PUX-304 */
        /* Start : PUX-305 ,PUX-543*/
        var conEle = component.find("filterDiv");
        if ($A.util.hasClass(conEle, 'slds-is-open')) $A.util.removeClass(conEle, 'slds-is-open');
        /* End : PUX-543 */

        component.find("inputFilterFirst").set("v.value", null);
        component.find("selectFieldSecond").set("v.value", null);
        component.find("selectFieldThird").set("v.value", null);
        component.find("inputFilterSecond").set("v.value", null);
        component.find("inputFilterSecond").set("v.disabled", true);
        component.find("inputFilterThird").set("v.value", null);
        component.find("inputFilterThird").set("v.disabled", true);
        component.set("v.selectedFilter", null);
        //Start : PUX-625
		helper.getUserDetail(component, event);
        //End : PUX-625
        helper.getQueueDropdown(component, event);
        helper.getFieldDropdown(component, event);
        helper.getCases(component, event);
        /* End : PUX-305 */
        //	PUX-390 
        helper.getSortDropdownList(component, event);
        //	PUX-390
    },
    /*
		Authors: Atul Hinge
		Purpose: It will be called when HTTP Req is being processed at server side	
	*/
    waiting: function(component, event, helper) {
        document.getElementById("Accspinner").style.display = "block";
        var noRecordfoundMsgDiv = component.find("noRecordfoundMsg");
        $A.util.addClass(noRecordfoundMsgDiv, 'toggleDiv');
    },
    
    /*
		Authors: Atul Hinge
		Purpose: It will be called when HTTP Res is received at client side	
	*/
    doneWaiting: function(component, event, helper) {
        if (document.getElementById("Accspinner") != null || document.getElementById("Accspinner") != undefined) {
            document.getElementById("Accspinner").style.display = "none";
        }
        var noRecordfoundMsgDiv = component.find("noRecordfoundMsg");
        $A.util.removeClass(noRecordfoundMsgDiv, 'toggleDiv');
    },
    /*
		Authors: Rahul Pastagiya
		Purpose: It will be called on change page number in pagination
	*/
    renderPages: function(component, event, helper) {
        //Start : PUX-207, PUX-501	
        if (component.get("v.pageNumber") != component.get("v.tempPageNumber")) {
            helper.getCases(component, event);
        }
        //End : PUX-207, PUX-501	
    },

    /*
		Authors: Tushar Moradiya
		Purpose: Expand filter Div on click of filter button 
		//PUX-305	, PUX-543
	*/
    expandFilterDiv: function(component, event, helper) {
        var mainDiv = component.find('filterDiv');
        $A.util.toggleClass(mainDiv, 'slds-is-open');
    },
    /*
		Authors: Tushar Moradiya
		Purpose: Apply selected filter on cases
		//PUX-305	
	*/
    filterCase: function(component, event, helper) {
        var conEle = component.find("filterDiv");

        if (component.find("selectFieldSecond").get("v.value") != null && component.find("selectFieldSecond").get("v.value") != undefined && component.find("selectFieldSecond").get("v.value") != "" &&
            (component.find("inputFilterSecond").get("v.value") == null || component.find("inputFilterSecond").get("v.value").trim() == "")) {
            //component.find('inputFilterSecond').set("v.disabled", true);
            //component.find("selectFieldSecond").set("v.value", null);
            component.set("v.errors", "Please Enter Value for '" + component.find("selectFieldSecond").get("v.value").split(":")[1] + "'");
            component.find("applyFilterBtn").set("v.disabled", true);
        } else if (component.find("selectFieldThird").get("v.value") != null && component.find("selectFieldThird").get("v.value") != undefined && component.find("selectFieldThird").get("v.value") != "" &&
            (component.find("inputFilterThird").get("v.value") == null || component.find("inputFilterThird").get("v.value").trim() == "")) {
            //component.find('inputFilterThird').set("v.disabled", true);
            //component.find("selectFieldThird").set("v.value", null);
            component.set("v.errors", "Please Enter Value for '" + component.find("selectFieldThird").get("v.value").split(":")[1] + "'");
            component.find("applyFilterBtn").set("v.disabled", true);
        } else {
            component.set("v.errors", "");
            component.set('v.sortField', 'LastModifiedDate');
            component.set('v.sortDirection', 'desc');
            component.set('v.pageNumber', 1);
            /* Code start for PUX-421 */
            window.inputFilterFirst = null;
            window.inputFilterSecond = null;
            window.selectFieldSecond = null;
            window.inputFilterThird = null;
            window.selectFieldThird = null;
            /* Code end for PUX-421 */
            helper.getCases(component, event);
            //start PUX-543
            if ($A.util.hasClass(conEle, 'slds-is-open'))
                $A.util.removeClass(conEle, 'slds-is-open');
            //end PUX-543
        }
    },

    /*
		Authors: Tushar Moradiya
		Purpose: used for remove selected filter when close filter pill
		//PUX-305	
	*/
    handleRemove: function(component, event, helper) {
        var conEle = component.find("filterDiv");
        //start PUX-543
        if ($A.util.hasClass(conEle, 'slds-is-open'))
            $A.util.removeClass(conEle, 'slds-is-open');
        //end PUX-543
        var filterId = event.getSource().get("v.name");
        var oldValue = component.get("v.selectedFilter");
        var caseId = event.getSource().get("v.label").split(':')[1];

        var values = [];
        for (var i = 0; i < oldValue.length; i++) {
            if (caseId != oldValue[i].displayValue) {
                values.push(oldValue[i]);
            } else {
                if (filterId == 'filter1') {
                    component.find("inputFilterFirst").set("v.value", null);
                    window.inputFilterFirst = null;
                } else if (filterId == 'filter2') {
                    component.find("selectFieldSecond").set("v.value", null);
                    component.find("inputFilterSecond").set("v.value", null);
                    component.find("inputFilterSecond").set("v.disabled", true);
                    window.inputFilterSecond = null;
                    window.selectFieldSecond = null;

                } else if (filterId == 'filter3') {
                    component.find("selectFieldThird").set("v.value", null);
                    component.find("inputFilterThird").set("v.value", null);
                    component.find("inputFilterThird").set("v.disabled", true);
                    window.inputFilterThird = null;
                    window.selectFieldThird = null;

                } /* Code start for PUX-358 */
                else if (filterId == 'filter4') {
                    window.overDueIn = '';
                } /* Code End for PUX-358 */
            }
        }
        component.set("v.selectedFilter", values);
        //alert('in '+window.overDueIn);
        helper.getCases(component, event);
    },
    //PUX-305 end
    /*
		Authors: Parth Lukhi
		Purpose: handleNewCase function is used for navigation to New Case on click of New Case button
		Dependencies: PLMLayout.cmp
                
    */
    handleNewCase: function(component, event, helper) {
        var compEvent = component.getEvent("caseAction");
        compEvent.setParams({
            "Action": "redirectNewCase",

        });
        compEvent.fire();
    },
    /* Start : PUX-304 */
    /*
		Authors: Parth Lukhi
		Purpose: handleMenuSelect is called when action is selected from menu button on list view
		Dependencies: PLMLayout.cmp
                
    */
    handleMenuSelect: function(component, event, helper) {
        var selectedMenuItemValue = event.getParam("value");
        var id = event.getSource().get("v.name");
        if (selectedMenuItemValue == "Accept") {
            helper.makeMeOwner(component, event, false);
        } else if (selectedMenuItemValue == "Split") {
            var splitCaseEvt = component.getEvent("splitMergeCaseEvent");
            splitCaseEvt.setParams({
                "cases": component.get("v.selectedCases"),
                "action": "split",
                "TabName": "Split Case"
            });
            // Fire the event
            splitCaseEvt.fire();
        } else if (selectedMenuItemValue == "Merge") {
            var mergeCaseEvt = component.getEvent("splitMergeCaseEvent");
            mergeCaseEvt.setParams({
                "cases": component.get("v.selectedCases"),
                "action": "merge",
                "TabName": "Merge Case"
            });
            // Fire the event
            mergeCaseEvt.fire();
        }
        /** Added By Uzeeta
         * Purpose : Change Owner Functionality */
       else if (selectedMenuItemValue == "ChangeOwner") {
           console.log(component.get("v.selectedCases"));
           component.set("v.isDisplay",true);
          
        }
    },
    /* End : PUX-304 */
    /*
		Authors: Tushar Moradiya
		Purpose: close exapanded div on cancel or apply button
		//PUX-305	
	*/
    //PUX-305 start
    collepsFilterDivNew: function(component, event, helper) {
        component.find("inputFilterFirst").set("v.value", null);
        component.find("selectFieldSecond").set("v.value", null);
        component.find("selectFieldThird").set("v.value", null);
        component.find("inputFilterSecond").set("v.value", null);
        component.find("inputFilterSecond").set("v.disabled", true);
        component.find("inputFilterThird").set("v.value", null);
        component.find("inputFilterThird").set("v.disabled", true);
        component.set("v.selectedFilter", null);
        component.set("v.errors", '');
        var mainDiv = component.find('filterDiv');
        //start PUX-543
        $A.util.removeClass(mainDiv, 'slds-is-open');
        //end PUX-543
        //helper.showPopupHelper(component, 'modaldialog', 'slds-');		
    },
    /*
		Authors: Tushar Moradiya
		Purpose: enabled/disabled input filter text box on selection of dropdown
		//PUX-305	
	*/
    enabledInput: function(component, event, helper) {
        var id = event.getSource().get("v.name");
        if (id == "selectFieldSecond" && component.find("selectFieldSecond").get("v.value") != "") {
            component.find("inputFilterSecond").set("v.disabled", false);
        } else if (id == "selectFieldThird" && component.find("selectFieldThird").get("v.value") != "") {
            component.find("inputFilterThird").set("v.disabled", false);
        } else {
            if (id == "selectFieldSecond" && component.find("selectFieldSecond").get("v.value") == "") {
                component.find("inputFilterSecond").set("v.value", "");
                component.find("inputFilterSecond").set("v.disabled", true);
            } else if (id == "selectFieldThird" && component.find("selectFieldThird").get("v.value") == "") {
                component.find("inputFilterThird").set("v.value", "");
                component.find("inputFilterThird").set("v.disabled", true);
            }
        }
        component.set("v.errors", "");
        component.find("applyFilterBtn").set("v.disabled", false);
    },
    /*
		Authors: Tushar Moradiya
		Purpose: validate date and email value of input filter based on selected filter
		//PUX-305	
	*/
    validateFilter: function(component, event, helper) {
        component.find("applyFilterBtn").set("v.disabled", false);
        var inputValue = event.getSource().get("v.value").trim();
        var inputName = event.getSource().get("v.name");
        var fieldtype, fieldName;
        if (inputValue != null && inputValue != '' && inputName == "inputFilterSecond") {
            fieldName = component.find("selectFieldSecond").get("v.value").split(":")[1];
            fieldtype = component.find("selectFieldSecond").get("v.value").split(":")[2];
        } else if (inputValue != null && inputValue != '' && inputName == "inputFilterThird") {
            fieldName = component.find("selectFieldThird").get("v.value").split(":")[1];
            fieldtype = component.find("selectFieldThird").get("v.value").split(":")[2];
        }

        if (fieldtype == "DATETIME" && inputValue != null && inputValue != '') {
            if (inputValue.length >= 8 && !isNaN(Date.parse(inputValue))) {
                component.set("v.errors", "");
                component.find("applyFilterBtn").set("v.disabled", false);
            } else {
                component.set("v.errors", "Please Enter valid Date '" + fieldName + "'");
                component.find("applyFilterBtn").set("v.disabled", true);
            }
        } else if (fieldtype == "EMAIL" && inputValue != null && inputValue != '') {
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

            if (reg.test(inputValue)) {
                component.set("v.errors", "");
                component.find("applyFilterBtn").set("v.disabled", false);
            } else {
                component.set("v.errors", "Please Enter valid Email '" + fieldName + "'");
                component.find("applyFilterBtn").set("v.disabled", true);
            }
        }else if ((fieldtype == "CURRENCY" || fieldtype == 'DOUBLE' ) && inputValue != null && inputValue != '') {
			var numb=inputValue.split(',');
			for(var i=0 ; i < numb.length ; i++){
				if (!isNaN(numb[i])) {
					component.set("v.errors", "");
					component.find("applyFilterBtn").set("v.disabled", false);
				} else {
					component.set("v.errors", "Please Enter valid '" + fieldName + "'");
					component.find("applyFilterBtn").set("v.disabled", true);
					break;
				}
			}
        }else if ((fieldtype == "BOOLEAN") && inputValue != null && inputValue != '') {
			var bool=inputValue.split(',');
			for(var i=0 ; i < bool.length ; i++){
				if (bool[i].toUpperCase()=='TRUE' || bool[i].toUpperCase() == 'FALSE') {
					component.set("v.errors", "");
					component.find("applyFilterBtn").set("v.disabled", false);
				} else {
					component.set("v.errors", "Please Enter valid '" + fieldName + "'");
					component.find("applyFilterBtn").set("v.disabled", true);
					break;
				}
			}
        }
    },
    //PUX-305 end
    /*
		Authors: Rahul Pastagiya
		Purpose: Open Sorting dropdown on Case list page
		
	*/
    openSortDropDown: function(cmp, event, helper) {
        var conEle1 = cmp.find("sortDropdown");
        $A.util.toggleClass(conEle1, 'slds-is-open');
    },
    /*
		Authors: Rahul Pastagiya
		Purpose: Close Sorting dropdown on Case list page
		
	*/
    closeSortDropDown: function(cmp, event, helper) {
        var conEle1 = cmp.find("sortDropdown");
        $A.util.removeClass(conEle1, 'slds-is-open');
    },
    /*
		Authors: Rahul Pastagiya
		Purpose: Select sorting value from Sorting dropdown on Case list page
		
	*/
    selectedSortValue: function(cmp, event, helper) {
        var ctarget = event.currentTarget;
        var id_str = ctarget.dataset.value;
        var sort = ctarget.dataset.sort;
        var currentSelected = cmp.get('v.sortField');

        if (currentSelected == id_str) {
            if (sort == 'desc') {
                sort = 'asc';
            } else {
                sort = 'desc';
            }
        } else {
            sort = 'desc'
        }
        ctarget.dataset.sort = sort;
        cmp.set('v.sortField', id_str);
        cmp.set('v.sortDirection', sort);
        cmp.set('v.pageNumber', 1);
        helper.getCases(cmp, event);


    },
    /*
		Authors: Rahul Pastagiya
		Purpose: Reset sort on List view page
		
	*/
    resetSort: function(cmp, event, helper) {
        cmp.set('v.sortField', 'LastModifiedDate');
        cmp.set('v.sortDirection', 'desc');
        cmp.set('v.pageNumber', 1);
        helper.getCases(cmp, event);
    },
    /*
		Authors: Rahul Pastagiya
		Purpose: toggle Sorting direction i.e desc or asc
		
	*/
    toggleSortDirection: function(cmp, event, helper) {
        var ctarget = event.currentTarget;
        var id_str = ctarget.dataset.value;
    },
    //PUX-390	
		
	fun: function(component, event, helper) {
		alert(1);
	},
	//PUX-723 Manual refresh button needs on Case list view page
    refreshCaseList: function(component, event, helper) {
		component.set("v.errors", "");
        component.set('v.sortField', 'LastModifiedDate');
        component.set('v.sortDirection', 'desc');
        component.set('v.pageNumber', 1);
        window.inputFilterFirst = null;
        window.inputFilterSecond = null;
        window.selectFieldSecond = null;
        window.inputFilterThird = null;
        window.selectFieldThird = null;
        component.find("inputFilterFirst").set("v.value", null);
        component.find("selectFieldSecond").set("v.value", null);
        component.find("selectFieldThird").set("v.value", null);
        component.find("inputFilterSecond").set("v.value", null);
        component.find("inputFilterSecond").set("v.disabled", true);
        component.find("inputFilterThird").set("v.value", null);
        component.find("inputFilterThird").set("v.disabled", true);
        component.set("v.selectedFilter", null);
        helper.getCases(component, event);
       
    },
	//PUX-723 Manual refresh button needs on Case list view page
})