/*
 *	Name 		   : CaseListController.js
 *   Author         : Niraj Prajapati
 *   Date   		   : 09-Aug-2017
 *   Description    : To display all Cases tab wise with the help of CaseListItem Component.
 *   JIRA ID        : PUX-23
 *
 *	____________________________________________________________
 *	Modifications: 1
 *			Authors:  Tushar Moradiya
 *			Date:  18-Sep-2017
 *			Purpose of modification:  Implement filters on list page.
 *			Method/Code segment modified: PUX-305  
 	Modifications 2:
                   Date: 02/02/2018
   				   Changed by : Rahul Pastagiya
                   Purpose of modification: [PUX-678] Configuration ability of Case view
    Modifications 3:
                   Date: 09/02/2018
   				   Changed by : Niraj Prajapati
                   Purpose of modification: [PUX-625] Buttons visibility based on profile level access
 */
({
    /*
		Authors: Tushar Moradiya
		Purpose: used for get cases on load and after applying filter
		//PUX-305	
	*/
    getCases: function(component, event) {

        var action = component.get("c.getCases");
        //Start : PUX-501	
        var curPageNo = component.get("v.pageNumber")
        component.set("v.tempPageNumber", curPageNo);
        //End : PUX-501
        /* start : PUX-305 ,PUX-421*/
        component.set("v.filterMap", {});
        var inputFilterFirst, inputFilterSecond, inputFilterThird;
        var selectFieldSecond, selectFieldThird;
        if (window.inputFilterFirst != null) {
            inputFilterFirst = window.inputFilterFirst;
        } else {
            inputFilterFirst = component.find("inputFilterFirst").get("v.value") != null && component.find("inputFilterFirst").get("v.value") != "" ? component.find("inputFilterFirst").get("v.value") : null;
            window.inputFilterFirst = inputFilterFirst;
        }
        /* start : PUX-305 */
        var filterMap = component.get("v.filterMap");
        if (window.selectFieldSecond != null && window.selectFieldSecond != window.selectFieldThird) {
            inputFilterSecond = window.inputFilterSecond;
            selectFieldSecond = window.selectFieldSecond;
            filterMap[selectFieldSecond.split(":")[0]] = inputFilterSecond;
            component.find("inputFilterSecond").set("v.disabled", false);
        } else if (component.find("inputFilterSecond").get("v.value") != null && component.find("inputFilterSecond").get("v.value").trim() != "" && component.find("selectFieldSecond").get("v.value") !=
            component.find("selectFieldThird").get("v.value")) {
            filterMap[component.find("selectFieldSecond").get("v.value").split(":")[0]] = component.find("inputFilterSecond").get("v.value").trim();
            inputFilterSecond = component.find("inputFilterSecond").get("v.value");
            window.inputFilterSecond = inputFilterSecond;
            selectFieldSecond = component.find("selectFieldSecond").get("v.value");
            window.selectFieldSecond = selectFieldSecond;
        }

        if (window.selectFieldThird != null && window.selectFieldSecond != window.selectFieldThird) {
            inputFilterThird = window.inputFilterThird;
            selectFieldThird = window.selectFieldThird;
            filterMap[selectFieldThird.split(":")[0]] = inputFilterThird;
            component.find("inputFilterThird").set("v.disabled", false);
        } else if (component.find("inputFilterThird").get("v.value") != null && component.find("inputFilterThird").get("v.value").trim() != "" && component.find("selectFieldSecond").get("v.value") != component.find("selectFieldThird").get("v.value")) {
            filterMap[component.find("selectFieldThird").get("v.value").split(":")[0]] = component.find("inputFilterThird").get("v.value").trim();
            inputFilterThird = component.find("inputFilterThird").get("v.value");
            window.inputFilterThird = inputFilterThird;
            selectFieldThird = component.find("selectFieldThird").get("v.value");
            window.selectFieldThird = selectFieldThird;
        }

        if (window.selectFieldSecond != null && window.selectFieldThird != null && window.selectFieldSecond == window.selectFieldThird) {
            inputFilterSecond = window.inputFilterSecond;
            selectFieldSecond = window.selectFieldSecond;
            inputFilterThird = window.inputFilterThird;
            selectFieldThird = window.selectFieldThird;
            filterMap[selectFieldSecond.split(":")[0]] = inputFilterSecond + ',' + inputFilterThird;
            component.find("inputFilterSecond").set("v.disabled", false);
            component.find("inputFilterThird").set("v.disabled", false);
        } else if (component.find("inputFilterSecond").get("v.value") != null && component.find("inputFilterSecond").get("v.value").trim() != "" &&
            component.find("inputFilterThird").get("v.value") != null && component.find("inputFilterThird").get("v.value").trim() != "" &&
            component.find("selectFieldSecond").get("v.value") == component.find("selectFieldThird").get("v.value")) {
            filterMap[component.find("selectFieldSecond").get("v.value").split(":")[0]] = component.find("inputFilterSecond").get("v.value") + ',' + component.find("inputFilterThird").get("v.value");
            inputFilterSecond = component.find("inputFilterSecond").get("v.value");
            window.inputFilterSecond = inputFilterSecond;
            selectFieldSecond = component.find("selectFieldSecond").get("v.value");
            window.selectFieldSecond = selectFieldSecond;
            inputFilterThird = component.find("inputFilterThird").get("v.value");
            window.inputFilterThird = inputFilterThird;
            selectFieldThird = component.find("selectFieldThird").get("v.value");
            window.selectFieldThird = selectFieldThird;
        }
        /* end : PUX-421 */
        /* Code start for PUX-358 */
        var overDue = window.overDueIn;
        //alert(overDue);
        if (overDue != undefined && overDue != '') {
            /* Code start for PUX-485 */
            if (overDue == 'Overdue')
                overDue = 'Overdue';
            else
                overDue = 'Pending';
            /* Code end for PUX-485 */
        } else {
            overDue = '';
        }
        /* Code End for PUX-358 */
        /* end : PUX-305  */
        //remove alert
        //alert(JSON.stringify(filterMap));
        //action.setStorable();

        action.setParams({
            whereClause: component.get("v.condition"),
            //PUX-207
            sortField: component.get("v.sortField"),
            sortDirection: component.get("v.sortDirection"),
            pageNumber: component.get("v.pageNumber"),
            recordsToDisplay: component.get("v.recordsToDisplay"),
            //PUX-207
            /* start : PUX-305 PUX-421 */
            selectedQueue: inputFilterFirst,
            filterMap: filterMap,
            overDueIn: overDue
            /* end : PUX-305 PUX-421 */
        });
        component.set("v.showSpinner", true);
        action.setCallback(this, function(response) {
            component.set("v.showSpinner", false);
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
		/* start : PUX-305  PUX-421 */
                //PUX-678
                component.set("v.fieldSetKeyList", response.getReturnValue().fieldSetKeyList);  
                component.set("v.fieldSetFieldLabelMap", response.getReturnValue().keyFieldLabelMap);
                //PUX-678
                component.set("v.caseList", response.getReturnValue().cases);
                component.set("v.selectedQueue", inputFilterFirst);
                component.set("v.selectedFilterSecond", selectFieldSecond);
                component.set("v.inputFilterSecond", inputFilterSecond);
                component.set("v.selectedFilterThird", selectFieldThird);
                component.set("v.inputFilterThird", inputFilterThird);
                var tempList = [];

                if (component.find("inputFilterFirst").get("v.value") != null && component.find("inputFilterFirst").get("v.value").trim() != "") {
                    tempList.push({
                        'fieldName': 'Queue',
                        'displayValue': component.find("inputFilterFirst").get("v.value"),
                        'key': 'filter1'
                    });
                }
                //alert(component.find("selectFieldSecond").get("v.value"))
                if (component.find("inputFilterSecond").get("v.value") != null && component.find("inputFilterSecond").get("v.value").trim() != "") {
                    tempList.push({
                        'fieldName': component.find("selectFieldSecond").get("v.value").split(":")[1],
                        'displayValue': component.find("inputFilterSecond").get("v.value"),
                        'key': 'filter2'
                    });
                } else {
                    component.find("inputFilterSecond").set("v.value", '');
                }
                if (component.find("inputFilterThird").get("v.value") != null && component.find("inputFilterThird").get("v.value").trim() != "") {
                    tempList.push({
                        'fieldName': component.find("selectFieldThird").get("v.value").split(":")[1],
                        'displayValue': component.find("inputFilterThird").get("v.value"),
                        'key': 'filter3'
                    });
                } else {
                    component.find("inputFilterThird").set("v.value", '');
                }
                /* Code start for PUX-358 */
                if (overDue != undefined && overDue != '') {
                    /* Code start for PUX-485 */
                    if (overDue == 'Overdue') tempList.push({
                        'fieldName': 'OverDue In',
                        'displayValue': 'Not In OverDue',
                        'key': 'filter4'
                    });
                    else tempList.push({
                        'fieldName': 'Status In',
                        'displayValue': 'Awaiting Response',
                        'key': 'filter4'
                    });
                    /* Code end for PUX-485 */
                }
                /* Code End for PUX-358 */
                component.set("v.selectedFilter", tempList);
                /* end : PUX-305 PUX-421*/
                //	console.log('Records No : '+response.getReturnValue().length+'\nmaxPage : '+Math.floor((response.getReturnValue().length+9)/10));
                //PUX-207							
                component.set("v.maxPage", Math.ceil((response.getReturnValue().total) / 15));
                //Start : PUX-504
                component.set("v.totalRecords", response.getReturnValue().total);
                //End : PUX-504
                //PUX-207
                //this.renderPage(component);
                //component.find("selectedfilterDiv").set("v.value", test);
            } else if (state === "INCOMPLETE") {
                //Logic
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        //console.log("Error message: " +
                        //  errors[0].message);
                    }
                } else {
                    //console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    /*
    	Authors: Rahul Pastagiya
    	Purpose: It will be called on change page number in pagination
    */
    renderPage: function(component) {
        var records = component.get("v.caseList"),
            pageNumber = component.get("v.pageNumber"),
            pageRecords = records.slice((pageNumber - 1) * 10, pageNumber * 10);
        //	consoleProxy.log('Render Page '+pageRecords);
        component.set("v.currentList", pageRecords);
    },
    /*
		Authors: Rahul Pastagiya
		Purpose: To get sort dropdown list data
        //PUX-390 
	*/
    getSortDropdownList: function(component, event) {
        var action = component.get("c.getSortFieldList");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.sortDropdownList", response.getReturnValue());
            } else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    //PUX-390 : End
    /*
		Authors: Tushar Moradiya
		Purpose: To get all related Queue name based on logic user cases
		//PUX-305	
	*/
    /* start : PUX-305 */
    getQueueDropdown: function(component, event) {
        var action = component.get("c.getQueueDropdown");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.displayField", response.getReturnValue());
            } else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    /* end : PUX-305 */
    /*
    	Authors: Niraj Prajapati
    	Purpose: To update Owner Id as current login user id
    	Dependencies: CaseDetailSection.cmp	
    */
    makeMeOwner: function(component, event, isRedirect) {
        var ct = component.get("v.selectedCases");
        //Start code change for PUX-277/PUX-275
        ct[0].OwnerId = component.get("v.loginUserId");
        
        //End code change for PUX-277/PUX-275
        var compEvent = component.getEvent("caseAction");
        var caseObj = ct[0];
        compEvent.setParams({
            /*Code Start For PUX-295 */
            "Action": "updateOwner",
            /*Code End For PUX-295 */
            "Case": caseObj,
            "callback": function(resp) {
                
                var state = resp.getState();
                if (state === "SUCCESS") {
                    var message = '';
                    if (!isRedirect) {
                        message = "Case-" + caseObj.Name + " is accepted!";
                    } else {
                        message = "Case-" + caseObj.Name + " is accepted and navigated to detail page!";
                    }
                    var appEvent = $A.get("e.c:ShowToast");
                    appEvent.setParams({
                        "title": "Success !",
                        "message": message
                    });
                    appEvent.fire();
                    var comVar = component.get("v.refreshCounter");
                    component.set("v.refreshCounter", comVar + 1);
                    component.set("v.showHideSplitMerge", "");
                    var emptyList = [];
                    component.set("v.selectedCases", emptyList);
                } else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
                    //Start code change for PUX-277/PUX-275
                    //ct[0].OwnerId = owner;
                    //component.set("v.case", ct[0]);
                    var np = component.toString().split('":"')[1].split('"}')[0];
                    
                    var errorMessage = (np == 'c') ? $A.get("$Label.c.Error_Message") : $A.get("$Label.CoraPLM.Error_Message");
                    var appEvent = $A.get("e.c:ShowToast");
                    appEvent.setParams({
                        "title": "Success !",
                        "message": errorMessage
                    });
                    
                    appEvent.fire();
                    //End code change for PUX-277/PUX-275
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            }
        });
        compEvent.fire();
    },
    /* End : PUX-304 */
    /*
		Authors: Tushar Moradiya
		Purpose: To get fieldset for filter dropdown
		//PUX-305	
	*/
    /* start : PUX-305 */
    getFieldDropdown: function(component, event) {
        var action = component.get("c.getFieldDropdown");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.fields", response.getReturnValue());
            } else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    /* end : PUX-305 */
    /*
		Authors: Niraj Prajapati
		Purpose: To get UserDetail
		//PUX-305	
	*/
    /* start : PUX-625 */
    getUserDetail: function(component, event) {
        var action = component.get("c.getUserInfo");
		var nameSpace =component.getConcreteComponent().getDef().getDescriptor().getNamespace();
        nameSpace=(nameSpace=='c')?'':nameSpace+'__';
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				var UserInfo=response.getReturnValue().UserInfo;
				
                component.set("v.newCasePermission",UserInfo[nameSpace+'New_Case_Permission__c']);
				component.set("v.splitPermission",UserInfo[nameSpace+'Split_Permission__c']);
				component.set("v.mergePermission", UserInfo[nameSpace+'Merge_Permission__c']);
				if(UserInfo[nameSpace+'Change_Owner_Permission__c'] == true && response.getReturnValue().UserInfo.Profile.PermissionsTransferAnyEntity == true){
					component.set("v.changeOwnerPermission", true);
				}else{
					component.set("v.changeOwnerPermission", false);
				}
				//component.set("v.changeOwnerPermission", response.getReturnValue().UserInfo.New_Case_Permission__c);
            } else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    /* end : PUX-625 */
})