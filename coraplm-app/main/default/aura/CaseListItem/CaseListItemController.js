/*
  Authors	   :   Rahul Pastagiya
  Date created :   14/08/2017
  Purpose      :   Client side controller to handle all actions of component
  Dependencies :   CaseListItemHelper.js, CaseListItem.css, CaseListItemController.cls
  __________________________________________________________________________
  	Modifications 1:
                   Date: 02/02/2018
   				   Changed by : Rahul Pastagiya,Niraj Prajapati
                   Purpose of modification: [PUX-678] Configuration ability of Case view 
											[PUX-679] Read Email from Case view 
   Modifications 2:
                   Date: 06/02/2018
   				   Changed by : Rahul Pastagiya
                   Purpose of modification: [PUX-12] Optimize the list view to ensure key information is readily available
 */
({
	/*
		Authors: Rahul Pastagiya
		Purpose: To format Email created Date and To Trim email Text body to display one line Email Response on List View
	*/
	doInit: function(component, event, helper) {
        var caseDetail = component.get('v.ct');
        //PUX-678
        var fieldSetKeyList = component.get('v.fieldSetKeyList');
        var map = new Object(); // or var map = {};
        for(var key in fieldSetKeyList){                        
           map[fieldSetKeyList[key]] = caseDetail[fieldSetKeyList[key]];
        }        
        component.set('v.filedSetCol1',(Math.ceil(fieldSetKeyList.length/2) > 3 )? 3:Math.ceil(fieldSetKeyList.length/2));
        component.set('v.filedSetCol2',(Math.ceil(fieldSetKeyList.length/2) - 1) > 2 ? 2 : Math.ceil(fieldSetKeyList.length/2) - 1);
        component.set('v.fieldSetMap',map);
        component.set('v.render',true);
        //PUX-678
		var date = new Date(component.get('v.ct.Emails[0].CreatedDate'));
		component.set('v.ct.Emails[0].CreatedDate', date);
		//PUX-331
		/* Purpose: To update PredictedValue status of Indexing Field		start	*/	
		if (component.get('v.ct.PredictedClosureDate__c') != undefined) {
			var expectedDate = new Date(component.get('v.ct.PredictedClosureDate__c'));
			
			component.set('v.expectedDate', expectedDate.getDate());
			component.set('v.expectedTime', expectedDate.toLocaleTimeString());
			component.set('v.expectedMonth', expectedDate.toLocaleString('en-us', {
				month: "short"
			}));

			var hours = expectedDate.getHours();
			var minutes = expectedDate.getMinutes();
			var ampm = hours >= 12 ? 'PM' : 'AM';
			hours = hours % 12;
			hours = hours ? hours : 12; // the hour '0' should be '12'
			minutes = minutes < 10 ? '0'+minutes : minutes;
			var strTime = hours + ':' + minutes + ' ' + ampm;
			component.set('v.expectedTime', strTime);
			

		}else if(component.get('v.ct.Target_TAT_Time__c') != undefined)
        {
            var expectedDate = new Date(component.get('v.ct.Target_TAT_Time__c'));
			component.set('v.expectedDate', expectedDate.getDate());
			component.set('v.expectedTime', expectedDate.toLocaleTimeString());
			component.set('v.expectedMonth', expectedDate.toLocaleString('en-us', {
				month: "short"
			}));

			var hours = expectedDate.getHours();
			var minutes = expectedDate.getMinutes();
			var ampm = hours >= 12 ? 'PM' : 'AM';
			hours = hours % 12;
			hours = hours ? hours : 12; // the hour '0' should be '12'
			minutes = minutes < 10 ? '0'+minutes : minutes;
			var strTime = hours + ':' + minutes + ' ' + ampm;
			component.set('v.expectedTime', strTime);
        }
		/* Purpose: To update PredictedValue status of Indexing Field		end	*/	
		//PUX-331
		helper.setOneLiners(component, event);		
		var isCaseFav =  component.get('v.ct.Favourited__c');             
		if (isCaseFav == true) {                    
			component.set('v.isFavCase', true);
		}
	},
	/*
		Authors: Atul Hinge
		Purpose: To call email related actions
	*/
	handleCaseAction: function(component, event, helper) {
		var compEvent = component.getEvent("caseAction");
		var callback = function(response) {
			component.set("v.ct", response.case[0]);
			helper.setOneLiners(component, event);
		}
		if (event.currentTarget.id == "CaseListItemDiv") {
			compEvent.setParams({
				"Action": "redirectToDetail", // PUX-308 
				"Case": component.get("v.ct")
			});
		} else if (event.currentTarget.id == "reply") {
			compEvent.setParams({
				"Action": "reply",
				"Case": component.get("v.ct"),
				"interactionId": component.get("v.ct.Emails[0].Id"),
				"callback": callback
			});
		} else if (event.currentTarget.id == "quickreply") {
			compEvent.setParams({
				"Action": event.currentTarget.id,
				"Case": component.get("v.ct"),
				"interactionId": component.get("v.ct.Emails[0].Id")
			});
		} else {
			compEvent.setParams({
				"Action": event.currentTarget.id,
				"Case": component.get("v.ct"),
				"interactionId": component.get("v.ct.Emails[0].Id")
			});
		}
		compEvent.fire();
	},
	/*
		Authors: Ashish Kumar
		Purpose: toggleCheckBox will use to display accept,split,merge buttons
	*/
	toggleCheckBox: function(component, event, helper) {
		var selectedCheckBox = component.find("chkBox").getElements();
		var selection = $(selectedCheckBox).is(':checked');
		var action;
		if (selection == true) {
			action = 'selected';
		} else {
			action = 'unselected';
		}
        
		var toggleEvent = component.getEvent("selectedCasesEvent");
		toggleEvent.setParams({
			"cases": component.get("v.ct"),
			"action": action
		});
		toggleEvent.fire();
	},
	/*
		Authors: Ashish Kumar
		Purpose: mark any interaction as favourite 
    */
	markFavourite: function(component, event, helper) {
		var favCase = component.get("v.ct");
		var action = component.get("c.toggleFavourite");
		action.setParams({
			"favCase": favCase,
			"action": 'Favourite'
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set('v.isFavCase', true);
			} else {
				component.set('v.isFavCase', false);
			}
		});
		$A.enqueueAction(action);
	},
    closeOtherPopups : function(component, event, helper) {
        //alert(component.get("v.currentListItem"));
        if(component.get("v.currentListItem")!=component.get("v.ct.Id")){
            //alert(component.get("v.currentListItem")+' and '+ component.get("v.ct.Id"));
            var toggleBody = component.find("emailPopUp");
            $A.util.addClass(toggleBody, 'toggleDisplay');
            var toggleBody = component.find("additionalFields");
            $A.util.addClass(toggleBody, 'toggleDisplay');
        }

    },
	/*
		Authors: Ashish Kumar
		Purpose: mark any interaction as unfavourite 
    */
	markUnFavourite: function(component, event, helper) {
		var favCase = component.get("v.ct");
		var action = component.get("c.toggleFavourite");
		action.setParams({
			"favCase": favCase,
			"action": 'UnFavourite'
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set('v.isFavCase', false);
			} else {
				component.set('v.isFavCase', true);
			}
		});
		$A.enqueueAction(action);
	},
	/*
		Authors: Nirajj Prajapati
		Purpose: showEmail will use to Show/Hide EmailPopUp
	*/
    showEmail: function(component, event, helper) {
        if(component.get('v.showEmailPopover')){
            component.set('v.showEmailPopover', false);
        }else{
            component.set('v.showEmailPopover', true);
        }
        if(component.get('v.showAdditionalDetail')){
            component.set('v.showAdditionalDetail', false);
        }
        
		var toggleBody = component.find("emailPopUp");
		$A.util.toggleClass(toggleBody, 'toggleDisplay');
        component.set("v.currentListItem",component.get("v.ct.Id"));
    },
    
    /*
		Authors: Rahul Pastagiya
		Purpose: [PUX-12] Show/Hide additional detail section
	*/
    showAdditionalFields: function(component, event, helper) { 
        if(component.get('v.showAdditionalDetail')){
            component.set('v.showAdditionalDetail', false);
        }else{
            component.set('v.showAdditionalDetail', true);
        }
        if(component.get('v.showEmailPopover')){
            component.set('v.showEmailPopover', false);
        }
        
		var toggleBody = component.find("additionalFields");
		$A.util.toggleClass(toggleBody, 'toggleDisplay');
        component.set("v.currentListItem",component.get("v.ct.Id"));
    },
    
   //PUX-678
    /* End : Opening Attachment View when CLick on Attachment icons on Interaction Details */
    //PUX-305 end
    /*
		Authors: Rahul Pastagiya
		Purpose: Open Sorting dropdown on Case list page
		
	*/
    openSortDropDown: function(cmp, event, helper) {
        var conEle1 = cmp.find("sortDropdown");
        $A.util.toggleClass(conEle1, 'slds-is-open');
    },
    /*
		Authors: Rahul Pastagiya
		Purpose: Close Sorting dropdown on Case list page
		
	*/
    closeSortDropDown: function(cmp, event, helper) {
        var conEle1 = cmp.find("sortDropdown");
        $A.util.removeClass(conEle1, 'slds-is-open');
    },
    //PUX-678
})