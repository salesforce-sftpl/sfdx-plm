/*
  Authors	   :   Rahul Pastagiya
  Date created :   14/08/2017
  Purpose      :   Client side helper for controller
  Dependencies :   CaseListItemController.js
  __________________________________________________________________________
  Modifications:
		    Date:  02/02/2018
		    Purpose of modification: [PUX-679] Read Email from Case view 
		    Method/Code segment modified: 
 */
({
	/*
		Authors: Rahul Pastagiya
		Purpose: To set one line email to display on Case List
	*/
    //PUX-678 : Replace
	setOneLiners: function(component, event) {
		var emailMsgObj = component.get('v.ct.Emails');
		if (emailMsgObj == undefined) {
			component.set('v.oneLineEmialTxtBody', 'No interaction yet!');
			//Start code for PUX-679
            component.set('v.latestEmailTxtBody', 'No interaction yet!');
			//End code for PUX-679
		} else if (emailMsgObj[0].TextBody != undefined) {
			component.set('v.oneLineEmialTxtBody', emailMsgObj[0].TextBody.substring(0, 85));
			//Start code for PUX-679
            component.set('v.latestEmailTxtBody', emailMsgObj[0].HtmlBody);
            component.set('v.toAddress',emailMsgObj[0].ToAddress);
            component.set('v.fromAddress',emailMsgObj[0].FromAddress);
            component.set('v.subject',emailMsgObj[0].Subject);
			//End code for PUX-679
		}
       	
        var caseSubject = component.get('v.ct.Subject__c');
		if (caseSubject != undefined) {
			component.set('v.ct.Subject__c', caseSubject.substring(0, 85));
		}		
	},
    //PUX-678
     
    
})