({
    
    handleActive : function(cmp, event, helper) {
        //Code start for PUX-358
		if(event.getSource().get("v.id")!='My Cases')
			window.overDueIn=''; 
        //Code End for PUX-358
        // Start PUX 421
		if(event.getSource().get("v.id") != window.selectedTab)	{
			window.inputFilterFirst=null;	
			window.inputFilterSecond=null;	
			window.selectFieldSecond=null;	
			window.inputFilterThird=null;	
			window.selectFieldThird=null;
		}	
        // End PUX 421
        //PUX-390
		cmp.set('v.sortField','LastModifiedDate');
		cmp.set('v.sortDirection','desc');
		//PUX-390
		var comVar = cmp.get("v.refreshCounter");
		var tab = event.getSource();
		var emptyList = [];
		cmp.set("v.showHideSplitMerge", 'none');
		cmp.set("v.selectedCases", emptyList);
		cmp.set("v.refreshCounter", comVar + 1);
		var compEvent = cmp.getEvent("caseAction");
		compEvent.setParams({
			"Action": "setBreadCrumb"
		});
		compEvent.setParams({
			"TabName": event.getSource().get("v.id")
		});
		compEvent.fire();
		cmp.set("v.selectedTabId", tab.get('v.id'));
        //PUX-207
		cmp.set("v.resetPagination", true);		
		//PUX-207
		window.selectedTab=event.getSource().get("v.id");
    },
    waiting: function(component, event, helper) {
	   document.getElementById("Accspinner").style.display = "block";
	},
	acceptCaseAction : function(component, event, helper){
		//alert(component.get("v.selectedCases"));
		helper.makeMeOwner(component, event, false);
    },
    doneWaiting: function(component, event, helper) {
	if( document.getElementById("Accspinner")!=null ||  document.getElementById("Accspinner")!=undefined){
		document.getElementById("Accspinner").style.display = "none";
	}
     
    } ,
	handleNewCase : function(component, event, helper) {
       
         var compEvent = component.getEvent("caseAction");
	     compEvent.setParams({
                "Action": "redirectNewCase",
				"Case" : component.get("v.ct")
		});

		compEvent.fire();
	} ,	
	    checkSplitMerge : function(component,event,helper){
        var caseDetails = event.getParam("cases");
        var actionDetails = event.getParam("action");
        var selectedCasesList = component.get("v.selectedCases");
        if(actionDetails == 'selected'){
            selectedCasesList.push(caseDetails);
        }else{
            selectedCasesList = jQuery.grep(selectedCasesList, function(value) {
              return value.Id != caseDetails.Id;
            });
        }
        component.set("v.selectedCases", selectedCasesList);  
        console.log(selectedCasesList.length);
		
        if(selectedCasesList.length == 0){
            component.set("v.showHideSplitMerge",'none');
            //hide both buttons
        }else if(selectedCasesList.length == 1){
		//show only split button
		component.set("v.showHideSplitMerge",'Split');
        }else if(selectedCasesList.length >1){		
		component.set("v.showHideSplitMerge",'Merge');
        }
        
     
    }
	
})