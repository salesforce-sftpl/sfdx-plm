({
    
    doInit : function(component, event, helper) {
		 helper.getLoginUser(component,event);
    },
    acceptCase: function(component, event, helper) {
		component.find("btnAccept").set("v.disabled",true);
		helper.makeMeOwner(component, event, false);
	},
    handleCaseActionEvent: function(component, event, helper) {
      
        var action=  event.getParam("Action");
        var caseObj=event.getParam("Case");
        var InteractionId=event.getParam("interactionId");
		var filesList=event.getParam("Files");
        var plmUtility = component.find('plmUtility');
        var callback=event.getParam("callback");
		if(action == 'setBreadCrumb'){
		    helper.setBreadCrumb(component, event);
        }else if(action == 'reply' || action == 'replyall' || action == 'forward'){
               plmUtility.reply(event.getParam("Case"), InteractionId, action,callback);
        }else if(action == 'create'){
           var callback=event.getParam("callback");
            plmUtility.create(event.getParam("Case"),callback);
        }else if(action == 'update'){
           var callback=event.getParam("callback");
            plmUtility.update(event.getParam("Case"),callback);
        }/*Code Start For PUX-295 */else if(action == 'updateOwner'){
            var callback=event.getParam("callback");
            //alert(1);
            plmUtility.updateOwner(event.getParam("Case"),callback);
        }/*Code End For PUX-295 */else if(action =='redirect'){ 
            component.set("v.isSplitCaseView", false); //PUX-191
            component.set("v.isMergeCaseView", false); //PUX-192
           component.set("v.case", event.getParam("Case")); 
	       component.set("v.isDetailView", false);  // PUX-308
        }/* Start : PUX-308 */
         else if(action =='redirectToDetail'){  
            
           component.set("v.case", event.getParam("Case")); 
              // alert( component.get("v.case.name"));
           component.set("v.isSearchResultView", false); 
	       component.set("v.isDetailView", true); 
             component.set("v.isSplitCaseView", false);
                component.set("v.isMergeCaseView", false);
		   
         }/* End : PUX-308 */
         else if(action=='redirectNewCase'){
		  component.set("v.isNewCaseView", !component.get("v.isNewCaseView")); 
		 }else{
			
		}

    },
	navtoparent : function(component, event, helper) {
				component.set("v.isNewCaseView", false);  
   			component.set("v.isDetailView", false);    
        	component.set("v.isSplitCaseView", false);
            component.set("v.isMergeCaseView", false);  
        	component.set("v.isSearchResultView", false);
	},
    /*
		Authors: Ashish Kr.
		Purpose: navtochild function will be used to go to child breadcrumb layout
		Dependencies: PLMLayout.cmp
                
    */
    navtochild : function(component,event,helper){
        if(component.get('v.breadChild')!='New Case'){
            component.set("v.isNewCaseView", false);  
            component.set("v.isDetailView", false);    
            component.set("v.isSplitCaseView", false);
            component.set("v.isMergeCaseView", false);
            if(component.get('v.breadChild') == 'Search Results'){
                component.set("v.isSearchResultView", true);
            }else{
                component.set("v.isSearchResultView", false);
            }
        }
    	
    },
    /*
		Authors: Ashish Kr.
		Purpose: handleSearchAction function is used for navigation to Search Result View
		Dependencies: PLMLayout.cmp
                
    */
    handleSearchAction : function(component,event,helper){
        component.set("v.isNewCaseView", false);  
        component.set("v.isDetailView", false); 
        component.set("v.isSplitCaseView", false);
        component.set("v.isMergeCaseView", false);
    	component.set('v.isSearchResultView',true);
        component.set("v.breadChild", "Search Results");
    },
    
    /*
		Authors: Mohit and Team
		Purpose: handleSplitMergeCase function will be used handle split and merge functionality
		Dependencies: PLMLayout.cmp
                
    */
    handleSplitMergeCase : function(component,event,helper){
        var action = event.getParam("action");
        console.log('check action:'+action);
        var cases = event.getParam("cases");
        if(action == 'split'){
            component.set("v.case", cases[0]); 
            component.set("v.isSplitCaseView", true);
            console.log('check split:'+component.get("v.isSplitCaseView"));
            component.set("v.isMergeCaseView", false);
        }else if(action == 'merge'){
            component.set("v.casesToMerge",cases);
            component.set("v.isMergeCaseView", true);
            component.set("v.isSplitCaseView", false);
            
        }
        helper.setBreadCrumb(component, event);
    },
})