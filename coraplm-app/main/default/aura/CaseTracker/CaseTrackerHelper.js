({
	setBreadCrumb : function(component, event) {
          component.set("v.breadChild", event.getParam("TabName"));
	},
    /*
		Authors: Niraj Prajapati
		Purpose: makeMeOwner function is used to update ownerid of the case record.
		Dependencies: PLMLayout.cmp,CaseDetailSection.cmp
                
    */
    makeMeOwner: function(component, event, isRedirect) {
		var ct = component.get("v.case");
        //Start code change for PUX-277/PUX-275
        var owner = component.get("v.case.OwnerId");
        //End code change for PUX-277/PUX-275
		ct.OwnerId = component.get("v.loginUserId");
		//component.set("v.case", ct);
		if (component.get("v.case.Status__c") == "Ready For Processing") {
            //alert(component.get("v.loginUserId"));
			ct.OriginalProcessor__c = component.get("v.loginUserId");
		}
		var compEvent = component.getEvent("caseAction");
		var myCompEvent = component.getEvent("caseAction");
		var caseObj = ct;
		compEvent.setParams({
			/*Code Start For PUX-295 */
            "Action": "updateOwner",
            /*Code End For PUX-295 */
			"Case": caseObj,
			"callback": function(response) {
				var state = response.getState();
                //alert(state);
				if (state === "SUCCESS") {
					
					var message = '';
					component.set("v.case", response.getReturnValue()[0]);
					if (!isRedirect) {
						message = "Case-" + caseObj.Name + " is accepted!";
					} else {
						message = "Case-" + caseObj.Name + " is accepted and navigated to detail page!";
					}
					var appEvent = $A.get("e.c:ShowToast");
					appEvent.setParams({
						"title": "Success !",
						"message": message
					});
					appEvent.fire();
					//component.destroy(); 
					//component.set("v.templates",response.getReturnValue().templates);
					//component.set("v.body",component.get("v.templates")[0].Body);
				} else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
                    var np = component.toString().split('":"')[1].split('"}')[0];
                    
                    //Start code change for PUX-277/PUX-275
                    ct.OwnerId = owner;
                    component.set("v.case", ct);
					var errorMessage = (np == 'c') ? $A.get("$Label.c.Error_Message") : $A.get("$Label.CoraPLM.Error_Message");
					var appEvent = $A.get("e.c:ShowToast");
					appEvent.setParams({
						"title": "Success !",
						"message": errorMessage
									
					});
					appEvent.fire();
					component.find("btnAccept").set("v.disabled",false);
                    //End code change for PUX-277/PUX-275
					var errors = response.getError();
					if (errors) {
                        //alert(errors[0].message);
						if (errors[0] && errors[0].message) {
							console.log("Error message: " + errors[0].message);
						}
					} else {
						console.log("Unknown error");
					}
				}
			}
		});
		compEvent.fire();
	},
	 getLoginUser : function(component, event){
        //console.log('Helper Log');
		//Start Code for PUX-221
		var action = component.get("c.getUserInfo");    
        action.setCallback(this, function(response) {
            var state = response.getState();
			
            if (state === "SUCCESS") {
                //console.log('123'+response.getReturnValue());
				
                component.set("v.loginUserId",response.getReturnValue().UserId);
                window.LoginUserId=response.getReturnValue().UserId;
                window.userId= window.LoginUserId;
                window.SessionId=response.getReturnValue().UserSessionId;
                window.InstanceName=response.getReturnValue().InstanceName;
                
                
            
                
                
              
			//End Code for PUX-221
            }else if (state === "INCOMPLETE") {
                
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        //console.log("Error message: " + 
                             //       errors[0].message);
                    }
                } else {
                    //console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
            },
})