({
    doInitUser: function(component){
        var caseIdList =[];
        var changeOwn = component.get("v.changeOwnerCase");
        var arrayLength = changeOwn.length;
        for( var i=0; i<arrayLength;i++){
            var caseId = changeOwn[i].Id;
            caseIdList.push(caseId);
            component.set("v.caseToChange" ,caseIdList);
        }
        
        var action = component.get("c.userDetail");
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state== "SUCCESS"){
                var first = response.getReturnValue();
                var firstUser = first[0].Id;
                component.set("v.userList",response.getReturnValue());
                component.set("v.selectedName" , firstUser);
               
            }
            
        });
        $A.enqueueAction(action); 	
    },
    initQueue : function(component,event,helper){
        var action = component.get("c.queueData");
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                 var first = response.getReturnValue();
                 var firstUser = first[0].Id;
                 component.set("v.queueList" ,response.getReturnValue());
                 component.set("v.selectedName" , firstUser);
               
            }
        });
        $A.enqueueAction(action);
    },
    UserView : function(component,event,helper){
        var eventsource = event.getSource();
        var picklistVal = eventsource.get("v.value");
       
        component.set('v.selectUser',picklistVal);
        
        
    },
    handleCase : function(component, event) {
       
        var caseId = event.getParam("caseId");
        
        component.set("v.caseSelectId",caseId);
        var selectId = component.get("v.caseSelectId");
       
    },
    changeOwner:function(component,event,helper){
        
        var ownerName = component.get("v.selectedName");
        console.log('selected owner' +ownerName);
        var selectCaseId = component.get("v.caseToChange");
        console.log('heyyyy' +ownerName);
        var action = component.get("c.userDetail");
        action.setParams({
            "owner" : ownerName,
            "caseId" :selectCaseId
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            
            if(state=="SUCCESS"){
                component.set("v.isDisplay", false);
                var comVar = component.get("v.refreshCounter"); 
                component.set("v.refreshCounter", comVar + 1); 
                 var appEvent = $A.get("e.c:ShowToast");
                    appEvent.setParams({
                        "title": "Success ",
                        "message": 'Owner change successfully'
                    });
                    appEvent.fire();
            } else if(state=="ERROR"){
               
                var errors = response.getError();
				
                component.set("v.errorMessage", errors[0].message );
                var eerr = component.get("v.errorMessage");
                component.set("v.showError",true);
                
            }
        });
        
         helper.changeQueue(component);
         $A.enqueueAction(action); 
    },
    hideChangeOwner:function(component,event){
        component.set("v.isDisplay",false);
        var comVar = component.get("v.refreshCounter"); 
        component.set("v.refreshCounter", comVar + 1); 
    },
    hideModal: function(component,event){
        component.set("v.showError" ,false);
        component.set("v.isDisplay",false);
        var comVar = component.get("v.refreshCounter"); 
        component.set("v.refreshCounter", comVar + 1); 
    }
})