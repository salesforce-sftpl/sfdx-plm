/*
Authors: Ashish Kumar
Date created: 18/11/2017
Purpose: This controller will used to handle search results.
Dependencies: 
-------------------------------------------------
Modifications:
                Date: 
                Purpose of modification: 
                Method/Code segment modified:  
*/ 
({
  init: function(component, event, helper) {
    var idsJson = component.get("v.searchResultIds"); 
    if (!$A.util.isUndefinedOrNull(idsJson)) {
      var ids = JSON.parse(idsJson);
        if(ids.length ===0){
            component.set('v.noResultFound',true);
        }else{
            component.set('v.noResultFound',false);
            
        }
      component.set('v.resultCount', ids.length);
      component.set('v.recordIds', ids);
        
    }
  },
  /*
		Authors: Ashish Kr.
		Purpose: It will be called when HTTP Req is being processed at server side	
	*/
    waiting: function(component, event, helper) {
    	component.set('v.showSpinner',true);
    },
    /*
		Authors: Ashish Kr.
		Purpose: It will be called when HTTP Res is received at client side	
	*/
    doneWaiting: function(component, event, helper) {
    	component.set('v.showSpinner',false);
    },
})