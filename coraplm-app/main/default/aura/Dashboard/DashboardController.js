/*
  * Authors		: Parth Lukhi
  * Date created : 10/08/2017 
  * Purpose      : Controller JS used for Dashboard 
  * Dependencies : DashboardHelper.js
  * -------------------------------------------------
  * Modifications:
                Date: 
                Purpose of modification:  
                Method/Code segment modified: 
                
*/
({	/*
	 * Authors: 
	 * Purpose:  Highchart chart function for hetting User Details
	 * Dependencies:  DashbaordController.apxc
	 * 
	 * Start : Start : Highchart chart function */
    scriptsLoaded : function(component, event, helper) {
        //Need for future HighChart implementation	
        ////helper.getCases(component, event);
        var today = new Date()
        var curHr = today.getHours()
        var greet='Have a nice day';
        
        if (curHr < 12) {
            greet='Good Morning';
        } else if (curHr < 18) {
            greet='Good Afternoon';
        } else {
            greet='Good Evening';
        }
        component.set("v.greetMessage",greet);
        helper.getDashDetail(component, event);
    },
  
    
    /*
	 * Author:  Uzeeta Siloth
	 * Purpose:  Init  function  for leaderboard part in Dashboard component
	 * Dependencies:  DashbaordHelper.js
	 * 
	 * Start : Init  function  for leaderboard part in Dashboard component*/
    doInitUser: function(component){
        var action = component.get("c.getUserDetails");
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state== "SUCCESS"){
                component.set("v.userList",response.getReturnValue());
                //console.log('response.getReturnValue():'+JSON.stringify(response.getReturnValue()));
            }
            
        });
        $A.enqueueAction(action); 	
    },
    
    changeLeaderBoardView : function(component,event,helper){
        var eventsource = event.getSource();
        var picklistVal = eventsource.get("v.value");
        //console.log(picklistVal);
        component.set('v.selectedScore',picklistVal);
        
        
    },
    
    
    
    
    /* End : Init  function  for Dashboard component
	doInit1: function(component){
     	var action = component.get("c.getUserDetails");
		action.setCallback(this,function(response){
		var state = response.getState();
     	if (state== "SUCCESS"){
			component.set("v.userList",response.getReturnValue());
			
		}
           
	});
	$A.enqueueAction(action); 	
	},*/
    
    /*
	 * Authors:   Parth Lukhi
	 * Purpose:  function  Navigation from Dashboard to Related Case Tab
	 * Dependencies:  NA
	 * 
	 * navToMyCase  function  Navigation from Dashboard to Related Case Tab*/
    navToMyCase: function(component, event, helper) {
        //helper.getCases(component, event);
        component.set("v.showLayout", 'CaseTracker');
        component.set("v.selectedTabId", 'My Cases');
        
    },
    /* End : navToMyCase  function  Navigation from Dashboard to Related Case Tab*/
    /*
	 * Authors:   Niraj Prajapati
	 * Purpose:  function  Navigation from Dashboard to Related Case Tab with OverDueIn cases
	 * Dependencies:  NA
	 * Code Start for PUX-358
	 * navToOverDueInCase  function  Navigation from Dashboard to Related Case Tab with OverDueIn cases*/
    navToOverDueInCase: function(component, event, helper) {
        window.overDueIn='Overdue';
        //helper.getCases(component, event);
        component.set("v.showLayout", 'CaseTracker');
        component.set("v.selectedTabId", 'My Cases');
        
    },
    /* End : navToOverDueInCase  function  Navigation from Dashboard to Related Case Tab with OverDueIn cases*/
    /*
	 * Authors:   Niraj Prajapati
	 * Purpose:  function  Navigation from Dashboard to Related Case Tab with OverDueIn cases
	 * Dependencies:  NA
	 * Code Start for PUX-485
	 * navToPendingCase  function  Navigation from Dashboard to Related Case Tab with status "Awaiting Email Response" or "Awaiting Supervisory Resolution" cases*/
    navToPendingCase: function(component, event, helper) {
        window.overDueIn='Pending';
        //alert(window.overDueIn);
        //helper.getCases(component, event);
        component.set("v.showLayout", 'CaseTracker');
        component.set("v.selectedTabId", 'My Cases');
        
    }
    /* End : navToPendingCase  function  Navigation from Dashboard to Related Case Tab with status "Awaiting Email Response" or "Awaiting Supervisory Resolution" cases*/
})