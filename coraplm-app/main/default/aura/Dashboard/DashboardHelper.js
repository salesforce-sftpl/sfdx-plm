/**  Authors		: Parth Lukhi
  * Date created : 10/08/2017 
  * Purpose      : Helper File for Dashboard.cmp
  * Dependencies : DashboardController.apxc
  * -------------------------------------------------
  * Modifications:
                Date: 
                Purpose of modification:  
                Method/Code segment modified: 
*/
({
 	/*
	 * Authors: Parth Lukhi
	 * Purpose:  Gettign Dashboard Detail From Apex Class
	 * Dependencies:  DashboardController.apxc
	 * 
	 * start : Gettign Dashboard Detail From Apex Class*/
	getDashDetail: function(component, event) {
        var action = component.get("c.getDashDetail");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
		           component.set("v.dashWrapper", response.getReturnValue())
                   this.scriptsLoaded(component, event);  
            } else if (state === "INCOMPLETE") {
				//Logic
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
    },
    /* End : Gettign Dashboard Detail From Apex Class*/
    
    /*
	 * Authors: Parth Lukhi
	 * Purpose:  Chart Rendering Method
	 * Dependencies:   DashboardController.apxc
	 * 
     *Start : Chart Rendering Method */
	 scriptsLoaded : function(component, event) {         
        try {
            var dashWrapper= component.get("v.dashWrapper");
            var charType=dashWrapper.dashMap["chartType"];
            charType='column';
            
            var xList=[];
            var yAssigned=[];
            var yClosed=[];
        /*
          for (var i in dashWrapper.dashMap["assignedMap"]){
            xList.push(i);
          }*/
          xList=dashWrapper.dashMap["listOfMonths"];
          yAssigned=dashWrapper.dashMap["closedOdList"];//PUX-122
          yClosed=dashWrapper.dashMap["closedList"];
          var dashChart=  Highcharts.chart('chartContainer', {
                
                xAxis: {
                    categories: xList
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Cases'
                    }
                },  
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        
                    }
                },
                colors: ['#4ebab8','#cee6e5'],
                 tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                },
                 
                exporting: 
                { 
                    enabled: false
                 } ,
                title: {
                        text: 'Total Cases',
                       align: 'left'
                },
                credits: {
                      enabled: false
                  },
    
                  series: [{
                        name: 'SLA Missed',//PUX-122
                        data: yAssigned,
                    }, 
                    {
                        name: 'Closed',
                        data: yClosed,
                    }]
                
            });
            
            var options = dashChart.options;
            // Column chart
            options.chart.type = charType;
            dashChart = new Highcharts.Chart('chartContainer',options);
        }catch(err) {
            //setTimeout(this.scriptsLoaded(component, event) , 300);
            //this.scriptsLoaded(component, event);
            console.log(err);
        }
         
		
		
    },
     /* End : Chart Rendering Method */
})