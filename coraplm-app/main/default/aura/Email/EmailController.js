/*
* Authors	   :  Atul Hinge
* Date created :  14/08/2017
* Purpose      :  For email releated activity like reply,replyAll, forward.
* Dependencies :  Email.cmp(Aura component)
* JIRA ID      :  PUX-113,PUX-39,PUX-118,PUX-113,PUX-132,PUX-217,PUX-218,PUX-225,PUX-194
* -----------------------------------------------------------
* Modifications: 
*        Date:  27/11/2017
*        Purpose of modification:  Inline image paste in email body
*        Method/Code segment modified: handlePaste   
*/
({	
    //added by Atul Hinge.
    //To initialised component data
    doInit : function(component, event, helper) {
       component.set("v.LoginUserId",LoginUserId);
        var nameSpace =component.getConcreteComponent().getDef().getDescriptor().getNamespace();
        nameSpace=(nameSpace=='c')?'':nameSpace+'__';
        var caseTracker=component.get("v.Case");
        component.set("v.selectedFileObjects",[]);
        helper.initializeComponent(component, event);
        var fromAddress={};
        if(caseTracker[nameSpace+"SkipIndexing__c"] || LoginUserId != caseTracker.OwnerId){
            component.set("v.screenNo",2);
        }
        if(component.get("v.screenNo")!=2){
            component.set("v.screenNo",1);
        }  
        
    },
    //added by Atul Hinge.
    //To Send Email
    sendEmail : function(component, event, helper) {
        helper.sendEmail(component, event);
        
    },
    //added by Atul Hinge.
    //To save caseTracker object
    save:function(component, event, helper) {
        var appEvent = $A.get("e.c:ValidateComponentEvent");
        var test=component.get("v.processingfields");
        
        appEvent.setParams({ "components" : test,"callback":function(response){ 
            if(response.success== true){
                helper.updateCase(component, event);
            }
        }
                           });
        appEvent.fire();
    },
    //added by Atul Hinge.
    //To close Email Editor
    close : function(component, event, helper) {
        component.set("v.showReplyScreen",!component.get("v.showReplyScreen"));  
    },
    //added by Atul Hinge.
    //To move to next screen
    next:function(component, event, helper) {
        var appEvent = $A.get("e.c:ValidateComponentEvent");
        var test=component.get("v.processingfields");
        debugger;
        appEvent.setParams({ "components" : test,"callback":function(response){ 
            if(response.success== true){
                helper.updateCaseandNext(component, event);
            }
        }
                           });
        appEvent.fire();
        
    },
    //added by Atul Hinge.
    //To move to next screen
    Previous:function(component, event, helper) {
        component.set("v.screenNo",component.get('v.screenNo')-1);
        
    },
    toogleFromAddressEditor:function(component, event, helper) {
        component.set("v.showFromAddressSelector",!component.get('v.showFromAddressSelector'));
        component.find("selectFrom").focus();
    },
    onFromAddressChange:function(component, event, helper) {
        // component.get
        var owea=component.get("v.orgWideEmailAddress");
        var selectedFromAddress=component.get("v.selectedFromAddress");
        for(var i=0;i<owea.length;i++){
            if(selectedFromAddress==owea[i].Id){
                component.set("v.fromAddress",owea[i].DisplayName)
            }
        }
        component.set("v.showFromAddressSelector",!component.get('v.showFromAddressSelector'));
    },
    //added by Atul Hinge.
    //To display cc Email addresses
    toggleCC:function(component, event, helper) {
        component.set("v.showCC",!component.get('v.showCC'));
    },
    //added by Atul Hinge.
    //To display bcc Email addresses
    toggleBCC:function(component, event, helper) {
        component.set("v.showBCC",!component.get('v.showBCC'));
    },
    //added by Atul Hinge.
    //To display cc Email addresses
    toggleESC:function(component, event, helper) {
        component.set("v.showESC",!component.get('v.showESC'));
    },
    //added by Atul Hinge.
    //To Change Email Templates
    changeTemplate :function(component, event, helper){
        helper.changeTemplate(component, event);
    },
    //added by Atul Hinge.
    // call if case record changes 
    caseFieldChange:function(component, event, helper) {
        helper.changeTemplate(component, event);
    },
    //added by Atul Hinge.
    //display or hide Tool bar on email editor
    toogleToolbar:function(component, event, helper) {
        component.set("v.disabledCategories",!component.get('v.disabledCategories'));
    },
    //added by Atul Hinge.
    //display or hide attachment select section
    toogleModal:function(component, event, helper){
        component.find('addAttachment').toogle();
    },
    //added by Atul Hinge.
    //display or hide selected attachment list
    selectedFileObjectsChange:function(component, event, helper){
        
        if(component.get("v.selectedFileObjects").length>0){
            component.set("v.showAttached",true);
        }else{
            component.set("v.showAttached",false);
        }
        
    },
    //added by Atul Hinge.
    //remove attachment from selected attachment list
    removeAttachment:function(component, event, helper){
        var removeName=event.getSource().get("v.name");
        
        var uploadedFileObjects=component.get("v.selectedFileObjects");
        var uploadedFileObjectsSet=[];
        for(var i=0;i<uploadedFileObjects.length;i++){
            if(uploadedFileObjects[i].Id !=removeName){
                uploadedFileObjectsSet.push(uploadedFileObjects[i]);
            }
        }
        component.set("v.selectedFileObjects",uploadedFileObjectsSet);
        
        
    },
    /* Start : PUX-199  */
    changeToAddress:function(component, event, helper){
        var cmpTarget = component.find('messageId');
        $A.util.addClass (cmpTarget, 'hideme');
        $A.util.removeClass(cmpTarget, 'showme');
    },
    /* End : PUX-199  */
    
})