/*
* Authors	   :  Atul Hinge
* Date created :  14/08/2017
* Purpose      :  For email releated activity like reply,replyAll, forward.
* Dependencies :  Email.cmp(Aura component)
* JIRA ID      :  PUX-113,PUX-39,PUX-118,PUX-113,PUX-132,PUX-217,PUX-218,PUX-225, PUX-194
* -----------------------------------------------------------
* Modifications: 1 
*        Date:  27/11/2017
*        Purpose of modification:  Inline image paste in email body
*        Method/Code segment modified: handlePasteFunction 
  Modifications: 2 
*        Date:  09/02/2018
*        Purpose of modification:  PUX-138 TO/CC/ESC category wise email Id caching
*        Method/Code segment modified: initializeComponent 
*/
({
    //added by Atul Hinge.
    //To initialised component data
    initializeComponent : function(component, event) {
        
        if(component.get("v.screenNo")!=2){
            component.set("v.showSpinner",true);
            var createCmpEvent = $A.get("e.c:GetFieldSetComponent");
            createCmpEvent.setParams({
                "objectType":"CaseManager__c",
                "fsName":"EmailProcessingFields",
                "paramName":"Case",
                "size":3,
                "component": component,
                "callback" : function( response){
                    component.set("v.showSpinner",false);
                    component.set("v.processingfields",response);
                } ,
                "Action":'getFieldset',
            });
            createCmpEvent.fire();
        } 
        var action = component.get("c.initializeComponent");
        //  action.setParams({ interactionId : component.get("v.interactionId")});
        //alert(component.get("v.interactionId"));
        var parameters = {
            "interactionId" : component.get("v.interactionId"),
            "emailTemplateFolder" : 'Notification'
        };
        action.setParams({
            "requestParm": JSON.stringify(parameters)
        });
        action.setCallback(this, function(response) {
            var title=component.get('v.title');
            var state = response.getState();
            if (state === "SUCCESS") {
                var responceVar =response.getReturnValue();
                component.set("v.orgWideEmailAddress",response.getReturnValue().orgWideEmailAddress);
                //component.set("v.fromAddress",response.getReturnValue().orgWideEmailAddress[0].DisplayName);
                //component.set("v.selectedFromAddress",response.getReturnValue().orgWideEmailAddress[0].Id);
                component.set("v.templates",response.getReturnValue().templates);
                
               // alert(response.getReturnValue().templates);
			   /* Purpose: To update PredictedValue status of Indexing Field		start	*/	
               if(response.getReturnValue().predictedTemplate!=undefined && (title=='Reply' || title=='Reply All')){					
					component.set("v.selectedTemplate" ,response.getReturnValue().predictedTemplate);				
				}
				else{
					component.set("v.selectedTemplate",response.getReturnValue().templates[0].value);
				}
				/* Purpose: To update PredictedValue status of Indexing Field		end	*/	
                //console.log(response.getReturnValue().trailMailBody);
                /**PUX-262**/
                if(response.getReturnValue().trailMailBody == ''){	
                    
                    component.set("v.emailbody",this.populateValues(component,response.getReturnValue().templates[0].body));
                }else{
                    
                    component.set("v.emailbody",response.getReturnValue().trailMailBody);
                }
                component.set("v.trailMailBody",response.getReturnValue().trailMailBody);
                component.set("v.subject",this.populateValues(component,response.getReturnValue().templates[0].Subject));
                component.set("v.allContacts",response.getReturnValue().contacts); 
                //Start PUX-138 : TO/CC/ESC category wise email Id caching
				var allContact = response.getReturnValue().contacts;
				var allToContact=[];
				var allEscContact=[];
				var allBccContact=[];
				var allCcContact = [];
				var nameSpace =component.getConcreteComponent().getDef().getDescriptor().getNamespace();
				nameSpace=(nameSpace=='c')?'':nameSpace+'__';
				for(var i=0;i<allContact.length;i++){
                    //alert(allContact[i][nameSpace+'EmailType__c']);
					if(allContact[i][nameSpace+'EmailType__c'].includes('To') == true)
						allToContact.push(allContact[i]);
					if(allContact[i][nameSpace+'EmailType__c'].includes('Bcc') == true)
						allBccContact.push(allContact[i]);
					if(allContact[i][nameSpace+'EmailType__c'].includes('Cc') == true)
						allCcContact.push(allContact[i]);
					if(allContact[i][nameSpace+'EmailType__c'].includes('Esc') == true)
						allEscContact.push(allContact[i]);
				}
				component.set("v.allToContacts",allToContact); 
				component.set("v.allBccContacts",allBccContact); 
				component.set("v.allCcContacts",allCcContact); 
				component.set("v.allEscContacts",allEscContact); 
                //End PUX-138 : TO/CC/ESC category wise email Id caching
                //EmailMessage
                var emailMessage =response.getReturnValue().EmailMessage;
                
                if(title=='Reply' || title=='Reply All'){
				/* Purpose: To update PredictedValue status of Indexing Field		start	*/	
					this.changeTemplate(component,event);
				/* Purpose: To update PredictedValue status of Indexing Field		end	*/	
                    if(emailMessage.FromAddress !== undefined){
                        component.set("v.toContacts",this.populateContacts(component,emailMessage.FromAddress.split(";")));
                    }
                }
                if(title=='Reply All'){
                    if(emailMessage.CcAddress !== undefined){
                        component.set("v.ccContacts",this.populateContacts(component,emailMessage.CcAddress.split(";")));
                    }
                    if(emailMessage.BccAddress !== undefined){
                        component.set("v.bccContacts",this.populateContacts(component,emailMessage.BccAddress.split(";")));
                    }
                }
				//var owea=component.get("v.orgWideEmailAddress");
				component.set("v.fromAddress",response.getReturnValue().orgWideEmailAddress[0].DisplayName);
				if(response.getReturnValue().orgWideEmailAddress[0].Id!=null){
					component.set("v.selectedFromAddress",response.getReturnValue().orgWideEmailAddress[0].Id);
				}
				else{
					component.set("v.selectedFromAddress",'');
				}

            }else if (state === "INCOMPLETE") {
                
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    //added by Atul Hinge.
    //To change Template
    changeTemplate : function(component, event) {
        var templates=component.get("v.templates");
        for(var i =0 ; i<templates.length ;i++){			
            if(templates[i].value==component.get("v.selectedTemplate") && component.get("v.selectedTemplate")!=="TrailMail"){      
                /**PUX-262 & PUX-261**/  
               //component.set("v.emailbody",this.populateValues(component,templates[i].body)+'<p>'+component.get("v.trailMailBody")+'</p>');
                /**PUX-262 & PUX-261**/
                var templateEmailBody = '<br/>' + this.populateValues(component,templates[i].body);
                var trailEmailBody ='<br/>' + this.populateValues(component, component.get("v.trailMailBody"));
                var finalEmailBody = templateEmailBody + trailEmailBody;
                console.log(templateEmailBody);
                component.set("v.emailbody",finalEmailBody);
                component.set("v.subject",this.populateValues(component,templates[i].Subject));  
                console.log(component.get("v.emailbody"));
            }
        }
    },
    //added by Atul Hinge.
    //To send email for (Reply,ReplyAll,Forward,Compose)
    sendEmail : function(component, event) {
        component.set("v.showSpinner",true);
        var action = component.get("c.sendMail");
        var emailWrapper={};
        /*-- Start : PUX-199 */
        if(component.get("v.toContacts")=='' || component.get("v.toContacts")==undefined ){
            var cmpTarget = component.find('messageId');
            $A.util.removeClass(cmpTarget, 'hideme');
            $A.util.addClass(cmpTarget, 'showme');
            component.set("v.showSpinner",false);
            return false;
        }else{
            var cmpTarget = component.find('messageId');
            $A.util.removeClass(cmpTarget, 'showme');
        } 
        /*-- End : PUX-199 */
        
        emailWrapper.sendTo=this.getEmailList(component,component.get("v.toContacts"));
        emailWrapper.sendCc=this.getEmailList(component,component.get("v.ccContacts"));
        emailWrapper.sendBcc=this.getEmailList(component,component.get("v.bccContacts"));
        emailWrapper.sendEsc=this.getEmailList(component,component.get("v.escContacts"));
        
        emailWrapper.subject=component.get("v.subject");
        emailWrapper.fromAddress=component.get("v.selectedFromAddress");
		//alert(emailWrapper.fromAddress);
        emailWrapper.attachmentIds=[];
        var nameSpace =component.getConcreteComponent().getDef().getDescriptor().getNamespace();
        nameSpace=(nameSpace=='c')?'':nameSpace+'__';
        var attachments=component.get("v.selectedFileObjects");
        for(var i=0;i<attachments.length;i++){
            emailWrapper.attachmentIds.push(attachments[i].ContentDocument.Id);
        }
        var caseTracker=component.get("v.Case"); 
        if(component.get("v.responseRequired") && LoginUserId == caseTracker.OwnerId){
            caseTracker[nameSpace+"UserAction__c"]='Send For Email Resolution';
        }
        action.setParams({ emailJSON : JSON.stringify(emailWrapper),
                          emailBody:component.get("v.emailbody"),
                          ct:caseTracker
                         });                
        action.setCallback(this, function(response) {
            component.set("v.showSpinner",false);
            var state = response.getState();
            if (state === "SUCCESS") {
                var callback=component.get("v.callback");
                if(typeof callback ==='function'){
                    callback(response.getReturnValue());
                }
                var appEvent = $A.get("e.c:ShowToast");
                appEvent.setParams({ "title" : "Success !","message":"Email has been sent" });
                appEvent.fire();
                component.set("v.showReplyScreen",!component.get("v.showReplyScreen"));
                /* Start : PUX-189 */
                component.set("v.interactionRefCounter",component.get("v.interactionRefCounter")+1);
                /* End : PUX-189 */
            }else if (state === "INCOMPLETE") {
                alert('incomplete');
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    //added by Atul Hinge.
    //replace regular expression in EmailTemplate with proper values
    populateValues : function(component, str) {
        while(str.indexOf("{!")>=0){
            var expr =str.substring(str.indexOf("{!"),str.indexOf("}")+1);
            str =str.replace(expr,this.getValue(component,expr.substring(2,expr.indexOf("}"))));
        }
     //   console.log(str);
        return str;        
    },
    //added by Atul Hinge.
    //To populate contacts which are not in contact object list
    populateContacts : function(component,emailIds) {
        var contacts=[];
        for(var i=0;i<emailIds.length;i++){
            var obj = {
                "Name": emailIds[i],
                "Id": emailIds[i],
                "Email": emailIds[i]
            };
            contacts.push(obj);
        }
        return contacts;
    },
    getValue : function(component, str) {
        var obj =component.get("v.Case");
        return obj[str.split('.')[1]];
    },
    //added by Atul Hinge.
    //To update case detail record
    updateCase: function(component, event) {
        var caseId = component.get("v.Case.Id");
        var compEvent = component.getEvent("caseAction");
        var nameSpace =component.getConcreteComponent().getDef().getDescriptor().getNamespace();
        nameSpace=(nameSpace=='c')?'':nameSpace+'__';
        var caseTracker=component.get("v.Case");
       // if(LoginUserId == caseTracker.OwnerId){
        // caseTracker[nameSpace+"SkipIndexing__c"]=true;
        //}
        compEvent.setParams({
            "Action": "update",
            "Case": caseTracker,
            "callback": function(response) {
                component.set("v.Case", response.getReturnValue().Cases[0]);
                var appEvent = $A.get("e.c:ShowToast");
                appEvent.setParams({
                    "title": "Success !",
                    "message": " Case is successfully saved!"
                });
                appEvent.fire();
            }
        });
        compEvent.fire();
    },
    //added by Atul Hinge.
    //To update case detail record
    updateCaseandNext: function(component, event) {
        var caseId = component.get("v.Case.Id");
        var compEvent = component.getEvent("caseAction");
        var nameSpace =component.getConcreteComponent().getDef().getDescriptor().getNamespace();
        	nameSpace=(nameSpace=='c')?'':nameSpace+'__';
        var caseTracker=component.get("v.Case");
            if(LoginUserId == caseTracker.OwnerId){
                caseTracker[nameSpace+"SkipIndexing__c"]=true;
            }
        compEvent.setParams({
            "Action": "update",
            "Case": caseTracker,
            "callback": function(response) {
                 component.set("v.Case", response.getReturnValue().Cases[0]);
                var appEvent = $A.get("e.c:ShowToast");
                appEvent.setParams({
                    "title": "Success !",
                    "message": " Case is successfully saved!"
                });
                appEvent.fire();
                component.set("v.screenNo",component.get('v.screenNo')+1);
            }
        });
        compEvent.fire();
    },
    //added by Atul Hinge.
    //returns a list of all Email addressed in Contact list;
    getEmailList : function(component, contactList) {
        var emails=[];
        for(var i=0;i<contactList.length;i++){
            emails.push(contactList[i].Email);
        }
        return emails;
    },
    //added by Atul Hinge.
    //To prepopulate contacts
    getPreContacts : function(component, str) {
        var interaction = response.getReturnValue().EmailMessage.FromAddress; 
        var contacts=response.getReturnValue().contacts;
        var toContacts=[];
        for(var i=0;i<contacts.length;i++){
            if(contacts[i].Email==interaction){
                toContacts.push(contacts[i]);
            }
        }
    },
})