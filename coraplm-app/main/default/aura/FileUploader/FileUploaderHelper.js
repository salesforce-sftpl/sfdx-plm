/*
* Authors	   :  Atul Hinge
* Date created :  24/10/2017
* Purpose      :  To create new attachment.
* Dependencies :   SelectAttachment.cmp,Email.cmp(Aura component)
* JIRA ID      :  PUX-335,PUX-266,PUX-265,PUX-252	
* -----------------------------------------------------------
* Modifications: 
*        Date:  
*        Purpose of modification:  
*        Method/Code segment modified:    
*/
({
    /* Authors: Atul Hinge || Purpose: send message from Lightning to VF page */ 
    sendToVF : function(component,event) {
        var message={"relatedId":component.get("v.relatedId"),"status":component.get("v.status")};
        component.set("v.message",message);
        var message = component.get("v.message");
        var vfOrigin = "https://" + component.get("v.vfHost");
        var vfWindow = component.find("vfFrame").getElement().contentWindow;
        vfWindow.postMessage(message, vfOrigin);
    },
    /* Authors: Atul Hinge || Purpose: receive message from VF page to Lightning */
    receiveFromVF: function(component,event) {
        var vfOrigin = "https://" + component.get("v.vfHost");
        window.addEventListener("message", function(event) {
            if (event.origin !== vfOrigin) {
                return;
            }
            if(event.data.response != undefined){
                component.set("v.contentDocumentLinks",event.data.response.contentDocumentLinks);
            }
            // alert(event.data.status);
            component.set("v.status",event.data.status);
        }, false);
    },
})