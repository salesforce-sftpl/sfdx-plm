/*
Authors: Ashish Kumar
Date created: 17/11/2017
Purpose: This controller will used to handle custom search logic.
Dependencies: Header.cmp
-------------------------------------------------
Modifications:
                Date: 18/12/2017
                Purpose of modification: PUX-455 | Applied Search criteria not displayed on Search result page
                Method/Code segment modified: added new param for event in "doSearch"
                
*/ 
({
    myAction : function(cmp, event, helper) {
        //var theLabel = cmp.get("v.label");
    },
    handleMenuSelect : function(cmp, event, helper) {
		var theLabel = event.getParam("value");
        var url='';
        var np=cmp.toString().split('":"')[1].split('"}')[0]; 
              np=(np=='c')?'':np+'__';
        if(theLabel=='SetUp'){
            url='https://'+window.location.hostname+"/one/one.app#/setup/home";
        }else if(theLabel=='Report'){
            url='https://'+window.location.hostname+"/one/one.app#/sObject/Report/home";
        }else if(theLabel=='Dashboard'){
            url='https://'+window.location.hostname+"/one/one.app#/sObject/Dashboard/home";
        }else if(theLabel=='Standard View'){
            url='https://'+window.location.hostname+"/one/one.app#/sObject/"+np+"CaseManager__c/list";
        }else if(theLabel=='Classic View'){
            url="/one/one.app#/sObject/Dashboard/home";
        }
		window.open(url,"_blank")
	},
    /*
		Authors: Ashish Kumar
		Purpose: doSearch function responsible to call apex search logic
		Dependencies: Header.cmp
    */
	doSearch : function(component, event, helper) {
		if (event.getParams().keyCode == 13 || event.getParams().keyCode == undefined) {
			var searchText = component.get('v.searchText');
			if(searchText != ''){
				var action = component.get('c.searchForIds');
		        action.setParams({searchText: searchText});
		        action.setCallback(this, function(response) {
		            var state = response.getState();
		            if (state === 'SUCCESS') {
		                var ids = response.getReturnValue();
		                var searchResultEvent = component.getEvent("searchResult");
		                var jsonString = JSON.stringify(ids);
		                searchResultEvent.setParams({
		                	"RecordIds" : jsonString,
		                	"SearchText" : searchText
		                });
		                searchResultEvent.fire(); 
		                component.set('v.searchText','');
		            }
		        });
		        
		        $A.enqueueAction(action);
			}
		}
    },
    
})