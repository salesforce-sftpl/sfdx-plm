/*
  Authors	   :   Rahul Pastagiya
  Date created :   10/08/2017
  Purpose      :   Client side helper for controller
  Dependencies :   HighlightPanelController.cls
  __________________________________________________________________________
  Modifications:
		    Date:  
		    Purpose of modification:  
		    Method/Code segment modified: 
 */
({
	/*
		Authors: Rahul Pastagiya
		Purpose: To get specific records from given object, field Set and record id dynamically
	*/
	getRecord: function(component, event) {
		var action = component.get("c.getRecord");
		var noOfCols = component.get("v.noOfCols");
		action.setParams({
			objectName: component.get("v.objectName"),
			fieldSetLstStr: JSON.stringify(component.get("v.fieldSet")),
			recordId: component.get("v.recordId"),
			noOfCols: noOfCols,
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var arrayOfMapKeys = [];
				var setLst = [];
				var StoreResponse = response.getReturnValue();
                console.log('test'+StoreResponse.fieldValuesMap);
				component.set('v.fieldValuesMap', StoreResponse.fieldValuesMap);
				setLst = StoreResponse.fieldList;
				for (var i = 0; i < setLst.length - noOfCols; i++) {
					arrayOfMapKeys.push(setLst[i]);
				}
				component.set('v.expandLstKey', arrayOfMapKeys);
				arrayOfMapKeys = [];
				for (i = setLst.length - noOfCols; i < setLst.length; i++) {
					arrayOfMapKeys.push(setLst[i]);
				}
				component.set('v.collapseLstKey', arrayOfMapKeys);
			} else if (state === "INCOMPLETE") {
				//Logic
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);
	}
})