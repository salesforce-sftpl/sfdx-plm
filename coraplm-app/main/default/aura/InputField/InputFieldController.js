({
	doInit : function(component, event, helper) {
        var fieldName =component.get("v.value");
        var ref = component.getReference("v.sObject."+fieldName);
       // alert(fieldName);
        component.set("v.ref",ref);
        if (component.get("v.type") == "REFERENCE") {
            helper.getLookUpValues(component, event, helper);
        }
        component.set("v.filterValues",[]);
		component.set("v.isFirstCall",true);
	},
	afterRender: function (component,event, helper) {
	},
    changeValue : function(component, event, helper) {
		helper.fieldChangeHelper(component, event, helper);
	},
    sObjectChange : function(component, event, helper) {
        var controlField = component.get("v.fieldDetails").controlField;//
        var sObject =component.get("v.sObject");
        var fieldDetails=component.get("v.fieldDetails");


        if (controlField !== undefined) {
            var fieldName =component.get("v.value");			
         	 component.set("v.fieldDetails.options", component.get("v.fieldDetails").dependentValues[sObject[controlField]]);
        }
        if (sObject['hasError']) {
           var inpComponent = component.find("inputField");
		   if(inpComponent !== undefined){
               //try{
				inpComponent.showHelpMessageIfInvalid();
               //}
               //catch(err){}
		   }
        }
	},
    getLookUpValues : function(component, event, helper) {
		var toggleText = component.find("listbox");
		$A.util.removeClass(toggleText, "slds-hide");
		var filterValues = component.get("v.filterValues");
        filterValues=[];
		var lookupValues = component.get("v.lookupValues");
        var val=component.get("v.searchValue");
        for(var i=0;i<lookupValues.length;i++){
            if(lookupValues[i]['Name'].toLowerCase().indexOf(val.toLowerCase()) >= 0){
                filterValues.push(lookupValues[i]);
            }
        }
		component.set("v.filterValues",filterValues);
		if (filterValues.length == 0 || event.getParams().keyCode==27) {
			var toggleText = component.find("listbox");
			$A.util.addClass(toggleText, 'slds-hide');
		}
	},
    getSelectedValue : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        component.set("v.ref", selectedItem.dataset.record);
        component.set("v.searchValue", selectedItem.dataset.name);
        var toggleText = component.find("listbox");
        $A.util.removeClass(toggleText, "show");
        $A.util.addClass(toggleText, "slds-hide");
	},
    toogleModal: function(component, event, helper) {
        var options = component.get("v.listValues");
        var val = component.get("v.searchValue");
        var filterValues = [];
        if (val != 'null' && val != null && val != "") {
            for (var i = 0; i < options.length; i++) {
                var contact = options[i];
                if (contact.Name.toLowerCase().indexOf(val.toLowerCase()) >= 0) {
                    filterValues.push(options[i]);
                }
            }
            component.set("v.filterValues", filterValues);
        } else {
            component.set("v.filterValues", options);
        }
        $A.util.toggleClass(component.find('lookupValues'), 'slds-hide');
	},
    /*
		Authors: Niraj Prajapati
		Purpose: getSelectedID function will be called when we select any value from dropdown or lookup popup
		Dependencies: CaseDetailSection.cmp
                
    */
    getSelectedID: function(component, event, helper) {
        var selectedItem = event.currentTarget;
        component.set("v.ref", selectedItem.dataset.record);
        component.set("v.searchValue", selectedItem.dataset.name);
        $A.util.toggleClass(component.find('lookupValues'), 'slds-hide');
    },
     /*
		Authors: Niraj Prajapati
		Purpose: lookUpSearchValues function will be used to search lookup values
		Dependencies: CaseDetailSection.cmp
                
    */
    lookUpSearchValues: function(component, event, helper) {
        var options = component.get("v.listValues");
        var val = component.get("v.searchValue");
        var filterValues = [];
        if(val != null){
            for (var i = 0; i < options.length; i++) {
                var contact = options[i];
                if (contact.Name.toLowerCase().indexOf(val.toLowerCase()) >= 0) {
                    filterValues.push(options[i]);
                }
            }
            component.set("v.filterValues", filterValues);
        }
    },
})