/*
  * Authors		 : Parth Lukhi
  * Date created : 30/08/2017 
  * Purpose      :InteractionListViewHelper JS used for getting Interactions based on Case
  * Dependencies : InterActionListController.apxc
  * -------------------------------------------------
  * Modifications:
                Date: 
                Purpose of modification:  
                Method/Code segment modified: 
*/
({
	/*
	 * Authors: Parth Lukhi
	 * Purpose:   Getting Interaction on CaseDetail View
	 * Dependencies: NA
	 * 
	 * Start :  Getting Interaction on CaseDetail View*
	 */
	getInterbyCase: function(component, event) {
		var action = component.get("c.getInteractionByCase");
		//PUX-391
		var caseRecordId = component.get("v.Case.Id");
        
        if(caseRecordId == undefined){
            caseRecordId = 'a010Y00000gvWq8QAE';
        }
		action.setParams({
			caseId: caseRecordId,
			pageNumber: component.get("v.pageNumber"),
			recordsToDisplay: component.get("v.recordsToDisplay")
		});
		//PUX-391 ends
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.interactionList", response.getReturnValue().emails);
              //  alert(response.getReturnValue().emails.length);
				//PUX-478							
				component.set("v.maxPage", Math.ceil((response.getReturnValue().total) / 15));
				//Start : PUX-504
				component.set("v.totalRecords", response.getReturnValue().total);
                this.toogleSpiner(component, event);
				//End : PUX-504
				//PUX-478                
			} else if (state === "INCOMPLETE") {
				//Logic
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);
	},
    toogleSpiner : function(component, event) {
         $A.util.toggleClass( component.find("Intspinner"), 'slds-hide')
    }
	/* End : Getting Interaction on CaseDetail View*/
})