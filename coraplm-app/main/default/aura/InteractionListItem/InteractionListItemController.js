/**
  * 
  * Authors		: Parth Lukhi
  * Date created : 17/08/2017 
  * Purpose      : InteractionListItemController JS used for getting/displaying Particular Interaction Item
  * Dependencies : InteractionListItem.cmp
  * -------------------------------------------------
  *Modifications:
                Date:  09/11/2017 
                Purpose of modification:  PUX-235 : Getting Contact Name on interction 
                Method/Code segment modified: showInterBody
  

*/
({
	/*
	 * Authors: Parth Lukhi
	 * Purpose: On Load method used for setting initials of Sender Name, Setting Interaction Date Format
	 * 			Setting Mail First Liner on Item View of Interaction
	 * Dependencies:  InteractionListItemHelper.js
	 */
	doInit: function(component, event, helper) {
        var isIncomingMail = component.get("v.Interaction.Incoming");
        var isNote = component.get("v.Interaction.Is_Note__c");
        var isPrivateNote = component.get("v.Interaction.Is_Private__c");
        var iconName = 'inbound_email.png';
        if(!isNote){
            if(isIncomingMail){
                iconName = 'inbound_email.png';
            }else{             
            	iconName = 'Outbound_email.png';   
            }
        } else{ 
            if(isPrivateNote){
                iconName = 'Private_Note.png';
            }else{
                iconName = 'Public_Note.png';
            }
    	}
        component.set("v.iconName", iconName);
		var intials = component.get("v.Interaction.FromName");
		if (intials) {
			intials = intials.charAt(0).toUpperCase();
			//intials='A';
			component.set("v.initialName", intials);
		}
		var interDt = component.get("v.Interaction.CreatedDate");
		var interDate = new Date(interDt);
		var today = new Date();
		var isToday = (today.toDateString() == interDate.toDateString());
		if (isToday) {
			var todayAMPM = helper.formatAMPM(interDate);
			//console.log(todayAMPM);
			component.set("v.interItemDate", todayAMPM);
		} else {
			var timeDiff = Math.abs(today.getTime() - interDate.getTime());
			var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
			if (diffDays < 7) {
				var weekDay = helper.foramteWeekDate(interDate);
				component.set("v.interItemDate", weekDay);
			} else {
				component.set("v.interItemDate", helper.formatDate(interDate));
			}
		}
		var mailFirstLine;
		if (component.get("v.Interaction.TextBody")) {
			if (component.get("v.Interaction.TextBody").length < 90) {
				mailFirstLine = component.get("v.Interaction.TextBody");
			} else {
				mailFirstLine = component.get("v.Interaction.TextBody").substring(0, 90) + '...';
			}
			component.set("v.mailFirstLine", mailFirstLine);
		}
	},
	/* End : on Load method used for setting initials of Sender Name, Setting Interaction Date Format
	 * Setting Mail First Liner on Item View of Interaction
	 */
    
    /*
	 * Authors: Parth Lukhi
	 * Purpose: Toggle On Mouse Hover on Reply,ReplyAll,Forward ICons 
	 * Dependencies:  InteractionListItemHelper.js
	 *
	 * Start : Toggle On Mouse Hover on Reply,ReplyAll,Forward ICons */
	mailActtionMouseOver: function(component, event, helper) {
		var toggleMail = component.find("mailActionTgtDiv");
		$A.util.removeClass(toggleMail, 'hideMailBtn');
		$A.util.addClass(toggleMail, 'showMailBtn');
	},
	mailActtionMouseOut: function(component, event, helper) {
		var toggleMail = component.find("mailActionTgtDiv");
		$A.util.addClass(toggleMail, 'hideMailBtn');
		$A.util.removeClass(toggleMail, 'showMailBtn');
	},
	mailActIntMouseOver: function(component, event, helper) {
		var toggleMail = component.find("mailActTgtDiv");
		$A.util.removeClass(toggleMail, 'hideMailBtn');
		$A.util.addClass(toggleMail, 'showMailBtn');
	},
	mailActIntMouseOut: function(component, event, helper) {
		var toggleMail = component.find("mailActTgtDiv");
		$A.util.addClass(toggleMail, 'hideMailBtn');
		$A.util.removeClass(toggleMail, 'showMailBtn');
	},
	/* End : Toggle On Mouse Hover on Reply,ReplyAll,Forward ICons */
    
     /*
	 * Authors: Parth Lukhi
	 * Purpose: Toggle Interaction Mail Body when clicked on Subject of Interaction
	 * Dependencies:  InteractionListItemHelper.js
	 *
	 * Start : Toggle Interaction Mail Body when clicked on Subject of Interaction*/
	showInterBody: function(component, event, helper) {
        var toggleBtn = component.find("toggleBtn");
        var toggleBody = component.find("interBody");
		var toggleParent = component.find("interItemParent");    
        if(toggleBtn.get('v.iconName') == 'utility:chevronright'){        	
        	toggleBtn.set('v.iconName', 'utility:switch');   
        }else{
            toggleBtn.set('v.iconName', 'utility:chevronright');  
        }
		$A.util.toggleClass(toggleParent, 'toggleDisplay');
		$A.util.toggleClass(toggleBody, 'toggleDisplay');
		helper.getAttachList(component, event);
        /* Start : PUX-235 */ 
        helper.getContactName(component, event);
        /* End : PUX-235 */ 
        /* Start : PUX-211 */
        var ctObj= component.get("v.ct");
        var loggedUser= component.get("v.loginUserId");
        if(ctObj.OwnerId==loggedUser){
              helper.makeReadMail(component, event);
        }
      	/* End : PUX-211 */
	},
	hideInterBody: function(component, event, helper) {
		var toggleBody = component.find("interBody");
		var toggleParent = component.find("interItemParent");
		$A.util.toggleClass(toggleParent, 'toggleDisplay');
		$A.util.toggleClass(toggleBody, 'toggleDisplay');
	},
	/* End : Toggle Interaction Mail Body when clicked on Subject of Interaction*/
    showAttachment: function(component, event, helper) {
		helper.getAttachList(component, event);
        var toggleBody = component.find("attchmentPopUp");
		//var toggleParent = component.find("interItemParent");
		//$A.util.toggleClass(toggleParent, 'toggleDisplay');
		$A.util.toggleClass(toggleBody, 'toggleDisplay');
    },
    hideAttachment: function(component, event, helper) {
        var toggleBody = component.find("attchmentPopUp");
		//var toggleParent = component.find("interItemParent");
		//$A.util.toggleClass(toggleParent, 'toggleDisplay');
		$A.util.toggleClass(toggleBody, 'toggleDisplay');
    },
    /*
	 * Authors: Parth Lukhi
	 * Purpose: On CLick of  Reply,ReplyAll,Forward ICons 
	 * Dependencies:  InteractionListItemHelper.js
	 * Start : On CLick of  Reply,ReplyAll,Forward ICons */
	handleCaseAction: function(component, event, helper) {
        var compEvent = component.getEvent("caseAction");
        var params={
            "Case": component.get("v.ct"),
            "interactionId":  component.get("v.Interaction.Id"),
            "callback":function(){
                
            },
       };
        
		if (event.currentTarget.id == "reply_sup" || event.currentTarget.id == "reply_sub") {
			params["Action"]="reply";
        } else if (event.currentTarget.id == "replyall_sup" || event.currentTarget.id == "replyall_sub") {
			params["Action"]="replyall";
        } else if (event.currentTarget.id == "forward_sup" || event.currentTarget.id == "forward_sub") {
			params["Action"]="forward";
        }
		compEvent.setParams(params);
        compEvent.fire();
	},
	/* End : On CLick of  Reply,ReplyAll,Forward ICons */
    /*
	 * Authors: Parth Lukhi
	 * Purpose:  Opening Attachment View when CLick on Attachment icons on Interaction Details
	 * Dependencies:  InteractionListItemHelper.js
	 * 
     * Start : Opening Attachment View when CLick on Attachment icons on Interaction Details */
	openAttachViewer: function(component, event, helper) {
		var attachId = event.currentTarget.id;
        var att=component.get("v.attachList");
        var object={};
        for(var i=0;i<att.length;i++){
            if(att[i].ContentDocument.LatestPublishedVersionId==attachId){
                object=att[i];
                break;
            }
        }
        if(event.currentTarget.dataset.visible=='true'){
            var appEvent = $A.get("e.c:OpenSubTab");
            appEvent.setParams({
                "parentTabName": "case",
                "id": attachId,
                "label": event.currentTarget.dataset.value,
                "object":object,
            });
            appEvent.fire();
        }else{
            window.open('/sfc/servlet.shepherd/document/download/'+event.currentTarget.dataset.id,'_self');        
        }
	},
	/* End : Opening Attachment View when CLick on Attachment icons on Interaction Details */
    //PUX-305 end
    /*
		Authors: Rahul Pastagiya
		Purpose: Open Sorting dropdown on Case list page
		
	*/
    openSortDropDown: function(cmp, event, helper) {
        var conEle1 = cmp.find("sortDropdown");
        $A.util.toggleClass(conEle1, 'slds-is-open');
    },
    /*
		Authors: Rahul Pastagiya
		Purpose: Close Sorting dropdown on Case list page
		
	*/
    closeSortDropDown: function(cmp, event, helper) {
        var conEle1 = cmp.find("sortDropdown");
        $A.util.removeClass(conEle1, 'slds-is-open');
    },
    
})