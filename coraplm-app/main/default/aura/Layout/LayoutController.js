({
    doInit: function(component, event, helper) {
		debugger;
        var sObj=component.get('v.sObject');
        if(sObj==null){
            var sObject={"sobjectType":component.get("v.sObjectName")};
            component.set('v.sObject',sObject);
            
        }
        helper.getFields(component, event);
    },
    handleClick: function(component, event, helper) {
		alert(JSON.stringify(component.get("v.sObject")));
	},
    validate: function(component, event, helper) {
        helper.validate(component, event);
	},
	/* Purpose: To update PredictedValue status of Indexing Field		start	*/	
    getFieldDetails: function(component, event, helper) {
        helper.getFieldDetails(component, event);
    },
	/* Purpose: To update PredictedValue status of Indexing Field		end	*/	
    tooglePannel: function(component, event, helper) {
        var layoutMeta=component.get("v.layoutMeta");
        for(var i=0;i<layoutMeta.length;i++){
            if(layoutMeta[i].sectionName==event.currentTarget.dataset.value){
                if(layoutMeta[i]['isOpen']){
                    layoutMeta[i]['isOpen']=false;
                }else{
                    layoutMeta[i]['isOpen']=true;
                }
            }
        }
        component.set("v.layoutMeta",layoutMeta);
		
	}
})