({
    getFields : function(component, event) {
        var action = component.get("c.getFields");
        var requestParm ={};
        requestParm["objectName"]=component.get("v.sObjectName");
        requestParm["recordTypeName"]=component.get("v.recordTypeName");
        requestParm["sessionId"]=window.SessionId;
		requestParm["caseManagerId"]=component.get("v.sObject").Id;
        console.log(requestParm);
        action.setParams({ requestParm : JSON.stringify(requestParm)});
        

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var layoutMeta=response.getReturnValue().sectionFields;
                for(var i=0;i<layoutMeta.length;i++){
                            layoutMeta[i]['isOpen']=true;
                    
                }
				var layoutJSON=response.getReturnValue().fieldJson;
				component.set("v.layoutJSON",layoutJSON);
				component.set("v.layoutMeta",layoutMeta);
				
              //  component.set('v.layoutMeta',response.getReturnValue());
            } else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    validate: function(component, event) {
        var layoutMeta=component.get("v.layoutMeta");
        var sObject=component.get("v.sObject");
         sObject['hasError']=false;
        for(var i=0;i<layoutMeta.length;i++){
            var lMeta =layoutMeta[i];
            for(var j=0;j< lMeta.relatedFields.length;j++){
                var fieldDetails =lMeta.relatedFields[j];
                if(fieldDetails.fieldBehavior=='Required' && (sObject[fieldDetails.fieldAPIName]==undefined || sObject[fieldDetails.fieldAPIName]=='')){
                   //  alert(sObject[fieldDetails.fieldAPIName]);// && sObject[fieldDetails.fieldAPIName] ==""
                     sObject['hasError']=true;
                } 
            }
        }
        component.set("v.sObject",sObject);
    },
	/* Purpose: To update PredictedValue status of Indexing Field		start	*/	
    getFieldDetails : function(component, event) {
        var params = event.getParam('arguments');
        var sObject=component.get("v.sObject");
        if (params) {
            var fieldName = params.fieldName;
        }
        var layoutMeta=component.get("v.layoutMeta");
        for(var i=0;i<layoutMeta.length;i++){
            var lMeta =layoutMeta[i];
            for(var j=0;j< lMeta.relatedFields.length;j++){
                var fieldDetails =lMeta.relatedFields[j];
                if(fieldDetails.fieldAPIName==fieldName){
				
                //  component.set("v.methodResponse",fieldDetails.dependentValues[sObject[fieldDetails.controlField]]);
                  component.set("v.methodResponse",fieldDetails);
                } 
            }
        }
       
    }
    /* Purpose: To update PredictedValue status of Indexing Field		end	*/	
})