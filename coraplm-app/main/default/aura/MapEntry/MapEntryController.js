/*
  Authors	   :   Rahul Pastagiya
  Date created :   07/02/2018
  Purpose      :   To render Data from Map in form of Label and Value
  Dependencies :   NA
  __________________________________________________________________________
  Modifications:
		    Date:  
		    Purpose of modification:  
		    Method/Code segment modified: 
*/
({
	/*
		Authors: Rahul Pastagiya
		Purpose: To get value from Map and set UI display/format attribute
	*/
	doInit: function(component, event, helper) {
		var key = component.get("v.key");
		var map = component.get("v.map");
        var labelMap = component.get("v.labelMap");
        
		component.set("v.label", labelMap[key] == undefined ? '-': labelMap[key]);
        component.set("v.value", map[key] == undefined ? '-': map[key]);
	},
})