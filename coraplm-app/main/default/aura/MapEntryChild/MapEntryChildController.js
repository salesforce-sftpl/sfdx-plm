/*
  Authors	   :   Rahul Pastagiya
  Date created :   11/08/2017
  Purpose      :   Client side controller to provide all actions for component
  Dependencies :   None
  __________________________________________________________________________
  Modifications:
  			Name : Rahul PASTAGIYA
		    Date:  07/02/2018
		    Purpose of modification:Optimize the list view to ensure key information is readily available  
		    Method/Code segment modified:  PUX-12
*/
({
	/*
		Authors: Rahul Pastagiya
		Purpose: To get value from Map and set UI display/format attribute
	*/
	doInit: function(component, event, helper) {
        var key = component.get("v.key");
        var map = component.get("v.map");
        var noOfCols = component.get("v.noOfCols");
            if (noOfCols == 3) {
                component.set("v.colWidth", 2);
            } else if (noOfCols == 1) {
                component.set("v.colWidth", 6);
            } else {
                component.set("v.colWidth", 3);
            }
        //PUX-12
        var labelMap = component.get("v.labelMap");
        if(labelMap != undefined){
            component.set("v.label", labelMap[key] == undefined ? '-': labelMap[key]);
        	component.set("v.value", map[key] == undefined ? '-': map[key]);
        }else{
            component.set("v.label", map[key]['label']);
            component.set("v.value", map[key]['value']);
        }
        //PUX-12
	},
})