({
	doinit : function(component, event, helper) {
        var emailMsgObj = component.get('v.mergeCase.Emails');
		if (emailMsgObj == undefined) {
			component.set('v.EmailTxtBody', 'No interaction yet!');
		} else if (emailMsgObj[0].TextBody != undefined) {
			component.set('v.EmailTxtBody', emailMsgObj[0].TextBody.substring(0, 100));
		}
    },
    toggleRadio : function(component,event,helper){
        var selCase = component.get("v.mergeCase");
        var action = 'chosen';
        var chosenCaseEvt = component.getEvent("chosenCases");
        chosenCaseEvt.setParams({
            "selectedCase" : selCase,
            "action" : action
        });
        chosenCaseEvt.fire();
    },
    //handle Case Action on click of Case Number
    handleCaseAction: function(component, event, helper) {
    	var compEvent = component.getEvent("caseAction");
		compEvent.setParams({
				"Action": "redirectToDetail",
				"Case": component.get("v.mergeCase")
			});
	
		compEvent.fire();
	}
})