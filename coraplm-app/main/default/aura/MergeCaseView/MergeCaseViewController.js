({
	doInit : function(component, event, helper) {
        var checkChosen = component.get('v.isChosen');
        if(checkChosen==true){
            var emailMsgObj = component.get('v.selectedCase.Emails');
            if (emailMsgObj == undefined) {
                component.set('v.oneLineEmail', 'No interaction yet!');
            } else if (emailMsgObj[0].TextBody != undefined) {
                component.set('v.oneLineEmail', emailMsgObj[0].TextBody.substring(0, 100));
            }
        }else{
            var emailMsgObj = component.get('v.merge[0].Emails');
            if (emailMsgObj == undefined) {
                component.set('v.oneLineEmail', 'No interaction yet!');
            } else if (emailMsgObj[0].TextBody != undefined) {
                component.set('v.oneLineEmail', emailMsgObj[0].TextBody.substring(0, 100));
            }
        }
    },
   
    handlechosenCases : function(component, event, helper){
        var action = event.getParam("action");
        var selectedCase = event.getParam("selectedCase");
        component.set('v.isChosen', true);
        component.set('v.selectedCase',selectedCase);
        var emailMsgObj = component.get('v.selectedCase.Emails');
        if (emailMsgObj == undefined) {
            component.set('v.oneLineEmail', 'No interaction yet!');
        } else if (emailMsgObj[0].TextBody != undefined) {
            component.set('v.oneLineEmail', emailMsgObj[0].TextBody.substring(0, 100));
        }
    },
   
    closeMergeWindow : function(component, event, helper){
        var compEventRedirect = component.getEvent("caseAction");
			compEventRedirect.setParams({
				"Action": "redirect",
				"Case": component.get("v.merge[0]")
			});
			compEventRedirect.fire();
    },
   
    performMerge : function(component,event,handler){
        var mergeComments = component.get("v.comment");
        if(mergeComments == '' || mergeComments == null){
            alert("Please provide Merge Comments before Merging the Case");
        }
        else{
        	var mergeItemList = component.get('v.merge');
	        var chosen = component.get('v.isChosen');
	        if(chosen==false){
	        var selectedCase = component.get('v.merge[0]');
	        }else{
	          var selectedCase = component.get('v.selectedCase');  
	        }
	        var action = component.get("c.performMergeAction");
	        action.setParams({
	            "mergeList" : mergeItemList,
	            "mainCase" : selectedCase,
	            "mergeComments" : mergeComments
	        });
	        action.setCallback(this,function(response){
	            var state = response.getState();
	            if(state==="SUCCESS"){
	            var compEventRedirect = component.getEvent("caseAction");
                compEventRedirect.setParams({
                    "Action": "redirect",
                    "Case": component.get("v.merge[0]")
                });
                compEventRedirect.fire();   
	            }else if (state === "ERROR"){
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +errors[0].message);
                        }
                    }else{
                        console.log("Unknown error");
                    }
                }
	        });
	        $A.enqueueAction(action);
        }  
    },
    //handle Case Action on click of Case Number
    handleCaseAction: function(component, event, helper) {
    	var compEvent = component.getEvent("caseAction");
    	console.log(component.get('v.selectedCase'));
    	var checkChosen = component.get('v.isChosen');
    	if(checkChosen == false){
    		compEvent.setParams({
				"Action": "redirectToDetail",
				"Case": component.get("v.merge[0]")
			});
    	}else{
    		compEvent.setParams({
				"Action": "redirectToDetail",
				"Case": component.get("v.selectedCase")
			});
    	} 
    	compEvent.fire();
	}
})