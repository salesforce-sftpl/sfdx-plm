/*
  * Authors		: Parth Lukhi
  * Date created : 17/08/2017 
  * Purpose      : Controller JS used for Creat New Case View
  * Dependencies : NewCaseGenHelper.js
  * -------------------------------------------------
  * Modifications:
                Date: 
                Purpose of modification:  
                Method/Code segment modified: 

*/
({	/*
	 * Authors: Parth Lukhi
	 * Purpose: Init  function  for New Case Gen component
	 * Dependencies:  NewCaseGenHelper.js
	 * 
	 * Start : Init  function  for New Case Gen component*/
	doInit: function(component, event, helper) {
        helper.createLayout(component, event, helper);
	},
    /* Authors: Atul Hinge || Purpose: On file upload status change  */
    onFileUploadStatusChange:function(component, event, helper){
        var status=component.get("v.fileUploadStatus");
        if(status=="complete"){
            var compEventRedirect = component.getEvent("caseAction");
            compEventRedirect.setParams({
                "Action": "redirectNewCase",
                "Case": component.get("v.case")
            });
            compEventRedirect.fire();
        }
    },
	/* End : Init  function  for New Case Gen component*/
    
	/*
	 * Authors: Parth Lukhi
	 * Purpose: It will be called when HTTP Req is being processed at server side
	 * Dependencies:  NA
	 * 
	 * It will be called when HTTP Req is being processed at server side
     */
	waiting: function(component, event, helper) {
		document.getElementById("Accspinner").style.display = "block";
		var noRecordfoundMsgDiv = component.find("noRecordfoundMsg");
		$A.util.addClass(noRecordfoundMsgDiv, 'toggleDiv');
	},
	/*
	 * Authors: Parth Lukhi
	 * Purpose: It will be called when HTTP Res is received at client side
	 * Dependencies:  NA
	 * 
	 * It will be called when HTTP Res is received at client side
     */
	doneWaiting: function(component, event, helper) {
		if (document.getElementById("Accspinner") != null || document.getElementById("Accspinner") != undefined) {
			document.getElementById("Accspinner").style.display = "none";
		}
		var noRecordfoundMsgDiv = component.find("noRecordfoundMsg");
		$A.util.removeClass(noRecordfoundMsgDiv, 'toggleDiv');
	},

	/* End : OnCLick function for Attachment selected for making it as a primary*/
	/*
	 * Authors: Parth Lukhi
	 * Purpose: OnClick function on New Case Button
	 * Dependencies:  NewCaseGenHelper.js
	 * 
     * Start : OnClick function on New Case Button */
	handleCaseCreate: function(component, event, helper) {
		var appEvent = $A.get("e.c:ValidateComponentEvent");
		appEvent.setParams({
			"components": component.get("v.processingfields"),
			"callback": function(response) { //alert(response.success);	
				if (response.success == true) {
					
           			helper.createCaseHelper(component, event);
				} else {
					//alert('Resposne Error');
				}
			}
		});
		appEvent.fire();
	},
	/* End : OnClick function on New Case Button  */
    /*
	 * Authors: Parth Lukhi
	 * Purpose: OnClick function on Cancel Button 
	 * Dependencies:  NewCaseGenHelper.js
	 * 
     *  Start : OnClick function on Cancel Button : PUX-175
     */
	handleCancel: function(component, event, helper) {
        
		var compEventRedirect = component.getEvent("caseAction");
				compEventRedirect.setParams({
					"Action": "redirectNewCase",
					"Case": component.get("v.ct")
				});
				compEventRedirect.fire();
	},
    /* End : OnClick function on Cancel Button : PUX-175*/
    
})