/**
  * Authors		: Parth Lukhi
  * Date created : 17/08/2017 
  * Purpose      : Helper File for newCaseGen.cmp 
  * Dependencies : PLMUtilitiesController.apxc
  * -------------------------------------------------
  *Modifications:
                Date: 
                Purpose of modification:  
                Method/Code segment modified: 
*/
({
	 /*
	 * Authors: Atul Hinge
	 * Purpose: Create Layout for fields
	 * Dependencies:  NewCaseGenHelper.js
	 * 
     *Start : called upon case create button click , it calls CaseAction Event :create */
    createLayout: function(component, event, files) {
         var createCmpEvent = $A.get("e.c:GetFieldSetComponent");
        console.log(createCmpEvent);
        createCmpEvent.setParams({
			"objectType": "CaseManager__c",
			"fsName": "NewCase",
			"paramName": "case",
			"component": component,
			"size": 2,
			"callback": function(response) {
				var breadCrumbEvent = component.getEvent("caseAction");
				breadCrumbEvent.setParams({
					"Action": "setBreadCrumb"
				});
				breadCrumbEvent.setParams({
					"TabName": "New Case"
				});
				breadCrumbEvent.fire();
                component.set("v.processingfields", response);
                      
			}
		});
		createCmpEvent.fire();
	},
	createCaseHelper: function(component, event) {
        debugger;
        var nameSpace =component.getConcreteComponent().getDef().getDescriptor().getNamespace();
        nameSpace=(nameSpace=='c')?'':nameSpace+'__';
		var compEvent = component.getEvent("caseAction");
        var caseObject=component.get("v.case");
                //   alert(nameSpace);
        caseObject[nameSpace+'Comments__c'] = component.get("v.casecomment");
       // caseObject.CoraPLM__Comments__c = component.get("v.casecomment");
        //Code start for PUX-469
        caseObject[nameSpace+"Input_Source__c"]='Manual';
        //Code start for PUX-469
        compEvent.setParams({
			"Action": "create",
			"Case": caseObject,
			"callback": function(response) {
				component.set("v.case", response.getReturnValue()[0]);
			    component.set("v.fileUploadStatus",'create');
			}
		});
		compEvent.fire();
	},
	/*	 End :  called upon case create button click*/
})