({
	
    doInit : function(component, event, helper) {
        helper.getLoginUser(component,event);
    },
    handleCaseActionEvent: function(component, event, helper) {
        var action=  event.getParam("Action");
        var caseObj=event.getParam("Case");
        var InteractionId=event.getParam("interactionId");
        var filesList=event.getParam("Files");
        var plmUtility = component.find('plmUtility');
        var callback=event.getParam("callback");
        if(action == 'create'){
            var callback=event.getParam("callback");
            plmUtility.create(event.getParam("Case"),callback);
        }else if(action=='redirectNewCase'){
            var homeEvent = $A.get("e.force:navigateToObjectHome");
            homeEvent.setParams({
                "scope": "CaseManager__c"
            });
            homeEvent.fire(); 
        }else{
            
        }
    },
})