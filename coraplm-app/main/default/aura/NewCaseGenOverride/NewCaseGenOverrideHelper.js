({
    getLoginUser : function(component, event){
        //Start Code for PUX-221
        var action = component.get("c.getUserInfo");    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.loginUserId",response.getReturnValue().UserId);
                window.SessionId=response.getReturnValue().UserSessionId;
                window.InstanceName = response.getReturnValue().InstanceName;
                component.set('v.render',true);              
                //End Code for PUX-221
            }else if (state === "INCOMPLETE") {
                //console.log("INCOMPLETE error");
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        //console.log("Error message: " + errors[0].message);
                    }
                } else {
                    //console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

})