/*
Authors: 
Date created: 
Purpose: This controller will used to handle client side logic for PLM.app mainly for PLM Assist.
Dependencies: PLM.app
-------------------------------------------------
Modifications:
                Date: 
                Purpose of modification: 
                Method/Code segment modified: 
                
*/
({

	/*
		Authors: Ashish Kumar
		Purpose: function to be executed after script load
		Dependencies: PLM.app     
    */
    scriptsLoaded : function(component, event, helper) {    
    	
         $(".drag-me").draggable({
            containment: "parent",
            scroll: false
	        });
	        //Variables
	        var overlay = $("#overlay"),
	        	latestInt = $("#latestInteraction"),
	        	starredCases = $("#starredCases"),
	            fab = $(".popup"),
	            cancel = $("#cancel"),
	            submit = $("#submit");
	        //fab click
	        latestInt.on('click', openFAB);
	        starredCases.on('click',openStarredCases);
	        overlay.on('click', closeFAB);
	        cancel.on('click', closeFAB);
	
	        function openFAB(event) {
	            if (event)
	                event.preventDefault();
	            component.set("v.showEmail", true);
	            component.set("v.showPinnedCases", false);
	            fab.addClass('active');
	            overlay.addClass('dark-overlay');
	        }
	        
	        function openStarredCases(event) {
	            if (event)
	                event.preventDefault();
	            component.set("v.showEmail", false);
	            component.set("v.showPinnedCases", true);
	            fab.addClass('active');
	            overlay.addClass('dark-overlay');
	        }
	
	        function closeFAB(event) {
	            if (event) {
	                event.preventDefault();
	                event.stopImmediatePropagation();
	            }
	            fab.removeClass('active');
	            overlay.removeClass('dark-overlay');
	            component.set("v.showEmail", false);
	            component.set("v.showPinnedCases", false);
	        }
        
    },
    
    myAction : function(component,event,helper){        
    	 document.title = "Genpact Cora"; //Added by Tej Pal Kumawat | 16 Nov 2017
    },
})