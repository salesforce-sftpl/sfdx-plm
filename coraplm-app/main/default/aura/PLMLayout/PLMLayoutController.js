/*
Authors: Niraj Prajapati,Parth Lukhi
Date created: 14/08/2017
Purpose: This controller will used active dashboard,help or case tracker portion. Also 
Dependencies: 
-------------------------------------------------
Modifications:
                Date: 27/11/2017
                Purpose of modification: Added handler for global search functionality by Ashish Kr. 
                Method/Code segment modified: handleSearchAction
-------------------------------------------------
 				Date: 18/12/2017
                Purpose of modification: PUX-455 | added search text functionality. 
                Method/Code segment modified: handleSearchAction
                
*/
({
    /*
		Authors: Niraj Prajapati
		Purpose: setSideBar function will be called when value of showLayout is changed
		Dependencies: CaseTracker.cmp,Dashboard.cmp
    
    */
    setSideBar : function(component, event, helper) {
        var myDiv1 = component.find("lDashboard");
        var myDiv2 = component.find("lCaseTracker");
        //var myDiv3 = component.find("lHelp");
        var id= component.get("v.showLayout");
        
        $A.util.removeClass(myDiv1, 'activeIcon');
        $A.util.removeClass(myDiv2, 'activeIcon');
        //$A.util.removeClass(myDiv3, 'activeIcon');
        var myDiv = component.find("l"+id);
        $A.util.addClass(myDiv, 'activeIcon');
    },
    /*
		Authors: Niraj Prajapati
		Purpose: setShowMe function will be used to set showLayout value.
		Dependencies: CaseTracker.cmp,Dashboard.cmp
    
    */
    setShowMe : function(cmp, event, helper)
    {
        /* Code start for PUX-498 */
        window.overDueIn='';
        
        /* Code start for PUX-498 */
        var id = event.currentTarget.id;
        cmp.set("v.showLayout", id);
        
        var myParentDiv = cmp.find("parentSideDiv");
        var myDiv = cmp.find("l"+id);
        var myDiv1 = cmp.find("lDashboard");
        var myDiv2 = cmp.find("lCaseTracker");
        //var myDiv3 = cmp.find("lHelp");
        $A.util.removeClass(myDiv1, 'activeIcon');
        $A.util.removeClass(myDiv2, 'activeIcon');
        //$A.util.removeClass(myDiv3, 'activeIcon');
        var myDiv = cmp.find("l"+id);
        $A.util.addClass(myDiv, 'activeIcon');
    
    },
    /*
		Authors: Ashish Kr.
		Purpose: handleSearchAction function will be used to handle global search functionality event.
		Dependencies: Header.cmp          
    */
    handleSearchAction : function(component,event,helper){
    	var recordIds = event.getParam("RecordIds");
    	var searchText = event.getParam("SearchText");
        
    	 if(!$A.util.isUndefinedOrNull(recordIds)) {
                if(component.get('v.showLayout') == 'Dashboard'){
                	component.set('v.showLayout','CaseTracker');
                }
                component.set("v.searchResultIds",recordIds);
                component.set("v.searchText", searchText);
    	}
    }
})