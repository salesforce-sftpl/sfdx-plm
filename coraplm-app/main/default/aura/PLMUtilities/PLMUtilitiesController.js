({
    /*Start : Harshil Added on 02/02/2018 For PUX-299 */
    
    scriptsLoaded : function(component, event, helper) {
        

        var action = component.get("c.getUserSession");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                       $.cometd.init({
                             url: window.location.protocol+'//'+window.location.hostname+'/cometd/41.0/',
                             requestHeaders: { Authorization: 'OAuth '+response.getReturnValue().sessionId},
                             appendMessageTypeToURL : false
                       });
                        
						$.cometd.subscribe('/event/Case_Manager__e', function(message) {                            
                    //        if(component.get("v.userId") == message.data.payload.ReceiverId__c)
					        if(response.getReturnValue().userId == message.data.payload.ReceiverId__c)
							{    
                                toastr.success(JSON.stringify(message.data.payload.Case_Id__c), 'Case Detail', {timeOut: 8000})
                                var msg='Created Case Id  :  ' + JSON.stringify(message.data.payload.Case_Id__c);                        
                                console.log(msg);
                            }    
                        });
            }
        });
        $A.enqueueAction(action);
        
    },
    /*End : Harshil Added on 02/02/2018*/
	//to Display Email Screen
    toggleScreen : function(component, event, helper) {
        var params = event.getParam('arguments');
        if (params) {
            var Case = params.Case;
            component.set("v.Case",Case);
            component.set("v.InteractionId",params.interactionId);
            var title='';
            if(params.Action == 'reply'){
                title = 'Reply';
            }else if(params.Action == 'replyall'){
                title = 'Reply All';
            }else if(params.Action == 'forward'){
                title = 'Forward';
            }
            component.set("v.Action",title);
            component.set("v.afterSendCallback",params.callback);
            
        }
        component.set("v.showReplyScreen",!component.get("v.showReplyScreen"));
        
    },
    //to update Case object record
    updateCase : function(component, event, helper) {
	   
        var params = event.getParam('arguments');
        if (params) {
            var Case = params.Case;
             var callback1 = params.callback;
             
        }
        var action = component.get("c.updateCaseApex");
        action.setParams({ ct : Case });
        action.setCallback(this, function(response) {
           callback1(response);
            var state = response.getState();
            if (state === "SUCCESS") {
            }else if (state === "INCOMPLETE") {
				//alert('INCOMPLETE');                
            }else if (state === "ERROR") {
                //alert('ERROR'); 
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
       });

       $A.enqueueAction(action);
	},
    //to update Case Owner field
    updateCaseOwner : function(component, event, helper) {
        
        var params = event.getParam('arguments');
        if (params) {
            var Case = params.Case;
            var callback1 = params.callback;
            
        }
               var action = component.get("c.updateOwner");
        action.setParams({ ct : Case });
        
        action.setCallback(this, function(response) {
            callback1(response);
            var state = response.getState();
            //alert(response.getState());
            
            if (state === "SUCCESS") {
            }else if (state === "INCOMPLETE") {
                alert('INCOMPLETE');                
            }else if (state === "ERROR") {
                //alert('ERROR'); 
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    //to create New Case object record
    createCase : function(component, event, helper) {
        var params = event.getParam('arguments');
        if (params) {
            var Case = params.Case;
			 var callback1 = params.callback;
        }
        var action = component.get("c.createCaseApex");
        action.setParams({ ct : Case});
        action.setCallback(this, function(response) {
            var state = response.getState();
            var message='';
         //   var statusMessage='';
            if (state === "SUCCESS") {
                 message = "Case-" + response.getReturnValue()[0].Name+ " Created Successfully";
               //  statusMessage = response.getReturnValue()[0].status__c;
				helper.showToast(component,'Create Case',message);
			//	pushNFmessage = response.getReturnValue()[0].Name;
                callback1(response);
				      //helper.showToast(component,'Create Case','Case Created Successfully');
					  //callback1(response);
            }else if (state === "INCOMPLETE") {
				//alert('INCOMPLETE');                
            }else if (state === "ERROR") {
                //alert('ERROR'); 
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
					helper.showToast(component,'Error in CC','Case Creation Failed');
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
       });

       $A.enqueueAction(action);
	},
     //to display success message
    showToast: function(component, event, helper) {
        console.log('');
        helper.showToast(component,event.getParam('title'),event.getParam('message'),event.getParam('type'));
        
    },
    closeToast: function(component, event, helper) {
		component.set("v.showMessage",!component.get("v.showMessage"));
     },
    getFieldSetMember : function(component, event, helper) {
      	helper.getFieldSetMember(component, event);
     },
    getFieldSetComponentHandler: function(component, event, helper) {
       component.getFieldSetMember(event.getParam('objectType'),event.getParam('fsName'),event.getParam('paramName'),event.getParam('size'),event.getParam('component'),event.getParam('callback'));
     },
    //to validate inputs before going to save
     validateInputs: function(component, event, helper) {
         helper.validateUtility(event.getParam('components'),event.getParam('callback'),null);
      },

})