({
    showToast123: function(component,title,message) {
        component.set("v.title",title);
        component.set("v.message",message);
        //component.set("v.showMessage",!component.get("v.showMessage"));
    },
    validateUtility: function(components,callBack,response){
        var response = {
            "success": true,
            "errors": []
        };
        for(var i=0;i<components.length;i++){
            components[i].validate();
            var err=components[i].get('v.errors');
            if(err != null && err !=''){
                response.success=false;
                response.errors.push(components[i].get('v.errors'));
            }
        }
        callBack(response);
    },
    
    getFieldSetMember: function(component,event,message) {
        var params = event.getParam('arguments');
        var objectType = params.objectType;
        var fieldSetName=params.fieldSetName;
        var helper =this;
        var action = component.get("c.getFields");
        //Start Code for PUX-221
        //alert(window.SessionId);
        action.setParams({ objectType : objectType,fsName:fieldSetName,ct : params.component.get("v."+params.paramName),strSessionId : window.SessionId});
        //End Code for PUX-221
		
        action.setCallback(helper, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.createComponent(response.getReturnValue(),params);
            }else if (state === "INCOMPLETE") {
                alert('e');
            }else if (state === "ERROR") {
                alert('e');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    createComponent: function(response,params) {
		debugger;
        var refCmp=params.component;
        var callback1 = params.callback;
        var paramName=params.paramName;
        var fields=response;
        var cmpts=[];
		
         if(fields.fieldMembers.length==0){
            //alert(callback1);
            return callback1(response);
        }
		

        for (var i=0; i<fields.fieldMembers.length;i++){
			var display='';
			
			if(fields.fieldJSON!=null && fields.fieldJSON.defaultHiddenFields!=null){
				$.each(fields.fieldJSON.defaultHiddenFields, function( ind, defValue ) {
					if(fields.fieldMembers[i].fieldPath==defValue){
						display='none';
					}
				});
			}
			console.log(fields.fieldMembers[i].fieldPath);
            fields.fieldMembers[i]['controlFieldValue']=refCmp.get("v."+paramName+"."+ fields.fieldMembers[i].controlField);
            var cmp=["c:input",{
                "aura:id":fields.fieldMembers[i].fieldPath,
                "value" : refCmp.getReference("v."+paramName+"."+ fields.fieldMembers[i].fieldPath),
				"APIName" : fields.fieldMembers[i].fieldPath,
                "options" : fields.fieldMembers[i].options,
                "type" : fields.fieldMembers[i].type,
                "label" : fields.fieldMembers[i].label,
                "size" : params.size,
                "required":fields.fieldMembers[i].required,
                "fieldSetMembers" : fields.fieldMembers[i],
				"layoutJSON": fields.fieldJSON,
				"display":display,
				"sObject":refCmp.getReference("v."+paramName),
				"fields":fields.fieldMembers,
            }];
            cmpts.push(cmp);
            
        }
        
        $A.createComponents(cmpts,  function(components, status){
            
            if (status == 'SUCCESS'){
                callback1(components);
            }else{
                alert('error');
            }
        }
                            
                           );
    },
    showToast: function(component,title,message,type){
        //   console.log(type);
        var  type = type;
        if(type == undefined){
            type = 'success';
        }
        var  css = 'toast-top-center';
        var  msg = message;
        toastr.options.positionClass = 'toast-top-full-width';
        toastr.options.extendedTimeOut = 0; //1000;
        toastr.options.timeOut = 3000;
        toastr.options.fadeOut = 250;
        toastr.options.fadeIn = 250;
        toastr.options.positionClass = css;
        toastr[type](msg);
    }
    
    
    
    
})