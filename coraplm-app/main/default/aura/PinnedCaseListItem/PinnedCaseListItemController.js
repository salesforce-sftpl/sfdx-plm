({
	//It will be called at very first time when component render
	//To format Email created Date and //To Trim email Text body to display one line Email Response on List View
	doInit: function(component, event, helper) {
		var date = new Date(component.get('v.ct.Emails[0].CreatedDate'));
		component.set('v.ct.Emails[0].CreatedDate', date.toLocaleString());
		//PUX-331
		if(component.get('v.ct.Target_TAT_Time__c') != undefined){
			var expectedDate = new Date(component.get('v.ct.Target_TAT_Time__c'));
			component.set('v.expectedDate', expectedDate.getDate());
			component.set('v.expectedMonth', expectedDate.toLocaleString('en-us', { month: "short" }));
		}		
		//PUX-331
        helper.setOneLineEmailTxtBody(component, event);
		var emailMsgObj = component.get('v.ct.Emails');
		if (emailMsgObj == undefined) {
			component.set('v.oneLineEmialTxtBody', 'No interaction yet!');
		} else if (emailMsgObj[0].TextBody != undefined) {
			component.set('v.oneLineEmialTxtBody', emailMsgObj[0].TextBody.substring(0, 100));
		}
	},
})