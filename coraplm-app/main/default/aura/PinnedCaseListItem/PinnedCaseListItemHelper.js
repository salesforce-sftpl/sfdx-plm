({
    setOneLineEmailTxtBody : function(component, event) {
        var emailMsgObj = component.get('v.ct.Emails');
        if (emailMsgObj == undefined) {
            component.set('v.oneLineEmialTxtBody', 'No interaction yet!');
        } else if (emailMsgObj[0].TextBody != undefined) {
            component.set('v.oneLineEmialTxtBody', emailMsgObj[0].TextBody.substring(0, 100));
        }
    },
})