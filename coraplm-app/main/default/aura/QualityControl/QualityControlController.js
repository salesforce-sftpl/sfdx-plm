/*
Authors: Niraj Prajapati
Date created: 01/11/2017
Purpose: This controller will get the fields as per field set configured on Quality Check Object.
Dependencies: 
-------------------------------------------------
Modifications:
                Date: 
                Purpose of modification: 
                Method/Code segment modified: 
                
*/ 

({
    /*
		Authors: Niraj Prajapati
		Purpose: It will be called at very first time when component render,To fetch field set wise fields
		Dependencies: QualityControl.cmp
                
    */
    doInit: function(component, event, helper) {
      //alert(component.getDef().getDescriptor().getNamespace());
      var np=component.toString().split('":"')[1].split('"}')[0]; 
      var  nameSpace=(np=='c')?'':np+'__';
        var eventNameSpace=(np=='c')?'c':np;
            
        var createCmpEvent = $A.get("e."+eventNameSpace+":GetFieldSetComponent");
        createCmpEvent.setParams({
            "objectType": nameSpace+"QualityCheck__c",
            //Code start for PUX-512
            "fsName": component.get("v.qcInfo.qcFormFieldsetName"),
            //Code End for PUX-512
            "paramName": "qc",
            "component": component,
            "size": 3,
            "callback": function(response) {
                helper.toggleSpinner(component, event);
                component.set("v.qcfields", response);
                /******************************/
                component.find("qualitycheckRecordCreator").getNewRecord(
                     nameSpace+"QualityCheck__c", // sObject type (objectApiName)
                    null,      // recordTypeId
                    false,     // skip cache?
                    $A.getCallback(function() {
                        var rec = component.get("v.newQualityCheck");
                        var error = component.get("v.newQualityCheckError");
                        if(error || (rec === null)) {
                            console.log("Error initializing record template: " + error);
                            return;
                        }
                        console.log("Record template initialized: " + rec.sobjectType);
                    })
                );
                /*****************************/
                 
            }
        });
        createCmpEvent.fire();
        helper.getQC(component, event);
    },
    /*
		Authors: Niraj Prajapati
		Purpose: It will be called to get all Quliaty check records.
		Dependencies: QualityControl.cmp
                
    */
    getQC: function(component, event, helper){
        helper.getQC(component, event);
    },
    /*
		Authors: Niraj Prajapati
		Purpose: It will be called to insert Quliaty check records.
		Dependencies: QualityControl.cmp
                
    */
    insertQC : function(component, event, helper){
        var np=component.toString().split('":"')[1].split('"}')[0]; 
      var  nameSpace=(np=='c')?'':np+'__';
        var eventNameSpace=(np=='c')?'c':np;
        var appEvent = $A.get("e."+eventNameSpace+":ValidateComponentEvent");
        var test = component.get("v.qcfields");
        component.set("v.isDisabled",true);
        appEvent.setParams({
            "components": test,
            "callback": function(response) {
                if (response.success == true) {
                    helper.insertQC(component, event);
                    
                }
            }
        });
        appEvent.fire();   
    }    
})