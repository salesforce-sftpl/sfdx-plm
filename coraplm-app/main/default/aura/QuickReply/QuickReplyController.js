/*
Authors: Tej Pal Kumawat
Date created: 16-Oct-2017
Purpose: PUX-298: QuickReply.cmp child component for Quick reply on interaction
Dependencies: QuickReply.cmp
-------------------------------------------------
Modifications:  1
                Date: 13-Nov-2017
                Purpose of modification: Standardize Code comments
                Method/Code segment modified: None
                
*/ 
({	
    /*
	Authors: Tej Pal Kumawat
	Purpose: PUX-298: Client side method use for open popup window & get email templates
	Dependencies: None
    */
    openQuickReply: function(component, event, helper) {
        var mainDiv = component.find('main-div');
        $A.util.toggleClass(mainDiv, 'slds-is-open');
        var action = component.get("c.initializeComponent");
        var parameters = {
            "interactionId" : component.get("v.ct.Emails[0].Id"),
            "emailTemplateFolder" : component.get("v.emailTemplateFolder")
        };
        action.setParams({
            "requestParm": JSON.stringify(parameters)
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var responceVar =response.getReturnValue();
                component.set("v.templates",response.getReturnValue().templates);
                component.set("v.emailmessage",response.getReturnValue().EmailMessage);
				/* Purpose: To update PredictedValue status of Indexing Field		start	*/	
                if(response.getReturnValue().predictedTemplate!=undefined){
					console.log(response.getReturnValue().predictedTemplate);				
					//component.find("selectItem").set("v.value", response.getReturnValue().templates);	
					component.set("v.selectedTemplate" ,response.getReturnValue().predictedTemplate);
				}
				else{
					component.set("v.selectedTemplate",response.getReturnValue().templates[0].value);
				}	
				/* Purpose: To update PredictedValue status of Indexing Field		end	*/	
            }else{
                alert(response.getReturnValue().error);
            }
        });
        $A.enqueueAction(action);
    },
    
    /*
	Authors: Tej Pal Kumawat
	Purpose: PUX-298: Client side method use for quick reply email
	Dependencies: None
    */
    QuickReply :function(component, event, helper){
        var templatesid=component.get("v.selectedTemplate");
        var interactionId = component.get("v.ct.Emails[0].Id");
        var TrackerId = component.get("v.ct.Id");
        console.log(TrackerId);
        var action = component.get("c.quickReplyMethod");
        action.setParams({ "action" : event.getSource().getLocalId(), 
                          "emailTemplateId" : templatesid,
                          "interactionId" : interactionId,
                          "trackerId":TrackerId
                         });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === 'SUCCESS') {
                var np = component.toString().split('":"')[1].split('"}')[0]; 
                
                var responceVar =response.getReturnValue();  
                if(!responceVar.hasOwnProperty('error')){
                    component.set("v.ct",responceVar.case[0]);
                    var a = component.get('c.closeQuickReply');                
                    $A.enqueueAction(a);                
                    var appEvent = $A.get("e.c:ShowToast");
                    
                    appEvent.setParams({
                        "title" : (np == 'c') ? $A.get("$Label.c.Success_Title") : $A.get("$Label.CoraPLM.Success_Title"), 
                        "message": (np == 'c') ? $A.get("$Label.c.Email_Sent_Message") : $A.get("$Label.CoraPLM.Email_Sent_Message")
                    });
                    appEvent.fire();
                }else{
                    var a = component.get('c.closeQuickReply');                
                    $A.enqueueAction(a);                
                    var appEvent = $A.get("e.c:ShowToast");
                    
                    appEvent.setParams({
                        "title" : 'Error', 
                        "message": response.getReturnValue().error,
                        "type" : "error"
                    });
                    appEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
    },       
    
    /*
	Authors: Tej Pal Kumawat
	Purpose: PUX-298: Client side method use for close popup window
	Dependencies: None
    */
    closeQuickReply: function(component, event, helper) {
        component.set("v.dropdownOver",false);
        var mainDiv = component.find('main-div');
        $A.util.removeClass(mainDiv, 'slds-is-open');
    }
})