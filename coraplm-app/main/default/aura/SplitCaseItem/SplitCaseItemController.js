({
    doInit: function(component, event, helper) {
        var ct = {};
        ct = component.get('v.ct');
        console.log('indexValue');
        console.log(component.get('v.indexValue'));
        ct['auraId'] = component.get('v.indexValue');
        component.set('v.ct', ct);
        console.log('ct : ' +ct);
        //splitted field set
        var getSplittedFs = $A.get("e.c:GetFieldSetComponent");
        getSplittedFs.setParams({
            "objectType": "CaseManager__c",
            "fsName": "SplittedCase",
            "paramName": "ct",
            "component": component,
            "size": 2,
            "callback": function(response) {
                component.set('v.splittedFields', response);
            },
        }); console.log(component.get('v.splittedFields'));
        getSplittedFs.fire();
        /*    var attachments = [];
            attachments = component.get("v.attachments");
            console.log(attachments);
            console.log(attachments.length);
            var attachmentLabel = component.find("checkboxMenuLabel");
            if(attachments.length == 0){
            	var attachmentLabel = component.find("checkboxMenuLabel");
            	attachmentLabel.set("v.label",'No Attachment Available');
            } */

    },
    ToggleSubCase: function(component, event, helper) {
        var existingText = component.get("v.collapseSubCaseText");
        var subCaseContainer = component.find("subCaseContainer");
        if (existingText == "View more") {
            component.set("v.collapseSubCaseText", "View less");
            $A.util.toggleClass(subCaseContainer, 'hide');
        } else {
            component.set("v.collapseSubCaseText", "View more");
            $A.util.toggleClass(subCaseContainer, 'hide');
        }

    },
   
    waiting: function(component, event, helper) {
    	component.set('v.showSpinner',true);
    },
    doneWaiting: function(component, event, helper) {
    	component.set('v.showSpinner',false);
    },
    
    selectAttachment: function(component, event, helper) {
    	var allAttachments = component.get('v.attachments');
        var menuItems = component.find("checkboxList");
        var totalAttachmentCount = menuItems.length;
        var selectedAttachments = 0;
        var values = [];
        var attachmentIds = [];
        for (var i = 0; i <menuItems.length; i++) {
            var c = menuItems[i];
            if (c.get("v.selected") === true) {
                selectedAttachments = selectedAttachments + 1;
                values.push(c.get("v.label"));
                for(var j=0; j<allAttachments.length; j++){
                    if(allAttachments[j].ContentDocument.Title == c.get('v.label')){
                    	attachmentIds.push(allAttachments[j].ContentDocument.Id);    
                    }
                }
            }
        }
        var resultCmp = component.find("checkboxMenuLabel");
        if (selectedAttachments == totalAttachmentCount) {
            resultCmp.set("v.label", 'All Attachments Selected');
        } else if (selectedAttachments == 0) {
            resultCmp.set("v.label", 'Select Attachments');
        } else if (selectedAttachments == 1) {
            resultCmp.set("v.label", values[0]);
        } else if (selectedAttachments >= 2) {
            resultCmp.set("v.label", selectedAttachments + ' Attachments Selected');
        }
        
        var splitCase = component.get('v.ct');
        splitCase.attachment = attachmentIds;
        component.set('v.ct', splitCase);
    },
    handleAttachments : function(component, event, helper) {
        var attachmentList = component.get('v.attachments');
      //  console.log(attachmentList.length);
    	var attachmentLabel = component.find("checkboxMenuLabel");
        if (attachmentList.length === 0) {
            attachmentLabel.set("v.label", 'No Attachment Available');
        } else {
            attachmentLabel.set("v.label", 'All Attachments Selected');
        }
        console.log('handling rerender');
    }
})