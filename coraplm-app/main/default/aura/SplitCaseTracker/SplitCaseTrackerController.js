({
    doInit : function(component, event, helper) {
        //calling helper method to set default split to 2.
        console.log('doInit');
        helper.noOfSplitsHelper(component,event);
        helper.getAttachmentHelper(component,event,helper);
        //helper method call ends
        
        var caseList = component.get("v.CaseList");
        var ct=component.get('v.ct');
        ct['auraId']= 'uniqueID'; 
        component.set('v.ct',ct); 
        var createCmpEvent = $A.get("e.c:GetFieldSetComponent");
        createCmpEvent.setParams({
            "objectType":"CaseManager__c",
            "fsName":"SplitCase",
            "paramName":"ct",
            "component": component,
            "size":2,
            "callback" : function( response){
                component.set('v.processingfields',response);
            } ,
        });
        createCmpEvent.fire();
    },
    
    waiting: function(component, event, helper) {
    	component.set('v.showSpinner',true);
    },
    doneWaiting: function(component, event, helper) {
    	component.set('v.showSpinner',false);
    },
    
    noOfSplits : function(component, event, helper){  
        helper.noOfSplitsHelper(component,event);
    },
    
    ToggleCollapse : function(component, event, helper) { 
        var existingText = component.get("v.collapseText"); 
        var container = component.find("containerCollapsable") ;
        
        if(existingText === "View more"){
            component.set("v.collapseText","View less");
            $A.util.toggleClass(container, 'hide');  
        }else{
            component.set("v.collapseText","View more");
            $A.util.toggleClass(container, 'hide');  
        }  
    },
   //Function called on Cancel button
    closeSplitWindow : function(component, event, helper){
        //console.log('close SPlit Window fired');
        var compEventRedirect = component.getEvent("caseAction");
				compEventRedirect.setParams({
					"Action": "redirect",
					"Case": component.get("v.ct")
				});
				compEventRedirect.fire();
    },
   
    // Function called on Split
    splitAction : function (component,event,helper){
        //	component.set('v.openPopup',false);
        var splitComments = component.get('v.splitComments');
        
        if(splitComments == '' || splitComments == null){
            alert("Please provide Comments before Splitting the Case");
        }
        else {  
            var tempCases = component.get('v.CaseList');
            var caseWithAttachmentObjList = [];
            for(var i=0; i<tempCases.length; i++){
                var caseWithAttachmentObj = {};
                var splitCase = tempCases[i];
                caseWithAttachmentObj.attachment = splitCase.attachment;
                delete splitCase.attachment;
                caseWithAttachmentObj.case = splitCase;
                caseWithAttachmentObjList.push(caseWithAttachmentObj);
            }
            console.log(caseWithAttachmentObjList);
            var cases = JSON.stringify(caseWithAttachmentObjList);
            var mainCaseTracker = component.get('v.ct');
            
            var actionSplit = component.get("c.performSplitAction");
            actionSplit.setParams({
                splitList : cases,
                mainCase : mainCaseTracker,
                objectType : 'CaseManager__c' ,
                fsName : 'SplittedCase',
                Comments : splitComments
            });
            
            actionSplit.setCallback(this,function(response){
                var state = response.getState();
                console.log('state:' +state);
                if(state==="SUCCESS"){
                    var compEventRedirect = component.getEvent("caseAction");
                    compEventRedirect.setParams({
                        "Action": "redirect",
                        "Case": component.get("v.ct")
                    });
                    compEventRedirect.fire(); 
                }else if (state === "ERROR"){
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +errors[0].message);
                        }
                    }else{
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(actionSplit);
        }  
    },
    //handle Case Action on click of Case Number
    handleCaseAction: function(component, event, helper) {
    //	var ct = component.get('v.ct');
    //	ct.Id = component.get('v.simpleRecord.Id');
    //	component.set('v.ct',ct);
    console.log(component.get('v.ct'));
		var compEvent = component.getEvent("caseAction");
		compEvent.setParams({
				"Action": "redirectToDetail",
				"Case": component.get("v.ct")
			});
	
		compEvent.fire();
	},
})