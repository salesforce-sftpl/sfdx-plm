({
	noOfSplitsHelper : function(component,event){    
    var parentCase = component.get('v.ct');
    var picklistVal = component.get('v.noOfSplits').toString();
    var returnSplitCases = component.get("c.returnSplitCases");
        returnSplitCases.setParams({
                parentCase : parentCase,
                noOfSplit : picklistVal
        });
     	returnSplitCases.setCallback(this,function(response){
            var state = response.getState();
            if(state==="SUCCESS"){
                var returnValue = response.getReturnValue();
                component.set("v.CaseList",returnValue);
                console.log(component.get("v.CaseList"));
            }else if (state === "ERROR"){
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +errors[0].message);
                    }
                }else{
                    console.log("Unknown error");
                }
            }
        });
     $A.enqueueAction(returnSplitCases);
},

    getAttachmentHelper : function(component,event,helper){ 
	var ct = component.get('v.ct.Id').toString();
        
	var returnAttachments = component.get("c.getAttachments");
        returnAttachments.setParams({
                caseId : ct
        });
        returnAttachments.setCallback(this,function(response){
            var state = response.getState();
            var attachmentList = [];
            if(state==="SUCCESS"){
               component.set('v.mainCaseAttachments',helper.addIcon(response.getReturnValue().attachments));
            }else if (state === "ERROR"){
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +errors[0].message);
                    }
                }else{
                    console.log("Unknown error");
                }
            }
        });
     $A.enqueueAction(returnAttachments);
    },
        // To add Attachment specific icon 
    addIcon :function(attachments) {
        for(var i=0;i<attachments.length;i++){
            var extension =attachments[i].ContentDocument.FileExtension;
            
            attachments[i]['isPreviewAvailable']=false;
            var size=attachments[i].ContentDocument.ContentSize;
            if((size/1000).toFixed(2)<1){
                size=size+' BYTES';
            }else if(((size/1000)/1024).toFixed(2)<1){
                size=(size/1000).toFixed(2)+' KB';
            }else{
                size=((size/1000)/1024).toFixed(2)+' MB';
            }
            	
            attachments[i].Size=size;
            if(extension=='pdf'){
                attachments[i]['icon']='doctype:pdf';
                attachments[i]['isPreviewAvailable']=true;
            }else if(extension=='xlsx'){
                attachments[i]['icon']='doctype:excel';
                
            }else if(extension=='html'){
                attachments[i]['icon']='doctype:html';
                attachments[i]['isPreviewAvailable']=true;
            }else if(extension=='docx' || extension=='doc'){
                attachments[i]['icon']='doctype:word';
                
            }else if(extension=='txt'){
                attachments[i]['icon']='doctype:txt';
                
            }else if(extension=='csv'){
                attachments[i]['icon']='doctype:csv';
                
            }else if(extension=='jpg' || extension=='png' || extension=='gif' || extension=='jpg'){
                attachments[i]['icon']='doctype:image';
                attachments[i]['isPreviewAvailable']=true;
            }else{
                alert(extension);
                attachments[i]['icon']='doctype:attachment';
            }
            
        }
        return attachments;
    },
    
})