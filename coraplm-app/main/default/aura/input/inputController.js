/*
Authors: Niraj Prajapati
Date created: 14/08/2017
Purpose: This controller will used to create field as per field type.
Dependencies: CaseDetailSection.cmp
-------------------------------------------------
Modifications:
                Date: 15/09/2017
                Purpose of modification: [PUX-124,129] Fields Display from fieldset and Validation on fields
                Method/Code segment modified: populateRelatedField function and UpdateCase function
                
*/ 
({
    /*
		Authors: Niraj Prajapati
		Purpose: doInit function will be called at very first time when component render
		Dependencies: CaseDetailSection.cmp
                
    */
    doInit: function(component, event, helper) {
		debugger;
        component.set("v.filterValues", []);
        if (component.get("v.fieldSetMembers") !== null) {
            if (component.get("v.fieldSetMembers").controlField !== undefined && component.get("v.fieldSetMembers").controlField !==
                '-') {
                component.set("v.options", component.get("v.fieldSetMembers").dependentValues[component.get(
                    "v.fieldSetMembers")['controlFieldValue']]);
            }
        }
        if (component.get("v.type") == "REFERENCE") {
            if (component.get("v.value") != null) {
                helper.getLookUpName(component, event, helper);
            }
            helper.getLookUpValues(component, event, helper);
        }

		/*var layoutJSON=component.get("v.layoutJSON");
		console.log(layoutJSON);
		if(layoutJSON!=null){
			if(layoutJSON.defaultHiddenFields!=null){
				$.each(layoutJSON.defaultHiddenFields, function( ind, defValue ) {
					$('#'+defValue).hide();
				});
			}
		}*/
    },
    /*
		Authors: Atul Hinge
		Purpose: onChangeInput function will be used to auto search email address
		Dependencies: CaseDetailSection.cmp
                
    */
    onChangeInput: function(component, event, helper) {
        var values = component.get("v.value");
        var keyCode = event.getParams().keyCode;
        var toaddress = component.get("v.to");
        toaddress = toaddress.replace(/\;$/, '').trim();
        $A.util.removeClass( component.find("listbox"), "slds-hide");
        if (keyCode == 8 && toaddress == "") {
            //backspace
            if(values.length > 1){
            	values.pop();
            }
           component.set("v.value", values);
            /*setTimeout(function(){
                var scrollelem = document.getElementById('selected-recipients-to'); 
                scrollelem.scrollTop = scrollelem.scrollHeight;
            },1000);*/
           
        } else if ( toaddress != "" && (keyCode == 32 || keyCode == 13 || keyCode == 186 || keyCode == 188)){
            //space
            var obj = {
                "Name": toaddress,
                "Id": toaddress,
                "Email": toaddress
            };
            values.push(obj);
            component.set("v.value", values);
            setTimeout(function(){
                var scrollelem = document.getElementById('selected-recipients-to'); 
                scrollelem.scrollTop = scrollelem.scrollHeight;
            },1000);
            component.set("v.to", "");
            $A.util.addClass( component.find("listbox"), "slds-hide");
                          
        } else if (keyCode == 27) {
            //ESC
            $A.util.addClass( component.find("listbox"), "slds-hide");           	
        } else if (event.getParams().keyCode == 40) {
            //downKey
        } else if (event.getParams().keyCode == 38) {
            //upkey38
        } else if (event.getParams().keyCode == 13) {
            //enter
        } else {
            
            var options = component.get("v.options");
            var filterValues = [];
            for (var i = 0; i < options.length; i++) {
                var contact = options[i];
                if (contact.Email != undefined && (contact.Name.toLocaleLowerCase().indexOf(toaddress.toLocaleLowerCase()) >=
                                                   0 || contact.Email.toLocaleLowerCase().indexOf(toaddress.toLocaleLowerCase()) >= 0)) {
                    filterValues.push(options[i]);
                }
            }
            component.set("v.filterValues", filterValues);
            setTimeout(function(){
                var scrollelem = document.getElementById('selected-recipients-to'); 
                scrollelem.scrollTop = scrollelem.scrollHeight;
            },1000);
            if (filterValues.length == 0) {
                //var toggleText = component.find("listbox");
                //$A.util.removeClass(toggleText, "show");
                //$A.util.addClass(toggleText, "hide");
                $A.util.addClass( component.find("listbox"), "slds-hide");
            }
            
            
        }
        
    },
    /*
		Authors: Niraj Prajapati
		Purpose: toogleModal function will be used to open lookup popup
		Dependencies: CaseDetailSection.cmp
                
    */
    toogleModal: function(component, event, helper) {
        var options = component.get("v.listValues");
        var val = component.get("v.value1");
        var filterValues = [];
        if (val != 'null' && val != null && val != "") {
            for (var i = 0; i < options.length; i++) {
                var contact = options[i];
                if (contact.Name.toLowerCase().indexOf(val.toLowerCase()) >= 0) {
                    filterValues.push(options[i]);
                }
            }
            component.set("v.filterValues", filterValues);
        } else {
            component.set("v.filterValues", options);
        }
        $A.util.toggleClass(component.find('lookupValues'), 'slds-hide');
    },
    /*
		Authors: Niraj Prajapati
		Purpose: lookUpSearchValues function will be used to search lookup values
		Dependencies: CaseDetailSection.cmp
                
    */
    lookUpSearchValues: function(component, event, helper) {
        var options = component.get("v.listValues");
        var val = component.get("v.value1");
        var filterValues = [];
        for (var i = 0; i < options.length; i++) {
            var contact = options[i];
            if (contact.Name.toLowerCase().indexOf(val.toLowerCase()) >= 0) {
                filterValues.push(options[i]);
            }
        }
        component.set("v.filterValues", filterValues);
    },
    /*
		Authors: Niraj Prajapati
		Purpose: getLookUpValues function will be used to search lookup values in dropdown
		Dependencies: CaseDetailSection.cmp
    */
    getLookUpValues: function(component, event, helper) {
        //alert('Hi');
        var toggleText = component.find("listbox");
        $A.util.removeClass(toggleText, "slds-hide");
        var options = component.get("v.listValues");
        var val = component.get("v.value1");
        var filterValues = [];
        for (var i = 0; i < options.length; i++) {
            var contact = options[i];
            if (contact.Name.toLowerCase().indexOf(val.toLowerCase()) >= 0) {
                filterValues.push(options[i]);
            }
        }
        //alert(filterValues);
        component.set("v.filterValues", filterValues);
        if (filterValues.length == 0 || event.getParams().keyCode==27) {
            var toggleText = component.find("listbox");
            //$A.util.toggleClass(toggleText, "show");
            //$A.util.removeClass(toggleText, "show");
            $A.util.addClass(toggleText, 'slds-hide');
        }
    },
    /*
		Authors: Atul Hinge
		Purpose: selectOption function will be used to select Email from DropDown
		Dependencies: CaseDetailSection.cmp
                
    */ 
    selectOption: function(component, event, helper) {
        var value = component.get("v.value");
        var filterValues = component.get("v.filterValues");
        var selectedItem = event.currentTarget;
        var caseId = selectedItem.dataset.record;
        for (var i = 0; i < filterValues.length; i++) {
            if (caseId == filterValues[i].Id) {
                value.push(filterValues[i]);
                break;
            }
        }
        component.set("v.value", value);
        component.set("v.to", '');
        var toggleText = component.find("listbox");
        //$A.util.removeClass(toggleText, "show");
        $A.util.addClass(toggleText, 'slds-hide');
    },
    /*
		Authors: Atul Hinge
		Purpose: selectOption function will be used to remove Email from selected email id
		Dependencies: CaseDetailSection.cmp
                
    */ 
    removeOption: function(component, event, helper) {
        var oldValue = component.get("v.value");
        var caseId = event.getSource().get("v.name");
        
        var values = [];  
        

        for (var i = 0; i < oldValue.length; i++) {
            
            if (caseId != oldValue[i].Id) {
                values.push(oldValue[i]);
            }
            
        }
        component.set("v.value", values);
    },
    /*
		Authors: Atul Hinge
		Purpose: selectOption function will be used to Hide Email drop down
		Dependencies: CaseDetailSection.cmp
                
    */ 
    hideOptions: function(component, event, helper) {
        var toggleText = component.find("listbox");
        $A.util.removeClass(toggleText, "show");
        $A.util.addClass(toggleText, "hide");
    },
    /*
		Authors: Niraj Prajapati
		Purpose: getSelectedID function will be called when we select any value from dropdown or lookup popup
		Dependencies: CaseDetailSection.cmp
                
    */
    getSelectedID: function(component, event, helper) {
        var selectedItem = event.currentTarget;
        component.set("v.value", selectedItem.dataset.record);
        component.set("v.value1", selectedItem.dataset.name);
        $A.util.toggleClass(component.find('lookupValues'), 'slds-hide');
    },
    /*
		Authors: Niraj Prajapati
		Purpose: getSelectedValue function will be called when we select any value from dropdown or lookup popup
		Dependencies: CaseDetailSection.cmp
                
    */
    getSelectedValue: function(component, event, helper) {
        var selectedItem = event.currentTarget;
        component.set("v.value", selectedItem.dataset.record);
        component.set("v.value1", selectedItem.dataset.name);
        var toggleText = component.find("listbox");
        $A.util.removeClass(toggleText, "show");
        $A.util.addClass(toggleText, "slds-hide");
    },
    /*
		Authors: Niraj Prajapati
		Purpose: changeValue function will be used to achive dependentPicklist functionality
		Dependencies: CaseDetailSection.cmp
                
    */
    changeValue: function(component, event, helper) {
        if (component.get("v.type") == 'PICKLIST') {
            var appEvent = $A.get("e.c:PopulateDependentPickList");
            appEvent.setParams({
                "controllingField": component.get("v.fieldSetMembers").fieldPath,
                "controllingFieldValue": component.get("v.value"),
                "id": component.get("v.id")
                /*function(response)
					{ //alert(response.success);	
						if(response.success== true)
						{
							helper.updateCase(component, event);
						}
					}*/
            });
            appEvent.fire();
        }
        component.set("v.value", component.get("v.value"));
        if (component.get("v.value") != '' || component.get("v.value") != null) {
            component.set("v.errors", "");
        }
		if (component.get("v.type") == 'PICKLIST') {
			//var caseObject=component.get("v.sObject");
			helper.fieldChangeHelper(component, event, helper);
		}
    },
    /*
		Authors: Niraj Prajapati
		Purpose: validate function will be used to validate field if it is mandatory
		Dependencies: CaseDetailSection.cmp
                
    */
    validate: function(component, event, helper) {
		var apiName=component.get("v.APIName");
        var np = component.getConcreteComponent().getDef().getDescriptor().getNamespace();
        var fields=component.get("v.fields");
		var isRequired=false;
		if(fields!=null){
			$.each(fields,function(indexInner, fieldDetails ){
				if(fieldDetails.fieldPath==apiName){
					isRequired=fieldDetails.required;
				}
			});
		}
        //Start code changed for PUX-268
        if (component.get("v.required") || isRequired) {
            if (component.get("v.value") == '' || component.get("v.value") == null) {
                var inputCmp = component.find("inputField");
				inputCmp.showHelpMessageIfInvalid();
                component.set("v.errors", (np == 'c') ? $A.get("$Label.c.Error_Message_Require_Field") : $A.get("$Label.CoraPLM.Error_Message_Require_Field"));
            }else{
                component.set("v.errors", "");
            }
        }
        else{
			component.set("v.errors", "");
		}
        if (component.get("v.type")=="EMAIL" && component.get("v.value") != '' && component.get("v.value") != null ) {
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            
            if (reg.test(component.get("v.value")) == false) 
            {
                component.set("v.errors", (np == 'c') ? $A.get("$Label.c.Error_Message_Email_Field") : $A.get("$Label.CoraPLM.Error_Message_Email_Field"));
            }else{
                component.set("v.errors", "");
            }
        }
        //End code changed for PUX-268
        
    },
    /*
		Authors: Niraj Prajapati
		Purpose: getDependentPicklistValues function will be used to achive dependentPicklist functionality
		Dependencies: CaseDetailSection.cmp
                
    */
    getDependentPicklistValues: function(component, event, helper) {
        if(component.get("v.id")=='all' || component.get("v.id") ==event.getParam('id')){
            if (component.get("v.type") != 'lookup' && component.get("v.fieldSetMembers").controlField == event.getParam('controllingField')) {
                component.set("v.options", component.get("v.fieldSetMembers").dependentValues[event.getParam('controllingFieldValue')]);
            }
        }
    },
    /*
		Authors: Niraj Prajapati
		Purpose: focusInput function will be used to focus on inputbox
		Dependencies: CaseDetailSection.cmp
                
    */
    
    focusInput: function(component, event, helper) {
        component.find("lookupInput").focus();
    },
    blurInput: function(component, event, helper) {
		  var toggleText = component.find("listbox");
		 $A.util.removeClass(toggleText, "slds-show");
		  $A.util.addClass(toggleText, "slds-hide");
		//alert(event.relatedTarget.classList)
	},    
   
}
})