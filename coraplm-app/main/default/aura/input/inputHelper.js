/*
Authors: Niraj Prajapati
Date created: 14/08/2017
Purpose: This Helper will call from controller and update case record.
Dependencies: 
-------------------------------------------------
Modifications:
                Date: 15/09/2017
                Purpose of modification: [PUX-128] Fields Display from fieldset and Validation on fields
                Method/Code segment modified: populateRelatedField function and UpdateCase function
                
*/ 
({
    /*
		Authors: Niraj Prajapati
		Purpose: getLookUpValues function will be called to get lookup values
		Dependencies: CaseDetailSection.cmp
                
    */
	getLookUpValues: function(component, event, helper) {
		var action = component.get("c.getLookupValue");
		action.setParams({
			objType: component.get("v.fieldSetMembers").relationship,
			whereClause: ''
		});
		action.setCallback(helper, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set('v.lookupValues', response.getReturnValue().value);
				component.set('v.fieldName', response.getReturnValue().fldName);
				component.set('v.fieldLabel', response.getReturnValue().fldLabel);
				component.set('v.nooflabels', component.get('v.fieldLabel').length);
				var listval = [];
				var values = response.getReturnValue().value;
				var fldName = response.getReturnValue().fldName;
				for (var i = 0; i < values.length; i++) {
					var v1 = {};
					v1['Id'] = values[i]['Id'];
					v1['Name'] = values[i]['Name'];
					for (var j = 1; j <= fldName.length; j++) {
						v1['field' + j] = values[i][fldName[j - 1]];
					}
					listval.push(v1);
				}
				component.set('v.listValues', listval);
			} else if (state === "INCOMPLETE") {
				alert('e');
			} else if (state === "ERROR") {
				alert('e');
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);
	},
    /*
		Authors: Niraj Prajapati
		Purpose: getLookUpName function will be called to get name from lookup field
		Dependencies: CaseDetailSection.cmp
                
    */
	getLookUpName: function(component, event, helper) {
		var action = component.get("c.getLookupNameApex");
		action.setParams({
			objType: component.get("v.fieldSetMembers").relationship,
			whereClause: component.get("v.value")
		});
		action.setCallback(helper, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set('v.value1', response.getReturnValue());
			} else if (state === "INCOMPLETE") {
				alert('e');
			} else if (state === "ERROR") {
				alert('e');
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);
	},
	fieldChangeHelper: function(component, event, helper) {
        var fieldName =component.get("v.APIName");
		var layoutJSON=component.get("v.layoutJSON");
		var sobjValue=component.get("v.sObject");
		var fields=component.get("v.fields");
		var actionFields=[];
		var actionFieldsOther=[];
		if(layoutJSON!=null && layoutJSON.rules!=null){
			console.log(layoutJSON.rules);
			$.each(layoutJSON.rules,function(index, value ){
					console.log(value);
					if(value.criterias!=null){
						if(value.field==fieldName){
							var isMatch=helper.isMatchCriteria(value.criterias,sobjValue);
							if(isMatch){
								if(value.actions!=null){
									$.each(value.actions,function(innerIndex, innerValue ){
										actionFields.push(innerValue);
									});
								}
							}
						}
						else{
							var isMatch=helper.isMatchCriteria(value.criterias,sobjValue);
							if(isMatch){
								if(value.actions!=null){
									$.each(value.actions,function(innerIndex, innerValue ){
										actionFieldsOther.push(innerValue);
									});
								}
							}
						}
					}
				
			});
		}
		
		if(layoutJSON!=null){
			$.each(fields,function(indexInner, fieldDetails ){
				if(layoutJSON.defaultHiddenFields!=null){
					$.each(layoutJSON.defaultHiddenFields, function( ind, defValue ) {
						if(fieldDetails.fieldPath==defValue){
							fieldDetails.display='none';
							fieldDetails.required=false;
							$('#'+defValue).hide();
							$('#'+defValue).find('.slds-select').removeAttr('required');
							$('#'+defValue).find('.slds-input').removeAttr('required');
						}
					});
				}
			});
		}

		if(layoutJSON!=null){
			$.each(layoutJSON,function(index, value ){
				$.each(fields,function(indexInner, fieldDetails ){
					if(actionFields.length>0){
						helper.executeMatchAction(actionFields,fieldDetails);
					}
					if(actionFieldsOther.length>0){
						helper.executeMatchAction(actionFieldsOther,fieldDetails);
					}
				});
			});
		}
		
        component.set("v.sObject",component.get("v.sObject"));
		component.set("v.fields",fields);
	},
	isMatchCriteria: function(criterias,sobjValue) {
		var isMatch=false;
		$.each(criterias,function(innerIndex, innerValue ){
			var fieldValue=sobjValue[innerValue.field];
			if(fieldValue!=null){
				if(innerValue.operator=='equals'){
					isMatch=fieldValue==innerValue.value;
				}
				else if(innerValue.operator=='not equal to'){
					isMatch=fieldValue!=innerValue.value;
				}
				else if(innerValue.operator=='less than'){
					isMatch=fieldValue>innerValue.value;
				}
				else if(innerValue.operator=='greater than'){
					isMatch=fieldValue<innerValue.value;
				}
				else if(innerValue.operator=='less or equal'){
					isMatch=fieldValue>=innerValue.value;
				}
				else if(innerValue.operator=='greater or equal'){
					isMatch=fieldValue<=innerValue.value;
				}
				else if(innerValue.operator=='contains in list'){
					$.each(innerValue.value.split(','),function(ind,val){
						if(val==fieldValue){
							isMatch=true;
							return;
						}
					});
				}
				else if(innerValue.operator=='does not contains in list'){
					isMatch=true;
					$.each(innerValue.value.split(','),function(ind,val){
						if(val==fieldValue){
							isMatch=false;
							return;
						}
					});
				}
			}
			if(isMatch==false){
				return false;
			}
		});
		return isMatch;
	},
	executeMatchAction:function(actionFields,fieldDetails){
		$.each(actionFields,function(ind, actionField){
			if(fieldDetails.fieldPath==actionField.field){
				if(actionField.action=='Hidden'){
					fieldDetails.display='none';
					fieldDetails.required=false;
					$('#'+actionField.field).hide();
					$('#'+actionField.field).find('.slds-select').removeAttr('required');
					$('#'+actionField.field).find('.slds-input').removeAttr('required');

				}
				else if(actionField.action=='Editable'){
					fieldDetails.display='';
					fieldDetails.required=false;
					$('#'+actionField.field).show();
					$('#'+actionField.field).find('.slds-select').removeAttr('required');
					$('#'+actionField.field).find('.slds-input').removeAttr('required');

					if($("#"+actionField.field).find("label").has("abbr").length==1){
						$("#"+actionField.field).find("label").find("abbr").remove();
					}
				}
				else if(actionField.action=='Mandatory'){
					fieldDetails.display='';
					fieldDetails.required=true;
					$('#'+actionField.field).show();
					$('#'+actionField.field).find('.slds-select').prop('required',true);
					$('#'+actionField.field).find('.slds-input').prop('required',true);
					if($("#"+actionField.field).find("label").has("abbr").length==0){
						$("#"+actionField.field).find("label").find("span").before('<abbr class="slds-required" title="required" >*</abbr>');
					}
				}
			}
		});
	},
  	
    
})