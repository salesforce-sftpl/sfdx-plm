/*
Authors: Tejas Patel, Tej Pal Kumawat - t
Date created: 23-Oct-2017
Purpose: PUX-340: All methods to add notes and releated entities
Dependencies: AddNotes.cmp, CaseService.cls
-------------------------------------------------
Modifications:  1 Tej Pal Kumawat
                Date: 13-Nov-2017 
                Purpose of modification: Standardize Code comments
                Method/Code segment modified: None
                
                2 Tej Pal Kumawat
                Date: 11-Dec-2017
                Purpose of modification: Method for get user name & time stamp
                Method/Code segment modified: getUserName() 1
                
*/ 
public with sharing class AddNoteController {
    /*
    Authors: Te Pal Kumawat
    Purpose: PUX-462: Method for get user name & time stamp
    Dependencies: NA
    */
    @AuraEnabled
    public static String getUserName() {
        return userinfo.getName() +' [ '+datetime.now()+' ]';
    } 
      

    /*
    Authors: Tejas Patel
    Purpose: PUX-340: Method for Add Note using EmailMessage
    Dependencies: CaseService.cls
    */
    @AuraEnabled
    public static void addNoteInfo(EmailMessage EM) { 
        try{
            CaseService  cs =new CaseService();
            cs.addNoteFromService(em);
        }catch(Exception ex){
            ExceptionHandlerUtility.writeException('AddNoteController', ex.getMessage(), ex.getStackTraceString());
        }
    }
}