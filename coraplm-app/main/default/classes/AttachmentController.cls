/*
* Authors      :  Atul Hinge
* Date created :  13/09/2017
* Purpose      :  A common controller for selectAttachment,AttachmentListView 
* Dependencies :  Email.cmp(Aura component)
* JIRA ID      :  PUX-335,PUX-266,PUX-265,PUX-252
* -----------------------------------------------------------
* Modifications: 
*        Date:  
*        Purpose of modification:  
*        Method/Code segment modified:    
*/

public class AttachmentController {
    /*
        Authors: Atul Hinge
        Purpose: To get a list of Attachment
        Dependencies:   AttachmentListView.cmp.   
    */
    @AuraEnabled
    public static Map<String,Object> getAttachments(String caseId) {
        Map<String,Object> resp=new Map<String,Object>();
        String errorMessage='Exception has occurred.';
        List<ContentDocumentLink> cdl = CaseService.getAttachmentLinks(caseId);
            resp.put('attachments',cdl);
        return resp;
    }
     @AuraEnabled
    public static Map<String,Object> updateFlagOnAttachments(String contentJson){
        Map<String,Object> resp=new Map<String,Object>();
        system.debug('#####'+contentJson);
        Map<String,String> content= (Map<String,String>)JSON.deserialize(contentJson, Map<String,String>.class);
        system.debug('#####'+content);
        List<ContentVersion> cv=new List<ContentVersion>();
        for(string s:content.keySet()){
            cv.add(new ContentVersion(Id=s,flag__c=content.get(s)));
        }
        update cv;
        return resp;
    }
    /*
        Authors: Atul Hinge
        Purpose: To get delete Attachment
        Dependencies:   AttachmentListView.cmp   
    */
    @AuraEnabled
    public static Map<String,Object> deleteAttachments(List<String> docId,String caseId) {
        Map<String,Object> resp=new Map<String,Object>();
        String errorMessage='The following exception has occurred: ';
        try{
            delete [SELECT Id FROM ContentDocument where LatestPublishedVersionId In :docId];
            resp.put('success',true);
            resp.put('attachments', CaseService.getAttachmentLinks(caseId));
        }catch (Exception e) {
            resp.put('error', errorMessage+e.getMessage());
            ExceptionHandlerUtility.writeException('Attachment', e.getMessage(), e.getStackTraceString());
        } 
        return resp;
       
    }
}