/**
* Authors    : Atul Hinge
* Date created : 16/11/2017 
* Purpose      : Test Class for AttachmentController
* Dependencies : -
* -------------------------------------------------
*Modifications:
Date:   
Purpose of modification:  
Method/Code segment modified: 

*/
@isTest
public class AttachmentControllerTest {
    //Test 1
    public static testMethod void unitTest01(){
        Test.startTest();        
        TestDataGenerator.createAppConfig();
        List<CaseManager__c> ctList= TestDataGenerator.createCases(1);
        TestDataGenerator.createEmailMessage(ctList[0]);
        String attachId= TestDataGenerator.createContent(ctList[0]);
        AttachmentController.getAttachments(ctList[0].Id);
        AttachmentController.deleteAttachments(new List<Id>{attachId},ctList[0].Id);
        AttachmentController.deleteAttachments(null,ctList[0].Id);
        Test.stopTest();
    }
    
    //Test 2
    public static testMethod void unitTest02(){
        
        try{
            Test.startTest();
            AttachmentController.getAttachments(null);
            Test.stopTest();
        }catch (Exception e) {}
        
        try{
            //AttachmentController.deleteAttachments(null);
        }catch (Exception e) {}
    }
    
    // Test 3
    public static testMethod void unitTest03(){
        try{
            TestDataGenerator.createAppConfig();
            List<CaseManager__c> ctList= TestDataGenerator.createCases(1);
            TestDataGenerator.createEmailMessage(ctList[0]);
            String attachId= TestDataGenerator.createContent(ctList[0]);
            
            String contentJson = '{"'+attachId+'":"Non Flag"}';
            Test.startTest();
            AttachmentController.updateFlagOnAttachments(contentJson);
            test.stopTest();
        }catch (Exception e) {}
        
    }
}