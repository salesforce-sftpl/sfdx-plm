/**
  * Authors    : Niraj Prajapati
  * Date created : 16/11/2017 
  * Purpose      : Test Class for BitSet
  * Dependencies : -
  * -------------------------------------------------
  *Modifications:
                Date:   
                Purpose of modification:  
                Method/Code segment modified: 

*/
@isTest
public class BitSetTest {
    public static testMethod void testFirst(){
    Integer i=0;   
    String pValidFor = 'AC97';
    List<Integer> nList= new  List<Integer>();
        for(i=0;i<16;i++){
            nlist.add(i);
        }
        System.assertEquals(nlist[0],0);
        BitSet bs = new BitSet();
        bs.testBits(pValidFor,nList);

    }
}