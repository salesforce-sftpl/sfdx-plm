/*
Authors: Ashish Kumar
Date created: 09/11/2017
Purpose: This class is used to create case coming from Bot Platform
Dependencies: Called from "AWS".
-------------------------------------------------
Modifications:
Date: 
Purpose of modification:
Method/Code segment modified:

*/
public class BotResponseJSON {
    public String speech;
    public String displayText;
    public String source;
    public List<Messages> messages {get;set;}
   
    public class Messages{
        public Integer type {get;set;} 
        public String platform {get;set;}
    	public String speech {get;set;}
    }
}