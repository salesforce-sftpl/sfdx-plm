/*
  Authors: Ashish Kumar
  Date created: 20/09/2017
  Purpose: This class is used to create case coming from Bot
  Dependencies: Called from "Bot".
  -------------------------------------------------
  Modifications:
  Date: 
  Purpose of modification:
  Method/Code segment modified:

*/
@RestResource(urlMapping = '/do/*')
global with sharing class BotToCase {


	/*
	  Authors: Ashish Kr.
	  Purpose: This method is responsible to handle the POST request coming from Bot Platform and to create the case.
	 */
	@HttpPost
	global static void createCaseFromBot() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		res.addHeader('Content-Type', 'application/json');
		BotRequestJSON reqObject = BotRequestJSON.parse(req.requestBody.tostring());

		String workType = reqObject.Result.contexts[0].parameters.WorkType;
		String description = reqObject.Result.contexts[0].parameters.Description;
		String userEmail = reqObject.Result.contexts[0].parameters.email;
		if (userEmail.containsIgnoreCase('>')) {
			userEmail = userEmail.remove('>');
		}
		if (userEmail.containsIgnoreCase(':')) {
			userEmail = userEmail.remove(':');
		}
		if (userEmail.containsIgnoreCase('<')) {
			userEmail = userEmail.remove('<');
		}
		CaseManager__c caseTracker = new CaseManager__c();
		caseTracker.Requestor_Email__c=userEmail;
		caseTracker.Input_Source__c='Email';
		caseTracker.Priority__c='Medium';
		caseTracker.Subject__c='Case created via bot for ' + workType + ' at ' + Datetime.now();
		caseTracker.Process__c='AP';
		caseTracker.Worktype__c=workType;
		caseTracker.Status__c='Start';
		caseTracker.UserAction__c='Create';
		caseTracker.LastResponseDate__c=Datetime.now();
		insert caseTracker;
		CaseManager__c createdCase = [SELECT Name FROM CaseManager__c WHERE Id = :caseTracker.Id];
		EmailMessage emailMsg = new EmailMessage();
		emailMsg.fromAddress = userEmail;

		emailMsg.fromName = userEmail;
		if (userEmail != null) {
			emailMsg.ToAddress = userEmail;
		}
		emailMsg.subject = 'Case created via Bot for ' + workType + ' at ' + String.ValueOf(DateTime.now()) + PLMConstants.SUBJECT_START_IDENTIFIER + createdCase.Name +  PLMConstants.SUBJECT_END_IDENTIFIER;
		emailMsg.TextBody = description;
		emailMsg.htmlBody = description;
		emailMsg.Incoming = true;

		emailMsg.RelatedToId = caseTracker.Id;
		emailMsg.status = '3';
		emailMsg.Headers = '';
		insert emailMsg;

		String resultSpeech = '';
		String checkUserElig = '';
		Boolean isProductAvail = false;

		BotResponseJSON resp = new BotResponseJSON();
		resp.source = 'Salesforce';
		List<BotResponseJSON.Messages> msgs = new List<BotResponseJSON.Messages> ();
		BotResponseJSON.Messages msg = new BotResponseJSON.Messages();
		msg.type = 0;
		msg.platform = 'skype';
		msg.speech = 'Thank you for providing the details. We have generated a new case for you. Your Case id is: #' + createdCase.Name;
		resp.speech = msg.speech;
		resp.displayText = msg.speech;
		msgs.add(msg);

		resp.messages = msgs;
		res.responseBody = blob.valueOf(JSON.serialize(resp));
		return;

	}

}