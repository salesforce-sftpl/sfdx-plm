/**
  * Authors     :  Ashish Kr.
  * Date created : 20/11/2017 
  * Purpose      : Test Class for BotToCase.cls
  * Dependencies : EmailToCaseSettingHelper.cls,
  * -------------------------------------------------
  *Modifications:
                Date:   
                Purpose of modification:  
                Method/Code segment modified: 

*/
@istest
public class BotToCaseTest {

    static testMethod void  createCaseFromBotTest(){
        String reqst = '{ ' +
    '\"id\": \"bbf865ce-8f4d-40ab-a683-b71beff730a2\",' +
    '\"timestamp\": \"2018-01-09T06:59:43.175Z\",'+
    '\"lang\": \"en\",'+
    '\"result\": {'+
    '\"source\": \"agent\",'+
    '\"resolvedQuery\": \"ashish@gmail.com\",'+
    '\"action\": \"get_process.get_subject.get_description.get_email\",'+
    '\"actionIncomplete\": false,'+
    '\"parameters\": {'+
    '\"email\": \"ashish@gmail.com\",'+
    '\"Process\": \"Log\",'+
    '\"WorkType\": \"Cash Management\",'+
    '\"Description\": \"my invoice has not been processed yet\"'+
    '},'+
    '\"contexts\": ['+
      '{'+
       '\"name\": \"defaultwelcomeintent-logcase-basicinfo-desc-email-followup\",'+
        '\"parameters\": {'+
          '\"email.original\": \"ashish@gmail.com\",'+
          '\"Description\": \"my invoice has not been processed yet\",'+
          '\"WorkType\": \"Cash Management\",'+
          '\"WorkType.original\": \"\",'+
          '\"Process\": \"Log\",'+
          '\"email\": \"<:ashish@gmail.com>\",'+
          '\"Description.original\": \"\",'+
          '\"Process.original\": \"\"'+
        '},'+
        '\"lifespan\": 2'+
      '},'+
      '{'+
        '\"name\": \"defaultwelcomeintent-logcase-followup\",'+
        '\"parameters\": {'+
          '\"email.original\": \"ashish@gmail.com\",'+
          '\"Description\": \"my invoice has not been processed yet\",'+
          '\"WorkType\": \"Cash Management\",'+
          '\"WorkType.original\": \"\",'+
          '\"Process\": \"Log\",'+
          '\"email\": \"ashish@gmail.com\",'+
          '\"Description.original\": \"\",'+
          '\"Process.original\": \"\"'+
        '},'+
        '\"lifespan\": 2'+
      '},'+
      '{'+
        '\"name\": \"defaultwelcomeintent-logcase-basicinfo-followup\",'+
        '\"parameters\": {'+
          '\"email.original\": \"ashish@gmail.com\",'+
          '\"Description\": \"my invoice has not been processed yet\",'+
          '\"WorkType\": \"Cash Management\",'+
          '\"WorkType.original\": \"\",'+
          '\"Process\": \"Log\",'+
          '\"email\": \"ashish@gmail.com\",'+
          '\"Description.original\": \"\",'+
          '\"Process.original\": \"\"'+
        '},'+
        '\"lifespan\": 3'+
      '},'+
      '{'+
        '\"name\": \"defaultwelcomeintent-logcase-basicinfo-desc-followup\",'+
        '\"parameters\": {'+
          '\"email.original\": \"ashish@gmail.com\",'+
          '\"Description\": \"my invoice has not been processed yet\",'+
          '\"WorkType\": \"Cash Management\",'+
          '\"WorkType.original\": \"\",'+
          '\"Process\": \"Log\",'+
          '\"email\": \"ashish@gmail.com\",'+
          '\"Process.original\": \"\",'+
          '\"Description.original\": \"\"'+
        '},'+
        '\"lifespan\": 1'+
      '}'+
    '],'+
    '\"metadata\": {'+
      '\"intentId\": \"935af99c-f379-4b28-88e4-a6e5832e7ef8\",'+
      '\"webhookUsed\": \"true\",'+
      '\"webhookForSlotFillingUsed\": \"false\",'+
      '\"webhookResponseTime\": 1504,'+
      '\"intentName\": \"Default Welcome Intent - log case - basic info - desc - email\"'+
    '},'+
    '\"fulfillment\": {'+
      '\"speech\": \"Thank you for providing the details. We have generated a new case for you. Your Case id is: #112863\",'+
      '\"source\": \"Salesforce\",'+
      '\"displayText\": \"Thank you for providing the details. We have generated a new case for you. Your Case id is: #112863\",'+
      '\"messages\": ['+
        '{'+
          '\"type\": 0,'+
          '\"speech\": \"Thank you for providing the details. We have generated a new case for you. Your Case id is: #112863\",'+
          '\"platform\": \"skype\"'+
        '}'+
      ']'+
    '},'+
    '\"score\": 1'+
  '},'+
  '\"status\": {'+
    '\"code\": 200,'+
    '\"errorType\": \"success\",'+
    '\"webhookTimedOut\": false'+
  '},'+
  '\"sessionId\": \"6880a9eb-d725-4042-98a0-977be1b689c7\" }';
        //String JsonMsg = JSON.serialize(reqst);
        
        Test.startTest();
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            
            req.requestURI = '/services/apexrest/do/createCase';  //Request URL
            req.httpMethod = 'POST';//HTTP Request 
            req.addHeader('Content-Type', 'application/json');
            req.requestBody = Blob.valueOf(reqst);
            
            RestContext.request = req;
            RestContext.response= res;
            BotToCase.createCaseFromBot();
        Test.stopTest();
    }
}