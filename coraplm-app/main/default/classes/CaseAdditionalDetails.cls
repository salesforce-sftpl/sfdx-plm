/*
Authors: Tej Pal Kumawat
Date created: 4-Dec-2017
Purpose: PUX-459: Comment entered by user should store and display in historical maner.
Dependencies: CaseAdditionalDetails.cmp, CaseAdditionalDetailsController.js, CaseAdditionalDetailsHelper.cls            
*/ 
public with sharing class CaseAdditionalDetails {
    
    /*
    Authors: Tejas Patel
    Purpose: PUX-459: Method to get case comments
    Dependencies: none
    */
    @AuraEnabled
    public static list<CaseCommentWrapper> getCaseComments(string childObjectId) {
        list<CaseCommentWrapper> commentList = new list<CaseCommentWrapper>();
        for(Comments__c com: [select CreatedBy.Name, CreatedDate,Comments__c from Comments__c where CaseManager__c =: childObjectId order by CreatedDate desc]){
            CaseCommentWrapper ccw = new CaseCommentWrapper();
            ccw.commentInfo = com.CreatedDate + ' (GMT +00:00) ' + ' : ' + com.CreatedBy.Name;
            ccw.commentDetail = 'Comments : ' + com.Comments__c;
            commentList.add(ccw);
        }
        return commentList;
    }
    
    //Wrapper class
    class CaseCommentWrapper{
        @AuraEnabled
        public String commentInfo { get; set; }
        @AuraEnabled
        public string commentDetail { get; set; }
    }
}