/*
  * Authors       : Niraj Prajapati,Parth Lukhi
  * Date created : 17/08/2017 
  * Purpose      : This CaseDetailViewController is used for getting Case Details and Prcessign Fields,FieldSetName
  * Dependencies :  NA
  * -------------------------------------------------
  *Modifications:
  Date:  09/11/2017 
  Purpose of modification:  PUX-235 : Getting Contact Name on interction 
  Method/Code segment modified: getContactListbyInter

*/
public with sharing class CaseDetailViewController {
	public static Map<String, Schema.SObjectType> allObjects = Schema.getGlobalDescribe();
	public static Map<Schema.SObjectType, Schema.DescribeSObjectResult> objDescribes = new Map<Schema.SObjectType, Schema.DescribeSObjectResult> ();

	/*
	  * Authors: Parth Lukhi
	  * Purpose:  It is used for gettign Case Details and Case Histories based in CaseId
	  * Dependencies: CaseManager__c, EmailMessage,ContentVersion,ContentDocumentId
	  * 
	  *  Start :It is used for gettign Case Details and Case Histories based in CaseId
	  * 
	*/
	@AuraEnabled
	public static CaseManager__c getCaseDetail(String caseId) {
		Set<String> fldSet = getAllFieldNames(ObjectUtil.getPackagedFieldName('CaseManager__c'));
		List<String> fldsList = new List<String> ();
		fldsList.addAll(fldSet);
		caseId = String.escapeSingleQuotes(caseId);
		Integer noOfRecordtype = Schema.SObjectType.CaseManager__c.getRecordTypeInfosByName().size();
		String qry = '';
		if (noOfRecordtype > 1)
		{
			qry = 'Select RecordType.Name,';
		} else {
			qry = 'Select ';
		}
		qry = qry + String.join(fldsList, ',') + ',  (SELECT OldValue, NewValue, Field, createdById FROM Histories order by createdDate Desc limit 10) from CaseManager__c where Id =: caseId';

		system.debug('Query=>'+qry);
		List<CaseManager__c> caseList;
		CaseManager__c caseTrack;
		try {
			caseList = (List<CaseManager__c>) Database.query(qry);
			if (caseList != null && caseList.size() > 0) {
				caseTrack = caseList.get(0);
			}
		}
		catch(Exception ex) {
			ExceptionHandlerUtility.writeException('CasesDetailViewController', ex.getMessage(), ex.getStackTraceString());
		}

		return caseTrack;
	}
	/**
	  *  End :It is used for getting Case Details and Case Histories based in CaseId
	  * 
	*/

	/*
	 * Authors: Niraj Prajapati
	 * Purpose:  It is used for For getting Quality Field set Name based in CaseId
	 * Dependencies: CaseManager__c, EmailMessage,ContentVersion,ContentDocumentId
	 * 
	 *  Start : code for PUX-512
	 */
	@AuraEnabled
	public static Map<String, object> getQCFieldSetName(String caseId) {
		Map<String, object> response = new Map<String, object> ();
		response.put('qcInfo', QCCalculation.getQCFormFieldInfo(caseId));
		return response;
	}
	//end code for PUX-512

	/*
	  * Authors: Parth Lukhi
	  * Purpose: It is used for getting all the fields name based on Object Name
	  * Dependencies: getObjectType
	  * 
	  *  Start : It is used for getting all the fields name based on Object Name
	*/
	public static Set<String> getAllFieldNames(String objName) {
		if (String.isBlank(objName)) {
			return null;
		}
		Schema.SObjectType objType = getObjectType(objName);
		return getAllFieldNames(objType);
	}
	/*
	  *   End : It is used for gettign Case Details and Case Histories based in CaseId
	*/

	/*
	  * Authors: Parth Lukhi
	  * Purpose: Returns Object Type for the given object name.
	  * Dependencies: getComponentNameWithoutNameSpace
	  * 
	  *  Start : Returns Object Type for the given object name.
	*/
	public static Schema.SObjectType getObjectType(String objName) {
		Schema.SObjectType objType = allObjects.get(objName.toLowerCase());
		if (objType == null) {
			objType = allObjects.get(getComponentNameWithoutNameSpace(objName.toLowerCase(), TRUE));
		}
		return objType;
	}
	/*
	  *   End : Returns Object Type for the given object name.
	*/

	/*
	  * Authors: Parth Lukhi
	  * Purpose: Returns all field names for the given object type.
	  * Dependencies: getObjDescribeResult
	  *
	  *   Start :  Returns all field names for the given object type.
	*/
	public static Set<String> getAllFieldNames(Schema.sObjectType objType) {
		Schema.DescribeSObjectResult objDescribe = getObjDescribeResult(objType);
		Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
		return fieldMap.keySet();
	}
	/*   
	  * End :  Returns all field names for the given object type.
	*/
	/*
	  * Authors: Parth Lukhi
	  * Purpose:This method returns the namespace used in the given custom component name. 
	  * Dependencies: NA
	  * 
	  * Start :This method returns the namespace used in the given custom component name. 
	*/
	public static String getComponentNameWithoutNameSpace(String aName, Boolean lowerCase) {

		if (aName == NULL) {
			return '';
		}
		String lowerCaseName = aName.toLowerCase();
		Integer firstIndex = lowerCaseName.indexOf('__');
		Integer lastIndex = lowerCaseName.lastIndexOf('__c');
		String nameToReturn = aName; // this is when no namespace is there
		if (firstIndex != lastIndex) {
			//prune the namespace and return it.
			nameToReturn = (aName.substring(firstIndex + 2));
		}
		if (lowerCase) {
			nameToReturn = nameToReturn.toLowerCase();
		}

		return nameToReturn;
	}
	/*   
	  * End : This method returns the namespace used in the given custom component name. 
	*/
	/*
	  * Authors: Parth Lukhi
	  * Purpose:Start: Returns Object Describe for the given object type.
	  * Dependencies: NA
	  * 
	  * Start: Returns Object Describe for the given object type.
	*/
	public static Schema.DescribeSObjectResult getObjDescribeResult(Schema.sObjectType objType) {
		Schema.DescribeSObjectResult objDesc = objDescribes.get(objType);
		if (objDesc == null) {
			try {
				objDesc = objType.getDescribe();
				objDescribes.put(objType, objDesc);
			} catch(Exception ex) {
				ExceptionHandlerUtility.writeException('CaseDetailViewController', ex.getMessage(), ex.getStackTraceString());
			}
		}
		return objDesc;
	}
	/*   
	  * End : Returns Object Describe for the given object type.
	*/

	/*
	  Authors: Niraj Prajapati
	  Purpose: getAllFieldSet function will return list of fieldset on given object.
	  Dependencies: CaseDetailSection.cmp   

	*/
	@AuraEnabled
	public static List<String> getAllFieldSet(String objType) {
		List<String> listOfFieldSetApiNames = new List<String> ();
		String strObjectApiName = objType;
		Schema.SObjectType sObjType = Schema.getGlobalDescribe().get(strObjectApiName);
		if (sObjType != NULL) {
			Map<String, Schema.FieldSet> fieldSetNameToItsRecord = sObjType.getDescribe().fieldsets.getMap();
			listOfFieldSetApiNames.addAll(fieldSetNameToItsRecord.keySet());
		}
		return listOfFieldSetApiNames;
	}
    
    /*
        Authors: Niraj P.
        Purpose: To get current login user id and Admin Session ID
        Dependencies : CaseTrackerHelper.js
    */
    @AuraEnabled
    public static Map < String, Object > getUserInfo() {
        Map < String, Object > userDetails = new Map < String, Object > ();
        try {           
            userDetails.put('UserId', userinfo.getuserid());
            userDetails.put('UserSessionId', getSessionID());
            userDetails.put('InstanceName',  [SELECT Id, InstanceName FROM Organization].InstanceName);
        } catch (Exception ex) {
            System.debug(ex.getStackTraceString());
        }
        return userDetails;
    }

    /* 
        Authors: Niraj P.
        Purpose: To get current login user id and Admin Session ID
        Dependencies : CaseTrackerHelper.js
    */
    public static string getSessionID() {
        String sessionId = '';
        try {
            AppConfig__c mhc = AppConfig__c.getInstance();
            String userName = mhc.UserName__c;
            String pwd = mhc.Password__c;
            HttpRequest request = new HttpRequest();
            request.setEndpoint('https://www.salesforce.com/services/Soap/u/40.0');
            request.setMethod('POST');
            request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
            request.setHeader('SOAPAction', '""');
            //not escaping username and password because we're setting those variables above
            //in other words, this line "trusts" the lines above
            //if username and password were sourced elsewhere, they'd need to be escaped below
            request.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/><Body><login xmlns="urn:partner.soap.sforce.com"><username>' + userName + '</username><password>' + pwd + '</password></login></Body></Envelope>');
            Dom.XmlNode resultElmt = (new Http()).send(request).getBodyDocument().getRootElement()
                .getChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/')
                .getChildElement('loginResponse', 'urn:partner.soap.sforce.com')
                .getChildElement('result', 'urn:partner.soap.sforce.com');

            //-------------------------------
            // Grab session id and server url
            //--------------------------------
            final String SERVER_URL = resultElmt.getChildElement('serverUrl', 'urn:partner.soap.sforce.com').getText().split('/services')[0];
            sessionId = resultElmt.getChildElement('sessionId', 'urn:partner.soap.sforce.com').getText();
        } catch (Exception ex) {
            System.debug(ex.getStackTraceString());
        }
        return sessionId;
    }
	/* 
    	Authors: Tushar Moradiya.
    	Purpose: To get PredictedValue of Indexing Field
		Dependencies : CaseTrackerHelper.js
	*/

  @AuraEnabled
    public static CaseManager__c getPredictedValue(String caseId) {
        caseId = String.escapeSingleQuotes(caseId);
        String qry ='';
       
            qry = 'SELECT Id, Name,AWSResponseJson__c,Predicted_Field_Status__c FROM CaseManager__c where Id =: caseId';
        
        List<CaseManager__c> caseList; 
        CaseManager__c caseTrack;
        try{
            caseList = (List<CaseManager__c>)Database.query(qry); 
            if(caseList!=null && caseList.size()>0){
                caseTrack=caseList.get(0);
            }
        }
        catch(Exception ex){
            ExceptionHandlerUtility.writeException('CasesDetailViewController', ex.getMessage(), ex.getStackTraceString());
        }
     
      return caseTrack;
    }
	  /* 
    	Authors: Tushar Moradiya.
    	Purpose: To update PredictedValue status of Indexing Field
		Dependencies : CaseTrackerHelper.js
	*/
    @AuraEnabled
    public static void updateCaseTracker(CaseManager__c caseObj,String status) {       
        caseObj.Predicted_Field_Status__c=status;
        update caseObj;
    }
}