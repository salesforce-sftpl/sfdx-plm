/**
  * Authors    : Niraj Prajapati
  * Date created : 16/11/2017 
  * Purpose      : Test Class for QualityControlController
  * Dependencies : -
  * -------------------------------------------------
  *Modifications:
                Date:   
                Purpose of modification:  
                Method/Code segment modified: 

*/
@isTest
public class CaseDetailViewControllerTest {
	public static testMethod void testFirst(){
        //set mock callout class
        //Test.setMock(HttpCalloutMock.class, new RequestDataFromAWSMock());
        CaseManager__c caseObj=TestDataGenerator.createCaseTracker();
        TestDataGenerator.createEmailMessage(caseObj);
        System.assertEquals(caseObj.Subject__c,'Invalid Invoice');
        CaseDetailViewController.getCaseDetail(caseObj.Id);
         CaseDetailViewController.getQCFieldSetName(caseObj.Id);
        CaseDetailViewController.getAllFieldNames(ObjectUtil.getPackagedFieldName('CaseManager__c'));
        CaseDetailViewController.getAllFieldNames('');
        CaseDetailViewController.getAllFieldSet(ObjectUtil.getPackagedFieldName('CaseManager__c'));
        CaseDetailViewController.getComponentNameWithoutNameSpace(NULL,false);
        CaseDetailViewController.getComponentNameWithoutNameSpace('Priority__c',false);
        CaseDetailViewController.getUserInfo();
        try{
        CaseDetailViewController.getAllFieldNames(ObjectUtil.getPackagedFieldName('CaseManager__c'));
        } catch(Exception e){
              System.debug('Error');  
            } 
       
    }
}