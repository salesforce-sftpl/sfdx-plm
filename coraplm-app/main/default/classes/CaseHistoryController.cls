/**
Authors: Rahul Pastagiya
Date created: 05/10/2017
Purpose: To perform all operation reated to Case history
Comment:It is without sharing because no sharing model will impose on history object
Dependencies: CaseHistory.cmp
----------------------------------------------------------------
Modifications:
                Date:  vf v vfvc cx
                Purpose of modification:
                Method/Code segment modified:
                
**/
public without sharing class CaseHistoryController {

    /**
        Authors: Rahul Pastagiya
        Purpose: To get The List of Cases History
    **/
    @AuraEnabled
    public static List < CaseHistoryWrapper > getCaseHistoryData(string childobjectname, string childObjectId) {
        List < CaseHistoryWrapper > trackerHistories = new List < CaseHistoryWrapper > ();

        childobjectname = ObjectUtil.getWithNameSpace(childobjectname);
        String historyObjName = childobjectname;
        if (historyObjName.contains('__c')) {
            historyObjName = historyObjName.replace('__c', '');
            historyObjName = historyObjName + '__History';
            historyObjName = ObjectUtil.getWithNameSpace(historyObjName);
        }        

        Map < string, Schema.SObjectField > mapFields = Schema.getGlobalDescribe().get(childobjectname).getDescribe().fields.getMap();
        //For Security scan
        SecurityHelper.checkFieldReadAccess(new List < String > {
            'Id',
            'Field',
            'NewValue',
            'OldValue',
            'CreatedDate',
            'CreatedById',
            'CreatedDate',
            'ParentId'
        }, historyObjName);
        //For Security scan
        String query = 'SELECT Id, Field, NewValue, OldValue, CreatedDate, CreatedById, CreatedBy.Name FROM ' + historyObjName + ' WHERE ParentId = :childObjectId ORDER BY CreatedDate DESC, Id DESC';
        List < SObject > listEntityHistory = Database.query(query);
        for (SObject oHistory: listEntityHistory) {

            CaseHistoryWrapper oCaseHistoryWrapper = new CaseHistoryWrapper();
            oCaseHistoryWrapper.fieldName = String.valueOf(oHistory.get('Field')).toLowerCase();
            if (oHistory.getSobject('CreatedBy') != null) {
                oCaseHistoryWrapper.createdBy = String.valueOf(oHistory.getSobject('CreatedBy').get('name'));
            }
            DateTime createdDate = (DateTime) oHistory.get('CreatedDate');

            oCaseHistoryWrapper.createdDate = DateTime.newInstance(createdDate.year(), createdDate.month(), createdDate.day(), createdDate.hour(), createdDate.minute(), createdDate.second()).format('dd MMMM YYYY hh:mm a');
            oCaseHistoryWrapper.oldValue = string.valueof(oHistory.get('OldValue'));
            oCaseHistoryWrapper.newValue = string.valueof(oHistory.get('NewValue'));
            if (((String) oHistory.get('Field')).equalsIgnoreCase('created') || oHistory.get('NewValue') != null && !String.valueOf(oHistory.get('NewValue')).startsWith('005') && !String.valueOf(oHistory.get('NewValue')).startsWith('00G')) {
                string fieldname = String.valueOf(oHistory.get('Field')).toLowerCase();
                string temp = fieldname; //.replace(defaultNamespace, '');
                if (mapFields.containsKey(temp)) {
                    oCaseHistoryWrapper.fieldLabel = mapFields.get(fieldname).getDescribe().Label;
                } else {
                    oCaseHistoryWrapper.fieldLabel = String.valueOf(oHistory.get('Field'));
                }
                trackerHistories.add(oCaseHistoryWrapper);
            }

            if (oCaseHistoryWrapper.newValue != null && oCaseHistoryWrapper.oldValue == null) {
                oCaseHistoryWrapper.formatedText = 'Changed <b>' + oCaseHistoryWrapper.fieldLabel + '</b> to <b>' + oCaseHistoryWrapper.newValue + '</b>.';
            } else if (oCaseHistoryWrapper.newValue != null && oCaseHistoryWrapper.oldValue != null) {
                oCaseHistoryWrapper.formatedText = 'Changed <b>' + oCaseHistoryWrapper.fieldLabel + '</b> from ' + oCaseHistoryWrapper.oldValue + ' to <b>' + oCaseHistoryWrapper.newValue + '</b>.';
            } else if (oCaseHistoryWrapper.fieldLabel != null && (oCaseHistoryWrapper.fieldLabel).equalsIgnoreCase('created')) {
                oCaseHistoryWrapper.formatedText = 'Created.';
            }

        }
        return trackerHistories;
    }

    /**
        Authors: Rahul Pastagiya
        Purpose: Wrapper to incorporate all case history related value
    **/
    class CaseHistoryWrapper {
        @AuraEnabled
        public String fieldName {
            get;
            set;
        }
        @AuraEnabled
        public string fieldLabel {
            get;
            set;
        }
        @AuraEnabled
        public String oldValue {
            get;
            set;
        }
        @AuraEnabled
        public String newValue {
            get;
            set;
        }
        @AuraEnabled
        public String createdDate {
            get;
            set;
        }
        @AuraEnabled
        public String createdBy {
            get;
            set;
        }
        @AuraEnabled
        public string formatedText {
            get;
            set;
        }
    }
}