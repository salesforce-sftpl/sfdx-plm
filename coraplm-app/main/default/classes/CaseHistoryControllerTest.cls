/**
  * Authors      : Rahul Pastagiya
  * Date created : 16/11/2017 
  * Purpose      : Test Class for CaseHistoryController
  * Dependencies : -
  * -------------------------------------------------
  *Modifications:
                Date:   
                Purpose of modification:  
                Method/Code segment modified: 

*/
@isTest
public class CaseHistoryControllerTest {
    
	@isTest
	public static void validateCaseHistoryControllerFirst() {	
        //set mock callout class
        //Test.setMock(HttpCalloutMock.class, new RequestDataFromAWSMock());
		CaseManager__c ct = CaseHistoryControllerTest.createCase();
        TestDataGenerator.createEmailMessage(ct);
        String caseId = ct.id;
        String query = 'SELECT Id, Field, NewValue, OldValue, CreatedDate, CreatedById, CreatedBy.Name FROM CaseManager__History WHERE ParentId = :caseId ORDER BY CreatedDate DESC, Id DESC';
        List<SObject> listEntityHistory = Database.query(query);
		CaseHistoryController.getCaseHistoryData(ObjectUtil.getPackagedFieldName('CaseManager__c'),ct.id);        
	}
    
    public static CaseManager__c createCase(){
		CaseManager__c caseObj=new CaseManager__c();
        caseObj.Requestor_Email__c='Tushar@hello.com';
        caseObj.Amount__c=12000;
        caseObj.escalation__c=true;
        insert caseObj;	
        
        CaseManager__c caseObj2=new CaseManager__c();
        caseObj2.Id = caseObj.Id;
        caseObj2.Amount__c=12001;
		upsert caseObj2;
        
        CaseManager__History caseTrackertHistory = new CaseManager__History(
            ParentId = caseObj.Id,           
            Field = 'created'
        );        
        insert caseTrackertHistory;
		return caseObj;
	}
}