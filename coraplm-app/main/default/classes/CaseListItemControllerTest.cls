/**
  * Authors     : Mohit Garg
  * Date created : 16/01/2018 
  * Purpose      : Test Class for  CaseListItemController
  * Dependencies : CaseListItemController.cls
  * -------------------------------------------------
 
*/

@isTest
public class CaseListItemControllerTest {
    public static testMethod void unitTest01(){
        TestDataGenerator.createAppConfig();
        
        //set mock callout class
        //Test.setMock(HttpCalloutMock.class, new RequestDataFromAWSMock());
        //variable Declaration
        Integer noOfSplits = 2;
        String Comments = 'Test Comments';
        List<CaseManager__c> caseList = new List<CaseManager__c>();
        //End Variable Declaration
         
        //Case Creation
        CaseManager__c caseObj=new CaseManager__c();
        caseObj.Process__c='AP';
        caseObj.WorkType__c='PO Invoices';
        caseObj.Document_Type__c='Electricity';
        caseObj.Amount__c=12000;
        caseObj.Status__c='Ready for processing';
        caseObj.escalation__c=true;
        insert caseObj;
        //TestDataGenerator.createEmailMessage(caseObj);
        caseList.add(caseObj);
        
        CaseManager__c caseObjNew=new CaseManager__c();
        caseObjNew.Process__c='AP';
        caseObjNew.WorkType__c='PO Invoices';
        caseObjNew.Document_Type__c='Electricity';
        caseObjNew.Amount__c=10000;
        caseObjNew.Status__c='Ready for processing';
        caseObjNew.escalation__c=true;
        insert caseObjNew;
        caseList.add(caseObjNew);
        //End Case Creation
        String attachId= TestDataGenerator.createContent(caseObjNew);
        String caseJson = '[{\"attachment\":[\"'+attachId+'\"],\"case\":{\"CoraPLM__Favourited__c\":true,\"CoraPLM__Process__c\":\"AP\",\"CoraPLM__OwnerName__c\":\"<a href=\\"/0050Y000003MV5B\\" target=\\"_blank\\">Tej Pal Kumawat</a>\",\"CoraPLM__Unread_Email_Count__c\":0,\"CoraPLM__Status__c\":\"Ready For QC\",\"CoraPLM__Indicator__c\":\"#fdc55b\",\"CoraPLM__escalation__c\":false,\"CoraPLM__OverdueIn__c\":\"Overdue\",\"CoraPLM__Subject__c\":\"RE: ddd test fff\",\"auraId\":0,\"CoraPLM__WorkType__c\":\"\",\"CoraPLM__Document_Type__c\":\"\",\"CoraPLM__Priority__c\":\"\",\"CoraPLM__Hold_Reason__c\":\"\",\"CoraPLM__Location__c\":\"\",\"CoraPLM__CaseOrigin__c\":\"\"}}]';        
        
        //Calling Methods of Controller
        CaseListItemController.returnSplitCases(caseObj, noOfSplits);
        CaseListItemController.getAttachments(caseObj.Id);
         CaseListItemController.performMergeAction(caseList,caseObjNew,Comments);
        CaseListItemController.performSplitAction(caseJson, caseObj, 'CaseTracker__c', 'DefaultFieldSet', Comments);
        CaseListItemController.toggleFavourite(caseObj,'Favourite');
        CaseListItemController.toggleFavourite(caseObj,'UnFavourite');
        //End Calling Methods of Controller
    }
}