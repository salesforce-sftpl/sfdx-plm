/*
 * Authors     :  Rahul Pastagiya
 * Date created :  09/08/2017
 * Purpose      :  To display all Cases tab wise with the help of CaseListItem Component.
 * Dependencies :  CaseList.cmp, CaseTracker.cmp
 * JIRA ID      :  PUX-23
 * -----------------------------------------------------------
 * Modifications: 1
 *        Authors:  Tushar Moradiya
 *        Date:  18-Sep-2017
 *        Purpose of modification:  Implement filters on list page.
 *        Method/Code segment modified: PUX-305   
 * 
 *  ____________________________________________________________
*   Modifications: 2
*           Authors:  Tushar Moradiya
*           Date:  11-Dec-2017
*           Purpose of modification: Filter removed while we go bake on case list view
*           Method/Code segment modified: PUX-421
*   Modifications: 3
*           Authors:  Rahul Pastagiya	
*           Date:  02-Feb-2018
*           Purpose of modification: Configuration ability of Case view
*           Method/Code segment modified: PUX-678
*   Modifications: 4
*           Authors:  Rahul Pastagiya	
*           Date:  07-Feb-2018
*           Purpose of modification: Optimize the list view to ensure key information is readily available
*           Method/Code segment modified: PUX-12
 */

public with sharing class CaseListViewController {

    /*
        Authors: Rahul Pastagiya
        Purpose: To get The List of Cases based on condition
        Dependencies : CaseListHelper.js
    */
    @AuraEnabled
    public static CasePagerWrapper getCases(String whereClause, Integer pageNumber, Integer recordsToDisplay, 
    String sortField, String sortDirection, List < String > selectedQueue, Map < String, String > filterMap, String overDueIn) {
        CasePagerWrapper cpw = new CasePagerWrapper();    
        try { 
            //PUX-678
            String objectName = ObjectUtil.getWithNameSpace('CaseManager__c');
            String fieldSet = PLMConstants.CASE_LIST_FIELD_SET;
            fieldSet= ObjectUtil.getValidFieldSetName(objectName,fieldSet); 			
            Schema.FieldSet fieldSetObj = Schema.getGlobalDescribe().get(objectName).getDescribe().FieldSets.getMap().get(fieldSet);
            Map<String, String> keyFieldLabelMap = new Map<String, String>();
            List < String > keyList = new List < String > ();
            keyList.add('escalation__c');
            //PUX-12
          //  Integer length = (fieldSetObj.getFields()).size() <= 6 ? (fieldSetObj.getFields()).size() : 6;
          	Integer length = (fieldSetObj.getFields()).size();
            //PUX-12
            for (Integer i=0; i<length ;i++) {				          
                Schema.FieldSetMember fsm = (fieldSetObj.getFields())[i];
                keyList.add(fsm.getFieldPath());
                keyFieldLabelMap.put(fsm.getFieldPath(), fsm.getLabel());      
            }
            
            set < String > fieldList = new set < String > (keyList);
            String emailfieldSet= ObjectUtil.getValidFieldSetName(objectName,'EmailProcessingFields'); 		
            Schema.FieldSet fieldSetObj1 = Schema.getGlobalDescribe().get(objectName).getDescribe().FieldSets.getMap().get(emailfieldSet);
            for(Schema.FieldSetMember fsm:fieldSetObj1.getFields()){
                fieldList.add(fsm.getFieldPath());
            }
            fieldList.add('UserAction__c');
            fieldList.add('OwnerId');
            fieldList.add('SkipIndexing__c');
            //PUX-678
            String uid = UserInfo.getUserId();
            Integer offset = (Integer.valueOf(pageNumber) - 1) * Integer.valueOf(recordsToDisplay);
            String queryWhereClause = '';
            String totalCaseCountQuery = 'select count() from CaseManager__c where';
            //PUX-678
            String query = 'select id,'+string.join(new List<String>(fieldList),',')+',OverdueIn__c,name,Indicator__c,Subject__c,Target_TAT_Time__c,PredictedClosureDate__c,(SELECT CreatedDate, FromAddress, FromName, TextBody, HtmlBody , Id, Subject FROM Emails order by CreatedDate desc limit 1), Unread_Email_Count__c from CaseManager__c where ';
            //PUX-678
			List<string> excludeCaseStatus=new List<string>();
			excludeCaseStatus.add('Junk Case');

			List<string> completedCaseStatus=new List<string>();
			completedCaseStatus.add('Completed');
			completedCaseStatus.add('Rejected');
			completedCaseStatus.add('Pending For Archival');

			if (whereClause == 'My Cases') {
                queryWhereClause += ' OwnerId = :uid and status__c not in : excludeCaseStatus and status__c Not in : completedCaseStatus';
            } else if (whereClause == 'Unassigned') {
				queryWhereClause += ' Owner.type = \'Queue\' and ( PreviousQueueName__c = null or PreviousQueueName__c = \'\' ) and status__c not in : excludeCaseStatus';
            } else if (whereClause == 'Closed') {
                queryWhereClause += ' OwnerId = :uid and status__c in :completedCaseStatus';
            } else if (whereClause == 'Shared Cases') {
                queryWhereClause += ' OwnerId != :uid and PreviousQueueName__c != null and PreviousQueueName__c != \'\' and status__c not in : excludeCaseStatus';
            }
            /* Code start for PUX-485 */
            if(overDueIn != ''){
                if(overDueIn == 'Overdue')
                    queryWhereClause += ' And OverdueIn__c != \'Overdue\' ';
                else
                    queryWhereClause += ' And Status__c in (\'Awaiting Email Response\',\'Awaiting Supervisory Resolution\') ';
            }
            /* Code End for PUX-485 */
            //For Security scan
            List<String> fieldsName = new List<String>{'id','Process__c','WorkType__c','Document_Type__c','Location__c','ERPSystem__c','Sender_Domain__c','Amount__c','SuppliedEmail__c','UserAction__c','OwnerName__c','Favourited__c','Target_TAT_Time__c','escalation__c','OverdueIn__c','name','status__c','Priority__c','Indicator__c','Subject__c',' Ownerid','PreviousQueueName__c','Owner_Queue__c'};
            
            //For Security scan
            /*
              Authors: Tushar Moradiya
              Purpose: apply selected filter to cases
              Dependencies: none.   
              Start - PUX-305 
             */

            //To apply Queue filter when applied on screen 
            if (selectedQueue != null && selectedQueue.size() > 0) {
                queryWhereClause += ' AND Owner_Queue__c IN: selectedQueue ';
            }
            // Start PUX 421
            if(filterMap!=null){
                for (String key: filterMap.keySet()) {
                
                    String fieldName=key.split(':')[0];
                    fieldsName.add((String)filterMap.get(fieldName));
                    System.debug('filterMap*********'+filterMap);
                    Map < String, Schema.SObjectField > fieldsMap = Schema.SObjectType.CaseManager__c.fields.getMap();                    
                    String fieldResult = String.valueOf(fieldsMap.get(fieldName).getDescribe().getType());   
					System.debug('fieldResult: '+fieldResult);             
                    if (fieldResult == 'DATETIME') {
                        Date myDate = date.valueOf(String.valueOf(filterMap.get(fieldName)));
                        Date myDate1 = date.valueOf(String.valueOf(filterMap.get(fieldName))) + 1;
                        queryWhereClause += ' AND ' + fieldName + ' >= :myDate AND ' + fieldName + ' < :myDate1';
                    }
					else if (fieldResult == 'CURRENCY' || fieldResult == 'DOUBLE' || fieldResult == 'BOOLEAN') {
                        queryWhereClause += ' AND ' + fieldName + ' in ('+filterMap.get(fieldName)+')';
                    } else {
                        //queryWhereClause += ' AND '+fieldName+' = \''+filterMap.get(fieldName)+'\'';
                        queryWhereClause += ' AND ' + fieldName + ' in (\''+filterMap.get(fieldName).replaceAll(',','\',\'')+'\')';                   
                    }
                }
            }
            // End PUX 421
            /*End - PUX-305 */
            //PUX-207   
           // SecurityHelper.checkFieldReadAccess(fieldsName, 'CaseManager__c');
        //    SecurityHelper.checkFieldReadAccess(new List<String>{'CreatedDate', 'FromAddress', 'FromName', 'TextBody', 'Id', 'Subject'}, 'EmailMessage');
            totalCaseCountQuery += queryWhereClause + ' limit 2010';
            System.debug('selectedQueue*********'+selectedQueue);   
            System.debug('totalCaseCountQuery*********'+totalCaseCountQuery);   
            cpw.total = (database.countQuery(totalCaseCountQuery));

            query += queryWhereClause;
            queryWhereClause = '';
            query += ' order by ' + sortField + ' ' + sortDirection + ' NULLS LAST';
            query += ' Limit ' + recordsToDisplay + ' OFFSET ' + offset;
            //PUX-207
            system.debug('########'+query);
            //cpw.cases =  CaseListViewController.prepareResponse(database.query(query)); 
            keyList.remove(0);                      
            cpw.cases = database.query(query);
            //PUX-678
            cpw.fieldSetKeyList = keyList;
            cpw.keyFieldLabelMap = keyFieldLabelMap;
            //PUX-678
            cpw.pageSize = recordsToDisplay;
            cpw.page = pageNumber;
            cpw.total = (database.countQuery(totalCaseCountQuery));
            
        } catch (Exception ex) {
            System.debug(ex.getStackTraceString());
        }
        return cpw;
    }
    
   

    /*
        Authors: Niraj P.
        Purpose: To get current login user id and Admin Session ID
        Dependencies : CaseTrackerHelper.js
    */
    @AuraEnabled
    public static Map < String, Object > getUserInfo() {
        Map < String, Object > userDetails = new Map < String, Object > ();
        try {           
            userDetails.put('UserId', userinfo.getuserid());
            userDetails.put('UserSessionId', getSessionID());
            //userDetails.put('UserSessionId', userinfo.getSessionId());
            userDetails.put('InstanceName',  [SELECT Id, InstanceName FROM Organization].InstanceName);
            userDetails.put('UserInfo',[ SELECT Id, Split_Permission__c, Merge_Permission__c, New_Case_Permission__c, Change_Owner_Permission__c, Profile.PermissionsTransferAnyEntity  FROM User where id=:userinfo.getuserid()][0]);
           
        } catch (Exception ex) {
            System.debug(ex.getStackTraceString());
        }
        return userDetails;
    }

    /* 
        Authors: Niraj P.
        Purpose: To get current login user id and Admin Session ID
        Dependencies : CaseTrackerHelper.js
    */
    public static string getSessionID() {
        String sessionId = '';
        try {
            AppConfig__c mhc = AppConfig__c.getInstance();
            String userName = mhc.UserName__c;
            String pwd = mhc.Password__c;
            HttpRequest request = new HttpRequest();
            request.setEndpoint('https://www.salesforce.com/services/Soap/u/40.0');
            request.setMethod('POST');
            request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
            request.setHeader('SOAPAction', '""');
            //not escaping username and password because we're setting those variables above
            //in other words, this line "trusts" the lines above
            //if username and password were sourced elsewhere, they'd need to be escaped below
            request.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/><Body><login xmlns="urn:partner.soap.sforce.com"><username>' + userName + '</username><password>' + pwd + '</password></login></Body></Envelope>');
            Dom.XmlNode resultElmt = (new Http()).send(request).getBodyDocument().getRootElement()
                .getChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/')
                .getChildElement('loginResponse', 'urn:partner.soap.sforce.com')
                .getChildElement('result', 'urn:partner.soap.sforce.com');

            //-------------------------------
            // Grab session id and server url
            //--------------------------------
            final String SERVER_URL = resultElmt.getChildElement('serverUrl', 'urn:partner.soap.sforce.com').getText().split('/services')[0];
            sessionId = resultElmt.getChildElement('sessionId', 'urn:partner.soap.sforce.com').getText();
        } catch (Exception ex) {
            System.debug(ex.getStackTraceString());
        }
        return sessionId;
    }
    /*
      Authors: Tushar Moradiya
      Purpose: To get all related Queue name based on logic user cases
      Dependencies: none.   
      Start - PUX-305 
     */
    @AuraEnabled
    public static List < GroupMember > getQueueDropdown() {
        List < GroupMember > queueList = new List < GroupMember > ();
        try {

            //Declaring a Set as we don't want Duplicate Group Ids
			Set<Id> results = new Set<Id>();

			///Declaring a Map for Group with Role
			Map<Id,Id> grRoleMap = new Map<Id,Id>();

			//Populating the Map with RelatedID(i.e.UserRoledId) as Key
			for(Group gr : [select id,relatedid,name from Group])
			{
				grRoleMap.put(gr.relatedId,gr.id);
			}

			//Groups directly associated to user
			Set<Id> groupwithUser = new Set<Id>();

			//Populating the Group with User with GroupId we are filtering only  for Group of Type Regular,Role and RoleAndSubordinates
			for(GroupMember  u :[select groupId from GroupMember where UserOrGroupId=: UserInfo.getUserID() and (Group.Type = 'Regular' OR Group.Type='Role' OR Group.Type='RoleAndSubordinates')])
			{
				groupwithUser.add(u.groupId);
			}

			//Groups with Role
			for(User  u :[select UserRoleId from User where id=:UserInfo.getUserID() ])
			{
				//Checking if the current User Role is part of Map or not
				if(grRoleMap.containsKey(u.UserRoleId))
				{
					results.add(grRoleMap.get(u.UserRoleId));
				}
			}
			//Combining both the Set
			results.addAll(groupwithUser);

			//Traversing the whole list of Groups to check any other nested Group
			Map<Id,Id> grMap = new Map<Id,Id>();
			for(GroupMember gr : [select id,UserOrGroupId,Groupid from GroupMember where
					(Group.Type = 'Regular' OR Group.Type='Role' OR Group.Type='RoleAndSubordinates')])
			{
				grMap.put(gr.UserOrGroupId,gr.Groupid);
			}
			for(Id i :results)
			{
				if(grMap.containsKey(i))
				{
					results.add(grMap.get(i));
				}
			}

			
			/*List < String > roleRelatedGroupIds = new List < String > ();
            List < Group > groupList = [SELECT id, RelatedId, Type FROM Group where RelatedId =: UserInfo.getUserRoleId() OR DeveloperName = 'AllInternalUsers'];
            for (Group g: groupList) {
                roleRelatedGroupIds.add(g.id);
            }
			*/
            set < string > groupIds = new set < string > ();
            queueList = [SELECT Group.Name, GroupId FROM GroupMember WHERE(UserOrGroupId =: UserInfo.getUserID() AND(Group.Type = 'Queue'
                    OR Group.Type = 'All Internal Users'))
                OR(UserOrGroupId IN: results AND(Group.Type = 'Queue'
                    OR Group.Type = 'All Internal Users'))
            ];
        } catch (Exception ex) {
            System.debug(ex.getStackTraceString());
        }
        return queueList;
    }
    /*End- PUX-305 */

    /*
        Authors: Rahul Pastagiya
        Purpose: To get all sort fields PUX-290
        Dependencies : CaseListHelper.js
    */
    @AuraEnabled
    public static List < map < String, String >> getSortFieldList() {
        List < map < String, String >> sortDropdownList = new List < map < String, String >> ();
        try {
            String objectType = ObjectUtil.getWithNameSpace('CaseManager__c');
            String fieldSet = PLMConstants.CASE_LIST_SORT_FIELD_SET;
            fieldSet=ObjectUtil.getValidFieldSetName(objectType,fieldSet); 
			Schema.FieldSet fieldSetObj = Schema.getGlobalDescribe().get(objectType).getDescribe().FieldSets.getMap().get(fieldSet);
            for (Schema.FieldSetMember fsm: fieldSetObj.getFields()) {
                Map < String, String > propMap = new Map < String, String > ();
                propMap.put('label', (String) fsm.getLabel());
                propMap.put('value', (String) fsm.getFieldPath());
                sortDropdownList.add(propMap);
            }
        } catch (Exception ex) {
            System.debug(ex.getStackTraceString());
        }
        return sortDropdownList;
    }
    //To get all sort fields PUX-290 

    /*
      Authors: Tushar Moradiya
      Purpose: To get fieldset for filter dropdown
      Dependencies: none.   
      Start - PUX-305 
     */
    @AuraEnabled
    public static List < MapWrapper > getFieldDropdown() {
        List < MapWrapper > lstfieldname = new List < MapWrapper > ();
        try {
            String objectType = ObjectUtil.getWithNameSpace('CaseManager__c');
            String fieldSet =PLMConstants.CASE_LIST_FILTER_FIELD_SET;
            fieldSet=ObjectUtil.getValidFieldSetName(objectType,fieldSet); 
            MapWrapper listKeyVal;
            Schema.FieldSet fieldSetObj = Schema.getGlobalDescribe().get(objectType).getDescribe().FieldSets.getMap().get(fieldSet);
            for (Schema.FieldSetMember fsm: fieldSetObj.getFields()) {
                listKeyVal = new MapWrapper();
                listKeyVal.key = fsm.getFieldPath();
                listKeyVal.value = fsm.getLabel();
                listKeyVal.type = String.valueOf(fsm.getType());
                lstfieldname.add(listKeyVal);
            }
        } catch (Exception ex) {
            System.debug(ex.getStackTraceString());
        }
        //lstfieldname.put
        return lstfieldname;
    }

    /*
      Authors: Tushar M.
      Purpose: Wrapper class for Filter field dropdown.
      Dependencies: used in method 'getFieldDropdown'. PUX-305
     */
    class MapWrapper {
        @AuraEnabled
        public String key;
        @AuraEnabled
        public String value;
        @AuraEnabled
        public String type;
    }
    /*End- PUX-305 */

    /*
        Authors: Rahul Pastagiya
        Purpose: To wrap pagination related values
        Dependencies : CaseListHelper.js
    */
    public class CasePagerWrapper {
        @AuraEnabled public Integer pageSize {get;set;}
        @AuraEnabled public Integer page {get;set;}
        @AuraEnabled public List<String> fieldSetKeyList {get;set;}
        @AuraEnabled public Integer total {get;set;}
        @AuraEnabled public List < CaseManager__c > cases {get;set;}
        @AuraEnabled public Map<String, String> keyFieldLabelMap;
    }
}