/**
  * Authors		: Tushar Moradia
  * Date created : 16/11/2017 
  * Purpose      : Test Class for  CaseListViewControllerTest
  * Dependencies : CaseListViewController.cls
  * -------------------------------------------------
  *Modifications:
                Date:   
                Purpose of modification:  
                Method/Code segment modified: 

*/
@isTest
public class CaseListViewControllerTest {
	/*
    	Authors: Tushar Moradiya
    	Purpose: Code Coverage
		Dependencies : CaseListViewController.cls
	*/
    public static testMethod void testFirst(){
        //set mock callout class
        //Test.setMock(HttpCalloutMock.class, new RequestDataFromAWSMock());
        CaseManager__c caseObj=new CaseManager__c();
		caseObj.Process__c='AP';
		caseObj.WorkType__c='PO Invoices';
		caseObj.Document_Type__c='Electricity';
		caseObj.Requestor_Email__c='Tushar@hello.com';
		caseObj.Amount__c=12000;
		caseObj.Status__c='Ready for processing';
		caseObj.escalation__c=true;
		insert caseObj;
		TestDataGenerator.createEmailMessage(caseObj);
		CaseManager__c caseObjNew=new CaseManager__c();
		caseObjNew.Process__c='AP';
		caseObjNew.WorkType__c='PO Invoices';
		caseObjNew.Document_Type__c='Electricity';
		caseObjNew.Requestor_Email__c='Moradiya@hello.com';
		caseObjNew.Amount__c=2000;
		caseObjNew.Status__c='Ready for processing';
		caseObj.escalation__c=true;
		insert caseObjNew;
        
        Map<String,String> mp=new Map<String,String>();
        mp.put( ObjectUtil.getPackagedFieldName('Process__c'),'Ap');
        mp.put('createddate','2017-10-10');
        List<String> list1= new List<String>();
        list1.add('asd');
        CaseListViewController.getCases('My Cases', 1, 15, ObjectUtil.getPackagedFieldName('Process__c'), 'desc', new List<String>(), mp, 'Overdue');
        CaseListViewController.getCases('Unassigned', 1, 15, ObjectUtil.getPackagedFieldName('Process__c'), 'desc', new List<String>(), mp,'Overdue');
        CaseListViewController.getCases('Closed', 1, 15, ObjectUtil.getPackagedFieldName('Process__c'), 'desc', new List<String>(), mp,'Overdue');
        CaseListViewController.getCases('Shared Cases', 1, 15, ObjectUtil.getPackagedFieldName('Process__c'), 'desc', new List<String>(), mp,'Overdue');
        CaseListViewController.getCases(null, 1, 15, ObjectUtil.getPackagedFieldName('Process__c'), 'desc', list1, mp,'Overdue2');
        CaseListViewController.getUserInfo();
        CaseListViewController.getSessionID();
        CaseListViewController.getQueueDropdown();
        CaseListViewController.getSortFieldList();
        CaseListViewController.getFieldDropdown(); 
        CaseListViewController.CasePagerWrapper abc= new CaseListViewController.CasePagerWrapper();
        abc.pageSize=2;
        abc.page=1;
        abc.cases=null;
    }
}