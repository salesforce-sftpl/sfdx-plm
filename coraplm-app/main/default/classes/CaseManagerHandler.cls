/*
  Authors	   :   Rahul Pastagiya
  Date created :   09/01/2018
  Purpose      :   Helper class for Case Manager object
  Dependencies :   NA
  __________________________________________________________________________
  Modifications:
		    Date:  
		    Purpose of modification:  
		    Method/Code segment modified: 
 */
public class CaseManagerHandler {
     /*
    	Authors: Rahul Pastagiya
    	Purpose: To update pervious queue when any user is assigned to case
		Dependencies : NA
	*/
    public static void updatePrevQueue(List<CaseManager__c> newCaseTracker, List<CaseManager__c> oldCaseTracker){
        Set<Id> ownerIds = new Set<Id> ();
        //To get all cases owner
        for (CaseManager__c ct : oldCaseTracker) {
            if (ct.OwnerId != null) {
                ownerIds.add(ct.OwnerId);
            }
        }
        
        //Logic for Previous Queue and Current Owner       
        Map<Id, Group> groups = new Map<Id, Group> ([SELECT Id, DeveloperName FROM Group where type = 'Queue' and Id in :ownerIds]);
        for (Integer i = 0; i < newCaseTracker.size(); i++) {
            Id newOwnerId = newCaseTracker[i].OwnerId;
            Id currentOwnerId = oldCaseTracker[i].OwnerId;
            if (newOwnerId != currentOwnerId) {
                if (string.valueOf(newOwnerId).startsWith('00G')) {
                    newCaseTracker[i].PreviousQueueName__c = null;
                } else if (string.valueOf(currentOwnerId).startsWith('00G') && string.valueOf(newOwnerId).startsWith('005')) {
                    newCaseTracker[i].PreviousQueueName__c = String.valueOf((groups.get(currentOwnerId)).get('DeveloperName'));
                }
            }
        }
    }
    
     /*
    	Authors: Niraj Prajapati
    	Purpose: To populate report related fields
		Dependencies : NA
	*/
    public static void populateReportRelatedFields(List<CaseManager__c> newCaseTracker, List<CaseManager__c> oldCaseTracker){
        //Code Start for Populate Report related Fields - Niraj Prajapati
        //Logic for update previous performer on every status change
        for (Integer i = 0; i < newCaseTracker.size(); i++) {
            Id newOwnerId = newCaseTracker[i].OwnerId;
            Id currentOwnerId = oldCaseTracker[i].OwnerId;
            if((newCaseTracker[i].status__c != oldCaseTracker[i].status__c) ){
                newCaseTracker[i].CurrentStatusTime__c = System.now();
                if((newCaseTracker[i].status__c != 'Start' || newCaseTracker[i].status__c !='') && (String.valueOf(currentOwnerId).startsWith('005')))
                    newCaseTracker[i].PreviousPerformer__c = oldCaseTracker[i].OwnerId;
            }
            //System.debug('*****'+String.valueOf(newOwnerId).startsWith('005'));
            if((newCaseTracker[i].status__c == 'Ready For Processing') && (String.valueOf(newOwnerId).startsWith('005')) && (String.isempty(oldCaseTracker[i].FirstTouch__c) || oldCaseTracker[i].FirstTouch__c==null) ){
                newCaseTracker[i].FirstTouch__c =  userinfo.getuserid();
                /*Code Start for PUX-464 */
                newCaseTracker[i].FirstTouchDate__c = System.now();
                /*Code End for PUX-464 */ 
            }
            //Logic for update Rejected Date
            if((newCaseTracker[i].UserAction__c != oldCaseTracker[i].UserAction__c) && (newCaseTracker[i].UserAction__c=='Reject' || newCaseTracker[i].UserAction__c=='Rejected')   ){
                newCaseTracker[i].RejectedDate__c = System.now();
            }
            //Logic for update Process Date,First Pass Yield and Original Processer
            if(newCaseTracker[i].status__c == 'Ready For Processing' && newCaseTracker[i].UserAction__c=='Process' && (newCaseTracker[i].UserAction__c != oldCaseTracker[i].UserAction__c)){
                newCaseTracker[i].ProcessedDate__c = System.today();
                if(oldCaseTracker[i].FirstPassYield__c=='' || oldCaseTracker[i].FirstPassYield__c == null)
                    newCaseTracker[i].FirstPassYield__c = 'Y';
                if(String.isempty(oldCaseTracker[i].ProcessorName__c) || oldCaseTracker[i].ProcessorName__c == null)
                    newCaseTracker[i].ProcessorName__c =  userinfo.getuserid();
                if(String.isempty(oldCaseTracker[i].OriginalProcessor__c) || oldCaseTracker[i].OriginalProcessor__c == null)
                    newCaseTracker[i].OriginalProcessor__c =  userinfo.getuserid();
            }
            if(newCaseTracker[i].status__c == 'Ready For Processing' && newCaseTracker[i].UserAction__c!='Process' && (newCaseTracker[i].UserAction__c != oldCaseTracker[i].UserAction__c)){
                //newCaseTracker[i].ProcessedDate__c = System.today();
                if(oldCaseTracker[i].FirstPassYield__c=='' || oldCaseTracker[i].FirstPassYield__c == null)
                    newCaseTracker[i].FirstPassYield__c = 'N';
                if(String.isempty(oldCaseTracker[i].ProcessorName__c) || oldCaseTracker[i].ProcessorName__c == null)
                    newCaseTracker[i].ProcessorName__c =  userinfo.getuserid();
                if(String.isempty(oldCaseTracker[i].OriginalProcessor__c) || oldCaseTracker[i].OriginalProcessor__c == null)
                    newCaseTracker[i].OriginalProcessor__c =  userinfo.getuserid();
            }
            //Logic for update isQCed,QCStatusand QCPerformer fields
            if(newCaseTracker[i].status__c == 'Ready For QC' ){
                if(String.isempty(oldCaseTracker[i].QCPerformer__c) || oldCaseTracker[i].QCPerformer__c == null)
                    newCaseTracker[i].QCPerformer__c =  userinfo.getuserid();
                if(oldCaseTracker[i].QCStatus__c=='' || oldCaseTracker[i].QCStatus__c == null){
                    if(newCaseTracker[i].UserAction__c == 'QC Complete')
                        newCaseTracker[i].QCStatus__c = 'Pass';
                    else if(newCaseTracker[i].UserAction__c == 'QC Reject'){
                        newCaseTracker[i].QCStatus__c = 'Fail';
                        newCaseTracker[i].RejectorName__c = userinfo.getuserid();
                    }
                }
                if(oldCaseTracker[i].IsQCed__c=='' || oldCaseTracker[i].IsQCed__c == null){
                    newCaseTracker[i].IsQCed__c = 'Yes';
                }      
            }
        }
        //Code End for Populate Report related Fields - Niraj Prajapati
    }
}