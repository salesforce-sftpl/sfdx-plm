/**
* Authors    : Atul Hinge
* Date created : 09/01/2018
* Purpose      : Test Class for CaseManagerTrigger
* Dependencies : -
* -------------------------------------------------
*Modifications:
Date:   
Purpose of modification:  
Method/Code segment modified: 

*/
@isTest
public class CaseManagerTriggerTest {
    public static testMethod void testCaseServiceMethods(){
        //set mock callout class
        //Test.setMock(HttpCalloutMock.class, new RequestDataFromAWSMock());
        CaseManager__c ct =TestDataGenerator.createCaseTracker();
        ct.Amount__c=1;
        ct.status__c = 'Ready For QC';
        ct.UserAction__c = 'QC Complete';
        update ct;
        CaseManager__c ct1 =TestDataGenerator.createCaseTracker();
        String interId=TestDataGenerator.createEmailMessage(ct1).Id;
        List<CaseManager__c> resp1=CaseService.getCaseById(ct1.Id);
        List<CaseManager__c> resp2=CaseService.getCaseByIdWithInteraction(ct1.Id);
        ct1.status__c = 'Ready For Processing';
        ct1.UserAction__c='Process';
        update ct1;
    }
}