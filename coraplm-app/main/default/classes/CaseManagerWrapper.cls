public class CaseManagerWrapper {
    @AuraEnabled
	public Id id;
    @AuraEnabled
    public String name;
    @AuraEnabled
    public String process;
    @AuraEnabled
    public String ownerName;
    @AuraEnabled
    public boolean favourited;
    @AuraEnabled
    public boolean escalation;
    @AuraEnabled
    public String overdueIn;
    @AuraEnabled
    public String status;
    @AuraEnabled
    public String priority;
    @AuraEnabled
    public String indicator;
    @AuraEnabled
    public String subject;
    @AuraEnabled
    public Decimal unreadEmailCount;
    @AuraEnabled
    public List<EmailMessage> emails;
}