/*
* Authors      :  Atul Hinge
* Date created :  18/08/2017
* Purpose      :  used as a abstraction layer over CaseTracker object.
* Dependencies :  SobjectSelector.cls
* JIRA ID      :  None
* -----------------------------------------------------------
* Modifications: 
*        Date:  
*        Purpose of modification:  
*        Method/Code segment modified:    
*/

public with sharing class CaseSelector extends SobjectSelector{
    static final String userId = UserInfo.getUserId();
    /*
        Authors: Atul Hinge
        Purpose: initialize with default config 
    */
    public CaseSelector(){
        super.addRelationShipFields(new Set<String>{'Contact__r.Name','RecordType.Name'});
    }
    /*
        Authors: Atul Hinge
        Purpose: initialize with Relational fields and subQueries 
    */
    public CaseSelector(Set<String> relationShipFields,Set<String> relationShipQueries){
        if(relationShipFields !=null && relationShipFields.size() != 0){
            super.addRelationShipFields(relationShipFields);
        }
        if(relationShipQueries !=null && relationShipQueries.size() != 0){
            super.addRelationShipQueries(relationShipQueries);
        }
    }
    
    /*
        Authors: Atul Hinge
        Purpose: implementation of getSObjectType for CaseManager__c
    */
    public Schema.SObjectType getSObjectType(){
        return CaseManager__c.sObjectType;
    }
    /*
        Authors: Atul Hinge
        Purpose: Select Cases by using Ids
    */
    public List<CaseManager__c> selectCasesByIds(Set<Id> idSet){
        return (List<CaseManager__c>)super.selectSObjectsById(idSet);
    }
    /*
        Authors: Atul Hinge
        Purpose: Select Cases by using clause
    */
    public List<CaseManager__c> selectCasesByClause(String clause){
        return (List<CaseManager__c>)super.selectSObjectsByClause(clause);
    }
    /*
        Authors: Atul Hinge
        Purpose: Select Shared Cases
    */
    public List<CaseManager__c> selectSharedCases(String whereClause){
        
        String clause=' OwnerId != \''+CaseSelector.userId+'\'and '+ObjectUtil.getPackagedFieldName('PreviousQueueName__c')+'!= null ';
        clause=(String.isBlank(whereClause))?clause:clause+whereClause;
        return (List<CaseManager__c>)super.selectSObjectsByClause(clause);
    }
    /*
        Authors: Atul Hinge
        Purpose: Select Closed Cases
    */
    public List<CaseManager__c> selectClosedCases(String whereClause){
		string statusCompleted='Completed';
        String clause=' OwnerId =\''+CaseSelector.userId+'\' and '+ObjectUtil.getPackagedFieldName('status__c')+' =\'Completed\'' ;
        clause=(String.isBlank(whereClause))?clause:clause+whereClause;
        return (List<CaseManager__c>)super.selectSObjectsByClause(clause);
    }
    /*
        Authors: Atul Hinge
        Purpose: Select My Task Cases
    */
    public List<CaseManager__c> selectMyTaskCases(String whereClause){
		string statusCompleted='Completed';
        String clause=' OwnerId =\''+CaseSelector.userId+'\' and status__c !=\'Completed\'';
        clause=(String.isBlank(whereClause))?clause:clause+whereClause;
        return (List<CaseManager__c>)super.selectSObjectsByClause(clause);
    }
    /*
        Authors: Atul Hinge
        Purpose: Select Unassigned Cases
    */
    public List<CaseManager__c> selectUnassignedCases(String whereClause){
		 
        String clause=' '+ObjectUtil.getPackagedFieldName('PreviousQueueName__c')+'!= null';
        clause=(String.isBlank(whereClause))?clause:clause+whereClause;
        return (List<CaseManager__c>)super.selectSObjectsByClause(clause);
    }
    /*
        Authors: Atul Hinge
        Purpose: Select All Cases
    */
    public List<CaseManager__c> selectAllCases(){
        return (List<CaseManager__c>)super.selectSObjectsByAll();
    }
    /*
        Authors: Atul Hinge
        Purpose: add Sub Queries
    */
    public void addRelationShip(Set<String> relationShipNames){
         super.addRelationShipQueries(relationShipNames);
    }
}