/*
* Authors      :  Atul Hinge
* Date created :  18/08/2017
* Purpose      :  All Methods To retrive case and case releated entities.
                  All business logic releated to CaseTracker should be written here
* Dependencies :  SobjectSelector.cls
* JIRA ID      :  None
* -----------------------------------------------------------
* Modifications: 
*        Date:  
*        Purpose of modification:  
*        Method/Code segment modified:    
*/
public class CaseService {
     public class CaseUpdateResponse{
		 @AuraEnabled
		public string ErrorMessage{
			get;set;
		}
		 @AuraEnabled
		public List<CaseManager__c> Cases{
			get;set;
		}

		@AuraEnabled
		public List<string> FieldNames{
			get;set;
		}
	}
	 /*
        Authors: Atul Hinge
        Purpose: Method for selecting files releated to a perticuler case
    */
    public static List<ContentDocumentLink> getAttachmentLinks(String caseId){
        List<ContentDocumentLink> contentDocumentLinks=new List<ContentDocumentLink>();
        String errorMessage='The following exception has occurred: ';
        try{
            contentDocumentLinks=[SELECT  ContentDocument.Owner.Name,ContentDocument.LatestPublishedVersion.Flag__c,ContentDocument.Id,ContentDocument.Title,ContentDocument.FileType,ContentDocument.LatestPublishedVersionId, LinkedEntityId, Id,ContentDocument.LastModifiedDate,ContentDocument.FileExtension,ContentDocument.ContentSize FROM ContentDocumentLink where LinkedEntityId=:caseId order by SystemModstamp desc];
        } catch (Exception e) {
            ExceptionHandlerUtility.writeException('CaseService', e.getMessage(), e.getStackTraceString());
    }
    
        return contentDocumentLinks;
       
    }
     /*
        Authors: Atul Hinge
        Purpose: Method for selecting files releated to a perticuler case
    */
    public static List<QualityCheck__c> getQualityControls(String caseId,String fsName){
        List<QualityCheck__c> qualityControls=new List<QualityCheck__c>();
        String errorMessage='The following exception has occurred: ';
        try{
            String queryString = 'select id';
            System.debug('#########'+SObjectType.QualityCheck__c.FieldSets.getMap());
            Map<String,Object> fieldMap=new  Map<String,Object>();
            for(Schema.FieldSetMember fld :SObjectType.QualityCheck__c.FieldSets.getMap().get(fsName).getFields()) {
                queryString += ', ' + fld.getFieldPath();
            }
			
            queryString += ' from '+ObjectUtil.getPackagedFieldName('QualityCheck__c')+' where '+ObjectUtil.getPackagedFieldName('CaseManager__c')+'=:caseId order by createddate desc';
            qualityControls=Database.query(queryString);
        } catch (Exception e) {
            ExceptionHandlerUtility.writeException('CaseService', e.getMessage(), e.getStackTraceString());
        } 
        return qualityControls;
    }
    /*
        Authors: Atul Hinge
        Purpose: Method for select cases by using Id along with Histories and Emails
    */
    public static List<CaseManager__c> getCaseById(String caseId){
        List<CaseManager__c> caseTrackerList =new List<CaseManager__c>(); 
        String errorMessage='The following exception has occurred: ';
        try{
            CaseSelector cs =new CaseSelector();
            cs.addRelationShip(new Set<String>{'Histories','Emails'});
            caseTrackerList =cs.selectCasesByIds(new Set<Id>{caseId});
        }  catch (Exception e) {
            ExceptionHandlerUtility.writeException('CaseService', e.getMessage(), e.getStackTraceString());
        } 
        return caseTrackerList;  
    }
    /*
        Authors: Atul Hinge
        Purpose: Method for select cases by using Id along with Emails ('Interaction')
    */
    public static List<CaseManager__c> getCaseByIdWithInteraction(String caseId){
        List<CaseManager__c> caseTrackerList =new List<CaseManager__c>(); 
        String errorMessage='The following exception has occurred: ';
        try{
            CaseSelector cs =new CaseSelector();
            cs.addRelationShip(new Set<String>{'Emails'});
            caseTrackerList = cs.selectCasesByIds(new Set<Id>{caseId});
        } catch (Exception e) {
            ExceptionHandlerUtility.writeException('CaseService', e.getMessage(), e.getStackTraceString());
        } 
        return caseTrackerList;  
    }
    /*
        Authors: Atul Hinge
        Purpose: Method for updating cases
    */
    public static CaseUpdateResponse updateCases(List<CaseManager__c> cases){
        List<CaseManager__c> caseTrackerList =new List<CaseManager__c>(); 
        String errorMessage='The following exception has occurred: ';
		CaseUpdateResponse caseUpdateResponseObj=new CaseUpdateResponse();

        try{
            // SecurityHelper.CheckObjectAccess('CaseManager__c',new List<Set>(){'Edit'});
            Set<Id> caseIds=new Set<Id>();
            list<CaseManager__c> updateCase = new list<CaseManager__c>();
            
            //JIRA: PUX-459 | Tej Pal Kumawat
            list<Comments__c> newComments = new list<Comments__c>();
            for(CaseManager__c ct: cases){
                if(ct.Comments__c != null && ct.Comments__c != ''){
                    newComments.add(new Comments__c(CaseManager__c = ct.Id, Comments__c = ct.Comments__c));
                    ct.Comments__c = '';
                }
                caseIds.add(ct.Id);
                updateCase.add(ct);
            }
           
            update updateCase;
           system.debug('##########updateCase'+updateCase);
            //JIRA: PUX-459 | Tej Pal Kumawat
            if(newComments.size() > 0){
                insert newComments;
            }
       
            CaseSelector cs =new CaseSelector();
            cs.addRelationShip(new Set<String>{'Emails'});
            caseTrackerList = cs.selectCasesByIds(caseIds);
			caseUpdateResponseObj.cases=caseTrackerList;
            
        } catch(DMLException ex){
            system.debug('##########'+ex);
			caseUpdateResponseObj.ErrorMessage=ex.getdmlMessage(0);
			caseUpdateResponseObj.FieldNames=ex.getDmlFieldNames(0);
		}
		catch (Exception e) {
            ExceptionHandlerUtility.writeException('CaseService', e.getMessage(), e.getStackTraceString());
        }
        return caseUpdateResponseObj;  
    }
    
    /*Code Start For PUX-295 */
    /*
        Authors: Niraj Prajapati
        Purpose: Method for updating Owner ID of the case
    */
    
    public static List<CaseManager__c> updateCaseOwner(List<CaseManager__c> cases){
        List<CaseManager__c> caseTrackerList =new List<CaseManager__c>(); 
        String errorMessage='The following exception has occurred: ';
        try{
           // SecurityHelper.CheckObjectAccess('CaseManager__c',new List<Set>(){'Edit'});
           List<CaseManager__c> caseLst = new List<CaseManager__c>();
            for(CaseManager__c ct:Cases)
            {
                caseLst.add(new CaseManager__c(id=ct.Id,ownerId=ct.ownerId));
                //System.debug('ct.ownerId'+ct.ownerId);
            }
           update caseLst;
           
            Set<Id> caseIds=new Set<Id>();
            for(CaseManager__c ct:cases){
                caseIds.add(ct.Id);
            }
            CaseSelector cs =new CaseSelector();
            caseTrackerList = cs.selectCasesByIds(caseIds);
        } catch (Exception e) {
            ExceptionHandlerUtility.writeException('CaseService', e.getMessage(), e.getStackTraceString());
        }
        return caseTrackerList;  
    }
    /*Code End For PUX-295 */
    /*
        Authors: Chandresh Koyani
        Purpose: create and add attachment to a case.
        Dependencies: 
    */
    
    //Bulkified Method to add attachments - added by ashish on 13-Nov-2017
    public static Map<String,ContentDocumentLink> addAttachments(Map<string,string> fileTypeBlobMap,CaseManager__c ct) { 
        Map<String,ContentDocumentLink> imageContentDocID = new Map<String,ContentDocumentLink>();
        List<ContentVersion> contentVersionList = new List<ContentVersion>();
        Integer i = 1;
        for(String fileType : fileTypeBlobMap.keySet()){
            ContentVersion cont = new ContentVersion();
            cont.Title = 'file'+i+'.png';
            cont.PathOnClient = 'file'+i+'.png';
            cont.VersionData = EncodingUtil.base64Decode(fileTypeBlobMap.get(fileType));
            cont.Origin = 'H';
            cont.IsMajorVersion =true;
            contentVersionList.add(cont);
            i++;
        }
        
        insert contentVersionList;
        Set<Id> contentVersionIDs = new Set<Id>();
        for(ContentVersion cv :contentVersionList){
            contentVersionIDs.add(cv.id);
        }

        for(ContentVersion tempContent : [SELECT id, ContentDocumentId,Title FROM ContentVersion where Id IN :contentVersionIDs]){
            ContentDocumentLink cntent=new ContentDocumentLink ();
            cntent.ContentDocumentId=tempContent.ContentDocumentId;
            cntent.LinkedEntityId=ct.Id;
            cntent.ShareType= 'V';
            cntent.Visibility='AllUsers';
            imageContentDocId.put(tempContent.Title, cntent);
        }
        insert imageContentDocId.values();
        return imageContentDocId;
    }
    
    /*
    Authors: Tejas Patel
    Purpose: PUX-340: Method for Add Note using EmailMessage
    Dependencies: None
    */
    public void addNoteFromService(EmailMessage em) {    
        em.FromAddress   = UserInfo.getUserEmail();
        em.FromName      = UserInfo.getName();      
        em.Is_Note__c    = true;
        insert em;
    }
    
}