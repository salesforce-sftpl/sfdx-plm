/**
* Authors    : Atul Hinge
* Date created : 16/11/2017 
* Purpose      : Test Class for EmailController
* Dependencies : -
* -------------------------------------------------
*Modifications:
Date:   
Purpose of modification:  
Method/Code segment modified: 

*/
@isTest
public class CaseServiceTest {
    public static testMethod void testGetAttachmentLinks(){
        CaseManager__c ct =TestDataGenerator.createCaseTracker();
        String interId=TestDataGenerator.createEmailMessage(ct).Id;
        List<ContentDocumentLink> resp1=CaseService.getAttachmentLinks(ct.Id);
        System.runAs(TestDataGenerator.createUser()) {
           List<ContentDocumentLink> resp2=CaseService.getAttachmentLinks(ct.Id);
         }
    }
    public static testMethod void testGetCaseById(){
        CaseManager__c ct =TestDataGenerator.createCaseTracker();
        String interId=TestDataGenerator.createEmailMessage(ct).Id;
        List<CaseManager__c> resp1=CaseService.getCaseById(ct.Id);
        List<CaseManager__c> resp2=CaseService.getCaseByIdWithInteraction(ct.Id);
        ct.status__c = 'Ready For Processing';
        ct.UserAction__c='Process';
        update ct;
        Comments__c c = new Comments__c();
        c.Comments__c ='test';
        c.CaseManager__c = ct.id;
        insert c;
        CaseAdditionalDetails.getCaseComments(ct.id);
    }
     public static testMethod void testUpdateCases(){
        CaseManager__c ct =TestDataGenerator.createCaseTracker();
        ct.Amount__c=1;
        /*
        List<CaseManager__c> resp1=CaseService.updateCases(new List<CaseManager__c>{ct});
        ct.status__c = 'Ready For Processing';
        ct.UserAction__c='On Hold';
        update ct;
        */
    }
    public static testMethod void testCaseServiceMethods(){
        CaseManager__c ct =TestDataGenerator.createCaseTracker();
        
        ct.Amount__c=1;
        TestDataGenerator.createAppConfig();
            
        CaseService.getQualityControls(ct.Id,'QC');
        Map<string,string> fileTypeBlobMap = new Map<string,string>();
        fileTypeBlobMap.put(ct.Id, 'TestBlob');
        CaseService.addAttachments(fileTypeBlobMap,ct);
        new CaseService().addNoteFromService(new EmailMessage());
        ct.status__c = 'Ready For QC';
        ct.UserAction__c = 'QC Complete';
        update ct;
    }
     public static testMethod void testCaseSelectorMethods(){
         CaseManager__c ct =TestDataGenerator.createCaseTracker();
         CaseSelector cs =new CaseSelector();
         cs.addRelationShip(new Set<String>{'Emails'});
         cs.selectCasesByIds(new Set<Id>{ct.Id});
        cs.selectCasesByClause(' (Id != null) ');
         cs.selectSharedCases('');
         cs.selectClosedCases('');
         cs.selectMyTaskCases('');
         cs.selectUnassignedCases('');
         CaseSelector cs1 =new CaseSelector(new Set<String>{'Contact__r.Name'},new Set<String>{'Emails'});
         cs1.selectAllCases();
    }
    
}