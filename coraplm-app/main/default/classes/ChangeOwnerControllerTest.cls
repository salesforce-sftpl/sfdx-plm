@isTest(seeAllData=false)
public class ChangeOwnerControllerTest{
    
    public static testMethod void unitTest01(){
        // Set mock callout class 
       // Test.setMock(HttpCalloutMock.class, new RequestDataFromAWSMock());
        
         Profile p=[SELECT Id From Profile WHERE Name='System Administrator'];
         User u2 =new User( Alias = 'myuser11' ,
                            Email ='myuser11@testorg.com',
                            EmailEncodingKey = 'UTF-8',
                            LastName = 'Testing',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US',
                            UserName='myuser@testorg.com',
                            ProfileId=p.Id,
                            TimeZoneSidKey    = 'America/Los_Angeles');
         insert u2;
        //String userName=UserInfo.getUserName();
        //List<User> userList  = new List<User>();
       	//userList =  Database.query('Select Id,Name,SmallPhotoUrl from User where isActive = true and name !=:' + userName);
        //System.debug('userList@@' +userList);
        List<CaseManager__c> ctList= TestDataGenerator.createCases(4);
        Test.startTest();
        List<User> users = ChangeOwnerController.userDetail(u2.Id, new List<String>{ctList[2].Id, ctList[3].Id});
        List<Group> groups = ChangeOwnerController.queueData(u2.Id, new List<String>{ctList[0].Id, ctList[1].Id});
        Test.stopTest();
        System.debug(users);
        
    }
    
    public static testMethod void unitTest02(){
        //User user = TestDataGenerator.createUser();
        ///Test.startTest();
       // List<User> users = ChangeOwnerController.userDetail(user.Id, null);
       // Test.stopTest();
        
    }
    
}