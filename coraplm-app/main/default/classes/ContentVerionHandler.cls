/*
  Authors	   :   Niraj Prajapati
  Date created :   29/01/2018
  Purpose      :   Helper class for Content Version object
  Dependencies :   NA
  __________________________________________________________________________
  Modifications:
		    Date:  
		    Purpose of modification:  
		    Method/Code segment modified: 
 */
public class ContentVerionHandler {
     /*
    	Authors: Niraj Prajapati
    	Purpose: To update Flag of Attachment
		Dependencies : NA
	*/
    public static void updateFlag(List<ContentVersion> newContentVersion, List<ContentVersion> oldContentVersion){
        AppConfig__c objApp= AppConfig__c.getInstance();
        Decimal allowedSize= objApp.Allow_Image_Size__c*1000;
         
        Set<String> ext = new Set<String>(objApp.Content_Version_Extension__c.split(','));
        String extension = '';
        for (Integer i = 0; i < newContentVersion.size(); i++) {
           if(newContentVersion[i].Title.split('\\.').size() > 1 )
               extension= newContentVersion[i].Title.split('\\.')[1].toLowerCase();
           
           if(ext.contains(extension))
           {    
               
                newContentVersion[i].flag__c='Flag';
           }else if((extension == 'jpg' || extension == 'png') && newContentVersion[i].VersionData.size()>allowedSize)
           {
                   newContentVersion[i].flag__c='Flag';
           }else{
               newContentVersion[i].flag__c='Non Flag';
           }
       }
        
    }

}