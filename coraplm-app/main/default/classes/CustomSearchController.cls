/*
* Authors	   :  Ashish Kumar
* Date created :  17/11/2017
* Purpose      :  For search functionality.
* Dependencies :  None
* JIRA ID      :  
* -----------------------------------------------------------
* Modifications:
*        Date:  
*        Purpose of modification:  
*        Method/Code segment modified:    
*/
public with sharing class CustomSearchController {
    
    /*
Authors: Ashish Kumar
Purpose: use to find the search text in casetracker, casetransition, comment object
Dependencies:  Header.cmp.   
*/
    @AuraEnabled
    public static List<String> searchForIds(String searchText) {
        List<String> ids = new List<String>();
        try{
            List<List<SObject>> results = [FIND :searchText IN ALL FIELDS  RETURNING CaseManager__c(Id)];
            for (List<SObject> sobjs : results) {
                for (SObject sobj : sobjs) {
                    ids.add(sobj.Id);
                }
            }
        }catch(Exception ex) {
            ExceptionHandlerUtility.writeException('CustomSearchController', ex.getMessage(), ex.getStackTraceString());
        }
        return ids;
    }
}