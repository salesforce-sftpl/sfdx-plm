/**
  * Authors		: Ashish Kumar
  * Date created : 04/12/2017 
  * Purpose      : Test Class for PinnedCaseController
  * Dependencies : CustomSearchController.apxc
  * -------------------------------------------------
  * Modifications:
    Date:   
    Purpose of modification:  
    Method/Code segment modified: 

*/
@isTest
public class CustomSearchControllerTest {
        
    	/*
        Authors: Ashish Kumar
        Purpose: Code Coverage
        Dependencies : CustomSearchController.cls
    	*/
        public static testMethod void testSearchForIds(){
            
            CaseManager__c caseObj=new CaseManager__c();
            caseObj.Process__c='AP';
            caseObj.WorkType__c='PO Invoices';
            caseObj.Document_Type__c='Electricity';
            caseObj.Requestor_Email__c='testEmail@hello.com';
            caseObj.Amount__c=12000;
            caseObj.Status__c='Ready for processing';
            caseObj.escalation__c=true;
            caseObj.Favourited__c = true;
            insert caseObj;  
            
            Test.startTest();
            CustomSearchController.searchForIds('AP');
            Test.stopTest();
        }   
    	/*
        Authors: Ashish Kumar
        Purpose: Code Coverage
        Dependencies : CustomSearchController.cls
    	*/
        public static testMethod void testSearchForIdsException(){
            
            CaseManager__c caseObj=new CaseManager__c();
            caseObj.Process__c='AP';
            caseObj.Subject__c = 'Ap Case';
            caseObj.WorkType__c='PO Invoices';
            caseObj.Document_Type__c='Electricity';
            caseObj.Requestor_Email__c='testEmail@hello.com';
            caseObj.Amount__c=12000;
            caseObj.Status__c='Ready for processing';
            caseObj.escalation__c=true;
            caseObj.Favourited__c = true;
            insert caseObj;  
            
            Test.startTest();
            CustomSearchController.searchForIds(caseObj.Name);
            Test.stopTest();
        }   
}