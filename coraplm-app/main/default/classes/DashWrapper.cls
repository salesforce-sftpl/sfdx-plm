public Class DashWrapper{
        @AuraEnabled
        public Map<String,Object> dashMap;
        
        public DashWrapper(){
            dashMap=new Map<String,Object>();
        }
        
        public void setDashMap(Map<String,Object> dashDet){
            this.dashMap=dashDet;
        }
        
        public Map<String,Object> getDashMap(){
            return this.dashMap;
        }
    }