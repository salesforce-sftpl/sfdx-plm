/*
 * Authors       : Parth Lukhi
 * Date created : 17/08/2017 
 * Purpose      : This DashboardController is used for getting Dashboard Details for PLM 
 *  i.e. for getting Total Cases,Assigned Cases, Pending Cases,Overdue cases, Chart Component and 
 *  Leaderboard on Dashboard
 * Dependencies :  NA
 * -------------------------------------------------
 * Modifications:
  Date:  
  Purpose of modification:  
  Method/Code segment modified:

 */
public with sharing class DashboardController {
	public static final Map<string, string> monthmap;
	static {
		monthmap = new Map<string, string> ();
		monthmap.put('1', 'Jan');
		monthmap.put('2', 'Feb');
		monthmap.put('3', 'Mar');
		monthmap.put('4', 'Apr');
		monthmap.put('5', 'May');
		monthmap.put('6', 'Jun');
		monthmap.put('7', 'Jul');
		monthmap.put('8', 'Aug');
		monthmap.put('9', 'Sep');
		monthmap.put('10', 'Oct');
		monthmap.put('11', 'Nov');
		monthmap.put('12', 'Dec');
	}

	/*
	 * Author: Uzeeta Siloth
	 * Purpose:    This method is used for fetching user score and returns the list to populate score on leaderboard.
	 * Dependencies:  DashboardController.js
	 * 
	 */

	@AuraEnabled
	public static Map<String, List<User>> getUserDetails() {
		ID userid = UserInfo.getUserRoleId();
		String userName = UserInfo.getUserName();
		Map<String, List<User>> leaderBoardMap = new Map<String, List<User>> ();
		List<User> userlist = new List<user> ();
		List<User> userCurrList = new List<User> ();
		List<User> userWeekList = new List<User> ();
		try {
			//SecurityHelper.CheckFieldReadAccess(new List<String> { 'Id', 'Name', 'Score__c' }, 'User');
			User Usermanger = [Select Id, Manager.name from User where Username = :userName];
			String managerName = Usermanger.Manager.name;
			userList = [Select Id, Name, Username, Score__c, SmallPhotoUrl, UserRole.Id, UserRole.Name, Manager.Name from User where Score__c != null and Manager.Name = :managerName and isActive = true order by Score__c desc];
			List<User> userRankings = calculateRanking(userList);
			leaderBoardMap.put('alltime', userRankings);

			userCurrList = [Select Id, Name, Username, Current_Month_Score__c, Last_Updated_Date__c, SmallPhotoUrl, UserRole.Id, Manager.Name, UserRole.Name from User where Current_Month_Score__c != null and Manager.Name = :managerName and isActive = true order by Current_Month_Score__c desc];

			List<user> sortedUserList = new List<user> ();
			List<user> zeroRecordList = new List<user> ();
			List<user> sortedUserList1 = new List<user> ();
			List<user> zeroRecordList1 = new List<user> ();
			for (User ur : userCurrList) {
				if (ur.Last_Updated_Date__c != null && !TATTriggerHelper.isSameMonth(ur.Last_Updated_Date__c, Date.today())) {
					ur.Current_Month_Score__c = 0;
					zeroRecordList.add(ur);
				}
				else {
					sortedUserList.add(ur);
				}
			}
			sortedUserList.addAll(zeroRecordList);

			List<User> monthlyRankingList = calculateRanking(sortedUserList);
			leaderBoardMap.put('currentMonth', monthlyRankingList);
			sortedUserList.clear();
			zeroRecordList.clear();

			userWeekList = [Select Id, Name, Username, Current_Week_Score__c, SmallPhotoUrl, UserRole.Id, Last_Updated_Date__c, UserRole.Name, Manager.Name from User where Current_Week_Score__c != null and Manager.Name = :managerName and isActive = true order by Current_Week_Score__c desc];
			for (User ur : userWeekList) {
				if (ur.Last_Updated_Date__c != null && !TATTriggerHelper.isSameWeek(ur.Last_Updated_Date__c, Date.today())) {
					ur.Current_Week_Score__c = 0;
					zeroRecordList.add(ur);
				}
				else {
					sortedUserList.add(ur);
				}
			}
			sortedUserList.addAll(zeroRecordList);
			List<User> weeklyList = calculateRanking(sortedUserList);
			leaderBoardMap.put('currentWeek', weeklyList);
		} catch(QueryException e) {
			ExceptionHandlerUtility.writeException('DashboardController', e.getMessage(), e.getStackTraceString());
		}
		return leaderBoardMap;
	}




	public static List<User> calculateRanking(List<User> userList) {
		List<User> userRanking = new List<User> ();
		String userName = UserInfo.getUserName();
		Map<Integer, User> userMap = new Map<Integer, User> ();
		Integer pos = 0;
		for (User userDetail : userList) {
			pos++;
			userMap.put(pos, userDetail);
		}
		for (Integer record : userMap.keySet()) {
			if (record <= 3) {
				userRanking.add(userMap.get(record));
			}
			if (userMap.get(record).Username == userName) {
				Integer curr = record;
				Integer next = curr + 1;
				Integer secNext = curr + 2;
				Integer prev = curr - 1;
				Integer befPrev = curr - 2;
				if (curr == 4) {
					userRanking.add(userMap.get(curr));
					userRanking.add(userMap.get(next));
					userRanking.add(userMap.get(secNext));
				}
				if (curr == 5) {
					userRanking.add(userMap.get(prev));
					userRanking.add(userMap.get(curr));
					userRanking.add(userMap.get(next));
					userRanking.add(userMap.get(secNext));
				}
				if (curr >= 6) {
					userRanking.add(userMap.get(befPrev));
					userRanking.add(userMap.get(prev));
					userRanking.add(userMap.get(curr));
					userRanking.add(userMap.get(next));
					userRanking.add(userMap.get(secNext));
				}
			}

		}
		return userRanking;
	}
	/*
	 
	 * Authors: Parth Lukhi
	 * Purpose:    This method is used for fetching dashboard details and it returns the map for diiferent counts of Cases 
	 * Dependencies:  InterActionListViewHelper.js
	 * 
	 */
	@AuraEnabled
	public static DashWrapper getDashDetail(String whereClause) {
		String userID = UserInfo.getUserID();
		DashWrapper dashWrap = new DashWrapper();
		Map<String, Object> lclDashMap = new Map<String, Object> ();
		Set<String> availableQueue = new Set<String> ();
		Map<Integer, Integer> assignedMap = new Map<Integer, Integer> ();

		Map<Integer, Integer> closedMap = new Map<Integer, Integer> ();
		/* Start : PUX-122 */
		Map<Integer, Integer> closedODMap = new Map<Integer, Integer> ();
		/* End : PUX-122 */
		List<CaseManager__c> listofCases = new List<CaseManager__c> ();
		/* Start : PUX-122 */
		Integer currentYear = System.Today().year();
		List<CaseManager__c> listofLastYrCases = new List<CaseManager__c> ();
		/* End : PUX-122 */

		List<Integer> overDueList = new List<Integer> ();
		Integer myTaskCount = 0, inProgressCount = 0, assignedCount = 0, pendingCount = 0, availableCount = 0;
		Integer highPriorityCount = 0, otherCount = 0, overdueCount = 0, overdueThree = 0, overdueSix = 0;
		Integer overdueNine = 0, overdueTwelve = 0, pendingSup = 0, pendingCust = 0;
		List<String> roleRelatedGroupIds = new List<String> ();
		List<Group> groupList;
		List<GroupMember> usrQueue;
		try {
			List<string> excludeCaseStatus=new List<string>();
			excludeCaseStatus.add('Junk Case');
			excludeCaseStatus.add('Completed');
			excludeCaseStatus.add('Rejected');
			excludeCaseStatus.add('Pending For Archival');

			//SecurityHelper.CheckFieldReadAccess(new List<String>{'CreatedDate','Status__c','OverdueIn__c','Target_TAT_Time__c','Actual_TAT_Time__c','Name','Tat_Hour__c'},'CaseManager__c'); 
			listofCases = [SELECT CreatedDate, Status__c, OverdueIn__c, Target_TAT_Time__c, Actual_TAT_Time__c, Name, Tat_Hour__c from CaseManager__c
			               where OwnerId = :userID AND(Status__c != null) AND(Status__c not in : excludeCaseStatus) limit 10000];

			/* Start : PUX-122 */
			listofLastYrCases = [SELECT CreatedDate, Status__c, Target_TAT_Time__c, Actual_TAT_Time__c, OverdueIn__c, Name from CaseManager__c
			                     where CreatedDate >= LAST_N_DAYS : 365 AND(Status__c != null) limit 10000];
			/* End : PUX-122 */
			myTaskCount = listofCases.size();
			for (CaseManager__c caseTracker : listofCases) {
				if (caseTracker.Status__c != 'Start' && caseTracker.Status__c != 'Pending For Archival' && caseTracker.Status__c != 'Rejected') {
					inProgressCount += 1;
				}
				if (caseTracker.Status__c.trim() == 'Awaiting Email Response') {
					pendingCust += 1;
				}

				if (caseTracker.Status__c == 'Awaiting Supervisory Resolution' || caseTracker.Status__c.trim() == 'Awaiting Email Response') {
					pendingCount += 1;
				}

				Integer mon = caseTracker.CreatedDate.month();
				Integer count = 0;
				Integer closed = 0;
				Integer closedOD = 0;
				if (caseTracker.Status__c != 'Pending For Archival') {
					if (assignedMap.containsKey(mon)) {
						count = assignedMap.get(mon);
						count = count + 1;
					}
					assignedMap.put(mon, count);
				}
				/** Start  PUX-358 **/
				if (caseTracker.Tat_Hour__c != null && caseTracker.OverdueIn__c != 'Overdue') {
					Decimal hourcount = caseTracker.Tat_Hour__c;
					if (hourcount > 0) {
						overdueCount += 1;
					}
					if (hourcount > 0) {
						if (hourcount > 0 && hourcount <= 3) {
							overdueThree += 1;
						}

						if (hourcount > 3 && hourcount <= 6) {
							overdueSix += 1;
						}

						if (hourcount > 6 && hourcount <= 9) {
							overdueNine += 1;
						}

						if (hourcount > 9) {
							overdueTwelve += 1;
						}
					}
				}
				/** End  PUX-358 **/
			}
			pendingSup = (pendingCount > pendingCust ? pendingCount - pendingCust : 0);
			//SecurityHelper.CheckFieldReadAccess(new List<String>{'id','RelatedId','Type'},'Group'); 
			groupList = [SELECT id, RelatedId, Type FROM Group where RelatedId = :UserInfo.getUserRoleId() OR DeveloperName = 'AllInternalUsers'];

			for (Group g : groupList) {
				roleRelatedGroupIds.add(g.id);
			}

			usrQueue = [SELECT Group.Name FROM GroupMember WHERE(UserOrGroupId = :UserInfo.getUserID() AND(Group.Type = 'Queue' OR Group.Type = 'All Internal Users')) OR(UserOrGroupId IN :roleRelatedGroupIds AND(Group.Type = 'Queue' OR Group.Type = 'All Internal Users'))];

			for (GroupMember queue : usrQueue) {
				availableQueue.add(queue.Group.Id);
			}
			availableCount = 0;
			// SecurityHelper.CheckFieldReadAccess(new List<String>{'CreatedDate','Status__c','Name','Priority__c','OwnerId'},'CaseManager__c'); 
			for (CaseManager__c caseObj :[SELECT CreatedDate, Status__c, Name, Priority__c
			     from CaseManager__c
			     where(OwnerId IN :availableQueue) AND(Status__c != null) limit 9999]) {

				availableCount += 1;
				if (caseObj.Priority__c == 'High') {
					highPriorityCount += 1;
				}
			}


			/* Start : PUX-122 */
			for (CaseManager__c caseTracker : listofLastYrCases) {
				Integer mon = caseTracker.CreatedDate.month();
				Integer count = 0;
				Integer closed = 0;
				Integer closedOD = 0;
				//if(caseTracker.Status__c == 'Pending For Archival' && caseTracker.OverdueIn__c != 'Overdue'){
				if (caseTracker.Status__c.equals('Pending For Archival') && (caseTracker.Target_TAT_Time__c > caseTracker.Actual_TAT_Time__c)) {
					if (closedMap.containsKey(mon)) {
						closed = closedMap.get(mon);
					}
					closed = closed + 1;
					closedMap.put(mon, closed);
				}
				// if(caseTracker.Status__c == 'Pending For Archival' && caseTracker.OverdueIn__c =='Overdue'){
				if (caseTracker.Status__c.equals('Pending For Archival') && (caseTracker.Target_TAT_Time__c <= caseTracker.Actual_TAT_Time__c)) {
					if (closedODMap.containsKey(mon)) {
						closedOD = closedODMap.get(mon);

					}
					closedOD = closedOD + 1;
					closedODMap.put(mon, closedOD);
				}

			}

		} Catch(Exception e) {
			ExceptionHandlerUtility.writeException('DashboardController', e.getMessage(), e.getStackTraceString());
		}

		/* End : PUX-122 */
		lclDashMap.put('assignedMap', assignedMap);
		lclDashMap.put('closedMap', closedMap);
		lclDashMap.put('closedODMap', closedODMap);
		lclDashMap.put('listofCases', listofCases);
		lclDashMap.put('myTaskCount', myTaskCount);
		lclDashMap.put('inProgressCount', inProgressCount);
		lclDashMap.put('pendingCount', pendingCount);
		lclDashMap.put('pendingSup', pendingSup);
		lclDashMap.put('pendingCust', pendingCust);
		lclDashMap.put('availableCount', availableCount);
		lclDashMap.put('loggedInUser', UserInfo.getName());
		lclDashMap.put('SmallPhotoUrl', [SELECT SmallPhotoUrl FROM User WHERE User.ID = :userID LIMIT 1].SmallPhotoUrl);
		lclDashMap.put('overdueCount', overdueCount);
		lclDashMap.put('overdueThree', overdueThree);
		lclDashMap.put('overdueSix', overdueSix);
		lclDashMap.put('overdueNine', overdueNine);
		lclDashMap.put('overdueTwelve', overdueTwelve);

		list<String> listOfMonths = new list<string> ();
		list<String> monthList = new list<String> ();
		list<Integer> assignedList = new list<Integer> ();
		list<Integer> closedOdList = new list<Integer> ();
		list<Integer> closedList = new list<Integer> ();
		for (integer month = 1; month <= 12; month++) {
			monthList.add(string.valueOf(month));
			Integer count = assignedMap.containsKey(month) ? assignedMap.get(month) : 0;
			Integer closed = closedMap.containsKey(month) ? closedMap.get(month) : 0;
			Integer closedOd = closedODMap.containsKey(month) ? closedODMap.get(month) : 0;
			assignedList.add(count);
			closedList.add(closed);
			/* Start : PUX-122 */
			closedOdList.add(closedOd);
			/* End : PUX-122 */
		}

		for (string mnth : monthList) {
			listOfMonths.add(monthmap.get(mnth));
		}
		lclDashMap.put('listOfMonths', listOfMonths);
		lclDashMap.put('monthList', monthList);
		lclDashMap.put('assignedList', assignedList);
		lclDashMap.put('closedList', closedList);
		/* Start : PUX-122 */
		lclDashMap.put('closedOdList', closedOdList);
		/* End : PUX-122 */
		/*String qryFldSet='select Chart_Type__c from DashChart__c ';
		 
		  //System.debug(':End +++++++getFieldSetByParam qryFldSet '+qryFldSet);
		  List<DashChart__c> chartTypeCriteria =Database.query(qryFldSet);
		  if(chartTypeCriteria.size()>0)
		  {
		  lclDashMap.put('chartType',chartTypeCriteria[0].Chart_Type__c);
		  }else{
		  //System.debug(':End +++++++getFieldSetName returning process field ');
		  lclDashMap.put('chartType','Column'); 
		  }
		 */

		dashWrap.setDashMap(lclDashMap);
		return dashWrap;


	}
	/*
	 *  End : This method is used for fetching dashboard details and it returns the map for diiferent counts of Cases 
	 */

	/*
	 *  Start : DashbaordWrapper class is used for wrapping Data together for displaying data on Front 
	 */

}