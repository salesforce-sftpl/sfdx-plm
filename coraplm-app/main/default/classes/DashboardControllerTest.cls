/**
  * Authors		: Parth Lukhi
  * Date created : 10/11/2017 
  * Purpose      : Test Class for  DashboardController
  * Dependencies : DashboardController.apxc
  * -------------------------------------------------
  *Modifications:
                Date:   
                Purpose of modification:  
                Method/Code segment modified: 

*/
@isTest
public class DashboardControllerTest {
      public static testMethod void testFirst(){
        
		CaseManager__c ctTest=createCaseTracker(); 
        EmailMessage emailObj= createEmailMessage(ctTest);
      
		DashboardController.getUserDetails();
		DashboardController.getDashDetail(null);
        DashboardController.getDashDetail(null).getDashMap();
          
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User testuser = new User(Alias = 'newUser', Email='newuser@newplm.dev',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='newuser@newplm.dev');
        System.runAs(testuser) {
              DashboardController.getUserDetails();
        }
          
    }

    public static CaseManager__c createCaseTracker(){
         CaseManager__c ct= new CaseManager__c();
         ct.Subject__c='Invalid Invoice';
         ct.Status__c='Ready For Processing';
         ct.Target_TAT_Time__c = Date.Today().addDays(4);
         insert ct;
        
         ct= new CaseManager__c();
         ct.Subject__c='Invalid Invoice';
         ct.Status__c='Pending For Archival';
         ct.Target_TAT_Time__c = Date.Today().addDays(3);
         ct.Actual_TAT_Time__c = Date.Today();
         insert ct;
        
        ct= new CaseManager__c();
        ct.Subject__c='Invalid Invoice_1';
        ct.Status__c='Pending For Archival';
       // ct.Target_TAT_Time__c= Date.Today().addDays(2);
        insert ct;
        
        ct= new CaseManager__c();
        ct.Subject__c='Invalid Invoice_1';
        ct.Status__c='Awaiting Supervisory Resolution';
        ct.Target_TAT_Time__c= Date.Today();
          insert ct;
        
            ct= new CaseManager__c();
            ct.Subject__c='Invalid Invoice_2';
            ct.Status__c='Awaiting Email Response';
             insert ct;
        
        return ct;
    }
    
      public static EmailMessage createEmailMessage(CaseManager__c ct){
         
		EmailMessage emailMsg=new EmailMessage();
		emailMsg.FromName='Mr. John Green';
		emailMsg.ToAddress='ToAbc@Domain.com';
        emailMsg.FromAddress='FromAbc@Domain.com';
		emailMsg.RelatedToId=ct.Id;
		insert emailMsg;
        return emailMsg;
    }
}