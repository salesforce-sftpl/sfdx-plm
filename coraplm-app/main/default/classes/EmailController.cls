/*
 * Authors      :  Atul Hinge
 * Date created :  14/08/2017
 * Purpose      :  For email releated activity like reply,replyAll, forward.
 * Dependencies :  None
 * JIRA ID      :  PUX-113,PUX-39,PUX-118,PUX-113,PUX-132,PUX-217,PUX-218,PUX-225
 * -----------------------------------------------------------
 * Modifications: 1
  *        Date: 17/12/2017 | 29/12/2017
 *        Purpose of modification: Inline image in email | PUX-239
  *        Method/Code segment modified: added salesforce instance base url to image url in "saveInlineImages" & "replaceImageLinkCID" method.   
 * -------------------------------------------------
 * Modifications:
  Date:  18/12/2017 
  Purpose of modification:  PUX-505 : Maintain attachment @ interaction level   
*/
public with sharing class EmailController {
    /*
      Authors: Atul Hinge
      Purpose: use to inilize contacts and EmailTemplates in component
      Dependencies:  Email.cmp.   
     */
    @AuraEnabled
    public static Map<String, Object> initializeComponent(String requestParm) {

        Map<String, Object> response = new Map<String, Object> ();
        List<Object> templateDropDowns = new List<Object> ();
        String errorMessage = 'The following exception has occurred: ';
        try {
            Map<String, Object> data = (Map<String, Object>) JSON.deserializeUntyped(requestParm);
            String interactionId = (String) data.get('interactionId');
            String emailTemplateFolder = (String) data.get('emailTemplateFolder');
            if (interactionId != null && interactionId != '') {
                EmailMessage em = [SELECT Id, FromAddress, ToAddress, BccAddress, CcAddress, HtmlBody, Subject, CreatedDate,RelatedToId FROM EmailMessage where Id = :interactionId];
                response.put('EmailMessage', em);
                String fromSection = '<br><br><br><span style="border:none;border-top:solid #B5C4DF 1.0pt;padding:3.0pt 0in 0in 0in"></span>' +
                '<p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">From:</span></b><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"> ' + em.FromAddress +
                '<br>' +
                '<b>Sent:</b> ' + em.CreatedDate.format('EEEE, MMMMM  d, YYYY  hh:mm a') + '<br>';
                if (em.ToAddress != null) {
                    fromSection += '<b>To:</b> ' + em.ToAddress + '<br>';
                }
                if (em.CcAddress != null) {
                    fromSection += '<b>Cc:</b> ' + em.CcAddress + ' <br>';
                }
                if (em.BccAddress != null) {
                    fromSection += '<b>Bcc:</b> ' + em.BccAddress + ' <br>';
                }
                fromSection += '<b>Subject:</b> ' + em.Subject + '<p></p></span></p>';
                /* PUX-262 */
                String trailMailBody = fromSection + em.htmlBody;
                response.put('trailMailBody', trailMailBody);
                Map<String, String> obj = new Map<String, string> ();
                obj.put('label', '-- None --');
                obj.put('value', 'TrailMail');
                obj.put('selected', 'false');
                obj.put('body', trailMailBody);
                obj.put('Subject', em.Subject);
                if (emailTemplateFolder != 'QuickReply') {
                    templateDropDowns.add(obj);
                }

                 /* Purpose: To update PredictedValue status of Indexing Field      start   */
                CaseManager__c caseObj=CaseDetailViewController.getPredictedValue(em.RelatedToId);                              
                if(caseObj.AWSResponseJson__c !=null && caseObj.AWSResponseJson__c!=''){
                    Map<String, Object> jsonMap = (Map<String, Object>)JSON.deserializeUntyped(caseObj.AWSResponseJson__c);
                    Map<String, Object> templateMap = (Map<String, Object>)jsonMap.get('RecommReplyTempl');
                    String tempName;
                    if(emailTemplateFolder != 'QuickReply'){
                        tempName=(String)templateMap.get('Long_Template');
                    }else{
                        tempName=(String)templateMap.get('Short_Template');
                    }
                    List<EmailTemplate> emailTempList=[SELECT Id, Name FROM EmailTemplate where name=:tempName];
                    if(emailTempList!=null && emailTempList.size()>0)
                    {   
                        System.debug('emailList'+emailTempList[0].Id);
                        response.put('predictedTemplate',emailTempList[0].Id);              
                    }
                }
                 /* Purpose: To update PredictedValue status of Indexing Field      end */
            } else {
                response.put('trailMailBody', '');
            }
            /* PUX-262 */
            for (EmailTemplate et : EmailController.getEmailTemplates(emailTemplateFolder)) {
                Map<String, String> obj = new Map<String, string> ();
                obj.put('label', et.Name);
                obj.put('value', et.Id);
                obj.put('selected', 'false');
                obj.put('body', (et.body == null) ? et.HtmlValue : et.body);
                obj.put('Subject', et.Subject);
                templateDropDowns.add(obj);
            }
            response.put('templates', templateDropDowns);
            List<OrgWideEmailAddress> orgWideEmailAddress=[SELECT Id, Address, DisplayName FROM OrgWideEmailAddress];
            if(orgWideEmailAddress.isEmpty()){
                OrgWideEmailAddress defaultAddress=new OrgWideEmailAddress();
                defaultAddress.DisplayName='noreply@salesforce.com';
                defaultAddress.address='';
                orgWideEmailAddress.add(defaultAddress);
            }
            response.put('orgWideEmailAddress', orgWideEmailAddress);
            response.put('contacts', [Select Id, Name, Email,EmailType__c from Contact]);
        } catch(LimitException e) {
            response.put('error', errorMessage + e.getMessage());
            ExceptionHandlerUtility.writeException('EmailController', e.getMessage(), e.getStackTraceString());
        } catch(Exception e) {
            response.put('error', errorMessage + e.getMessage());
            ExceptionHandlerUtility.writeException('EmailController', e.getMessage(), e.getStackTraceString());
        }
        System.debug('Response@@@@' + response);
        return response;
    }

    /*
      Authors: Atul Hinge
      Purpose: This is generic method for reply, replyAll, Forward and compose email
      Dependencies:   Email.cmp.   
     */
    @AuraEnabled
    public static Map<String, Object> sendMail(String emailJSON, String emailBody, CaseManager__c ct) {
        Map<String, Object> response = new Map<String, Object> ();
        String errorMessage = 'The following exception has occurred: ';
        try {
            EmailWrapper email = (EmailWrapper) JSON.deserialize(emailJSON, EmailWrapper.class);
            ct.LastResponseDate__c = DateTime.now();
            if (email.sendTo != null && email.sendTo.size() > 0) {
                ct.To_Addresses__c = string.join(email.sendTo, ';');
            }
            if (email.sendCc != null && email.sendCc.size() > 0) {
                ct.CC_Addresses__c = string.join(email.sendCc, ';');
            }
            if (email.sendBcc != null && email.sendBcc.size() > 0) {
                ct.BCC_Addresses__c = string.join(email.sendBcc, ';');
            }
            if (email.sendEsc != null && email.sendEsc.size() > 0) {
                ct.Esc_Addresses__c = string.join(email.sendEsc, ';');
            }

            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage> ();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(email.sendTo);
            mail.setCcAddresses(email.sendCc);
            mail.setBccAddresses(email.sendBcc);
            List<Contact> contactList=new List<Contact>();
            Set<String> emails=new Set<String>();
            for(Contact c:[Select Id, Name, Email,EmailType__c from Contact]){
                emails.add(c.Email);
            }
            for(String emailId:email.sendTo){
                if(!emails.contains(emailId)){
                    contactList.add(new Contact(Lastname=emailId,Email=emailId,EmailType__c='To'));    
                }
            }
            if(email.sendCc != null){
                for(String emailId:email.sendCc){
                    if(!emails.contains(emailId)){
                        contactList.add(new Contact(Lastname=emailId,Email=emailId,EmailType__c='Cc'));    
                    }
                }
            }
            if(email.sendBcc != null){
                for(String emailId:email.sendBcc){
                    if(!emails.contains(emailId)){
                        contactList.add(new Contact(Lastname=emailId,Email=emailId,EmailType__c='Bcc'));    
                    }
                }
            }
            if(email.sendEsc != null){
                for(String emailId:email.sendEsc){
                    if(!emails.contains(emailId)){
                        contactList.add(new Contact(Lastname=emailId,Email=emailId,EmailType__c='Esc'));    
                    }
                }
            }
            if(contactList.size()>0){
                insert contactList;    
            }
            
            mail.setWhatId(ct.id);
            if(!string.isEmpty(email.fromAddress)){
                mail.setOrgWideEmailAddressId(email.fromAddress);
                //mail.setReplyTo(email.fromAddress);
            }
            
            if (!email.subject.contains(PLMConstants.SUBJECT_START_IDENTIFIER)) {
                String refId = ' ' + PLMConstants.SUBJECT_START_IDENTIFIER + ct.name + PLMConstants.SUBJECT_END_IDENTIFIER;
                email.subject += refId;
            }
            mail.setSubject(email.subject);
            //added by ashish on 16-oct-2017 | PUX-133 
            //functionality : save inline images in database | Added by Ashish on 13-Nov-2017 | PUX-
            mail = saveInlineImages(mail, emailBody, ct);
            emailBody = mail.getHtmlBody();
            mail = replaceImageLinkCID(mail, emailBody);
            if (email.attachmentIds != null && email.attachmentIds.size() > 0) {
                mail.setFileAttachments(EmailController.ContentDocumentAsAttachement(email.attachmentIds));
            }
            mails.add(mail);
            Messaging.sendEmail(mails);


            //Added by Ashish Kr. on 08-Dec-2017 | PUX-239
            EmailMessage sentEmail = [Select Subject, HtmlBody, Id, HasAttachment__c from EmailMessage Where RelatedToId = :ct.id Order By CreatedDate DESC LIMIT 1];
            sentEmail.HtmlBody = emailBody;
            //PUX-505 start
            if (email.attachmentIds != null && email.attachmentIds.size() > 0) {
                sentEmail.HasAttachment__c = true;
            }
            update sentEmail;

            if (email.attachmentIds != null && email.attachmentIds.size() > 0) {
                for (Id id : email.attachmentIds) {
                    ContentDocumentLink cntent = new ContentDocumentLink();
                    cntent.ContentDocumentId = id;
                    cntent.LinkedEntityId = sentEmail.Id;
                    cntent.ShareType = 'V';
                    insert cntent;
                }
            }
            //PUX-505 End

            AddBodyAsAttachment(email, emailBody, ct.Id);
            update ct;
            response.put('case', CaseService.getCaseByIdWithInteraction(ct.Id));
        } catch(DMLException e) {
            response.put('error', errorMessage + e.getMessage());
            ExceptionHandlerUtility.writeException('EmailController', e.getMessage(), e.getStackTraceString());
        } catch(EmailException e) {
            response.put('error', errorMessage + e.getMessage());
            ExceptionHandlerUtility.writeException('EmailController', e.getMessage(), e.getStackTraceString());
        } catch(LimitException e) {
            response.put('error', errorMessage + e.getMessage());
            ExceptionHandlerUtility.writeException('EmailController', e.getMessage(), e.getStackTraceString());
        }
        return response;
    }

    /*
      Authors: Tej Pal Kumawat 
      Purpose: Method for quick reply - PUX-298
      Dependencies:   None 
     */
    @AuraEnabled
    public static Map<String, Object> quickReplyMethod(String action, String emailTemplateId, String interactionId, String trackerId) {
        Map<String, Object> response = new Map<String, Object> ();
        String errorMessage = 'The following exception has occurred: ';
        try {
            EmailTemplate et = [Select Subject, HtmlValue, Body from EmailTemplate where Id = :emailTemplateId];
            EmailMessage em = [SELECT Id, FromAddress, ToAddress, CcAddress, BccAddress, HtmlBody, Subject, CreatedDate FROM EmailMessage WHERE Id = :interactionId];
            CaseManager__c tracker = [SELECT Id, Name, LastResponseDate__c FROM CaseManager__c WHERE Id = :trackerId];

            string emailSubject = et.Subject;
            string emailBody = et.HtmlValue != null ? et.HtmlValue : et.Body;


            //Replace fields
            Set<String> tokens = new Set<String> ();

            String objectName = ObjectUtil.getPackagedFieldName('CaseManager__c');
            Pattern regex = Pattern.compile('\\{!([^}]*)\\}');

            Matcher regexMatcher = regex.matcher(emailSubject);
            while (regexMatcher.find()) {
                String dirtyVariable = regexMatcher.group();
                String variable = dirtyVariable.substring(2, dirtyVariable.length() - 1);

                //keep only Lead fields
                if (variable.startsWith(objectName + '.')) {
                    //remove self-referencing in fieldnames
                    tokens.add(variable.replace(objectName + '.', ''));
                }
            }

            regexMatcher = regex.matcher(emailBody);
            while (regexMatcher.find()) {
                String dirtyVariable = regexMatcher.group();
                String variable = dirtyVariable.substring(2, dirtyVariable.length() - 1);

                //keep only Lead fields
                if (variable.startsWith(objectName + '.')) {
                    //remove self-referencing in fieldnames
                    tokens.add(variable.replace(objectName + '.', ''));
                }
            }

            if (tokens.size() > 0) {
                Map<String, Schema.SObjectField> caseTrackerFieldMap = Schema.SObjectType.CaseManager__c.fields.getMap();

                //Build dynamic query
                String qry = 'Select ';
                for (String s : tokens) {
                    if (caseTrackerFieldMap.containsKey(s.toLowerCase())) {
                        qry += s + ',';
                    }
                }

                //remove last ","
                qry = qry.substring(0, qry.length() - 1);

                //Do the query
                SObject o = Database.query(qry + ' From ' + objectName + ' Where ID = :trackerId');

                //Replace values
                for (String s : tokens) {
                    if (caseTrackerFieldMap.containsKey(s.toLowerCase())) {
                        emailBody = emailBody.replace('{!' + objectName + '.' + s + '}', getValue(o, s));
                        emailSubject = emailSubject.replace('{!' + objectName + '.' + s + '}', getValue(o, s));
                    }
                }
            }

            emailBody += '\n\n\n' + em.HtmlBody;

            //Create Input JSON
            string inputJson = '';
            if (action == 'quickreply') {
                inputJson = JSON.serialize(
                                           new Map<String, Object> {
                                              'sendTo' => em.FromAddress.split(';'),
                                              'sendCc' => new list<string> (),
                                              'sendBcc' => new list<string> (),
                                              'subject' => 'RE: ' + emailSubject,
                                              'attachmentIds' => new list<string> ()
                                           }
                );
            } else if (action == 'quickreplyall') {
                inputJson = JSON.serialize(
                                           new Map<String, Object> {
                                              'sendTo' => (em.FromAddress != null ? em.FromAddress.split(';') : new list<string> ()),
                                              'sendCc' => (em.CcAddress != null ? em.CcAddress.split(';') : new list<string> ()),
                                              'sendBcc' => (em.CcAddress != null ? em.BccAddress.split(';') : new list<string> ()),
                                              'subject' => 'RE: ' + emailSubject,
                                              'attachmentIds' => new list<string> ()
                                           }
                );
            }

            response = EmailController.sendMail(inputJson, emailBody, tracker);
        } catch(Exception e) {
            response.put('error', errorMessage + e.getMessage());
            ExceptionHandlerUtility.writeException('EmailController', e.getMessage(), e.getStackTraceString());
        }
        return response;
    }

    /*
      Authors: Tej Pal Kumawat 
      Purpose:Method to get object field value - PUX-298
      Dependencies:   None 
     */
    private static String getValue(SObject o, String fieldName) {
        return(o.get(fieldName) != null) ? String.valueOf(o.get(fieldName)) : '';
    }

    /*
      Authors: Atul Hinge
      Purpose: AuraEnabled wrapperclass to get Email metadata from wrapper class 
      Dependencies: Email.cmp.   
     */
    Public class EmailWrapper {
        @AuraEnabled
        public List<String> sendTo {
            get;
            set;
        }
        @AuraEnabled
        public List<String> sendCc {
            get;
            set;
        }
        @AuraEnabled
        public List<String> sendBcc {
            get;
            set;
        }
        @AuraEnabled
        public List<String> sendEsc {
            get;
            set;
        }
        @AuraEnabled
        public String fromAddress { get; set; }
        @AuraEnabled
        public String subject {
            get;
            set;
        }
        @AuraEnabled
        public List<String> attachmentIds {
            get;
            set;
        }

    }
    /*
      Authors: Atul Hinge
      Purpose: use to create a list of EmailFileAttachment from contentdocument 
      Dependencies: Email.cmp.   
     */
    public static List<Messaging.EmailFileAttachment> ContentDocumentAsAttachement(Id[] contentDocumentIds) {
        List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment> { };
        List<ContentVersion> documents = new List<ContentVersion> { };
        documents.addAll([
                         SELECT Id, Title, FileType, VersionData, isLatest, ContentDocumentId, FileExtension
                         FROM ContentVersion
                         WHERE isLatest = true AND ContentDocumentId IN :contentDocumentIds
                         ]);

        for (ContentVersion document : documents) {
            Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
            attachment.setBody(document.VersionData);
            attachment.setFileName(document.Title);
            attachments.add(attachment);
        }

        return attachments;

    }

    /*
      Authors: Ashish Kumar
      Purpose: Save inline images with source as blob in salesforce after converting into attachments
      Dependencies: Use in "EmailController.cls".
     */
    public static Messaging.SingleEmailMessage saveInlineImages(Messaging.SingleEmailMessage mail, String body, CaseManager__c ct) {
        Map<String, String> fileTypeBlobMap = new Map<String, String> ();
        Map<String, ContentDocumentLink> imageContentDocID = new Map<String, ContentDocumentLink> ();
        try {
            String strToBeReplaced = 'src="data:';
            //check if link to be replaced exists
            if (body != null && body.indexOf(strToBeReplaced) != - 1) {
                Integer index = body.indexOf(strToBeReplaced);
                //collect document ids of attachments 
                Integer j = 1 ;
                while (index > 0) {
                    //String body = '</tbody></table><p><br></p><p><img src="data:image/png;base64,IUtO9Ff8H6gFBzlKZAhEAAAAASUVORK5CYII="></p>';
                    String srcSubstring = body.substring(index + 10, body.substring(index).indexOf('>') + index);
                    String fileType = srcSubstring.substring(0, srcSubstring.indexOf(';'));
                    String blobBase64 = srcSubstring.substring(srcSubstring.indexOf(',') + 1, srcSubstring.indexOf('"'));
                    fileTypeBlobMap.put(fileType + j, blobBase64);
                    //check next link in the body
                    index = body.indexOf(strToBeReplaced, index + 1);
                     j = j+1;
                }
                if (fileTypeBlobMap != null && fileTypeBlobMap.size() > 0) {
                    //save attachment to database
                    imageContentDocID = CaseService.addAttachments(fileTypeBlobMap, ct);
                }

                index = body.indexOf(strToBeReplaced);
                Integer i = 1;
                //collect document ids of attachments 
                while (index > 0) {
                    String srcSubstring = body.substring(index + 5, body.substring(index).indexOf('>') + index - 1);
                    String siteURL = EmailToCaseSettingHelper.getHttpsSalesforceBaseURL();
                    String fullFileURL = siteURL + '/sfc/servlet.shepherd/document/download/' + imageContentDocID.get('file' + i + '.png').ContentDocumentId + '?asPdf=false&operationContext=CHATTER0';
                    body = body.replace(srcSubstring, fullFileURL);
                    //check next link in the body
                    index = body.indexOf(strToBeReplaced, index + 1);
                    i++;
                }

            } else {
                System.debug('Inline cid string not found from attachment[]');
            }
        } catch(Exception ex) {
            System.debug(ex.getStackTraceString());
        }
        //replace the html body with the body having cid links
        mail.setHtmlBody(body);
        return mail;
    }

    //added by Ashish Kr. | 16-Oct-2017 | PUX-133 
    //method to replace attachment internal links to CID while sending email to the user
    public static Messaging.SingleEmailMessage replaceImageLinkCID(Messaging.SingleEmailMessage mail, String body) {
        Set<String> documentIDSet = new Set<String> ();
        List<Id> documentIDList = new List<Id> ();
        Map<String, contentVersion> cvDocIDMap = new Map<String, ContentVersion> ();
        try {
            String siteURL = EmailToCaseSettingHelper.getHttpsSalesforceBaseURL();
            String strToBeReplaced = 'src="' + siteURL + '/sfc/servlet.shepherd/document/download/';
            //check if link to be replaced exists
            if (body != null && body.indexOf(strToBeReplaced) != - 1) {
                Integer index = body.indexOf(strToBeReplaced);
                //collect document ids of attachments 
                while (index > 0) {
                    String cidString = body.substring(index + 5, body.substring(index).indexOf('>') + index);
                    String docID = cidString.substring(cidString.indexOf('download/') + 9, cidString.indexOf('download/') + 27);
                    documentIDSet.add(docId);
                    //check next link in the body
                    index = body.indexOf(strToBeReplaced, index + 1);
                }
                if (documentIDSet.size() > 0) {
                    for (String docId : documentIDSet) {
                        documentIDList.add(Id.ValueOf(docId));
                    }
                    //get content document id along with content version
                    cvDocIDMap = fetchContentDocID(documentIDSet);
                }

                index = body.indexOf(strToBeReplaced);
                //replcace internal attachment link with cid link
                while (index > 0) {
                    String cidString = body.substring(index + 5, body.substring(index).indexOf('>') + index - 1);
                    String docID = cidString.substring(cidString.indexOf('download/') + 9, cidString.indexOf('download/') + 27);
                    body = body.replace(cidString, 'cid:' + cvDocIDMap.get(docId).Title);
                    //check next link in the body
                    index = body.indexOf(strToBeReplaced, index + 1);
                }
                //attach all files in the email
                mail.setFileAttachments(ContentDocumentAsAttachement(documentIDList));

            } else {
                System.debug('Inline cid string not found from attachment[]');
            }
        } catch(Exception ex) {
            System.debug(ex.getStackTraceString());
        }
        //replace the html body with the body having cid links
        mail.setHtmlBody(body);
        return mail;
    }

    //added by ashish on 16-oct-2017 | PUX-133 
    //method to fetch the content version corresponding to content document id set
    public Static Map<String, ContentVersion> fetchContentDocID(Set<String> contentDocIdSet) {
        Map<String, ContentVersion> docIdCVMap = new Map<String, ContentVersion> ();
        List<ContentVersion> cvList = new List<ContentVersion> ();
        for (ContentVersion cv :[SELECT Id, Title, FileType, VersionData, isLatest, ContentDocumentId, FileExtension
             FROM ContentVersion
             WHERE isLatest = true AND ContentDocumentId IN :contentDocIdSet
             ]) {
            docIdCVMap.put(cv.ContentDocumentId, cv);
        }
        //system.debug(docIdCVMap);
        return docIdCVMap;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: add email body as attachment.
      Dependencies: 
     */
    private static void AddBodyAsAttachment(EmailWrapper email, string body, string caseId) {
        String FORM_HTML_START = '<HTML><head><title></title><meta charset="UTF-8"></head><BODY> <div style="font-family: Arial Unicode MS">';
        String FORM_HTML_END = '</div></BODY></HTML>';
        String emailBody = '';
        emailBody = emailBody + '' + FORM_HTML_START;
        //emailBody = emailBody + 'From:' + email.FromAddress + '<br>';
        emailBody = emailBody + 'Sent Date:' + Datetime.now().format('yyyy-MM-dd\'T\'hh:mm:ss\'z\'') + '<br>';
        emailBody = emailBody + 'To:' + email.sendTo + '<br>';
        if (email.sendCc != null && email.sendCc.size() > 0)
        emailBody = emailBody + 'Cc:' + email.sendCc + '<br>';
        emailBody = emailBody + 'Subject:' + email.subject + '<br><br><br><br>';

        emailBody = emailBody + body;
        emailBody = emailBody + FORM_HTML_END;

        string dateString = System.now().format('yyyyMMddHHmmssS');
        string fileName = dateString + '_OUTBOUND.html';
        ContentVersion contVersion = new ContentVersion();
        contVersion.Title = fileName;
        contVersion.PathOnClient = fileName;
        contVersion.VersionData = blob.valueOf(emailBody);
        insert contVersion;

        ContentVersion conetntVersionListWithId = [SELECT id, ContentDocumentId, Title FROM ContentVersion where Id = :contVersion.Id];

        //String workspaceName = 'PLM Files';
        ContentWorkspace shareWorkspace = [select id from ContentWorkspace where name = :PLMConstants.WORKSPACE_NAME limit 1];


        ContentDocumentLink contentDocumentLinkObj = new ContentDocumentLink();
        contentDocumentLinkObj.ContentDocumentId = conetntVersionListWithId.ContentDocumentId;
        contentDocumentLinkObj.LinkedEntityId = caseId;
        contentDocumentLinkObj.ShareType = 'V';

        ContentWorkspaceDoc contentWorspaceDocObj = new ContentWorkspaceDoc();
        contentWorspaceDocObj.ContentDocumentId = conetntVersionListWithId.ContentDocumentId;
        contentWorspaceDocObj.ContentWorkspaceId = shareWorkspace.id;


        insert contentDocumentLinkObj;
        insert contentWorspaceDocObj;

        //return emailBody;
    }



    /*
      Authors: Rahul Pastagiya
      Purpose: PUX-444: Email template folder wise segrigation, 
      Param : folderNameApis can be comma seprated string if more than one folder access is required
      Dependencies: None
     */
    public static List<EmailTemplate> getEmailTemplates(String folderNameApis) {
        List<EmailTemplate> emailTemplateList = new List<EmailTemplate> ();
        try {
            List<String> folderApiList = new List<String> ();
            for (String folderNameApi : folderNameApis.split(',')) {
                if (folderNameApi.trim() != '') {
                    folderApiList.add(folderNameApi);
                }
            }
            emailTemplateList = [Select Id, Name, body, Subject, HtmlValue from EmailTemplate where Folder.DeveloperName in :folderApiList];
        } catch(Exception e) {
            System.debug(e.getStackTraceString());
        }
        return emailTemplateList;
    }
}