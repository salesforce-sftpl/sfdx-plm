/**
* Authors    : Atul Hinge
* Date created : 16/11/2017 
* Purpose      : Test Class for EmailController
* Dependencies : -
* -------------------------------------------------
*Modifications:
Date:   
Purpose of modification:  
Method/Code segment modified: 

*/
@isTest
public class EmailControllerTest {
    public static testMethod void testInitializeComponent(){
        String interId=TestDataGenerator.createEmailMessage(TestDataGenerator.createCaseTracker()).Id;
        JsonObject obj = new JsonObject();
        obj.interactionId = interId;
        obj.emailTemplateFolder = 'Notification';
        String jsonString = JSON.serialize(obj);
        Map<String,Object> resp1=EmailController.initializeComponent(jsonString);
        Map<String,Object> resp2=EmailController.initializeComponent(null);
        Map<String,Object> resp3=EmailController.initializeComponent('invalidId');
         
    }
    public static testMethod void testSendMail(){
         CaseManager__c ct =TestDataGenerator.createCaseTracker();
         TestDataGenerator.createAppConfig();
         String attachId= TestDataGenerator.createContent(ct);
        EmailController.EmailWrapper emailWrapper =new EmailController.EmailWrapper();
                                        emailWrapper.sendTo = new List<String>{'abc@xyz.com'};
                                        emailWrapper.sendCc= new List<String>{'pqr@xyz.com'};
                                        emailWrapper.sendBcc= new List<String>{'mno@xyz.com'};
                                        emailWrapper.subject='Test Subject';
                                        emailWrapper.attachmentIds= new List<String>{attachId};
                
        Test.startTest();
         Map<String,Object> resp1=EmailController.sendMail(JSON.serialize(emailWrapper),'</tbody></table><p><br></p><p><img src="data:image/png;base64,IUtO9Ff8H6gFBzlKZAhEAAAAASUVORK5CYII="></p>',ct);
        // Map<String,Object> resp2=EmailController.sendMail(null,'Test Body',ct);
        Test.stopTest();
    }
     public static testMethod void testQuickResponce(){
         CaseManager__c ct =TestDataGenerator.createCaseTracker();
         String interId=TestDataGenerator.createEmailMessage(ct).Id;
         TestDataGenerator.createAppConfig();
         String attachId= TestDataGenerator.createContent(ct);
         
         EmailController.EmailWrapper emailWrapper =new EmailController.EmailWrapper();
                                        emailWrapper.sendTo = new List<String>{'abc@xyz.com'};
                                        emailWrapper.sendCc= new List<String>{'pqr@xyz.com'};
                                        emailWrapper.sendBcc= new List<String>{'mno@xyz.com'};
                                        emailWrapper.subject='Test Subject';
                                        emailWrapper.attachmentIds= new List<String>{attachId};
                
        
         Test.startTest();
         EmailTemplate validEmailTemplate =  TestDataGenerator.createEmailTemplate();
         Map<String,Object> resp1=EmailController.quickReplyMethod('quickreply',validEmailTemplate.Id,interId,ct.Id);
         Map<String,Object> resp2=EmailController.quickReplyMethod('quickreplyall',validEmailTemplate.Id,interId,ct.Id);
         
         Test.stopTest();
    }
    class JsonObject{
        String interactionId;
        String emailTemplateFolder;
    }  
    
}