/*
  Authors	   :   Rahul Pastagiya
  Date created :   09/01/2018
  Purpose      :   Helper class for EmailMessage object
  Dependencies :   NA
  __________________________________________________________________________
  Modifications:
		    Date:  
		    Purpose of modification:  
		    Method/Code segment modified: 
 */
public class EmailMessageHandler {
    /*
    	Authors: Rahul Pastagiya
    	Purpose: To calculate the unread emails for specific case and update case manager by that count
		Dependencies : NA
	*/
    public static void calUnreadEmailCntAndUpdateCase(List<EmailMessage> newEmailMessage){        
        //To preapre set of all cases of EmailMessages in trigger.new list     
        Set < Id > caseIdSet = new Set < Id > ();
        for (EmailMessage em: newEmailMessage) {
            if (em.RelatedToId != null) {
                caseIdSet.add(em.RelatedToId);
            }           
        }
        //To get aggregate unread count Case wise 
        AggregateResult[] groupedResults = [SELECT COUNT(Id), RelatedToId FROM EmailMessage where RelatedToId in: caseIdSet and Read__c = false GROUP BY RelatedToId];
        List < CaseManager__c > ctList = new List < CaseManager__c > ();
        for (AggregateResult ar: groupedResults) {
            Id caseId = (ID) ar.get('RelatedToId');
            caseIdSet.remove(caseId);
            Integer count = (INTEGER) ar.get('expr0');
            CaseManager__c ct = new CaseManager__c(Id = caseId);
            ct.Unread_Email_Count__c = count;
            ctList.add(ct);
        }
        //To set unread count 0 for those cases whose record not found in above query
        for (Id caseId: caseIdSet) {
            CaseManager__c ct = new CaseManager__c(Id = caseId);
            ct.Unread_Email_Count__c = 0;
            ctList.add(ct);
        }
        update ctList;
    }
    
     /*
    	Authors: Rahul Pastagiya
    	Purpose: To mark all out bound messages as read
		Dependencies : NA
	*/
    public static void markOutboundMessageAsRead(List<EmailMessage> newEmailMessage){
         for(EmailMessage em : newEmailMessage){
            if(!em.Incoming){
                em.Read__c=true;
            }
        }
    }
}