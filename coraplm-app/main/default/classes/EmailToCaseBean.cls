/*
  Authors: Chandresh Koyani
  Date created: 24/10/2017
  Purpose: This class is use to store email data.
  Dependencies: EmailToCaseAttachment,EmailToCaseBean,EmailToCaseManager,EmailToCaseService
  -------------------------------------------------
  Modifications:
  Date: 
  Purpose of modification:
  Method/Code segment modified:
 
*/
public with sharing class EmailToCaseBean {

    /*Address List*/
    public String[] ccAddresses { get; set; }
    public String[] toAddresses { get; set; }
    public String fromAddress { get; set; }
    public String fromName { get; set; }

    /* Body and Subject*/
    public String htmlBody { get; set; }
    public String plainTextBody { get; set; }
    public String subject { get; set; }
    public string planSubject{get;set;}

    /*Other Information*/
    public String messageId { get; set; }
    public String[] references { get; set; }
    public Map<string, string> header { get; set; }

    /*Attachment */
    public List<EmailToCaseAttachment> attachments { get; set; }

    /*Envelop Information*/
    public String envelopFromAddress { get; set; }
    public String envelopToAddress { get; set; }

    /*Mailbox information*/

    public String inReplyTo { get; set; }

	public List<string> debugLog{get;set;}

    /*
      Authors: Chandresh Koyani
      Purpose:  Get comma seperated attachment names.
      Dependencies: EmailToCaseManager.cls.
     */
    public string attachmentExtensions {
        get {
            if (attachments != null) {
                List<string> attchmentExt = new List<string> ();
                for (EmailToCaseAttachment attch : attachments) {
                    attchmentExt.add('.' + attch.extension);
                }
                return string.join(attchmentExt, ',');
            }
            return '';
        }
    }

	public void appendIntoDebugLog(string methodName,string log){
		string str=methodName+'::'+log;
		debugLog.add(str);
	}
}