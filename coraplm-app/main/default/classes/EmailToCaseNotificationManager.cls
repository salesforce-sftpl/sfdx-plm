/*
  Authors: Chandresh Koyani
  Date created: 24/10/2017
  Purpose: Purpose of this class is to send email(Notification).
  Dependencies: EmailToCaseAttachment.cls,EmailToCaseManager.cls,EmailToCaseNotificationManager.cls
  -------------------------------------------------
  Modifications:
  Date: 08/12/2017
  Purpose of modification: Inline image in email | PUX-239
  Method/Code segment modified: Changed logic of "sendMail" method.
 
*/
public with sharing class EmailToCaseNotificationManager {

    /*
      Wrapper class to store notification properties.
     */
    public class EmailToCaseNotification {
        public string TemplateId { get; set; }
        public List<string> toAddresses { get; set; }
        public List<string> ccAddresses { get; set; }
        public List<string> bccAddresses { get; set; }
        public CaseManager__c caseTracker { get; set; }
        public Contact contact { get; set; }
        public string orgWideId { get; set; }
        public List<ContentVersion> attachments { get; set; }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: This method is used to send email with data contains in EmailToCaseNotification object.
      - Create SingleEmailMessage Object
      - Create EmailFileAttachment object if inline attachment is exist.
      - Merge Template fields.
      Dependencies: Use from "EmailToCaseManager.cls" to send email.
     
     */
    public void sendMail(EmailToCaseNotification emailToCaseNotification, EmailToCaseBean emailtoCaseBean) {
        try {
            Messaging.SingleEmailMessage mailMsg = new Messaging.SingleEmailMessage();

            EmailToCaseTemplateResponse emailMergeResponse = mergeTemplate(emailToCaseNotification.templateId, emailToCaseNotification.caseTracker.id);

            string formSection = getFormSection(emailtoCaseBean);
            
            string finalHtml = emailMergeResponse.Body + '<hr/>' + formSection + emailtoCaseBean.HtmlBody;
            //finalHtml = EmailToCaseSettingHelper.handleInlineImage(finalHtml, emailToCaseNotification.attachments);
            mailMsg.setHtmlBody(finalHtml);
            mailMsg.setSubject(emailMergeResponse.Subject);
            if(emailtoCaseBean.toAddresses!=null && emailtoCaseBean.toAddresses.size()>0){
                mailMsg.setReplyTo(emailtoCaseBean.toAddresses[0]);
            }
            List<Messaging.EmailFileAttachment> emailAttachments = new List<Messaging.EmailFileAttachment> ();
            for (EmailToCaseAttachment attachment : emailtoCaseBean.attachments) {
                if (attachment.isInline) {
                    Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                    efa.setFileName(attachment.FileName);
                    efa.setBody(attachment.BinaryBody);
                    efa.setInline(true);
                    emailAttachments.add(efa);
                }
            }

            mailMsg.setFileAttachments(emailAttachments);
            if (emailToCaseNotification.contact != null) {
                mailMsg.setTargetObjectId(emailToCaseNotification.contact.Id);
            }
            if (emailToCaseNotification.caseTracker.id != null) {
                mailMsg.setWhatId(emailToCaseNotification.caseTracker.id);
            }
            if (emailToCaseNotification.toAddresses != null) {
                mailMsg.setToAddresses(emailToCaseNotification.toAddresses);
            }
            if (emailToCaseNotification.ccAddresses != null) {
                mailMsg.setccAddresses(emailToCaseNotification.ccAddresses);
            }
            if (emailToCaseNotification.bccAddresses != null) {
                mailMsg.setBccAddresses(emailToCaseNotification.bccAddresses);
            }
            if (!string.isEmpty(emailToCaseNotification.orgWideId)) {
                mailMsg.setOrgWideEmailAddressId(emailToCaseNotification.orgWideId);
            }
            
            mailMsg = EmailController.replaceImageLinkCID(mailMsg,finalHtml);
            Messaging.sendEmail(new List<Messaging.SingleEmailMessage> { mailMsg });
            
            //Added by Ashish Kr. on 08-Dec-2017 | PUX-239            
            EmailMessage sentEmail = [Select Id,Subject,HtmlBody from EmailMessage Where RelatedToId =:emailToCaseNotification.caseTracker.id Order By CreatedDate DESC LIMIT 1];
            sentEmail.HtmlBody = finalHtml;
            update sentEmail;
            
        }
        catch(Exception ex) {
            throw new EmailToCaseException(ex);
        }
    }

    /*
      Wrapper class handle Merge Template response.
     */
    public class EmailToCaseTemplateResponse {
        public string body {
            get; set;
        }
        public string subject {
            get; set;
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: This method take templateId and case object id as parameter and returns subject and body with merged values.
      Dependencies: Use in "SendMail" method.
     */
    private EmailToCaseTemplateResponse mergeTemplate(string templateId, string caseTrackerId) {
        EmailToCaseTemplateResponse response = new EmailToCaseTemplateResponse();

        Messaging.SingleEmailMessage email = Messaging.renderStoredEmailTemplate(templateId, null, caseTrackerId);

        response.body = email.getHTMLBody();
        response.subject = email.getSubject();

        return response;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Get email header section, create Html header setions using email fields.
      Dependencies: Use in "SendMail" method.
     
     */
    private string getFormSection(EmailToCaseBean emailtoCaseBean) {
        String fromSection = '<br><span style="border:none;border-top:solid #B5C4DF 1.0pt;padding:3.0pt 0in 0in 0in"></span>' +
        '<p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">From:</span></b><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"> '
        + emailtoCaseBean.fromAddress +
        '<br>';

        if (emailtoCaseBean.toAddresses != null) {
            fromSection += '<b>To:</b> ' + string.join(emailtoCaseBean.toAddresses, ',') + '<br>';
        }
        if (emailtoCaseBean.ccAddresses != null) {
            fromSection += '<b>Cc:</b> ' + string.join(emailtoCaseBean.ccAddresses, ',') + '<br>';
        }
        fromSection += '<b>Subject:</b> ' + emailtoCaseBean.subject + '<p></p></span></p>';
        return fromSection;
    }


}