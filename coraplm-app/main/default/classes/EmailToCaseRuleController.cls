/*
  Authors: Chandresh Koyani
  Date created: 24/10/2017
  Purpose: Controller class of EmailToCaseRule.page.
  Dependencies: EmailToCaseRules.page,EmailToCaseSettingHelper.cls,EmailToCaseManager.cls
  -------------------------------------------------
  Modifications:
  Date: 30/10/2017
  Purpose of modification: Changed Notification logic
  Method/Code segment modified: Added new field "SendACKMail" on JSONValue.
 
*/
public with sharing class EmailToCaseRuleController {

    /*
      Authors: Chandresh Koyani
      Purpose: Wrapper class write response.
      Dependencies: This method is called from "EmailToCaseRules.page".
     */
    public class RuleResponse {
        public List<EmailToCaseSettingHelper.RuleField> ruleFields { get; set; }
        public List<EmailToCaseSettingHelper.FieldInfo> caseTrackerFields { get; set; }
        public List<EmailTemplate> emailTemplates { get; set; }
        public List<OrgWideEmailAddress> orgWideAddresses { get; set; }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Get data for page load.
      Dependencies: This method is called from "EmailToCaseRules.page".
     */
    @RemoteAction
    public static RuleResponse getInitData() {
        RuleResponse response = new RuleResponse();
        response.ruleFields = getRuleFields();
        response.caseTrackerFields = getAllFields();
        response.emailTemplates = getAllEmailTemplate();
        response.orgWideAddresses = getAllOrgWideEmailAddress();
        return response;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Geat all rules fields.
      Dependencies: Use in "GetInitData" method.
     */
    @RemoteAction
    public static List<EmailToCaseSettingHelper.RuleField> getRuleFields() {
        return EmailToCaseSettingHelper.getRuleFields();
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Geat all case object fields.
      Dependencies: Use in "GetInitData" method.
     */
    @RemoteAction
    public static List<EmailToCaseSettingHelper.FieldInfo> getAllFields() {
        List<EmailToCaseSettingHelper.FieldInfo> fieldNames = new List<EmailToCaseSettingHelper.FieldInfo> ();

        Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(ObjectUtil.getPackagedFieldName('CaseManager__c')).getDescribe().fields.getMap();
        for (Schema.SObjectField sfield : fieldMap.Values())
        {
            schema.describefieldresult dfield = sfield.getDescribe();

            string type = String.valueOf(dfield.getType());
            if (dField.isCustom() && !dField.isCalculated() && !type.equalsIgnoreCase('REFERENCE')) {

                EmailToCaseSettingHelper.FieldInfo fieldInfo = new EmailToCaseSettingHelper.FieldInfo();
                fieldInfo.label = dfield.getLabel();
                fieldInfo.apiName = dfield.getName();
                fieldInfo.pickListValue = new list<EmailToCaseSettingHelper.Option> ();
                fieldInfo.type = type;
                fieldInfo.operator = EmailToCaseSettingHelper.OperatorMap.get(fieldInfo.Type);

                if (fieldInfo.Type.equalsIgnoreCase('PICKLIST')) {
                    List<Schema.PicklistEntry> picklist = dfield.getPicklistValues();
                    for (Schema.PicklistEntry pickListVal : picklist) {
                        fieldInfo.pickListValue.add(new EmailToCaseSettingHelper.option(pickListVal.getValue(), pickListVal.getLabel()));
                    }
                }
                else if (fieldInfo.Type.equalsIgnoreCase('BOOLEAN')) {
                    fieldInfo.pickListValue.add(new EmailToCaseSettingHelper.option('True', 'True'));
                    fieldInfo.pickListValue.add(new EmailToCaseSettingHelper.option('False', 'False'));
                }
                fieldNames.add(fieldInfo);
            }
        }
        return fieldNames;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Geat all templates for send mail.
      Dependencies: Use in "GetInitData" method.
     */
    @RemoteAction
    public static List<EmailTemplate> getAllEmailTemplate() {
        try {
            List<EmailTemplate> emailTemplates = [Select id, Name from EmailTemplate where isactive=true and Folder.Name=:PLMConstants.EMAIL_TO_CASE_NOTIFICATION_TEMPLATE_FOLDER];
            return emailTemplates;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('EmailToCaseRuleController', ex.getMessage(), ex.getStackTraceString());
            return null;
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Geat all OrgWide email addresses for send mail.
      Dependencies: Use in "GetInitData" method.
     */
    @RemoteAction
    public static List<OrgWideEmailAddress> getAllOrgWideEmailAddress() {
        try {
            return[SELECT Address, Id FROM OrgWideEmailAddress limit 999];
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('EmailToCaseRuleController', ex.getMessage(), ex.getStackTraceString());
            return null;
        }
    }



    /*
      Authors: Chandresh Koyani
      Purpose: Insert/Update Rules, Generate new order number based on last order number store in system.
      Dependencies: Called from "EmailToCaseRules.Page"
     */
    @RemoteAction
    public static string saveEmailToCaseRules(EmailToCaseSettingHelper.EmailToCaseRule emailToCaseRules) {
        try {
            String str = JSON.serialize(emailToCaseRules.JSONValue);
            Email_To_Case_Rule__c emailRules = new Email_To_Case_Rule__c();
            emailRules.Rule_Name__c = emailToCaseRules.RuleName;
            emailRules.JSON__c = str;
            emailRules.Type__c = emailToCaseRules.type;
            emailRules.IsActive__c = emailToCaseRules.isActive;
            if (!string.isEmpty(emailToCaseRules.Id)) {
                emailRules.Id = emailToCaseRules.Id;
            }
            else {
                AggregateResult[] groupedResults = [select MAX(Order__c) from Email_To_Case_Rule__c where Type__c = :emailToCaseRules.Type];
                integer nextOrder = 1;
                if (groupedResults != null && groupedResults.size() > 0) {
                    integer maxOrder = integer.valueOf(groupedResults[0].get('expr0'));
                    if (maxOrder != null) {
                        nextOrder = maxOrder + 1;
                    }
                }
                emailRules.Order__c = nextOrder;
            }

            upsert emailRules;
            return 'SUCCESS';
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('EmailToCaseRuleController', ex.getMessage(), ex.getStackTraceString());
            return '';
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Get all email to case rules.
      Dependencies: Called from "EmailToCaseRules.Page"
     */
    @RemoteAction
    public static List<EmailToCaseSettingHelper.EmailToCaseRule> getAllEmailToCaseRules() {
        List<EmailToCaseSettingHelper.EmailToCaseRule> emailRules = new List<EmailToCaseSettingHelper.EmailToCaseRule> ();

        List<Email_To_Case_Rule__c> emailToCaseRules = [select id, Name, Rule_Name__c, JSON__c, Order__c, Type__c, IsActive__c from Email_To_Case_Rule__c order by Order__c limit 999];

        for (Email_To_Case_Rule__c emailToCaseRuleObj : emailToCaseRules) {
            EmailToCaseSettingHelper.JSONValue jsonField = (EmailToCaseSettingHelper.JSONValue) System.JSON.deserialize(emailToCaseRuleObj.JSON__c, EmailToCaseSettingHelper.JSONValue.class);
            EmailToCaseSettingHelper.EmailToCaseRule emailRuleObj = new EmailToCaseSettingHelper.EmailToCaseRule();
            emailRuleObj.JSONValue = jsonField;
            emailRuleObj.ruleName = emailToCaseRuleObj.Rule_Name__c;
            emailRuleObj.order = Integer.valueOf(emailToCaseRuleObj.Order__c);
            emailRuleObj.type = emailToCaseRuleObj.Type__c;
            emailRuleObj.Id = emailToCaseRuleObj.Id;
            emailRuleObj.isActive = emailToCaseRuleObj.IsActive__c;
            emailRules.add(emailRuleObj);
        }

        return emailRules;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Remove email to case rules based on rule Id.
      Dependencies: Called from "EmailToCaseRules.Page"
     */
    @RemoteAction
    public static void removeEmailToCaseRules(string id) {
        try {
            List<Email_To_Case_Rule__c> emailToCaseRules = [select id from Email_To_Case_Rule__c where id = :id];
            delete emailToCaseRules;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('EmailToCaseRuleController', ex.getMessage(), ex.getStackTraceString());
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Change state of a rule(Active/Inactive).
      Dependencies: Called from "EmailToCaseRules.Page"
     */
    @RemoteAction
    public static void changeState(string id, boolean isActive) {
        try {
            Email_To_Case_Rule__c rules = new Email_To_Case_Rule__c();
            rules.Id = id;
            rules.IsActive__c = isActive;
            update rules;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('EmailToCaseRuleController', ex.getMessage(), ex.getStackTraceString());
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Change order of a rule.
      Dependencies: Called from "EmailToCaseRules.Page"
     */
    @RemoteAction
    public static void changeOrder(integer currentOrder, string orderType, string ruleType) {
        try {
            integer previousNextCount = 0;
            if (orderType.equalsIgnoreCase('UP')) {
                previousNextCount = currentOrder - 1;
            }
            else {
                previousNextCount = currentOrder + 1;
            }
            List<Email_To_Case_Rule__c> emailToCaseRules = [select id, Order__c from Email_To_Case_Rule__c where Type__c = :ruleType and(Order__c = :currentOrder OR Order__c = :previousNextCount)];
            for (Email_To_Case_Rule__c rule : emailToCaseRules) {
                if (rule.Order__c == currentOrder) {
                    rule.Order__c = previousNextCount;
                }
                else {
                    rule.Order__c = currentOrder;
                }
            }
            update emailToCaseRules;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('EmailToCaseRuleController', ex.getMessage(), ex.getStackTraceString());
        }
    }
}