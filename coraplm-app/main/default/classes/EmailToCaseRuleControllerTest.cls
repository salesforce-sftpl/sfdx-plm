/**
  * Authors     : Chandresh Koyani
  * Date created : 17/11/2017 
  * Purpose      : Test Class for EmailToCaseRuleControllerTest 
  * Dependencies : EmailToCaseSettingHelper.cls,EmailToCaseRuleController.cls
  * -------------------------------------------------
  *Modifications:
                Date:   
                Purpose of modification:  
                Method/Code segment modified: 

*/
@isTest
private class EmailToCaseRuleControllerTest { 
    @testSetup 
    static void setupTestData() {
        EmailToCaseSettingHelper.JSONValue jsonObj=new EmailToCaseSettingHelper.JSONValue();
        jsonObj.Criterias=new List<EmailToCaseSettingHelper.Criteria>();
        jsonObj.NotifiationCriterias=new List<EmailToCaseSettingHelper.Criteria>();
        jsonObj.SendACKMail=true;
        jsonObj.SendACKMailToSender=true;
        jsonObj.IsNotifiationCriteria=true;

        EmailToCaseSettingHelper.Criteria cre=null;
        cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field='Sender Email';
        cre.Operator='contains';
        cre.Value='@sftpl.com';
        cre.Type='TEXT';
        cre.Source='EMAIL';
        jsonObj.Criterias.add(cre);
        jsonObj.NotifiationCriterias.add(cre);

        cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field='Attachment Count';
        cre.Operator='greater or equal';
        cre.Value='2';
        cre.Type='NUMBER';
        cre.Source='EMAIL';
        jsonObj.Criterias.add(cre);

        Email_To_Case_Rule__c emailToCaseRule=new Email_To_Case_Rule__c();
        emailToCaseRule.IsActive__c=true;
        emailToCaseRule.JSON__c=JSON.serialize(jsonObj);
        emailToCaseRule.Rule_Name__c='BlacklistedDomain';
        emailToCaseRule.Order__c=1;
        emailToCaseRule.Type__c='Ignore';

        insert emailToCaseRule;
    }
    public static testMethod void testFirst(){
        EmailToCaseRuleController ruleController=new EmailToCaseRuleController();
        EmailToCaseRuleController.GetInitData();
        EmailToCaseRuleController.GetAllEmailToCaseRules();
        Email_To_Case_Rule__c emailToCaseRules = [select id,Name,JSON__c from Email_To_Case_Rule__c];
        
        EmailToCaseRuleController.ChangeState(emailToCaseRules.id,true);
        EmailToCaseRuleController.ChangeOrder(1,'UP','Ignore');
        

        EmailToCaseSettingHelper.EmailToCaseRule rule=new EmailToCaseSettingHelper.EmailToCaseRule();
        rule.Id=emailToCaseRules.Id;
        rule.RuleName='Test';
        rule.Type='Ignore';
        rule.IsActive=true;
        rule.JSONValue = (EmailToCaseSettingHelper.JSONValue) System.JSON.deserialize(emailToCaseRules.JSON__c, EmailToCaseSettingHelper.JSONValue.class);

        EmailToCaseRuleController.SaveEmailToCaseRules(rule);

        EmailToCaseSettingHelper.EmailToCaseRule ruleNew=new EmailToCaseSettingHelper.EmailToCaseRule();
        ruleNew.RuleName='Test';
        ruleNew.Type='Ignore';
        ruleNew.IsActive=true;
        ruleNew.JSONValue = (EmailToCaseSettingHelper.JSONValue) System.JSON.deserialize(emailToCaseRules.JSON__c, EmailToCaseSettingHelper.JSONValue.class);

        EmailToCaseRuleController.SaveEmailToCaseRules(ruleNew);

        EmailToCaseRuleController.RemoveEmailToCaseRules(emailToCaseRules.id);
    }
}