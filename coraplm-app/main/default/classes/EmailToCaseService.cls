/*
  Authors: Chandresh Koyani
  Date created: 24/10/2017
  Purpose: Landing class for email, When system recevied email
  Dependencies: 
  -------------------------------------------------
  Modifications:
  Date: 
  Purpose of modification:
  Method/Code segment modified:
 
*/
public with sharing class EmailToCaseService implements Messaging.InboundEmailHandler {

	/*
	  Authors: Chandresh Koyani
	  Purpose: When salesforce recevied new email, it will call this method.
	  - Extract email fields
	  - Extract attachments
	  - Extract envelope fields
	  - Extract body
	  - Call Manager class for processing.
	  Dependencies: Method of InboundEmailHandler interface.
	 
	 */
	public Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
		Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
		EmailToCaseBean emailtoCaseBean = new EmailToCaseBean();
		emailtoCaseBean.debugLog=new List<string>();

		try {
			emailtoCaseBean.appendIntoDebugLog('handleInboundEmail','Extract Data Start');

			emailtoCaseBean.fromAddress = email.fromAddress;
			emailtoCaseBean.toAddresses = email.toAddresses;
			emailtoCaseBean.ccAddresses = email.ccAddresses;
			emailtoCaseBean.fromName = email.fromName;

			emailtoCaseBean.envelopFromAddress = envelope.fromAddress;
			emailtoCaseBean.envelopToAddress = envelope.toAddress;

			emailtoCaseBean.attachments = extractAttachments(email);

			emailtoCaseBean.messageId = email.messageId;
			emailtoCaseBean.references = email.references;
			emailtoCaseBean.inReplyTo = email.inReplyTo;

			emailtoCaseBean.header = getHeaderMap(email.headers);

			
			if (email.plainTextBody != null) {
				emailtoCaseBean.plainTextBody = email.plainTextBody.escapeXml();
			} else {
				emailtoCaseBean.plainTextBody = '';
			}

			if (email.htmlBody != null) {
				emailtoCaseBean.htmlBody = email.htmlBody;

			}
			else {
				emailtoCaseBean.htmlBody = '';
			}
			emailtoCaseBean.subject = email.subject;

			if (!string.isEmpty(emailtoCaseBean.subject)) {
				emailtoCaseBean.planSubject = EmailToCaseSettingHelper.getPlanSubject(emailtoCaseBean.subject);
			}
			emailtoCaseBean.appendIntoDebugLog('handleInboundEmail','Extract Data End');
			EmailToCaseManager manager = new EmailToCaseManager();
			manager.processCase(emailtoCaseBean);
		}
		catch(EmailToCaseException ex) {
			ExceptionHandlerUtility.writeEmailException(emailtoCaseBean,ex.caseManager, ex.getMessage(), ex.getStackTraceString());
		}
		catch(Exception ex) {
			ExceptionHandlerUtility.writeEmailException(emailtoCaseBean,null, ex.getMessage(), ex.getStackTraceString());
		}
		return result;
	}

	/*
	  Authors: Chandresh Koyani
	  Purpose: Get attachment from InboundEmail and create list of EmailToCaseAttachment.cls
	  Dependencies: called from "handleInboundEmail" method to fill attachment.
	 
	 */
	private List<EmailToCaseAttachment> extractAttachments(Messaging.InboundEmail email) {
		List<EmailToCaseAttachment> emailAttachments = new List<EmailToCaseAttachment> ();
		if (email.textAttachments != null) {
			for (Messaging.InboundEmail.TextAttachment textAttachment : email.textAttachments) {
				EmailToCaseAttachment emailAttachment = new EmailToCaseAttachment();
				emailAttachment.body = textAttachment.Body;
				emailAttachment.fileName = textAttachment.fileName;
				emailAttachment.headers = getHeaderMap(textAttachment.headers);
				emailAttachment.mimeTypeSubType = textAttachment.mimeTypeSubType;
				emailAttachment.type = 'Text';
				emailAttachments.add(emailAttachment);
			}
		}
		if (email.binaryAttachments != null) {
			for (Messaging.InboundEmail.binaryAttachment binaryAttachment : email.binaryAttachments) {
				EmailToCaseAttachment emailAttachment = new EmailToCaseAttachment();
				emailAttachment.binaryBody = binaryAttachment.Body;
				emailAttachment.fileName = binaryAttachment.fileName;
				emailAttachment.headers = getHeaderMap(binaryAttachment.headers);
				emailAttachment.mimeTypeSubType = binaryAttachment.mimeTypeSubType;
				emailAttachment.type = 'Binary';
				emailAttachments.add(emailAttachment);
			}
		}

		return emailAttachments;
	}

	/*
	  Authors: Chandresh Koyani
	  Purpose: Get Key/Value pair of email header.
	  Dependencies: Use in "handleInboundEmail" method to get email header and "ExtractAttachments" method to get attachment header.
	 
	 */
	private Map<string, string> getHeaderMap(List<Messaging.InboundEmail.Header> emailHeader) {
		Map<string, string> headerMap = new Map<string, string> ();
		for (Messaging.InboundEmail.Header header : emailHeader) {
			headerMap.put(header.name, header.value);
		}
		return headerMap;
	}
	
}