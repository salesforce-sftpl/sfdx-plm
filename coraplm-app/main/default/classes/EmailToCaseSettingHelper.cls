/*
Authors: Chandresh Koyani
Date created: 24/10/2017
Purpose: This class contains method that used for settings and utility.
Dependencies: EmailToCaseManager.cls,EmailToCaseNotificationManager.cls,EmailToCaseRuleController.cls,EmailToCaseSettingHelper.cls
-------------------------------------------------
Modifications:
  Date: 17/12/2017
  Purpose of modification: Inline image in email | PUX-239
  Method/Code segment modified: added salesforce instance base url to image url in "createImageLinks" method.
  ------------------------------------------------
  Date: 29/12/2017
  Purpose of modification: Inline image in email | PUX-239
  Method/Code segment modified: added new method to get salesforce instance base url 
                
*/
public with sharing class EmailToCaseSettingHelper { 
    
    /*
      Authors: Chandresh Koyani
      Purpose: Wrapper class to store RuleField information like field,operator fieldType,values etc.
      Dependencies: EmailToCaseRuleController.cls,EmailToCaseRules.page,QCRules.page,ReminderRules.page,TATRules.page
     */
    public class RuleField{
        public RuleField(){

        }
        public RuleField(string field,string fieldType, List<string> operator,List<string> values){
            this.field=field;
            this.operator=operator;
            this.fieldType=fieldType;
            this.values=values;
        }
        public string field{get;set;}
        public List<string> operator{get;set;}
        public string fieldType{get;set;}
        public List<string> values{get;set;}
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Wrapper class to store option like value,text.
      Dependencies: EmailToCaseRuleController.cls,EmailToCaseRules.page.
     */
    public class Option {
        public string value { get; set; }
        public string text { get; set; }

        public Option() {
        }
        public Option(string value, string text) {
            this.value = value;
            this.text = text;
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Wrapper class to store field information like label,apiName etc.
      Dependencies: EmailToCaseRuleController.cls,EmailToCaseRules.page.
     */
    public class FieldInfo {
        public string label {
            get; set;
        }
        public string apiName {
            get; set;
        }
        public string type {
            get; set;
        }
        public list<Option> pickListValue {
            get; set;
        }
        public List<string> operator{
            get; set;
        }
    }
    
    /*
      Authors: Chandresh Koyani
      Purpose:Wrapper class to store Criteria values.
      Dependencies: EmailToCaseRuleController.cls,EmailToCaseRules.page.
     */
    public class Criteria {
        public string field { get; set; }
        public string operator { get; set; }
        public string value { get; set; }
        public string type { get; set; }
        public string source{get;set;}
        public boolean useEmailField{get;set;}
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Wrapper class to store field information for action.
      Dependencies: EmailToCaseRuleController.cls,EmailToCaseRules.page.
     */
    public class ActionField {
        public string field { get; set; }
        public string value { get; set; }
        public string fieldLabel { get; set; }
        public boolean useEmailField { get; set; }
        public string type{get; set;}
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Wrapper class to store JSON values for criterias,actionFields etc.
      Dependencies: EmailToCaseRuleController.cls,EmailToCaseRules.page.
     */
    public class JSONValue {
        Public List<Criteria> criterias { get; set; }
        Public List<ActionField> actionFields { get; set; }
        public boolean sendACKMail { get; set; }
        public boolean sendACKMailToSender { get; set; }
        public boolean includeCCInACK{ get; set; }
        public string templateId { get; set; }
        public boolean isNotifiationCriteria { get; set; }
        Public List<Criteria> notifiationCriterias { get; set; }
        public string fromId { get; set; }
        public string additionalEmails { get; set; }
        public string action { get; set; }
        public boolean findCaseIdInSubject{get;set;}
        public boolean findCaseIdInBody{get;set;}
        public string findCaseIdFrom{get;set;}
        public string excludeKeywords{get;set;}
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Wrapper class to store EmailToCaseRule values.
      Dependencies: EmailToCaseRuleController.cls,EmailToCaseRules.page.
     */
    public class EmailToCaseRule {
        public JSONValue JSONValue { get; set; }
        public string ruleName { get; set; }
        public integer order { get; set; }
        public string id { get; set; }
        Public string type { get; set; }
        public boolean isActive{get;set;}
    }

    public static Map<string,List<string>> operatorMap{
        get;set;
    }

    static{
        operatorMap=new Map<string,List<string>>();
        List<string> textOperator=new List<string>{'equals','not equal to','starts with','end with','contains','does not contain','contains in list'};
        List<string> numberOperator=new List<string>{'equals','not equal to','less than','greater than','less or equal','greater or equal'};
        List<string> pickListOperator=new List<string>{'equals','not equal to','contains in list','does not contains in list'};
        List<string> booleanOperator=new List<string>{'equals','not equal to'};

        operatorMap.put('TEXT',textOperator);
        operatorMap.put('NUMBER',numberOperator);
        operatorMap.put('CURRENCY',numberOperator);
        operatorMap.put('DOUBLE',numberOperator);
        operatorMap.put('PICKLIST',pickListOperator);
        operatorMap.put('BOOLEAN',booleanOperator);
        operatorMap.put('EMAIL',textOperator);
        operatorMap.put('PHONE',textOperator);
        operatorMap.put('STRING',textOperator);
        operatorMap.put('REFERENCE',new List<string>{'equals','not equal to','contains in list'});
        operatorMap.put('TEXTAREA',new List<string>{'contains','does not contain'});
    }
    /*
        Authors: Chandresh Koyani
        Purpose: Get list of all fields for configure Email To Case Rules.
        Dependencies: Use in "EmailToCaseRuleController.cls".
    */
    public static List<RuleField> getRuleFields(){
        List<RuleField> ruleFields=new List<RuleField>();

        ruleFields.add(new RuleField('Sender Email','TEXT',operatorMap.get('TEXT'),null));
        ruleFields.add(new RuleField('To Addresses','TEXT',operatorMap.get('TEXT'),null));
        ruleFields.add(new RuleField('CC Addresses','TEXT',operatorMap.get('TEXT'),null));
        ruleFields.add(new RuleField('Salesforce Email','TEXT',operatorMap.get('TEXT'),null));
        ruleFields.add(new RuleField('ReplyTo','TEXT',operatorMap.get('TEXT'),null));

        ruleFields.add(new RuleField('Subject','TEXT',operatorMap.get('TEXT'),null));

        ruleFields.add(new RuleField('Body','TEXT',new List<string>{'contains'},null));
        ruleFields.add(new RuleField('Importance','PICKLIST',operatorMap.get('BOOLEAN'),new List<string>{'High','Low'}));
        ruleFields.add(new RuleField('Attachment Count','NUMBER',operatorMap.get('NUMBER'),null));
        ruleFields.add(new RuleField('Attachment Ext','TEXT',new List<string>{'contains','does not contain'},null));
        ruleFields.add(new RuleField('Auto Forward','PICKLIST',operatorMap.get('BOOLEAN'),new List<string>{'True','False'}));
        return ruleFields;
    }

    /*
    ------------> Obsolete method <------------
        Authors: Chandresh Koyani
        Purpose: Replace inline image of email body with ContentVersion Title.
        Dependencies: Use in "EmailToCaseNotificationManager.cls".
    */
    public static String handleInlineImage(String body, List<ContentVersion> cvList) {
        for (ContentVersion cv : cvList) {
            try {
                String strToBeReplaced = 'src="cid:' + cv.Title;
                if (body != null && body.indexOf(strToBeReplaced) != - 1) {
                    Integer index = body.indexOf(strToBeReplaced);
                    String cidString = body.substring(index, body.substring(index + 1).indexOf('>') + index);
                    body = body.replace(cidString, 'src="cid:' + cv.Title);
                }
            } catch(Exception ex) {
                ExceptionHandlerUtility.writeException('EmailToCaseSettingHelper', ex.getMessage(), ex.getStackTraceString());
            }
        }
        return body;
    }
    /*
        Authors: Chandresh Koyani
        Purpose: Replace inline image of email body with ContentVersion URL.
        Dependencies: Use in "EmailToCaseManager.cls".
    */
    public static String createImageLinksBody(String body,List<ContentVersion> contentDocList){
        Map<String,String> imageWithURL = new Map<String,String>();
        imageWithURL = createImageLinks(contentDocList);
        for(ContentVersion cv: contentDocList){
            try{
                String strToBeReplaced = 'src="cid:' + cv.title;
                if(body!=null && body.indexOf(strToBeReplaced)!=-1){
                    Integer index = body.indexOf(strToBeReplaced);
                    String cidString = body.substring(index,body.substring(index+1).indexOf('>')+index);
                    
                    body = body.replace(cidString, 'src="' + imageWithURL.get(cv.title));
                } 
            }catch(Exception ex){
                ExceptionHandlerUtility.writeException('EmailToCaseSettingHelper', ex.getMessage(), ex.getStackTraceString());
            }
        }
        return body;
    }
    /*
        Authors: Chandresh Koyani
        Purpose: Get Title, URL map for ContentVersion object.
        Dependencies: Use in "EmailToCaseManager.cls".
    */
    public static Map<String,String> createImageLinks(List<ContentVersion> cvList){
        Map<String,String> imageWithURL = new Map<String,String>();
        String siteURL = EmailToCaseSettingHelper.getHttpsSalesforceBaseURL();
        for(ContentVersion cv : cvList){
           String fullFileURL = siteURL+'/sfc/servlet.shepherd/document/download/' + cv.ContentDocumentId + '?asPdf=false&operationContext=CHATTER0'; 
            imageWithURL.put(cv.title, fullFileURL);
        }
        return imageWithURL;
    }
    
    /*
        Authors: Ashish Kr.
        Purpose: Get Salesforce instance base url.
        Dependencies: Use in "EmailToCaseSettingHelper.cls, EmailController.cls".
    */
    public static String getHttpsSalesforceBaseURL(){
        String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        if (baseURL.startsWith('http:')){
            baseURL = baseURL.replaceFirst('http:', 'https:');
        }
        List<String> baseURLList = baseURL.split('\\.');
        String siteURL = baseURLList[0] + '.lightning.force.com';       
        return siteURL;
    }

    public static String limitLength(String input, Integer maxLength) {
        String results;
        if (input != null && input.length() > maxLength) {
            results= input.substring(0, maxLength);
        } else {
            results = input;
        }
        return results;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: This method retuns plan subject, remove Case ID From Subject.
      Dependencies: Use in AddAttachment Method.
     */
    public static string getPlanSubject(string subject) {
        string planSubject = subject.replaceAll(PLMConstants.REGEX_FOR_CASE_SEARCH_IN_SUBJECT, '');
        planSubject = planSubject.replaceAll('[ ]{2,}', ' ');
        planSubject = planSubject.trim();
        return planSubject;
    }
    
    public static string removePrefixFromSubject(string subject,List<string> subjectPrefix){

		string formatedSubject=subject.toLowerCase();

		for(string perfix : subjectPrefix){
			formatedSubject=formatedSubject.replaceAll(perfix.toLowerCase(),'');
			formatedSubject=formatedSubject.trim();
		}
		return formatedSubject;
    }

    public static Integer getFieldLength(String customObjectName,String fieldName){
        Map<string, Schema.sObjectType> describeInfo = Schema.getGlobalDescribe();
        Schema.sObjectType objSObjectType = describeInfo.get(customObjectName);
        Map<String, Schema.SobjectField> mapFields = objSObjectType.getDescribe().fields.getMap();
        Schema.SobjectField objSobjectField=mapFields.get(fieldName);
        return objSobjectField.getDescribe().getLength();
    }
}