/**
  * Authors     : Chandresh Koyani
  * Date created : 17/11/2017 
  * Purpose      : Test Class for EmailToCaseTest
  * Dependencies : EmailToCaseSettingHelper.cls,EmailToCaseBean.cls,EmailToCaseAttachment.cls,EmailToCaseManager.cls,EmailToCaseService.cls
  * -------------------------------------------------
  *Modifications:
                Date:   
                Purpose of modification:  
                Method/Code segment modified: 

*/
@isTest
private class EmailToCaseTest { 
    @testSetup 
    static void setupTestData() {
        
        EmailTemplate validEmailTemplate = new EmailTemplate();

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            //Create Default template
            
            validEmailTemplate.isActive = true;
            validEmailTemplate.Name = 'name';
            validEmailTemplate.DeveloperName = 'testTemplate';
            validEmailTemplate.TemplateType = 'text';
            validEmailTemplate.FolderId = UserInfo.getUserId();

            insert validEmailTemplate;
        }
        createIgnoreRule(validEmailTemplate.id);
        createAcceptRule(validEmailTemplate.id);
        createExitingCaseRule(validEmailTemplate.id);
		createIdentifyExitingCaseRule(validEmailTemplate.id);

        //Create Default account object
        Account acc=new Account();
        acc.Name=PLMConstants.DEFAULT_ACCOUNT_NAME;
        insert acc;

        CaseManager__c caseObj=new CaseManager__c();
        caseObj.Process__c='AP';
        caseObj.WorkType__c='Non-PO Invoices';
        caseObj.Document_Type__c='Electricity';
        caseObj.Requestor_Email__c='chandresh.koyani@test123.com';
        caseObj.Subject__c='Test';
        caseObj.Status__c='Closed';
		caseObj.Subject_Readonly__c='Test';
        insert caseObj;
        
		TestDataGenerator.createAppConfig();
    }
    private static void createIgnoreRule(string templateId){
        EmailToCaseSettingHelper.JSONValue jsonObj=new EmailToCaseSettingHelper.JSONValue();
        jsonObj.Criterias=new List<EmailToCaseSettingHelper.Criteria>();
        jsonObj.NotifiationCriterias=new List<EmailToCaseSettingHelper.Criteria>();
        jsonObj.SendACKMail=true;
        jsonObj.SendACKMailToSender=true;
        jsonObj.IsNotifiationCriteria=true;
        jsonObj.TemplateId=templateId;

        EmailToCaseSettingHelper.Criteria cre=null;
        cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field='Sender Email';
        cre.Operator='contains';
        cre.Value='@sftpl.com';
        cre.Type='TEXT';
        cre.Source='EMAIL';
        jsonObj.Criterias.add(cre);
        jsonObj.NotifiationCriterias.add(cre);

        cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field='Attachment Count';
        cre.Operator='greater or equal';
        cre.Value='2';
        cre.Type='NUMBER';
        cre.Source='EMAIL';
        jsonObj.Criterias.add(cre);

        Email_To_Case_Rule__c emailToCaseRule=new Email_To_Case_Rule__c();
        emailToCaseRule.IsActive__c=true;
        emailToCaseRule.JSON__c=JSON.serialize(jsonObj);
        emailToCaseRule.Rule_Name__c='BlacklistedDomain';
        emailToCaseRule.Order__c=1;
        emailToCaseRule.Type__c='Ignore';

        insert emailToCaseRule;
    }

    private static void createAcceptRule(string templateId){
        EmailToCaseSettingHelper.JSONValue jsonObj=new EmailToCaseSettingHelper.JSONValue();
        jsonObj.Criterias=new List<EmailToCaseSettingHelper.Criteria>();
        jsonObj.ActionFields=new List<EmailToCaseSettingHelper.ActionField>();
        jsonObj.SendACKMail=true;
        jsonObj.SendACKMailToSender=true;
        jsonObj.IsNotifiationCriteria=false;
        jsonObj.TemplateId=templateId;

        EmailToCaseSettingHelper.Criteria cre=null;
        cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field='Subject';
        cre.Operator='contains';
        cre.Value='AP_PO';
        cre.Type='TEXT';
        cre.Source='EMAIL';
        jsonObj.Criterias.add(cre);

        cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field='Attachment Count';
        cre.Operator='greater or equal';
        cre.Value='2';
        cre.Type='NUMBER';
        cre.Source='EMAIL';
        jsonObj.Criterias.add(cre);

        EmailToCaseSettingHelper.ActionField actionField=null;

        actionField=new EmailToCaseSettingHelper.ActionField();
        actionField.Field=ObjectUtil.getPackagedFieldName('Process__c');
        actionField.value='AP';
        actionField.UseEmailField=false;
        actionField.Type='PICKLIST';
        jsonObj.ActionFields.add(actionField);

        actionField=new EmailToCaseSettingHelper.ActionField();
        actionField.Field=ObjectUtil.getPackagedFieldName('WorkType__c');
        actionField.value='Non-PO Invoices';
        actionField.UseEmailField=false;
        actionField.Type='PICKLIST';
        jsonObj.ActionFields.add(actionField);

        actionField=new EmailToCaseSettingHelper.ActionField();
        actionField.Field=ObjectUtil.getPackagedFieldName('Document_Type__c');
        actionField.value='Electricity';
        actionField.UseEmailField=false;
        actionField.Type='PICKLIST';
        jsonObj.ActionFields.add(actionField);

        actionField=new EmailToCaseSettingHelper.ActionField();
        actionField.Field=ObjectUtil.getPackagedFieldName('Status__c');
        actionField.value='Start';
        actionField.UseEmailField=false;
        actionField.Type='PICKLIST';
        jsonObj.ActionFields.add(actionField);

        actionField=new EmailToCaseSettingHelper.ActionField();
        actionField.Field=ObjectUtil.getPackagedFieldName('Requestor_Email__c');
        actionField.value='Sender Email';
        actionField.UseEmailField=true;
        actionField.Type='TEXT';
        jsonObj.ActionFields.add(actionField);

        actionField=new EmailToCaseSettingHelper.ActionField();
        actionField.Field=ObjectUtil.getPackagedFieldName('Subject__c');
        actionField.value='Subject';
        actionField.UseEmailField=true;
        actionField.Type='TEXT';
        jsonObj.ActionFields.add(actionField);
        

        Email_To_Case_Rule__c emailToCaseRule=new Email_To_Case_Rule__c();
        emailToCaseRule.IsActive__c=true;
        emailToCaseRule.JSON__c=JSON.serialize(jsonObj);
        emailToCaseRule.Rule_Name__c='AP_PO';
        emailToCaseRule.Order__c=1;
        emailToCaseRule.Type__c='Accept';

        insert emailToCaseRule;
    }

    private static void createExitingCaseRule(string templateId){
        EmailToCaseSettingHelper.JSONValue jsonObj=new EmailToCaseSettingHelper.JSONValue();
        jsonObj.Criterias=new List<EmailToCaseSettingHelper.Criteria>();
        jsonObj.ActionFields=new List<EmailToCaseSettingHelper.ActionField>();
        jsonObj.SendACKMail=true;
        jsonObj.SendACKMailToSender=true;
        jsonObj.IsNotifiationCriteria=false;
        jsonObj.TemplateId=templateId;
        jsonObj.Action='Update Existing Case';

        EmailToCaseSettingHelper.Criteria cre=null;

        cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Status__c');
        cre.Operator='equals';
        cre.Value='Closed';
        cre.Type='PICKLIST';
        cre.Source='CASE';
        jsonObj.Criterias.add(cre);

        cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field='Attachment Count';
        cre.Operator='greater or equal';
        cre.Value='2';
        cre.Type='NUMBER';
        cre.Source='EMAIL';
        jsonObj.Criterias.add(cre);

        cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field='Sender Email';
        cre.Operator='contains';
        cre.Value='@gmail.com';
        cre.Type='TEXT';
        cre.Source='EMAIL';
        jsonObj.Criterias.add(cre);

        EmailToCaseSettingHelper.ActionField actionField=null;

        actionField=new EmailToCaseSettingHelper.ActionField();
        actionField.Field=ObjectUtil.getPackagedFieldName('UserAction__c');
        actionField.value='Reopen';
        actionField.UseEmailField=false;
        actionField.Type='PICKLIST';
        jsonObj.ActionFields.add(actionField);

        
        
        Email_To_Case_Rule__c emailToCaseRule=new Email_To_Case_Rule__c();
        emailToCaseRule.IsActive__c=true;
        emailToCaseRule.JSON__c=JSON.serialize(jsonObj);
        emailToCaseRule.Rule_Name__c='AP_PO';
        emailToCaseRule.Order__c=1;
        emailToCaseRule.Type__c='Existing Case';
        insert emailToCaseRule;
    }

	private static void createIdentifyExitingCaseRule(string templateId){
        EmailToCaseSettingHelper.JSONValue jsonObj=new EmailToCaseSettingHelper.JSONValue();
        jsonObj.Criterias=new List<EmailToCaseSettingHelper.Criteria>();
        jsonObj.ActionFields=new List<EmailToCaseSettingHelper.ActionField>();
        jsonObj.SendACKMail=true;
        jsonObj.SendACKMailToSender=true;
        jsonObj.IsNotifiationCriteria=false;
        jsonObj.TemplateId=templateId;
        jsonObj.Action='Update Existing Case';
		jsonObj.findCaseIdInSubject=true;
		jsonObj.findCaseIdInBody=false;
		jsonObj.findCaseIdFrom='Subject';
		jsonObj.excludeKeywords='';

        EmailToCaseSettingHelper.Criteria cre=null;

        /*cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field='Status__c';
        cre.Operator='equals';
        cre.Value='Closed';
        cre.Type='PICKLIST';
        cre.Source='CASE';
        jsonObj.Criterias.add(cre);*/

        /*cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field='Attachment Count';
        cre.Operator='greater or equal';
        cre.Value='2';
        cre.Type='NUMBER';
        cre.Source='EMAIL';
        jsonObj.Criterias.add(cre);*/

        cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field='Sender Email';
        cre.Operator='contains';
        cre.Value='@gmail.com';
        cre.Type='TEXT';
        cre.Source='EMAIL';
        jsonObj.Criterias.add(cre);

        /*EmailToCaseSettingHelper.ActionField actionField=null;

        actionField=new EmailToCaseSettingHelper.ActionField();
        actionField.Field='UserAction__c';
        actionField.value='Reopen';
        actionField.UseEmailField=false;
        actionField.Type='PICKLIST';
        jsonObj.ActionFields.add(actionField);*/

        
        
        Email_To_Case_Rule__c emailToCaseRule=new Email_To_Case_Rule__c();
        emailToCaseRule.IsActive__c=true;
        emailToCaseRule.JSON__c=JSON.serialize(jsonObj);
        emailToCaseRule.Rule_Name__c='AP_PO';
        emailToCaseRule.Order__c=1;
        emailToCaseRule.Type__c='Identify Existing';
        insert emailToCaseRule;
    }

    public static testMethod void testIgnoreRules(){
        EmailToCaseBean caseBean=new EmailToCaseBean();
        caseBean.FromAddress='chandresh.koyani@sftpl.com';
        caseBean.FromName='Chandresh';
        caseBean.CCAddresses=new List<string>();
        caseBean.ToAddresses=new List<string>();

        caseBean.HtmlBody='<body><h1>simple body</body>';
        caseBean.PlainTextBody='simple body';
        caseBean.Subject='Test Subject';
		caseBean.planSubject='Test Subject';

        caseBean.Header=new Map<string,string>();
        caseBean.Attachments=new List<EmailToCaseAttachment>();
        
        caseBean.EnvelopFromAddress='';
        caseBean.EnvelopToAddress='chandresh.koyani@sftpl.com';

        EmailToCaseAttachment attachment=new EmailToCaseAttachment();
        attachment.BinaryBody=Blob.valueOf('This is simple text');
        attachment.FileName='test.pdf';
        attachment.Headers=new Map<string,string>();
        attachment.Type='Binary';
        caseBean.Attachments.add(attachment);

        EmailToCaseAttachment attachmentInline=new EmailToCaseAttachment();
        attachmentInline.BinaryBody=Blob.valueOf('This is simple text');
        attachmentInline.FileName='testInline.png';
        attachmentInline.Headers=new Map<string,string>();
        attachmentInline.Headers.put('Content-Disposition','inline');
        attachmentInline.Type='Binary';
        caseBean.Attachments.add(attachmentInline);

		caseBean.debugLog=new List<string>();
        EmailToCaseManager caseManager=new EmailToCaseManager();
        caseManager.ProcessCase(caseBean);
    }

    public static testMethod void testAcceptRules(){
        EmailToCaseBean caseBean=new EmailToCaseBean();
        caseBean.FromAddress='chandresh.koyani@gmail.com';
        caseBean.FromName='Chandresh';
        caseBean.CCAddresses=new List<string>();
        caseBean.ToAddresses=new List<string>();

        caseBean.HtmlBody='<body><h1>simple body</body>';
        caseBean.PlainTextBody='simple body';
        caseBean.Subject='AP_PO Test Subject';
		caseBean.planSubject='AP_PO Test Subject';

        caseBean.Header=new Map<string,string>();
        caseBean.Attachments=new List<EmailToCaseAttachment>();
        
        caseBean.EnvelopFromAddress='';
        caseBean.EnvelopToAddress='chandresh.koyani@gmail.com';

        EmailToCaseAttachment attachment=new EmailToCaseAttachment();
        attachment.BinaryBody=Blob.valueOf('This is simple text');
        attachment.FileName='test.pdf';
        attachment.Headers=new Map<string,string>();
        attachment.Type='Binary';
        caseBean.Attachments.add(attachment);

        EmailToCaseAttachment attachmentInline=new EmailToCaseAttachment();
        attachmentInline.BinaryBody=Blob.valueOf('This is simple text');
        attachmentInline.FileName='testInline.png';
        attachmentInline.Headers=new Map<string,string>();
        attachmentInline.Headers.put('Content-Disposition','inline');
        attachmentInline.Type='Binary';
        caseBean.Attachments.add(attachmentInline);

        caseBean.debugLog=new List<string>();
        EmailToCaseManager caseManager=new EmailToCaseManager();
        caseManager.ProcessCase(caseBean);
    }

    public static testMethod void testExitingCaseRules(){
        CaseManager__c caseObj=[select id,name from CaseManager__c limit 1];

        EmailToCaseBean caseBean=new EmailToCaseBean();
        caseBean.FromAddress='chandresh.koyani@gmail.com';
        caseBean.FromName='Chandresh';
        caseBean.CCAddresses=new List<string>();
        caseBean.ToAddresses=new List<string>();

        caseBean.HtmlBody='<body><h1>simple body</body>';
        caseBean.PlainTextBody='simple body';
        caseBean.Subject='AP_PO Test Subject '+PLMConstants.SUBJECT_START_IDENTIFIER +caseObj.Name+PLMConstants.SUBJECT_END_IDENTIFIER;
		caseBean.planSubject='AP_PO Test Subject';

        caseBean.Header=new Map<string,string>();
        caseBean.Attachments=new List<EmailToCaseAttachment>();
        
        caseBean.EnvelopFromAddress='';
        caseBean.EnvelopToAddress='chandresh.koyani@gmail.com';

        EmailToCaseAttachment attachment=new EmailToCaseAttachment();
        attachment.BinaryBody=Blob.valueOf('This is simple text');
        attachment.FileName='test.pdf';
        attachment.Headers=new Map<string,string>();
        attachment.Type='Binary';
        caseBean.Attachments.add(attachment);

        EmailToCaseAttachment attachmentInline=new EmailToCaseAttachment();
        attachmentInline.BinaryBody=Blob.valueOf('This is simple text');
        attachmentInline.FileName='testInline.png';
        attachmentInline.Headers=new Map<string,string>();
        attachmentInline.Headers.put('Content-Disposition','inline');
        attachmentInline.Type='Binary';
        caseBean.Attachments.add(attachmentInline);

        caseBean.debugLog=new List<string>();
        EmailToCaseManager caseManager=new EmailToCaseManager();
        caseManager.ProcessCase(caseBean);
    }

    public static testMethod void testIsMatchMethod(){
        EmailToCaseManager caseManager=new EmailToCaseManager();
        EmailToCaseSettingHelper.Criteria cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field='Status__c';
        cre.Operator='equals';
        cre.Value='True';
        cre.Type='BOOLEAN';

        //Boolean check
        cre.Source='CASE';
        caseManager.IsMatch(cre,true);
        cre.Operator='not equal to';
        caseManager.IsMatch(cre,true);

        //Integer Check
        cre.Value='5';

        cre.Operator='not equal to';
        caseManager.IsMatch(cre,1);

        cre.Operator='equals';
        caseManager.IsMatch(cre,1);

        cre.Operator='less than';
        caseManager.IsMatch(cre,2);

        cre.Operator='greater than';
        caseManager.IsMatch(cre,2);

        cre.Operator='less or equal';
        caseManager.IsMatch(cre,2);

        cre.Operator='greater or equal';
        caseManager.IsMatch(cre,2);

        //String Check

        cre.Value='TEst';

        cre.Operator='not equal to';
        caseManager.IsMatch(cre,'Test');

        cre.Operator='equals';
        caseManager.IsMatch(cre,'Test');

        cre.Operator='starts with';
        caseManager.IsMatch(cre,'Test');

        cre.Operator='end with';
        caseManager.IsMatch(cre,'Test');

        cre.Operator='contains';
        caseManager.IsMatch(cre,'Test');

        cre.Operator='does not contain';
        caseManager.IsMatch(cre,'Test');

        cre.Operator='contains in list';
        caseManager.IsMatch(cre,'Test');

		EmailToCaseSettingHelper.getPlanSubject('Test Subject');
		EmailToCaseSettingHelper.limitLength('Test test',5);
		EmailToCaseSettingHelper.removePrefixFromSubject('RE: Test Subject',new List<string>{'RE:'});
    }

    public static testMethod void testGetFieldValueMethod(){
        EmailToCaseManager caseManager=new EmailToCaseManager();
        EmailToCaseBean caseBean=new EmailToCaseBean();
        caseBean.FromAddress='chandresh.koyani@gmail.com';
        caseBean.FromName='Chandresh';
        caseBean.CCAddresses=new List<string>();
        caseBean.CCAddresses.add('chandresh.koyani@sftpl.com');
        caseBean.ToAddresses=new List<string>();
        caseBean.ToAddresses.add('chandresh.koyani@sftpl.com');
        caseBean.EnvelopFromAddress='chandresh.koyani@sftpl.com';
        caseBean.EnvelopToAddress='chandresh.koyani@gmail.com';
        caseBean.Header=new Map<string,string>();
        caseBean.Header.put('Importance','High');
        caseBean.HtmlBody='<body><h1>simple body</body>';
        caseBean.PlainTextBody='simple body';
        caseBean.InReplyTo='Test';
        caseBean.MessageId='';
        caseBean.References=new List<string>();
        caseBean.References.add('test');

        caseBean.Attachments=new List<EmailToCaseAttachment>();

        EmailToCaseAttachment attachmentInline=new EmailToCaseAttachment();
        attachmentInline.BinaryBody=Blob.valueOf('This is simple text');
        attachmentInline.FileName='testInline.png';
        attachmentInline.Headers=new Map<string,string>();
        attachmentInline.Headers.put('Content-Disposition','inline');
        attachmentInline.Type='Binary';
        caseBean.Attachments.add(attachmentInline);

        EmailToCaseAttachment attachmentNew=new EmailToCaseAttachment();
        attachmentNew.Body='This is simple text';
        attachmentNew.FileName='testInline.txt';
        attachmentNew.Type='TEXT';
        caseBean.Attachments.add(attachmentNew);

        caseManager.GetFieldValue('Sender Email',caseBean);
        caseManager.GetFieldValue('To Addresses',caseBean);
        caseManager.GetFieldValue('CC Addresses',caseBean);
        caseManager.GetFieldValue('Salesforce Email',caseBean);
        caseManager.GetFieldValue('Subject',caseBean);
        caseManager.GetFieldValue('Body',caseBean);
        caseManager.GetFieldValue('Importance',caseBean);

        caseManager.GetFieldValue('Attachment Ext',caseBean);

    }
    public static testmethod void testEmailToCaseService(){
        EmailToCaseService service=new EmailToCaseService();

        Messaging.InboundEmail inboundEmail=new Messaging.InboundEmail();
        inboundEmail.fromAddress='';
        inboundEmail.toAddresses=new List<string>();
        inboundEmail.ccAddresses=new List<string>();
        inboundEmail.FromName='';

        inboundEmail.MessageId='';
        inboundEmail.references=new List<string>();
        inboundEmail.InReplyTo='';
        inboundEmail.plainTextBody='';
        inboundEmail.htmlBody='';
        inboundEmail.subject='';
        inboundEmail.headers=new List<Messaging.InboundEmail.Header>();
        
        inboundEmail.textAttachments=new List<Messaging.InboundEmail.TextAttachment>();
        Messaging.InboundEmail.TextAttachment txtAtt=new Messaging.InboundEmail.TextAttachment();
        txtAtt.Body='Test';
        txtAtt.fileName='Test';
        txtAtt.mimeTypeSubType='';
        txtAtt.Headers=new List<Messaging.InboundEmail.Header>();
        inboundEmail.textAttachments.add(txtAtt);

        inboundEmail.binaryAttachments=new List<Messaging.InboundEmail.binaryAttachment>();
        Messaging.InboundEmail.binaryAttachment biAtt=new Messaging.InboundEmail.binaryAttachment();
        biAtt.Body=blob.valueOf('Test');
        biAtt.fileName='Test';
        biAtt.mimeTypeSubType='';
        biAtt.Headers=new List<Messaging.InboundEmail.Header>();
        inboundEmail.binaryAttachments.add(biAtt);

        Messaging.InboundEnvelope envelope=new Messaging.InboundEnvelope();
        service.handleInboundEmail(inboundEmail,envelope);
    }
}