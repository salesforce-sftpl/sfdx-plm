/*
Authors: Chandresh Koyani
Date created: 14/11/2017
Purpose: This class contains method that used for exception handling, we have added debug log logic in one centre place to track exception.
Dependencies: 
-------------------------------------------------
Modifications:
                Date: 
                Purpose of modification:
                Method/Code segment modified:
                
*/
public with sharing class ExceptionHandlerUtility { 
    
    /*
        Authors: Chandresh Koyani
        Purpose: This method is used to write exception message to log or send email as execption, Currenlty we have just implemented as write log.
                 Parameter:
                 source=> Source of exeception like Class Name, Trigger name etc.
                 message=> Exeception message.
                 trackTrace=> Details of exeception.
        Dependencies: Use in "EmailToCaseNotificationManager.cls".
    */
    public static void writeException(string source,string message,string trackTrace){
        system.debug('Exeception on '+source+', Message =>'+message+', Details=>'+trackTrace);
        //To do(Send Notification)
    }
	
	public static void writeEmailException(EmailToCaseBean emailtoCaseBean, CaseManager__c caseManager,string message,string trackTrace){
		EmailMessageLog log = new EmailMessageLog();

		log.fromAddress = emailtoCaseBean.fromAddress;
		log.toAddresses = emailtoCaseBean.toAddresses;
		log.ccAddresses = emailtoCaseBean.ccAddresses;
		log.fromName = emailtoCaseBean.fromName;

		log.envelopFromAddress = emailtoCaseBean.envelopFromAddress;
		log.envelopToAddress = emailtoCaseBean.envelopToAddress;

		log.messageId = emailtoCaseBean.messageId;
		log.references = emailtoCaseBean.references;
		log.inReplyTo = emailtoCaseBean.inReplyTo;

		Email_Exception__c msg = new Email_Exception__c();
		msg.From_Address__c = emailtoCaseBean.fromAddress;
		msg.Subject__c = emailtoCaseBean.subject;
		msg.Email_Attribute__c = JSON.serialize(log);
		msg.Error_Message__c='Message=>'+message+'\n\n TrackTrace=>'+trackTrace;

		if (caseManager != null) {
			msg.Case_Manager__c = JSON.serialize(caseManager);
		}
		insert msg;
		
		createDebugLog(msg.id,emailtoCaseBean);

		createAttachment(msg.id,emailtoCaseBean);
		InsertHeader(msg.id,emailtoCaseBean.header);
	}
	private static void createAttachment(string emailExceptionId,EmailToCaseBean emailtoCaseBean){
		List<Attachment> attachmentList = new List<Attachment> ();

		Attachment attchObjBody = new Attachment();
		attchObjBody.ParentId = emailExceptionId;
		if (!string.isEmpty(emailtoCaseBean.plainTextBody)) {
			attchObjBody.Name = 'body.txt';
			attchObjBody.body = blob.valueOf(emailtoCaseBean.plainTextBody);
			attachmentList.add(attchObjBody);
		}
		else {
			if (!string.isEmpty(emailtoCaseBean.htmlBody)) {
				attchObjBody.Name = 'body.html';
				attchObjBody.body = blob.valueOf(emailtoCaseBean.htmlBody);
			}
		}
		if (emailtoCaseBean.attachments != null) {
			for (EmailToCaseAttachment attch : emailtoCaseBean.attachments) {
				Attachment attchObj = new Attachment();
				attchObj.Name = attch.fileName;
				attchObj.Body = attch.binaryBody;
				attchObj.ParentId = emailExceptionId;
				attachmentList.add(attchObj);
			}
		}
		insert attachmentList;

	}
	
	private static void InsertHeader(string parentId,Map<string,string> HeaderMap){
		List<Note> noteList=new List<Note>();
		string body='';
		for (string key : HeaderMap.keySet()) {
			body=body+''+key+' : '+HeaderMap.get(key)+'\n';
		}
		
		Note nt = new Note();
		nt.parentid=parentId;
		nt.Title = 'Header: '+String.valueOf(DateTime.Now());
		nt.Body = body;
		insert nt;
    }
	
	private static void createDebugLog(string emailExceptionId,EmailToCaseBean emailtoCaseBean){
		Attachment attchObjBody = new Attachment();
		attchObjBody.ParentId = emailExceptionId;
		attchObjBody.Name = 'debugLog.txt';
		attchObjBody.body = blob.valueOf(string.join(emailtoCaseBean.debugLog,'\n\n'));
		insert attchObjBody;
	}

	public class EmailMessageLog {
		public String[] ccAddresses { get; set; }
		public String[] toAddresses { get; set; }
		public String fromAddress { get; set; }
		public String fromName { get; set; }
		public String subject { get; set; }
		public String envelopFromAddress { get; set; }
		public String envelopToAddress { get; set; }
		public String inReplyTo { get; set; }
		public String messageId { get; set; }
		public String[] references { get; set; }
	}
}