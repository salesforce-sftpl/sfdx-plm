public  class FileWrapper{
        @AuraEnabled
        public String fileName{get;set;}
        @AuraEnabled
        public String content{get;set;}
        @AuraEnabled
        public String dataURL{get;set;}
        @AuraEnabled
        public String fileType{get;set;}
        @AuraEnabled
        public String fileExt{get;set;}
        @AuraEnabled
        public boolean isPrimary{get;set;}
        
    }