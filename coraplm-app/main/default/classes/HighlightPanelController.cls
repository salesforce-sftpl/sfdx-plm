/*
  Authors      :   Rahul Pastagiya
  Date created :   10/08/2017
  Purpose      :   To provide all require actions for HighlightPanel component
  Dependencies :   HighlightPanelWrapper.cls
  __________________________________________________________________________
  Modifications:
            Date:  
            Purpose of modification:  
            Method/Code segment modified: 
 */
public with sharing class HighlightPanelController {

    Final static String SPACE_CHAR = ' ';

    /*
        Authors: Rahul Pastagiya
        Purpose: To get the record for given object, field set and record id
        Dependencies : HighlightPanelHelper.js
    */
    @AuraEnabled
    public static HighlightPanelWrapper getRecord(String objectName, String fieldSetLstStr, Id recordId, Integer noOfCols) {
        try {
            Map < String, Map < String, Object >> fieldValuesMap = new Map < String, Map < String, Object >> ();
            List < String > fieldList = new List < String > ();
            objectName = ObjectUtil.getWithNameSpace(objectName);
            HighlightPanelWrapper hpw = new HighlightPanelWrapper();
            Integer colNo = noOfCols;
            System.debug(colNo);
            String selectClause = 'select ';
            String fromClause = 'from ' + objectName;
            String whereClause = 'where Id = :recordId';
            String queryBuilder = '';
            boolean visited = false;
            List < String > fieldSetLst = (List < String > ) System.JSON.deserializeStrict(fieldSetLstStr, List < String > .Class);
            for (String fieldSet: fieldSetLst) {
                integer k = 1;
                //For Any object and Field set is given           
                fieldSet=ObjectUtil.getValidFieldSetName(objectName,fieldSet); 			
				Schema.FieldSet fieldSetObj = Schema.getGlobalDescribe().get(objectName).getDescribe().FieldSets.getMap().get(fieldSet);
                //For Security scan
                SecurityHelper.checkFieldReadAccess(fieldSetObj.getFields(), objectName);
                //For Security scan
                for (Schema.FieldSetMember fsm: fieldSetObj.getFields()) {
                    if (visited && k > 4) {
                        break;
                    }
                    Map < String, Object > propMap = new Map < String, Object > ();
                    propMap.put('label', fsm.getLabel());
                    fieldList.add(fsm.getFieldPath());
                    fieldValuesMap.put(fsm.getFieldPath(), propMap);
                    K++;
                }
                //}
                visited = true;
            }
            hpw.fieldList = fieldList;
            Set < String > fieldSet = new Set < String > (fieldList);

            queryBuilder = selectClause + SPACE_CHAR + String.join(new List < String > (fieldSet), ', ') + SPACE_CHAR + fromClause + SPACE_CHAR + whereClause;
            SObject obj = Database.query(queryBuilder);

            for (String key: fieldList) {
                Map < String, Object > propMap = fieldValuesMap.get(key);
                if (key.toUpperCase().contains('DATE') && obj.get(key) != null && obj.get(key) != '') {
                    propMap.put('value', String.valueOf((DateTime.valueOf(obj.get(key))).format('d  MMM  YY  hh:mm a')));
                } else if (obj.get(key) != null && obj.get(key) != '') {
                    propMap.put('value', obj.get(key));
                } else {
                    propMap.put('value', '-');
                }
                fieldValuesMap.put(key, propMap);
            }
            hpw.fieldValuesMap = fieldValuesMap;
            return hpw;
        } catch (Exception e) {
            system.debug(e);
            return null;
        }

    }

    /*
        Authors: Rahul Pastagiya
        Purpose: wrapper for dataset
        Dependencies : HighlightPanelHelper.js
    */
    public class HighlightPanelWrapper {
        @AuraEnabled
        public Map < String, Map < String, Object >> fieldValuesMap;
        @AuraEnabled
        public List < String > fieldList;
    }
}