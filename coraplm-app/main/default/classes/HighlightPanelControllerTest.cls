/**
  * Authors		: Tushar Moradia
  * Date created : 16/11/2017 
  * Purpose      : Test Class for  CaseListViewControllerTest
  * Dependencies : CaseListViewController.cls
  * -------------------------------------------------
  *Modifications:
                Date:   
                Purpose of modification:  
                Method/Code segment modified: 

*/
@isTest
public class HighlightPanelControllerTest {
    /*
    	Authors: Tushar Moradiya
    	Purpose: Code Coverage
		Dependencies : CaseListViewController.cls
	*/
    public static testMethod void testFirst(){
        CaseManager__c caseObj=new CaseManager__c();
		caseObj.Process__c='AP';
		caseObj.WorkType__c='PO Invoices';
		caseObj.Document_Type__c='Electricity';
		caseObj.Requestor_Email__c='Tushar@hello.com';
		caseObj.Amount__c=12000;
		caseObj.Status__c='Ready for processing';
		caseObj.escalation__c=true;
		insert caseObj;
        List<String> strLst=new List<String>();
        strLst.add('StandardReadonly');
        strLst.add('StandardReadOnlyOneLine'); 
        strLst.add('Case_His'); 

        HighlightPanelController.getRecord(ObjectUtil.getPackagedFieldName('CaseManager__c'), System.JSON.serialize(strLst), caseObj.Id, 2);
        HighlightPanelController.getRecord(ObjectUtil.getPackagedFieldName('CaseManager__History'), System.JSON.serialize(strLst), caseObj.Id, 2);
        
    }

}