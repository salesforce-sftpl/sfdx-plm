/**
  * Authors     : Parth Lukhi
  * Date created : 10/11/2017 
  * Purpose      : Test Class for  InterActionListController
  * Dependencies : InterActionListController.apxc
  * -------------------------------------------------
  *Modifications:
                Date:   
                Purpose of modification:  
                Method/Code segment modified: 

*/
@isTest
public class InterActionListControllerTest {
    public static testMethod void testFirst(){
        
        CaseManager__c ctTest=createCaseTracker(); 
        EmailMessage emailObj= createEmailMessage(ctTest);
       
        InterActionListController.getInteractionByCase(ctTest.Id,1,1);
        boolean makeRead=InterActionListController.makeReadMail(emailObj.Id) ;
        InterActionListController.getContactsByInteraction(ctTest.Id,emailObj.Id,emailObj.FromAddress);
        //InterActionListController.getAttachMents(ctTest.Id,emailObj.Id);
        System.assertEquals(makeRead,true);
       
    }

    public static testMethod void testException(){
        
        CaseManager__c ctTest=createCaseTracker(); 
        EmailMessage emailObj= createEmailMessage(ctTest);
        InterActionListController.getAttachments(ctTest.Id);
        InterActionListController.getInteractionByCase(null,1,1);
        InterActionListController.getContactsByInteraction(null,null,null);
        boolean makeRead=InterActionListController.makeReadMail(null) ;
        System.assertEquals(makeRead,false); 
    }
    @isTest
    public static CaseManager__c createCaseTracker(){
         CaseManager__c ct= new CaseManager__c();
         ct.Subject__c='Invalid Invoice';
         ct.Status__c='Ready For Processing';
         insert ct;
         return ct;
    }
     
      public static EmailMessage createEmailMessage(CaseManager__c ct){
         
        EmailMessage emailMsg=new EmailMessage();
        emailMsg.FromName='Mr. John Green';
        emailMsg.ToAddress='ToAbc@Domain.com';
        emailMsg.FromAddress='FromAbc@Domain.com';
        emailMsg.RelatedToId=ct.Id;
        insert emailMsg;
        return emailMsg;
    }
}