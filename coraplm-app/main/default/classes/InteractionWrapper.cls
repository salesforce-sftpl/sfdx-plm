public class InteractionWrapper {
    
     @AuraEnabled
        public String caseNumber{get;set;}
        @AuraEnabled
        public String intrBody{get;set;}
        @AuraEnabled
        public String fromName{get;set;}
        @AuraEnabled
        public String subject{get;set;}
        @AuraEnabled
        public String createdDate {get;set;}
    
    	public InteractionWrapper(String caseNumber, String intrBody,String fromName, String subject, String createdDate){
            this.caseNumber = caseNumber;
            this.intrBody = intrBody;
            this.fromName = fromName;
            this.subject = subject;
            this.createdDate = createdDate;
        }

}