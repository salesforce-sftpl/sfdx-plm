/*
  * Authors	   :  Atul Hinge
  * Date created :  
  * Purpose      :  LayoutController used to fetch layout releated fields
  * Dependencies :  SobjectSelector.cls
  * JIRA ID      :  None
  * -----------------------------------------------------------

*/
public class LayoutController {
	/*
	  * Authors: Tej Pal Kumawat
	  * Purpose:  This method is used for get CaseManager__c fields using record type & page layout
	  * Dependencies: Page layout Name & record type
	  * 
	*/
	public static list<Metadata.Metadata> testLayout() {
		list<Metadata.Metadata> layoutsList = new list<Metadata.Metadata> ();
		Metadata.Layout md = new Metadata.Layout();
		md.layoutSections = new list<Metadata.LayoutSection> ();
		Metadata.LayoutSection layoutSection = new Metadata.LayoutSection();
		layoutSection.layoutColumns = new List<Metadata.LayoutColumn> ();
		Metadata.LayoutColumn lc = new Metadata.LayoutColumn();
		lc.layoutItems = new List<Metadata.LayoutItem> ();
		Metadata.LayoutItem li = new Metadata.LayoutItem();
		li.field = 'WorkType__c';
		lc.layoutItems.add(li);
		layoutSection.layoutColumns.add(lc);
		md.layoutSections.add(layoutSection);
		layoutsList.add(md);
		return layoutsList;
	}
	@AuraEnabled
	public static FieldResponse getFields(String requestParm) {
		FieldResponse fieldResponse = new FieldResponse();

		Map<String, Object> requestParms = (Map<String, Object>) JSON.deserializeUntyped(requestParm);
		String objectName = String.valueOf(requestParms.get('objectName'));
		string defaultRecordTypeName = String.valueOf(requestParms.get('recordTypeName'));
		string caseManagerId = String.valueOf(requestParms.get('caseManagerId'));

		system.debug('caseManagerId=>'+caseManagerId);

		Map<string, set<string>> pickListValuesBasedOnRecordType = new Map<string, set<string>> ();
		String strSessionId = String.valueOf(requestParms.get('sessionId'));
		system.debug('#######' + objectName);
		Schema.DescribeSObjectResult describeSObjectResult = Schema.getGlobalDescribe().get(objectName).getDescribe();
		list<SectionFields> sectionFieldList = new list<SectionFields> ();
		fieldResponse.sectionFields = sectionFieldList;

		string layoutName = '';
		if (String.isBlank(defaultRecordTypeName)) {
			for (Schema.RecordTypeInfo info : describeSObjectResult.getRecordTypeInfos()) {
				if (info.DefaultRecordTypeMapping) {
					defaultRecordTypeName = info.getName();
					break;
				}
			}
		}
		string layoutLabel = '';
		//Get layout name related record type
		if (defaultRecordTypeName != 'Master') {
			layoutName = objectName + '-' + defaultRecordTypeName;
			layoutLabel = defaultRecordTypeName;
			system.debug('defaultRecordTypeName=>' + defaultRecordTypeName);
		} else {
			layoutName = objectName + '-' + describeSObjectResult.getLabel() + ' Layout';
			layoutLabel =describeSObjectResult.getLabel() + ' Layout';
			system.debug('defaultRecordTypeName else=>' + layoutLabel);
		}
		//layoutName = 'CaseTracker__c-'+describeSObjectResult.getLabel()+' Layout';
		// layoutName = 'QualityCheck__c-Quality Check Layout';
		//Return fields
		// layoutName = 'CoraPLM__CaseManager__c-CoraPLM__Case Manager Layout';
		map<String, Schema.SObjectField> fieldMap = describeSObjectResult.fields.getMap();
		list<Metadata.Metadata> layoutsList;
		if (defaultRecordTypeName != null && defaultRecordTypeName != 'Master') {
			String strRecordTypeName = defaultRecordTypeName;
			pickListValuesBasedOnRecordType = PLMUtilitiesController.GetPicklistValuesBasedOnRecordType(objectName, defaultRecordTypeName, strSessionId);
		} else {
			pickListValuesBasedOnRecordType = null;
		}
		try {
			layoutsList = Metadata.Operations.retrieve(Metadata.MetadataType.Layout, new list<String> { layoutName });
			if (layoutsList.size() == 0) {
				layoutsList = Metadata.Operations.retrieve(Metadata.MetadataType.Layout, new list<String> { objectName + '-' + AppConfig__c.getInstance().Default_Layout__c + ' Layout' });
				// layoutsList = Metadata.Operations.retrieve(Metadata.MetadataType.Layout, new list<String> {objectName+'-'+'CoraPLM__Case Manager'});
			}
		} catch(Exception e) {
			layoutsList = Metadata.Operations.retrieve(Metadata.MetadataType.Layout, new list<String> { 'CaseManager__c-Case Tracker Layout' });
		}
		Metadata.Layout layoutMetadata = (Metadata.Layout) layoutsList.get(0);
		list<Metadata.LayoutSection> layoutSections = layoutMetadata.layoutSections;
		for (Metadata.LayoutSection section : layoutSections) {
			if (section.label != 'System Information' && section.label != 'Custom Links' && section.layoutColumns.get(0).layoutItems != null && section.layoutColumns.get(0).layoutItems.size() > 0) {
				list<FieldDetails> fds = new list<FieldDetails> ();
				for (Metadata.LayoutColumn layoutColumn : section.layoutColumns) {
					if (layoutColumn.layoutItems != null) {
						for (Metadata.LayoutItem ml : layoutColumn.layoutItems) {
							if (fieldMap.containsKey(ml.field.toLowerCase())) {
								Schema.DescribeFieldResult dfr = fieldMap.get(ml.field.toLowerCase()).getDescribe();
								FieldDetails fs = new FieldDetails();
								fs.fieldLabelName = dfr.getLabel();
								fs.fieldAPIName = ml.field;
								fs.fieldType = String.ValueOf(dfr.getType());
								fs.fieldBehavior = String.ValueOf(ml.behavior);
								fs.fieldRelationship = dfr.getRelationshipName();
								fs.display = '';
								if (fs.fieldType == 'REFERENCE') {
									Schema.DescribeFieldResult fieldResult = fieldMap.get(fs.fieldAPIName).getDescribe();
									List<Schema.SObjectType> relation = fieldResult.getReferenceTo();
									fs.relationship = relation[0].getDescribe().getName();
								}
								if (fs.fieldType == 'PICKLIST') {
									List<Schema.PicklistEntry> ple = dfr.getPicklistValues();
									List<Object> options = new List<Object> ();
									for (Schema.PicklistEntry f : ple) {
										Map<String, String> obj = new Map<String, string> ();
										obj.put('label', f.getLabel());
										obj.put('value', f.getValue());
										obj.put('selected', 'false');
										options.add(obj);
									}
									fs.options = options;
									if (dfr.getController() != null) {
										fs.controlField = dfr.getController().getDescribe().getName();
										Map<String, List<String>> dependentValueMap = getDependentOptionsImpl(objectName, (String) fs.controlField, ml.field);
										Map<String, List<Object>> dependentValueMapFinal = new Map<String, List<Object>> ();

										for (String fieldName : dependentValueMap.keySet()) {
											List<Object> suboptions = new List<Object> ();
											for (String Val : dependentValueMap.get(fieldName)) {
												// if(pickListValuesBasedOnRecordType==null || (recTypeValues !=null && recTypeValues.contains(Val))){
												Map<String, string> obj1 = new Map<String, string> ();
												obj1.put('label', Val);
												obj1.put('value', Val);
												obj1.put('selected', 'false');
												suboptions.add(obj1);
												//}
											}
											dependentValueMapFinal.put(fieldName, suboptions);
										}
										fs.dependentValues = dependentValueMapFinal;
									}
								}
								fds.add(fs);
							}
						}
					}
				}
				SectionFields sf = new SectionFields();
				sf.sectionName = section.label;
				sf.relatedFields = fds;
				sectionFieldList.add(sf);
			}
		}
		system.debug('layoutLabel=>' + layoutLabel);
		List<UI_Rule_config__c> uiConfiges = [select id, Rule_Name__c, JSON__c from UI_Rule_config__c where Rule_Name__c = :layoutLabel and Type__c = 'Processing Field'];
		system.debug('uiConfiges=>' + uiConfiges);
		if (uiConfiges.size() > 0) {
			PLMUISetupController.JSONValue jsonField = (PLMUISetupController.JSONValue) System.JSON.deserialize(uiConfiges[0].JSON__c, PLMUISetupController.JSONValue.class);
			system.debug('jsonField=>' + jsonField);
			fieldResponse.fieldJson = jsonField;

			List<PLMUISetupController.ActionField> actionsFields=new List<PLMUISetupController.ActionField>();
			//Check Rules
			if (jsonField.rules != null) {
				 List<CaseManager__c> caseTrackers = CaseService.getCaseById(caseManagerId);
				for (PLMUISetupController.Rule rule : jsonField.rules) {
					boolean isMatch=isRuleMatch(rule,caseTrackers[0]);
					if(isMatch){
						actionsFields.addAll(rule.actions);
					}
				}
			}


			if (jsonField != null && jsonField.defaultHiddenFields != null) {
				for (SectionFields fieldSection : fieldResponse.sectionFields) {
					for (FieldDetails relatedField : fieldSection.relatedFields) {
						for (string fieldName : jsonField.defaultHiddenFields) {
							if (relatedField.fieldAPIName == fieldName) {
								relatedField.display = 'none';
							}
						}
					}
				}
			}

			if(actionsFields.size()>0){
				for (SectionFields fieldSection : fieldResponse.sectionFields) {
					for (FieldDetails relatedField : fieldSection.relatedFields) {
						for (PLMUISetupController.ActionField actionField : actionsFields) {
							if (relatedField.fieldAPIName == actionField.field) {
								if(actionField.action=='Hidden'){
									relatedField.display = 'none';
								}
								else if(actionField.action=='Editable'){
									relatedField.display = '';
								}
								else if(actionField.action=='Mandatory'){
									relatedField.display = '';
									relatedField.fieldBehavior='Required';
								}
							}
						}
					}
				}
			}
		}

		return fieldResponse;
	}
	//This method is used to get dependent fields' option values
	@AuraEnabled
	public static Map<String, List<String>> getDependentOptionsImpl(string objApiName, string contrfieldApiName, string depfieldApiName) {
		String objectName = objApiName.toLowerCase();
		String controllingField = contrfieldApiName.toLowerCase();
		String dependentField = depfieldApiName.toLowerCase();

		Map<String, List<String>> objResults = new Map<String, List<String>> ();
		//get the string to sobject global map
		Map<String, Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();

		if (!Schema.getGlobalDescribe().containsKey(objectName)) {
			return null;
		}

		Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);
		if (objType == null) {
			return objResults;
		}
		Bitset bitSetObj = new Bitset();
		Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();
		//Check if picklist values exist
		if (!objFieldMap.containsKey(controllingField) || !objFieldMap.containsKey(dependentField)) {
			return objResults;
		}

		List<Schema.PicklistEntry> contrEntries = objFieldMap.get(controllingField).getDescribe().getPicklistValues();
		List<Schema.PicklistEntry> depEntries = objFieldMap.get(dependentField).getDescribe().getPicklistValues();

		objFieldMap = null;
		List<Integer> controllingIndexes = new List<Integer> ();
		for (Integer contrIndex = 0; contrIndex < contrEntries.size(); contrIndex++) {
			Schema.PicklistEntry ctrlentry = contrEntries[contrIndex];
			String label = ctrlentry.getLabel();
			objResults.put(label, new List<String> ());
			controllingIndexes.add(contrIndex);
		}
		List<Schema.PicklistEntry> objEntries = new List<Schema.PicklistEntry> ();
		List<PicklistEntryWrapper> objJsonEntries = new List<PicklistEntryWrapper> ();
		for (Integer dependentIndex = 0; dependentIndex < depEntries.size(); dependentIndex++) {
			Schema.PicklistEntry depentry = depEntries[dependentIndex];
			objEntries.add(depentry);
		}
		objJsonEntries = (List<PicklistEntryWrapper>) JSON.deserialize(JSON.serialize(objEntries), List<PicklistEntryWrapper>.class);
		List<Integer> indexes;

		for (PicklistEntryWrapper objJson : objJsonEntries) {
			if (objJson.validFor == null || objJson.validFor == '') {
				continue;
			}
			String myString = objJson.validFor;
			myString = myString.replaceAll('[^a-zA-Z0-9]', '');
			indexes = bitSetObj.testBits(myString, controllingIndexes);
			//indexes = bitSetObj.testBits(objJson.validFor,controllingIndexes);

			for (Integer idx : indexes) {
				String contrLabel = contrEntries[idx].getLabel();
				objResults.get(contrLabel).add(objJson.label);
			}

		}
		objEntries = null;
		objJsonEntries = null;
		return objResults;
	}
	//This method will used to get recordtype wise picklist value
	public static Map<string, set<string>> GetPicklistValuesBasedOnRecordType(string objectName, string recordTypeName, String strSessionId) {
		Map<string, set<string>> pickListValueMap = new Map<string, set<string>> ();
		PLMMetadataService.MetadataPort service = new PLMMetadataService.MetadataPort();
		service.SessionHeader = new PLMMetadataService.SessionHeader_element();
		service.SessionHeader.sessionId = strSessionId;
		PLMMetadataService.RecordType recordType = (PLMMetadataService.RecordType) service.readMetadata('RecordType', new String[] { objectName + '.' + recordTypeName }
		).getRecords() [0];
		for (PLMMetadataService.RecordTypePicklistValue rpk : recordType.picklistValues) {
			set<string> picklistvalues = new set<string> ();
			for (PLMMetadataService.PicklistValue val : rpk.values) {
				if (!string.isEmpty(val.fullName)) {
					picklistvalues.add(EncodingUtil.urldecode(val.fullName, 'UTF-8'));
				}
			}
			pickListValueMap.put(objectName + '.' + rpk.picklist, picklistvalues);
		}
		return pickListValueMap;
	}

	//Wrapper class for Section & fields

	public class SectionFields {
		@AuraEnabled
		public String sectionName { get; set; }
		@AuraEnabled
		public list<FieldDetails> relatedFields { get; set; }
	}


	public class FieldResponse {
		@AuraEnabled
		List<SectionFields> sectionFields { get; set; }
		@AuraEnabled
		PLMUISetupController.JSONValue fieldJson { get; set; }
	}

	//Wrapper class for Field
	public class FieldDetails {
		@AuraEnabled
		public String fieldLabelName { get; set; }
		@AuraEnabled
		public String fieldAPIName { get; set; }
		@AuraEnabled
		public String fieldType { get; set; }
		@AuraEnabled
		public String fieldBehavior { get; set; }
		@AuraEnabled
		public String fieldRelationship { get; set; }
		@AuraEnabled
		public Object options { get; set; }
		@AuraEnabled
		public String relationship { get; set; }
		@AuraEnabled
		public Object controlField { get; set; }
		@AuraEnabled
		public Object dependentValues { get; set; }
		@AuraEnabled
		public string display { get; set; }
	}

	private static boolean isRuleMatch(PLMUISetupController.Rule rule,CaseManager__c caseTrackerObject) {
		boolean isMatch = false;
		if (rule.criterias != null) {
			for (PLMUISetupController.Criteria cre : rule.criterias) {
				if (cre.type.equalsIgnoreCase('TEXT') || cre.type.equalsIgnoreCase('PICKLIST') || cre.type.equalsIgnoreCase('EMAIL') || cre.type.equalsIgnoreCase('PHONE') || cre.type.equalsIgnoreCase('STRING') || cre.type.equalsIgnoreCase('TEXTAREA')) {
					string fieldValue = string.valueOf(caseTrackerObject.get(cre.field));
					isMatch = isMatch(cre.operator, cre.value, fieldValue, cre.type);
				}
				else if (cre.type.equalsIgnoreCase('NUMBER') || cre.type.equalsIgnoreCase('CURRENCY') || cre.type.equalsIgnoreCase('DOUBLE')) {
					integer fieldValue = integer.valueOf(caseTrackerObject.get(cre.field));
					isMatch = isMatch(cre.operator, cre.value, fieldValue);
				}
				else if (cre.type.equalsIgnoreCase('BOOLEAN')) {
					boolean fieldValue = boolean.valueOf(caseTrackerObject.get(cre.field));
					isMatch = isMatch(cre.operator, cre.value, fieldValue);
				}
				if (!isMatch) {
					break;
				}
			}
		}
		return isMatch;
	}

	@testVisible
	private static boolean isMatch(string operator, string value, string fieldValue, string type) {
		if (operator.equalsIgnoreCase('equals')) {
			return fieldValue == value;
		}
		else if (operator.equalsIgnoreCase('not equal to')) {
			return fieldValue != value;
		}
		else if (operator.equalsIgnoreCase('starts with')) {
			return fieldValue.startsWithIgnoreCase(value);
		}
		else if (operator.equalsIgnoreCase('end with')) {
			return fieldValue.endsWithIgnoreCase(value);
		}
		else if (operator.equalsIgnoreCase('contains')) {
			return fieldValue.containsIgnoreCase(value);
		}
		else if (operator.equalsIgnoreCase('does not contain')) {
			return !fieldValue.containsIgnoreCase(value);
		}
		else if (operator.equalsIgnoreCase('contains in list')) {
			if (!string.isEmpty(value)) {
				List<string> tempList = value.split(',');
				for (string str : tempList) {
					if (type.equalsIgnoreCase('PICKLIST')) {
						if (fieldValue!=null && fieldValue.equalsIgnoreCase(str)) {
							return true;
						}
					}
					else {
						if (fieldValue!=null && fieldValue.containsIgnoreCase(str)) {
							return true;
						}
					}
				}
			}
		}
		else if (operator.equalsIgnoreCase('does not contains in list')) {
			if (!string.isEmpty(value)) {
				List<string> tempList = value.split(',');
				for (string str : tempList) {
					if (fieldValue!=null && fieldValue.equalsIgnoreCase(str)) {
							return false;
					}
				}
			}
			return true;
		}
		return false;
	}

	/*
	  Authors: Chandresh Koyani
	  Purpose: Check value is match based on operator is provided, This method is use only for integer value.
	  Dependencies: Called from "findRule" method.
	 */
	@testVisible
	private static boolean isMatch(string operator, string value, integer fieldValue) {
		if (operator.equalsIgnoreCase('equals')) {
			return fieldValue == integer.valueOf(value);
		}
		else if (operator.equalsIgnoreCase('not equal to')) {
			return fieldValue != integer.valueOf(value);
		}
		else if (operator.equalsIgnoreCase('less than')) {
			return fieldValue < integer.valueOf(value);
		}
		else if (operator.equalsIgnoreCase('greater than')) {
			return fieldValue > integer.valueOf(value);
		}
		else if (operator.equalsIgnoreCase('less or equal')) {
			return fieldValue <= integer.valueOf(value);
		}
		else if (operator.equalsIgnoreCase('greater or equal')) {
			return fieldValue >= integer.valueOf(value);
		}
		return false;
	}
	/*
	  Authors: Chandresh Koyani
	  Purpose: Check value is match based on operator is provided, This method is use only for boolean value.
	  Dependencies: Called from "findRule" method.
	 */
	@testVisible
	private static boolean isMatch(string operator, string value, boolean fieldValue) {
		if (operator.equalsIgnoreCase('equals')) {
			return fieldValue == boolean.valueOf(value);
		}
		else if (operator.equalsIgnoreCase('not equal to')) {
			return fieldValue != boolean.valueOf(value);
		}
		return false;
	}
}