@isTest
public class LayoutControllerTest {
    public static testMethod void testCaseServiceMethods(){
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        test.startTest();
            Map<String, Object> requestParms =new Map<String, Object>();
            requestParms.put('objectName', ObjectUtil.getWithNameSpace('CaseManager__c'));
           //requestParms.put('recordTypeName', 'Default');
            requestParms.put('sessionId', UserInfo.getSessionId());
            String jsonString = JSON.serialize(requestParms);
            LayoutController.getFields(jsonString);
        test.stopTest();
    }
    private class WebServiceMockImpl implements WebServiceMock {	
        public void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) {
            if(request instanceof PLMMetadataService.describeMetadata_element){
                response.put('response_x', new PLMMetadataService.describeMetadataResponse_element());
            }else if(request instanceof PLMMetadataService.readMetadata_element){
                PLMMetadataService.readRecordTypeResponse_element rrtr=new PLMMetadataService.readRecordTypeResponse_element();
                rrtr.result=new PLMMetadataService.ReadRecordTypeResult();
                PLMMetadataService.RecordType rt =new PLMMetadataService.RecordType();
                PLMMetadataService.RecordTypePicklistValue rtpv = new PLMMetadataService.RecordTypePicklistValue();
                PLMMetadataService.PicklistValue plv=new PLMMetadataService.PicklistValue();
                
                rtpv.values = new List<PLMMetadataService.PicklistValue>{plv};
                    rt.picklistValues = new List<PLMMetadataService.RecordTypePicklistValue>{rtpv};
                        rrtr.result.records=new List<PLMMetadataService.RecordType>{rt};
                            response.put('response_x', rrtr);
                //  response.put('response_x', new PLMMetadataService.readRecordTypeResponse_element());
            }
            return;
        } 
    }
}