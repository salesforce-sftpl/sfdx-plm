public with sharing class PLMAppController {
    //Namespace
    public string urlPrefix{get; set;}
    
    //Constructor
    public PLMAppController(){
    	String nameSpace = ObjectUtil.getNamespacePrefix();//PLMConstants.NAMESPACE;
		urlPrefix = (nameSpace == '' ? 'c' : nameSpace);
    }
}