/*
  Authors: Chandresh Koyani
  Date created: 24/10/2017
  Purpose: This class is use to store constant values that are use in Email To Case.
  Dependencies: EmailToCaseManager.cls
  -------------------------------------------------
  Modifications:
  Date: 
  Purpose of modification:
  Method/Code segment modified:
 
*/
public with sharing class PLMConstants {
    public static String SUBJECT_START_IDENTIFIER  = '[ref:_';
    public static String SUBJECT_END_IDENTIFIER = ':ref]';
    public static String BODY_START_IDENTIFIER = '[inlref:_';
    public static String BODY_END_IDENTIFIER = ':inlref]';
    public static string REGEX_FOR_CASE_SEARCH_IN_SUBJECT = '\\[ref:_(.*?):ref\\]';
    public static string REGEX_FOR_CASE_SEARCH_IN_BODY = '\\[inlref:_(.*?):inlref\\]';
    public static string WORKSPACE_NAME= 'PLM Files';
    public static string DEFAULT_ACCOUNT_NAME= 'PLM Users';
    public static string EMAIL_TO_CASE_NOTIFICATION_TEMPLATE_FOLDER='Email to Case Notification';
    public static string REMINDER_TEMPLATE_FOLDER='Reminder';
    public static string ESCALATION_TEMPLATE_FOLDER='Escalation';
	public static string TAT_NOTIFICATION_TEMPLATE_FOLDER='TAT Notification';
    //public static string NAMESPACE='CoraPLM';//get the namespace from custom setting
    
    //Default FieldSets
    public static string DEFAULT_FIELD_SET='DefaultFieldSet';
    public static string HIGHLIGHT_PANLE_COLLAPSE_FIELD_SET='StandardReadOnlyOneLine';
    public static string HIGHLIGHT_PANLE_EXPAND_FIELD_SET='StandardReadonly';
    public static string ADDITIONAL_DETAILS_COLLAPSE_FIELD_SET='';
    public static string ADDITIONAL_DETAILS_EXPAND_FIELD_SET='';
    public static string CASE_LIST_SORT_FIELD_SET='SortFieldSet';
    public static string CASE_LIST_FILTER_FIELD_SET='FilterFieldSet';
    public static string QC_FIELD_SET='';
	public static string EMAIL_EDITOR_FIELD_SET='';    
    public static string CASE_LIST_FIELD_SET='CaseListFieldSet';
}