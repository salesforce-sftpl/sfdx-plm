/**
  * Authors    : Niraj Prajapati
  * Date created : 17/11/2017 
  * Purpose      : Test Class for PLMMetadataService
  * Dependencies : -
  * -------------------------------------------------
  *Modifications:
                Date:   
                Purpose of modification:  
                Method/Code segment modified: 

*/
@isTest  
private class PLMMetadataServiceTest
{    
    /**
     * Dummy Metadata API web service mock class 
     **/
   
	private class WebServiceMockImpl implements WebServiceMock 
	{	
        public void doInvoke(
			Object stub, Object request, Map<String, Object> response,
			String endpoint, String soapAction, String requestName,
			String responseNS, String responseName, String responseType) 
		{
			if(request instanceof PLMMetadataService.describeMetadata_element)
				response.put('response_x', new PLMMetadataService.describeMetadataResponse_element());
			else if(request instanceof PLMMetadataService.readMetadata_element)
				response.put('response_x', new PLMMetadataService.readRecordTypeResponse_element());
			
			return;
		}
	}    
		
	@IsTest
	private static void coverGeneratedCodeCRUDOperations()
	{	
    	// Null Web Service mock implementation
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        // Only required to workaround a current code coverage bug in the platform
        PLMMetadataService metaDataService = new PLMMetadataService();
        // Invoke operations         
        PLMMetadataService.MetadataPort metaDataPort = new PLMMetadataService.MetadataPort();
	}
	
	@IsTest
    private static void coverGeneratedCodeFileBasedOperations1()
    {    	
    	// Null Web Service mock implementation
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        // Only required to workaround a current code coverage bug in the platform
        PLMMetadataService metaDataService = new PLMMetadataService();
        // Invoke operations         
        PLMMetadataService.MetadataPort metaDataPort = new PLMMetadataService.MetadataPort();
        Test.StartTest(); 
    	 metaDataPort.describeMetadata(null);
		
       
        List<String> str = new List<String>();
        metaDataPort.readMetadata('',str);
        Test.StopTest();
        
    }

    @IsTest
    private static void coverGeneratedCodeFileBasedOperations2()
    {       
        // Null Web Service mock implementation
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        // Only required to workaround a current code coverage bug in the platform
        PLMMetadataService metaDataService = new PLMMetadataService();
        // Invoke operations         
        PLMMetadataService.MetadataPort metaDataPort = new PLMMetadataService.MetadataPort();
               
        
    }
        
	@IsTest
    private static void coverGeneratedCodeTypes()
    {    	       
        // Reference types
        new PLMMetadataService();
        
        new PLMMetadataService.DescribeMetadataResult();
        new PLMMetadataService.RecordType();
        new PLMMetadataService.FilterItem();
        new PLMMetadataService.LogInfo();
        new PLMMetadataService.CallOptions_element();
        new PLMMetadataService.describeMetadataResponse_element();
        new PLMMetadataService.RecordTypePicklistValue();
        new PLMMetadataService.describeMetadata_element();
        new PLMMetadataService.DescribeMetadataObject();
        new PLMMetadataService.CustomField();
        new PLMMetadataService.DebuggingHeader_element();
        new PLMMetadataService.Picklist();
        new PLMMetadataService.Metadata();
        new PLMMetadataService.SessionHeader_element();
        new PLMMetadataService.PicklistValue();
        new PLMMetadataService.DebuggingInfo_element();
        new PLMMetadataService.LookupFilter();
       	new PLMMetadataService.readMetadata_element();
        new PLMMetadataService.ReadRecordTypeResult();
        new PLMMetadataService.readRecordTypeResponse_element();
        new PLMMetadataService.ReadCustomFieldResult();
        new PLMMetadataService.readCustomFieldResponse_element();
        new PLMMetadataService.AllOrNoneHeader_element();
       	new PLMMetadataService.GlobalPicklistValue();
        new PLMMetadataService.PicklistValue();
        new PLMMetadataService.ValueSetValuesDefinition();
        new PLMMetadataService.ValueSet();
        new PLMMetadataService.ValueSettings();
        

        
    }

    @IsTest
    private static void elfMissingGetRecordsTest() { 
        new PLMMetadataService.ReadCustomFieldResult().getRecords();
        new PLMMetadataService.ReadRecordTypeResult().getRecords();
        
    }

    @IsTest
    private static void elfMissingGetResultTest() {
        new PLMMetadataService.readRecordTypeResponse_element().getResult();

    }    
}