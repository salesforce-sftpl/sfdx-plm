public class PLMUISetupController {

	public class UIRule {
		public JSONValue JSONValue { get; set; }
		public string layoutName { get; set; }
		public string id { get; set; }
		public boolean isActive { get; set; }
		public string type { get; set; }
	}
	public class Criteria {
		@AuraEnabled
		public string field { get; set; }
		@AuraEnabled
		public string operator { get; set; }
		@AuraEnabled
		public string value { get; set; }
		@AuraEnabled
		public string type { get; set; }
	}

	public class Rule {
		@AuraEnabled
		Public string field { get; set; }
		@AuraEnabled
		Public List<Criteria> criterias { get; set; }
		@AuraEnabled
		public List<ActionField> actions { get; set; }
	}

	public class ActionField {
		@AuraEnabled
		public string field {
			get; Set;
		}
		@AuraEnabled
		public string action {
			get; Set;
		}
	}

	public class JSONValue {
		@AuraEnabled
		Public List<Rule> rules { get; set; }
		@AuraEnabled
		public List<string> defaultHiddenFields { get; set; }
	}
	/*
	  Authors: Chandresh Koyani
	  Purpose: Wrapper class to store option like value,text.
	  Dependencies: QCCalculation.cls,QCRuleController.cls.
	 */
	public class Option {
		public string value { get; set; }
		public string text { get; set; }

		public Option() {
		}
		public Option(string value, string text) {
			this.value = value;
			this.text = text;
		}
	}
	/*
	  Authors: Chandresh Koyani
	  Purpose: This wrapper class is used to store Field information.
	  Dependencies: QCCalculation.cls,QCRuleController.cls.
	 */
	public class FieldInfo {
		public string label {
			get; set;
		}
		public string apiName {
			get; set;
		}
		public string type {
			get; set;
		}
		public Boolean isHidden {
			get; set;
		}
		public list<Option> pickListValue {
			get; set;
		}
		public List<string> operator {
			get; set;
		}
		public boolean isForCriteria {
			get; set;
		}
	}
	public class LayoutField {
		public List<FieldInfo> fields { get; set; }
		public List<SectionField> sectionFields { get; set; }
	}
	public class SectionField {
		public string sectionName { get; set; }
		List<FieldInfo> fields { get; set; }
	}

	public class Result {

		public List<Record> records { get; set; }
	}

	public class Record {
		public string Id {
			get; set;
		}
		public string Name {
			get; set;
		}
	}
	public static string getSobjectId(){
		HttpRequest req = new HttpRequest();
		string endpoint = URL.getSalesforceBaseUrl().toExternalForm() + '/services/data/v41.0/tooling/query/?q=Select+Id+From+CustomObject+Where+DeveloperName+=\'CaseManager\'';
		req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
		req.setHeader('Content-Type', 'application/json');
		req.setEndpoint(endpoint);
		req.setMethod('GET');
		Http http = new Http();
		HttpResponse res = http.send(req);
		string strResponse = res.getBody();
		Result jsonField = (Result) System.JSON.deserialize(strResponse, Result.class);

		System.debug(jsonField.records);

		return jsonField.records[0].Id;
	}
	@RemoteAction
	public static List<Option> getLayouts() {
		List<Option> layouts = new List<Option> ();
		string objectId=getSobjectId();
		HttpRequest req = new HttpRequest();
		string endpoint = URL.getSalesforceBaseUrl().toExternalForm() + '/services/data/v41.0/tooling/query/?q=SELECT+Id,+Name+FROM+Layout+WHERE+TableEnumOrId+=\''+objectId+'\'';
		req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
		req.setHeader('Content-Type', 'application/json');
		req.setEndpoint(endpoint);
		req.setMethod('GET');
		Http http = new Http();
		HttpResponse res = http.send(req);

		string strResponse = res.getBody();
		system.debug(strResponse);
		Result jsonField = (Result) System.JSON.deserialize(strResponse, Result.class);

		For (Record recordObj : jsonField.records) {
			layouts.add(new Option(recordObj.Name, recordObj.Name));
		}

		return layouts;
	}
	@RemoteAction
	public static LayoutField getLayoutFields(string layoutName) {
		LayoutField layoutFieldObj = new LayoutField();
		layoutName = 'CaseManager__c-' + layoutName;
		Schema.DescribeSObjectResult describeSObjectResult = Schema.getGlobalDescribe().get('CaseManager__c').getDescribe();
		map<String, Schema.SObjectField> fieldMap = describeSObjectResult.fields.getMap();
		List<FieldInfo> fieldInfoList = new List<FieldInfo> ();
		List<SectionField> sectionFields = new List<SectionField> ();

		layoutFieldObj.fields = fieldInfoList;
		layoutFieldObj.sectionFields = sectionFields;

		List<Metadata.Metadata> layoutsList = Metadata.Operations.retrieve(Metadata.MetadataType.Layout, new list<String> { layoutName });
		System.debug('layoutsList=>' + layoutsList);
		Metadata.Layout layoutMetadata = (Metadata.Layout) layoutsList.get(0);
		list<Metadata.LayoutSection> layoutSections = layoutMetadata.layoutSections;
		for (Metadata.LayoutSection section : layoutSections) {
			if (section.label != 'System Information' && section.label != 'Custom Links' && section.layoutColumns.get(0).layoutItems != null && section.layoutColumns.get(0).layoutItems.size() > 0) {

				List<FieldInfo> fieldInfoListSection = new List<FieldInfo> ();
				for (Metadata.LayoutColumn layoutColumn : section.layoutColumns) {
					if (layoutColumn.layoutItems != null) {
						for (Metadata.LayoutItem layoutItem : layoutColumn.layoutItems) {
							if (fieldMap.containsKey(layoutItem.field.toLowerCase())) {
								Schema.DescribeFieldResult describeField = fieldMap.get(layoutItem.field.toLowerCase()).getDescribe();
								FieldInfo fieldInfo = new FieldInfo();
								fieldInfo.label = describeField.getLabel();
								fieldInfo.apiName = layoutItem.field;
								fieldInfo.type = String.ValueOf(describeField.getType());
								fieldInfo.pickListValue = new list<Option> ();
								if (fieldInfo.type == 'PICKLIST') {
									List<Schema.PicklistEntry> picklist = describeField.getPicklistValues();
									for (Schema.PicklistEntry pickListVal : picklist) {
										fieldInfo.pickListValue.add(new Option(pickListVal.getValue(), pickListVal.getLabel()));
									}
								}
								else if (fieldInfo.type == 'BOOLEAN') {
									fieldInfo.pickListValue.add(new Option('True', 'True'));
									fieldInfo.pickListValue.add(new Option('False', 'False'));
								}
								if (describeField.isCustom() && !describeField.isCalculated() && fieldInfo.type != 'REFERENCE') {
									fieldInfo.isForCriteria = true;
									fieldInfo.operator = EmailToCaseSettingHelper.OperatorMap.get(fieldInfo.type);
								}
								fieldInfoListSection.add(fieldInfo);
							}
						}
					}
				}
				SectionField sectionFieldObj = new SectionField();
				sectionFieldObj.sectionName = section.label;
				sectionFieldObj.fields = fieldInfoListSection;
				fieldInfoList.addAll(fieldInfoListSection);
				sectionFields.add(sectionFieldObj);
			}
		}
		return layoutFieldObj;
	}

	@RemoteAction
	public static LayoutField getFieldSetFields(string layoutName) {

		LayoutField layoutFieldObj = new LayoutField();
		List<FieldInfo> fieldInfoList = new List<FieldInfo> ();
		List<SectionField> sectionFields = new List<SectionField> ();

		Schema.DescribeSObjectResult describeSObjectResult = Schema.getGlobalDescribe().get('CaseManager__c').getDescribe();
		map<String, Schema.SObjectField> fieldMap = describeSObjectResult.fields.getMap();

		Schema.FieldSet fieldSetObj = describeSObjectResult.fieldSets.getMap().get(layoutName);
		if (fieldSetObj != null) {
			//List<FieldInfo> fieldInfoListSection = new List<FieldInfo> ();
			for (Schema.FieldSetMember fsm : fieldSetObj.getFields()) {
				Schema.DescribeFieldResult describeField = fieldMap.get(fsm.getFieldPath().toLowerCase()).getDescribe();
				FieldInfo fieldInfo = new FieldInfo();
				fieldInfo.label = describeField.getLabel();
				fieldInfo.apiName = describeField.getName();
				fieldInfo.type = String.ValueOf(describeField.getType());
				fieldInfo.pickListValue = new list<Option> ();
				if (fieldInfo.type == 'PICKLIST') {
					List<Schema.PicklistEntry> picklist = describeField.getPicklistValues();
					for (Schema.PicklistEntry pickListVal : picklist) {
						fieldInfo.pickListValue.add(new Option(pickListVal.getValue(), pickListVal.getLabel()));
					}
				}
				else if (fieldInfo.type == 'BOOLEAN') {
					fieldInfo.pickListValue.add(new Option('True', 'True'));
					fieldInfo.pickListValue.add(new Option('False', 'False'));
				}
				if (describeField.isCustom() && !describeField.isCalculated() && fieldInfo.type != 'REFERENCE') {
					fieldInfo.isForCriteria = true;
					fieldInfo.operator = EmailToCaseSettingHelper.OperatorMap.get(fieldInfo.type);
				}
				fieldInfoList.add(fieldInfo);
			}
		}
		layoutFieldObj.fields = fieldInfoList;
		SectionField sectionField = new SectionField();
		sectionField.sectionName = 'All Field';
		sectionField.fields = fieldInfoList;
		sectionFields.add(sectionField);

		layoutFieldObj.sectionFields = sectionFields;
		return layoutFieldObj;
	}
	/*
	  Authors: Chandresh Koyani
	  Purpose: Get Case Tracker Fields to create criteria.
	  Dependencies: Called from "QCRule.Page"
	 
	  @RemoteAction
	  public static List<FieldInfo> getCriteriaFields() {
	  List<FieldInfo> fieldNames = new List<FieldInfo> ();
	 
	  return fieldNames;
	  }

	  @RemoteAction
	  public static List<FieldInfo> getActionFields() {
	  List<FieldInfo> fieldNames = new List<FieldInfo> ();
	  Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get('CaseTracker__c').getDescribe().fields.getMap();
	  for (Schema.SObjectField sfield : fieldMap.Values())
	  {
	  schema.describefieldresult dfield = sfield.getDescribe();

	  string type = String.valueOf(dfield.getType());
	  if (dField.isCustom() && !dField.isCalculated()) {
	  FieldInfo fieldInfo = new FieldInfo();
	  fieldInfo.label = dfield.getLabel();
	  fieldInfo.apiName = dfield.getName();
	  fieldNames.add(fieldInfo);
	  }
	  }
	  return fieldNames;
	  }*/
	@RemoteAction
	public static List<UIRule> getAllRules() {
		List<UIRule> uiRules = new List<UIRule> ();

		List<UI_Rule_config__c> uiConfiges = [select id, Rule_Name__c, JSON__c, IsActive__c, Type__c from UI_Rule_config__c limit 999];
		if (uiConfiges.size() > 0) {
			for (UI_Rule_config__c uiConfigObj : uiConfiges) {
				JSONValue jsonField = (JSONValue) System.JSON.deserialize(uiConfigObj.JSON__c, JSONValue.class);
				UIRule uiRuleObj = new UIRule();
				uiRuleObj.JSONValue = jsonField;
				uiRuleObj.layoutName = uiConfigObj.Rule_Name__c;
				uiRuleObj.type = uiConfigObj.Type__c;
				uiRuleObj.id = uiConfigObj.id;
				uiRuleObj.isActive = uiConfigObj.IsActive__c;
				uiRules.add(uiRuleObj);
			}
		}
		return uiRules;
	}

	/*
	  Authors: Chandresh Koyani
	  Purpose: Save UI Rule entry if id is provided, it will create new rule if id is not provided.
	  Dependencies: Called from "UISetup.Page"
	 */
	@RemoteAction
	public static string saveRule(UIRule uiRuleObj) {
		try {
			UI_Rule_config__c uiConfig = new UI_Rule_config__c();
			uiConfig.Rule_Name__c = uiRuleObj.layoutName;
			uiConfig.JSON__c = JSON.serialize(uiRuleObj.JSONValue);
			uiConfig.IsActive__c = uiRuleObj.isActive;
			uiConfig.Type__c = uiRuleObj.type;
			if (!string.isEmpty(uiRuleObj.id)) {
				uiConfig.id = uiRuleObj.id;
			}


			upsert uiConfig;
			return 'SUCCESS';
		}
		catch(Exception ex) {
			ExceptionHandlerUtility.writeException('QCRuleController', ex.getMessage(), ex.getStackTraceString());
			return '';
		}
	}
	@RemoteAction
	public static void removeRule(string id) {
		try {
			List<UI_Rule_config__c> uiRule = [select id from UI_Rule_config__c where id = :id];
			delete uiRule;
		}
		catch(Exception ex) {
			ExceptionHandlerUtility.writeException('QCRuleController', ex.getMessage(), ex.getStackTraceString());
		}
	}
}