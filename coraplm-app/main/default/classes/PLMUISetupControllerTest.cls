@isTest(seeAllData=false)
public class PLMUISetupControllerTest{
    public static testMethod void unitTest01(){
        PLMUISetupController.LayoutField layoutFields=PLMUISetupController.getLayoutFields('Case Manager Layout');
        PLMUISetupController.LayoutField layoutFieldsNew=PLMUISetupController.getFieldSetFields('DefaultFieldSet');
        
        PLMUISetupController.UIRule uiRule=new PLMUISetupController.UIRule();
        uiRule.JSONValue=new PLMUISetupController.JSONValue();
        
        
        uiRule.JSONValue.rules=new List<PLMUISetupController.Rule>();
        uiRule.JSONValue.defaultHiddenFields=new List<String>();
        uiRule.JSONValue.defaultHiddenFields.add('Process__c');
        uiRule.JSONValue.defaultHiddenFields.add('Worktype__c');
        uiRule.layoutName='Test';
        uiRule.isActive=true;
        uiRule.type='Processing Field';
        PLMUISetupController.saveRule(uiRule);
        
        List<PLMUISetupController.UIRule> allRules= PLMUISetupController.getAllRules();
        PLMUISetupController.removeRule(uiRule.Id);
        
        try{
            PLMUISetupController.getSobjectId();
        }
        catch(Exception ex){}
        
        try{
            List<PLMUISetupController.Option> layout=PLMUISetupController.getLayouts();
        }
        catch(Exception ex){}
    }
}