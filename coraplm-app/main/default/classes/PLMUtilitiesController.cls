/*
  * Authors      :  Atul Hinge / Niraj Prajapati / Parth Lukhi
  * Date created :  18/08/2017
  * Purpose      :  This PLMUtilitiesController contains utility functions which are cutting across the PLM 
  * Dependencies :  SobjectSelector.cls
  * JIRA ID      :  None
  * -----------------------------------------------------------
  * Modifications: 1. Tej Pal Kumawat
  *        Date: 18/12/2017 
  *        Purpose of modification: PUX-537 
  *        Method/Code segment modified: createCaseApex  
 */

public with sharing class PLMUtilitiesController {
    /*
     * Authors: Atul Hinge
     * Purpose:  This method is used to get DropDowns.
     * Dependencies: CaseManager__c, CaseDetailSection.cmp
     */
    public static final string SPACE_CHAR = ' ';

    /*
     * Authors: Niraj Prajapati
     * Purpose:  This method is used to update Case record.
     * Dependencies: CaseManager__c, CaseDetailSection.cmp
     */
    @AuraEnabled
    Public static CaseService.CaseUpdateResponse updateCaseApex(CaseManager__c ct) {
        return CaseService.updateCases(new List<CaseManager__c> { ct });
    }



    /*Code Start For PUX-295 */
    /*
     * Authors: Niraj Prajapati
     * Purpose:  This method is used to update owner of the case.
     * Dependencies: CaseManager__c, CaseDetailSection.cmp
     */
    @AuraEnabled
    Public static List<CaseManager__c> updateOwner(CaseManager__c ct) {
        return CaseService.updateCaseOwner(new List<CaseManager__c> { ct });
    }
    /*Code End For PUX-295 */
    /*
     * Authors: Parth Lukhi
     * Purpose:  This method is used for creating Case Manully from the New Case button event
     * Dependencies: CaseManager__c, EmailMessage,ContentVersion,ContentDocumentId
     * 
     *  Start : This method is used for creating Case Manully from the New Case button event
     */
    @AuraEnabled
    Public static List<CaseManager__c> createCaseApex(CaseManager__c ct)
    {
        String caseComments = ct.Comments__c;
        List<FileWrapper> fileWrapList;
        String startToken = PLMConstants.SUBJECT_START_IDENTIFIER;
        String endToken = PLMConstants.SUBJECT_END_IDENTIFIER;
        List<CaseManager__c> caseData;

        //JIRA: PUX-537 | Tej Pal Kumawat
        list<Comments__c> newComments = new list<Comments__c> ();

        try {
            try {
                //SecurityHelper.CheckFieldEditAccess(new List<String>{'Subject__c','UserAction__c','Status__c','LastResponseDate__c'},'CaseManager__c');  
                ct.Subject__c = System.Label.Case_Created_Manually + SPACE_CHAR + System.now().format();
                ct.UserAction__c = System.Label.User_Action_Create;
                ct.Status__c = System.Label.Case_Status_Start;
                ct.LastResponseDate__c = DateTime.now();
                ct.Comments__c = ''; //JIRA: PUX-537 | Tej Pal Kumawat
                insert ct;

                //JIRA: PUX-537 | Tej Pal Kumawat
                if (caseComments != null && caseComments != '') {
                    newComments.add(new Comments__c(CaseManager__c = ct.Id, Comments__c = caseComments));
                    if (newComments.size() > 0) {
                        insert newComments;
                    }
                }
            } catch(Exception ex) {
                ExceptionHandlerUtility.writeException('PLMUtiltiesController', ex.getMessage(), ex.getStackTraceString());
            }


            ct = CaseDetailViewController.getCaseDetail(ct.Id);

            String emailBody;
            String emailSubject = System.Label.Case_Created_Manually_By + SPACE_CHAR + UserInfo.getFirstName() + SPACE_CHAR + System.Label.At + SPACE_CHAR + System.now().format() + SPACE_CHAR + PLMConstants.SUBJECT_START_IDENTIFIER + ct.Name + PLMConstants.SUBJECT_END_IDENTIFIER;
            if (String.isNotBlank(caseComments)) {
                emailBody = caseComments;
            } else {
                emailBody = System.Label.Case_Created_Manually_By + SPACE_CHAR + UserInfo.getFirstName() + SPACE_CHAR + System.Label.On_Date + System.now().format();
            }

            EmailMessage[] newEmail = new EmailMessage[0];
            // SecurityHelper.CheckFieldEditAccess(new List<String>{'Subject','TextBody','HtmlBody','FromName','Incoming','status'},'EmailMessage');  
            newEmail.add(new EmailMessage(Subject = emailSubject,
                                          TextBody = emailBody,
                                          HtmlBody = emailBody,
                                          FromName = UserInfo.getName(), //PUX-186
                                          FromAddress = UserInfo.getUserEmail(),
                                          Incoming = true,
                                          RelatedToId = ct.Id,
                                          status = '0'));
            insert newEmail;
        } catch(Exception ex) {
            ExceptionHandlerUtility.writeException('PLMUtiltiesController', ex.getMessage(), ex.getStackTraceString());
        }
        try {
            List<String> toList = new List<String> ();
            String mailBody = System.Label.Case_Created_Manually_Email + ct.Name;
            if (String.isNotBlank(ct.Supplier_Email__c))
            {
                toList.add(ct.Supplier_Email__c);
                sendSingleMail(null, toList, null, ct.Subject__c + ':' + startToken + ct.Name + endToken, mailBody, ct.Id);
            }
            // SecurityHelper.CheckFieldReadAccess(new List<String>{'id','name','status__c','UserAction__c','Reason__c','Amount__c','Process__c','Comments__c','WorkType__c','Priority__c','Indicator__c','Subject__c','ApprovalType__c','Ownerid'},'CaseManager__c');
            //String casesDataqry='';
            Id ctId = ct.Id;
            caseData = [select id, name, status__c, UserAction__c, Reject_Reason__c, Amount__c, Process__c, Comments__c, WorkType__c, Priority__c, Indicator__c, Subject__c, Ownerid, (SELECT OldValue, NewValue, Field, createdById FROM Histories order by createdDate Desc limit 10) from CaseManager__c where Id = :ctId];
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('PLMUtiltiesController', ex.getMessage(), ex.getStackTraceString());
        }
        return caseData;
    }
    public class FieldSetResponse {
        @AuraEnabled
        List<FieldSetMember> fieldMembers { get; set; }
        @AuraEnabled
        PLMUISetupController.JSONValue fieldJSON { get; set; }
    }
    //This method is used to get list of field as per given fieldset Name and Object Name 
    @AuraEnabled
    public static FieldSetResponse getFields(String objectType, String fsName, CaseManager__c ct, String strSessionId) {
        objectType = ObjectUtil.getWithNameSpace(objectType);
        fsName = ObjectUtil.getValidFieldSetName(objectType, fsName);
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectType);
        Schema.DescribeSObjectResult describe = targetType.getDescribe();
        Map<String, Schema.SObjectField> fieldsMap = describe.fields.getMap();
        Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();
        Schema.FieldSet fs = fsMap.get(fsName);
        List<Schema.FieldSetMember> fieldSet = fs.getFields();
        Set<String> recTypeValues = new Set<String> ();
        //Start Code for PUX-221
        Map<string, set<string>> pickListValuesBasedOnRecordType = new Map<string, set<string>> ();
        String strRecordType = '';
        if (ct != null)
        {
            sobject s = ct;
            if (s.get('recordtypeid') == null || s.get('recordtypeid') == '')
            {
                Schema.DescribeSObjectResult dsr = targetType.getDescribe();
                Schema.RecordTypeInfo defaultRecordType;
                for (Schema.RecordTypeInfo rti : dsr.getRecordTypeInfos()) {
                    if (rti.isDefaultRecordTypeMapping()) {
                        defaultRecordType = rti;
                        strRecordType = rti.getName() == 'Master' ? '' : rti.getName();
                    }
                }
            }
            if (s.getSobjectType().getDescribe().fields.getMap().keySet().contains('recordtypeid') && s.get('recordtypeid') != null && s.get('recordtypeid') != '') {
                strRecordType = (String) s.getSObject('RecordType').get('Name');
            }

        }
        if (ct != null && strRecordType != null && strRecordType != '')
        {
            String strRecordTypeName = strRecordType;
            pickListValuesBasedOnRecordType = PLMUtilitiesController.GetPicklistValuesBasedOnRecordType(objectType, strRecordTypeName, strSessionId);
        } else {
            pickListValuesBasedOnRecordType = null;
        }
        //END Code for PUX-221

        //*********************************************
        FieldSetResponse fieldResponse = new FieldSetResponse();
        if (fsName == 'NewCase' || fsName == 'EmailProcessingFields') {
            List<UI_Rule_config__c> uiConfiges = [select id, Rule_Name__c, JSON__c from UI_Rule_config__c where Rule_Name__c =:fsName and Type__c in ('New Case','Email Processing')];
            if (!uiConfiges.isEmpty()) {
                PLMUISetupController.JSONValue jsonField = (PLMUISetupController.JSONValue) System.JSON.deserialize(uiConfiges[0].JSON__c, PLMUISetupController.JSONValue.class);
                fieldResponse.fieldJSON = jsonField;
            }
        }

        List<FieldSetMember> fset = new List<FieldSetMember> ();
        fieldResponse.fieldMembers = fset;
        for (Schema.FieldSetMember f : fieldSet) {
            FieldSetMember fsm = new FieldSetMember(f);
            if (string.valueOf(f.getType()) == 'PICKLIST') {


                Schema.DescribeFieldResult fieldResult = fieldsMap.get(f.getFieldPath()).getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                List<Object> options = new List<Object> ();
                if (pickListValuesBasedOnRecordType != null)
                {
                    recTypeValues = pickListValuesBasedOnRecordType.get(objectType + '.' + fieldResult.getName());
                }


                for (Schema.PicklistEntry pe : ple) {
                    if (pickListValuesBasedOnRecordType == null || (recTypeValues != null && recTypeValues.contains(pe.getValue()))) {


                        Map<String, String> obj = new Map<String, string> ();
                        obj.put('label', pe.getLabel());
                        obj.put('value', pe.getValue());
                        obj.put('selected', 'false');
                        options.add(obj);
                    }
                }
                fsm.options = options;
                if (fieldResult.getController() == null)
                {
                    fsm.controlField = '-';
                }
                else
                {
                    fsm.controlField = fieldResult.getController().getDescribe().getName();
                    Map<String, List<String>> dependentValueMap = getDependentOptionsImpl(objectType, (String) fsm.controlField, f.getFieldPath());
                    Map<String, List<Object>> dependentValueMapFinal = new Map<String, List<Object>> ();
                    Map<String, String> obj1;
                    for (String fieldName : dependentValueMap.keySet())
                    {
                        List<Object> suboptions = new List<Object> ();
                        for (String Val : dependentValueMap.get(fieldName))
                        {
                            if (pickListValuesBasedOnRecordType == null || (recTypeValues != null && recTypeValues.contains(Val))) {
                                obj1 = new Map<String, string> ();
                                obj1.put('label', Val);
                                obj1.put('value', Val);
                                obj1.put('selected', 'false');
                                suboptions.add(obj1);
                            }
                        }
                        dependentValueMapFinal.put(fieldName, suboptions);
                    }
                    fsm.dependentValues = dependentValueMapFinal;
                }
            }
            if (string.valueOf(f.getType()) == 'REFERENCE')
            {
                Schema.DescribeFieldResult fieldResult = fieldsMap.get(f.getFieldPath()).getDescribe();
                List<Schema.SObjectType> relation = fieldResult.getReferenceTo();
                fsm.relationship = relation[0].getDescribe().getName();
            }
            //Start Code for PUX-128
            if (string.valueOf(f.getType()) == 'STRING')
            {

                Schema.DescribeFieldResult fieldResult = fieldsMap.get(f.getFieldPath()).getDescribe();
                if (fieldResult.isCalculated())
                {
                    fsm.type = 'FORMULA';
                }

            }
            //End Code for PUX-128
            fset.add(fsm);
        }
        return fieldResponse;
    }
    //This method is used to get list of all fieldsets name
    @AuraEnabled
    public static List<String> getAllFieldSet(String objType) {
        List<String> listOfFieldSetApiNames = new List<String> ();
        String strObjectApiName = objType;
        Schema.SObjectType sObjType = Schema.getGlobalDescribe().get(strObjectApiName);
        if (sObjType != NULL) {
            Map<String, Schema.FieldSet> fieldSetNameToItsRecord = sObjType.getDescribe().fieldsets.getMap();
            listOfFieldSetApiNames.addAll(fieldSetNameToItsRecord.keySet());
        }
        return listOfFieldSetApiNames;
    }

    @AuraEnabled
    public static String getFieldSetName(String caseType, String subType, String stage, String subStage, String fielLocation, String userProfile) {
        System.debug('Start getFieldSetName CaseDetailViewController Start::: ' + caseType + '--' + subType + '--' + stage + '--' + subStage + '--' + fielLocation + '--' + userProfile);
        String fldSetName;

        System.debug(':End +++++++getFieldSetName CaseDetailViewController End');
        return fldSetName;
    }
    //This method is used to get value of lookup fields 
    @AuraEnabled
    public static Map<String, List<Object>> getLookupValue(String objType, String whereClause) {
        Map<String, List<Object>> valueMap = new Map<String, List<Object>> ();
        List<String> fldName = new List<String> ();
        List<String> fldLabel = new List<String> ();
        String query = 'select Id,Name';
        List<String> lstFieldSet = PLMUtilitiesController.getAllFieldSet(objType);
        Set<String> mySet = new Set<String> ();
        mySet.addAll(lstFieldSet);
        String strFldSetName = objType.replace('__c', '').toLowerCase();
        if (mySet.contains(strFldSetName))
        {

            List<FieldSetMember> fsm = PLMUtilitiesController.getFields(objType, strFldSetName, null, '').fieldMembers;
            for (FieldSetMember f : fsm)
            {
                if (f.fieldPath != 'Name')
                {
                    query += ',' + f.fieldPath;
                    fldName.add(f.fieldPath);
                    fldLabel.add(f.label);
                }

            }
            if (whereClause != null && whereClause != '') {
                query += ' from ' + objType + ' where name like \'' + '%' + whereClause + '%\'';
            } else {
                query += ' from ' + objType;
            }
        } else {
            //fldName.add('Name');
            //fldLabel.add('Name');
            if (whereClause != null && whereClause != '')
            {
                query = 'select Id,Name from ' + objType + ' where name like \'' + '%' + whereClause + '%\'';
            } else {
                query = 'select Id,Name from ' + objType;
            }
        }

        valueMap.put('fldName', fldName);
        valueMap.put('fldLabel', fldLabel);
        valueMap.put('value', database.query(query));
        return valueMap;
    }

    //This method is used to get Name from the id of the lookup fields
    @AuraEnabled
    public static String getLookupNameApex(String objType, String whereClause) {
        String query = '';
        if (whereClause != null && whereClause != '')
        {
            query = 'select Name from ' + objType + ' where id = \'' + whereClause + '\'';
        } else {
            query = 'select id,name from ' + objType;
        }
        List<sObject> obj = database.query(query);
        return(String) obj[0].get('Name');
    }

    //This method is used to get dependent fields' option values
    @AuraEnabled
    public static Map<String, List<String>> getDependentOptionsImpl(string objApiName, string contrfieldApiName, string depfieldApiName) {


        String objectName = objApiName.toLowerCase();
        String controllingField = contrfieldApiName.toLowerCase();
        String dependentField = depfieldApiName.toLowerCase();

        Map<String, List<String>> objResults = new Map<String, List<String>> ();
        //get the string to sobject global map
        Map<String, Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();

        if (!Schema.getGlobalDescribe().containsKey(objectName)) {
            return null;
        }

        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);
        if (objType == null) {
            return objResults;
        }
        Bitset bitSetObj = new Bitset();
        Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();
        //Check if picklist values exist
        if (!objFieldMap.containsKey(controllingField) || !objFieldMap.containsKey(dependentField)) {
            return objResults;
        }

        List<Schema.PicklistEntry> contrEntries = objFieldMap.get(controllingField).getDescribe().getPicklistValues();
        List<Schema.PicklistEntry> depEntries = objFieldMap.get(dependentField).getDescribe().getPicklistValues();

        objFieldMap = null;
        List<Integer> controllingIndexes = new List<Integer> ();
        for (Integer contrIndex = 0; contrIndex < contrEntries.size(); contrIndex++) {
            Schema.PicklistEntry ctrlentry = contrEntries[contrIndex];
            String label = ctrlentry.getLabel();
            objResults.put(label, new List<String> ());
            controllingIndexes.add(contrIndex);
        }
        List<Schema.PicklistEntry> objEntries = new List<Schema.PicklistEntry> ();
        List<PicklistEntryWrapper> objJsonEntries = new List<PicklistEntryWrapper> ();
        for (Integer dependentIndex = 0; dependentIndex < depEntries.size(); dependentIndex++) {
            Schema.PicklistEntry depentry = depEntries[dependentIndex];
            objEntries.add(depentry);
        }
        objJsonEntries = (List<PicklistEntryWrapper>) JSON.deserialize(JSON.serialize(objEntries), List<PicklistEntryWrapper>.class);
        List<Integer> indexes;

        for (PicklistEntryWrapper objJson : objJsonEntries) {
            if (objJson.validFor == null || objJson.validFor == '') {
                continue;
            }

            String myString = objJson.validFor;
            myString = myString.replaceAll('[^a-zA-Z0-9]', '');
            indexes = bitSetObj.testBits(myString, controllingIndexes);
            //indexes = bitSetObj.testBits(objJson.validFor,controllingIndexes);

            for (Integer idx : indexes) {
                String contrLabel = contrEntries[idx].getLabel();
                objResults.get(contrLabel).add(objJson.label);
            }

        }
        objEntries = null;
        objJsonEntries = null;
        return objResults;
    }

    //This method will used to get recordtype wise picklist value
    public static Map<string, set<string>> GetPicklistValuesBasedOnRecordType(string objectName, string recordTypeName, String strSessionId) {
        Map<string, set<string>> pickListValueMap = new Map<string, set<string>> ();

        PLMMetadataService.MetadataPort service = new PLMMetadataService.MetadataPort();
        service.SessionHeader = new PLMMetadataService.SessionHeader_element();
        //service.SessionHeader.sessionId=Page.SessionId.getContent().toString().trim();
        service.SessionHeader.sessionId = strSessionId;
        //service.SessionHeader.sessionId = UserInfo.getSessionId(); // UserInfo.getSessionId();

        PLMMetadataService.RecordType recordType = (PLMMetadataService.RecordType) service.readMetadata('RecordType', new String[] { objectName + '.' + recordTypeName }
        ).getRecords() [0];

        for (PLMMetadataService.RecordTypePicklistValue rpk : recordType.picklistValues) {
            set<string> picklistvalues = new set<string> ();
            for (PLMMetadataService.PicklistValue val : rpk.values) {
                if (!string.isEmpty(val.fullName)) {
                    picklistvalues.add(EncodingUtil.urldecode(val.fullName, 'UTF-8'));
                }
            }
            pickListValueMap.put(objectName + '.' + rpk.picklist, picklistvalues);

        }
        return pickListValueMap;
    }


    /*
     * Authors: Parth Lukhi
     * Purpose:  Sending the single Mail 
     * Dependencies:   Messaging.SingleEmailMessage
     * 
     *  Start : Sending the single Mail 
     */
    @AuraEnabled
    public static void sendSingleMail(String fromMail, List<String> toMail, List<String> ccMail, String subject, String message, Id ctId) {
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage> ();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(toMail);
        mail.setSubject(subject);
        mail.setHtmlBody(message);
        mail.setWhatId(ctId);
        mails.add(mail);
        Messaging.sendEmail(mails);

    }
    /* End :  Start : Sending the single Mail */

    /*
     * Authors: Tej Pal Kumawat
     * Purpose:  In case custom labels are referenced only in lightning, add them here  so they are taken during Package generation
     * Dependencies: Custom labels
     * 
     */
    public static boolean plmLabels() {
        //Add notes labels
        String Error_Message_Subject = Label.Error_Message_Subject;
        String Error_Message_HtmlBody = Label.Error_Message_HtmlBody;
        String Success_Message_AddNote = Label.Success_Message_AddNote;

        //CaseDetailSubView
        String Error_Message = Label.Error_Message;

        //Input
        String Error_Message_Email_Field = Label.Error_Message_Email_Field;
        String Error_Message_Require_Field = Label.Error_Message_Require_Field;

        //Quick reply labels
        String Success_Title = Label.Success_Title;
        String Email_Sent_Message = Label.Email_Sent_Message;
        return true;
    }
    /*Start : Harshil Added on 31/01/2018  For PUX-299 */
    @AuraEnabled
    public static ResponseofUser getUserSession() {
        
        ResponseofUser ref = new ResponseofUser();
        ref.sessionId = UserInfo.getSessionId();
        ref.userId = UserInfo.getUserId();
        System.debug('@HARSHIL SID --> '+ref.sessionId);
        System.debug('@HARSHIL UID --> '+ref.userId);
        return ref;
    }
    
    public class ResponseofUser {
    @AuraEnabled public String userId {get;set;}
    @AuraEnabled public String sessionId {get;set;}
        
   }
 
    @AuraEnabled
    public static void sendNotification(List<CaseManager__c> caseTrackeListNew) {
        
  //      List<Case_Notifications_Event__e> events=new List<Case_Notifications_Event__e>();
  //        List<Case_Manager_Event__e> events=new List<Case_Manager_Event__e>();
		  List<Case_Manager__e> events=new List<Case_Manager__e>();

		  
  
        list<String> roleRelatedGroupIds = new list<String> ();
        for (Group g :[SELECT id, RelatedId, Type FROM Group where RelatedId = :UserInfo.getUserRoleId() OR DeveloperName = 'AllInternalUsers']) {
            roleRelatedGroupIds.add(g.id);
        }

		list<String> availableGroupIds = new List<String>();
        for (GroupMember queue :[SELECT Group.Name, UserOrGroupId, GroupId FROM GroupMember WHERE(UserOrGroupId = :UserInfo.getUserID() AND(Group.Type = 'Queue' OR Group.Type = 'All Internal Users')) OR(UserOrGroupId IN :roleRelatedGroupIds AND(Group.Type = 'Queue' OR Group.Type = 'All Internal Users'))]) {
			availableGroupIds.add(queue.GroupId);
        }

        for (CaseManager__c caseTrackerNew : caseTrackeListNew) {

		    for (GroupMember sgm :[SELECT Group.Name, UserOrGroupId FROM GroupMember WHERE GroupId IN :availableGroupIds])
            {
              if (caseTrackerNew.CreatedById != sgm.UserOrGroupId) {
                       if (caseTrackerNew.Name != null && caseTrackerNew.Process__c != null &&
                            caseTrackerNew.WorkType__c != null) {
                            
                            System.debug('CreatedById : ' + caseTrackerNew.CreatedById);
                            System.debug('USERorGROUPID : ' + sgm.UserOrGroupId);    
                            
							
				/*			Case_Manager_Event__e event = new Case_Manager_Event__e(Case_Id__c = caseTrackerNew.Name + ' Created Successfully ' +
                                                                '</n></br>' + 'Procees Type :  ' + caseTrackerNew.Process__c +
                                                                '</n></br>' + 'Work Type :  ' + caseTrackerNew.WorkType__c,ReceiverId__c=sgm.UserOrGroupId);
				*/		

                        Case_Manager__e event = new Case_Manager__e(Case_Id__c = caseTrackerNew.Name + ' Created Successfully ' +
                                                                '</n></br>' + 'Procees Type :  ' + caseTrackerNew.Process__c +
                                                                '</n></br>' + 'Work Type :  ' + caseTrackerNew.WorkType__c,ReceiverId__c=sgm.UserOrGroupId);
                        
						events.add(event);
                        System.debug('Event Detail :::: ' + event);                                
                    }
               }
            }
            if (events != null){
                    List<Database.SaveResult> sr = EventBus.publish(events);
                    for (Database.SaveResult sr1 : sr) {
                        if (sr1.isSuccess()) {
                            System.debug('Successfully published event.');
						} else {
                            for(Database.Error err : sr1.getErrors()) {
                                System.debug('Error returned: ' + err.getStatusCode() + ' - ' + err.getMessage());
                            }
                        }       
                    } 
            }   
        }
           

    }

    /*End : Harshil Added on 31/01/2018*/
}