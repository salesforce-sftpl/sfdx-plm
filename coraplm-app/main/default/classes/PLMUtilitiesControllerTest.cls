/**
* Authors		: Parth Lukhi
* Date created : 10/11/2017 
* Purpose      : Test Class for  DashboardController
* Dependencies : DashboardController.apxc
* -------------------------------------------------
*Modifications:
Date:   
Purpose of modification:  
Method/Code segment modified: 

*/
@isTest
public class PLMUtilitiesControllerTest {
    private class WebServiceMockImpl implements WebServiceMock {	
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType) 
        {
            if(request instanceof PLMMetadataService.describeMetadata_element){
                response.put('response_x', new PLMMetadataService.describeMetadataResponse_element());
            }else if(request instanceof PLMMetadataService.readMetadata_element){
                PLMMetadataService.readRecordTypeResponse_element rrtr=new PLMMetadataService.readRecordTypeResponse_element();
                rrtr.result=new PLMMetadataService.ReadRecordTypeResult();
                //{new PLMMetadataService.RecordType()}
                PLMMetadataService.RecordType rt =new PLMMetadataService.RecordType();
                
                PLMMetadataService.RecordTypePicklistValue rtpv = new PLMMetadataService.RecordTypePicklistValue();
                
                PLMMetadataService.PicklistValue plv=new PLMMetadataService.PicklistValue();
                
                rtpv.values = new List<PLMMetadataService.PicklistValue>{plv};
                    rt.picklistValues = new List<PLMMetadataService.RecordTypePicklistValue>{rtpv};
                        rrtr.result.records=new List<PLMMetadataService.RecordType>{rt};
                            //---------------
                            response.put('response_x', rrtr);
                //  response.put('response_x', new PLMMetadataService.readRecordTypeResponse_element());
            }system.debug('*******************');
            return;
        }
    }    
    
    public static testMethod void testGetFields(){
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        CaseManager__c caseObj=TestDataGenerator.createCaseTracker();
        test.startTest();
        //PLMUtilitiesController.GetPicklistValuesBasedOnRecordType('CaseManager__c','RT1',UserInfo.getSessionId());
        PLMUtilitiesController.getFields('CaseManager__c',ObjectUtil.getWithNameSpace('DefaultFieldSet'),caseObj,UserInfo.getSessionId());
        PicklistEntryWrapper pew=new PicklistEntryWrapper();
        pew.active='';
        pew.defaultValue='';
        pew.label='';
        pew.validFor='';
        pew.value='';
        test.stopTest();
        Contact c=new Contact(LastName='Test');
        insert c;
        
        PLMUtilitiesController.getLookupValue('Contact','Test');
        PLMUtilitiesController.getLookupValue('Contact','');
        PLMUtilitiesController.getLookupNameApex('Contact','');
        PLMUtilitiesController.getLookupNameApex('Contact',c.Id);
    }
    
    public static testMethod void testFirst(){
        List<CaseManager__c> caseTrackers;
        CaseManager__c ctTest=createCaseTracker(); 
        EmailMessage emailObj= createEmailMessage(ctTest);
        String fileWraperEncode=createFileWrapper();
        PLMUtilitiesController.createCaseApex(ctTest);
        ctTest.Comments__c='Test Comments in body';
        update ctTest;
        CaseManager__c ct=new CaseManager__c();
        ct.Comments__c='Test Comments in body';
        caseTrackers=PLMUtilitiesController.createCaseApex(ct);
        System.assert(caseTrackers!=null);
    }
    
    public static testMethod void testTwo(){
        CaseManager__c ctTest=createCaseTracker(); 
        EmailMessage emailObj= createEmailMessage(ctTest);
        //PLMUtilitiesController.getDropdowns(null);
        
        PLMUtilitiesController.updateCaseApex(ctTest);
        PLMUtilitiesController.updateOwner(ctTest);
        PLMUtilitiesController.getAllFieldSet('CaseManager__c');
        // PLMUtilitiesController.getFields('CaseManager__c', 'TestFldSetName' ,ctTest,'TestSession') ;
        // CaseManager__c.getSObjectType().getDescribe().getName()
    }
     public static testMethod void testPlmLabels(){
        PLMUtilitiesController.plmLabels();
    }
    public static CaseManager__c createCaseTracker(){
        CaseManager__c ct= new CaseManager__c();
        ct.Subject__c='Invalid Invoice';
        ct.Status__c='Ready For Processing';
        ct.Supplier_Email__c='testMail@newplm.dev';
        insert ct;
        return ct;
    }
    
    public static EmailMessage createEmailMessage(CaseManager__c ct){
        
        EmailMessage emailMsg=new EmailMessage();
        emailMsg.FromName='Mr. John Green';
        emailMsg.ToAddress='ToAbc@Domain.com';
        emailMsg.FromAddress='FromAbc@Domain.com';
        emailMsg.RelatedToId=ct.Id;
        insert emailMsg;
        return emailMsg;
    }
    
    public static String createFileWrapper(){
        List<FileWrapper> fileWrapList=new  List<FileWrapper> ();
        FileWrapper fileWrapData=new FileWrapper();
        fileWrapData.fileName='Invoice_16_Nov.pdf';
        fileWrapData.content='Base64EncodedString';
        fileWrapData.dataURL='FileDatURL';
        fileWrapData.fileType='PDF';
        fileWrapData.fileExt='.pdf';
        fileWrapData.isPrimary=true;
        fileWrapList.add(fileWrapData);
        return JSON.serialize(fileWrapList);
    }
   
}