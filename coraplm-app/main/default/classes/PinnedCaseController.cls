public with sharing class PinnedCaseController  {

  //Use @AuraEnabled to enable client- and server-side access to the method
    //To get The List of Cases based on condition
    @AuraEnabled
    public static List < CaseManager__c > getCases() { 
		String uid = UserInfo.getUserId();       		 
		//List<CaseManager__c> caseList=[select id,Target_TAT_Time__c,escalation__c,OverdueIn__c,name,status__c,UserAction__c,Contact__c,Reason__c,Amount__c,OwnerName__c,Process__c,Comments__c,WorkType__c,Priority__c,Indicator__c,Subject__c,toLabel(ApprovalType__c), Ownerid,(SELECT CreatedDate, FromAddress, FromName, TextBody, Id, Subject FROM Emails order by CreatedDate desc limit 1), Unread_Email_Count__c from CaseManager__c where Favourited__c = true order by LastResponseDate__c desc NULLS LAST, CreatedDate desc NULLS LAST Limit 50];
		
        String query = 'select id,Target_TAT_Time__c,escalation__c,OverdueIn__c,name,status__c,UserAction__c,Reject_Reason__c,Amount__c,OwnerName__c,Process__c,Comments__c,WorkType__c,Priority__c,Indicator__c,Subject__c, Ownerid,(SELECT CreatedDate, FromAddress, FromName, TextBody, Id, Subject FROM Emails order by CreatedDate desc limit 1), Unread_Email_Count__c from CaseManager__c where';
			query += ' Favourited__c = true ';
			query += ' order by LastResponseDate__c desc NULLS LAST, CreatedDate desc NULLS LAST Limit 50';
		System.debug('query : '+query );
        System.debug((List < CaseManager__c > ) database.query(query));
        return (List < CaseManager__c > ) database.query(query);
    }

}