/**
  * Authors		: Ashish Kr.
  * Date created : 10/11/2017 
  * Purpose      : Test Class for PinnedCaseController
  * Dependencies : PinnedCaseController.apxc
  * -------------------------------------------------
  * Modifications:
    Date:   
    Purpose of modification:  
    Method/Code segment modified: 

*/
@isTest
public class PinnedCaseControllerTest {
        
    	/*
        Authors: Ashish Kumar
        Purpose: Code Coverage
        Dependencies : PinnedCaseController.cls
    	*/
        public static testMethod void testFirst(){
            
            CaseManager__c caseObj=new CaseManager__c();
            caseObj.Process__c='AP';
            caseObj.WorkType__c='PO Invoices';
            caseObj.Document_Type__c='Electricity';
            caseObj.Requestor_Email__c='testEmail@hello.com';
            caseObj.Amount__c=12000;
            caseObj.Status__c='Ready for processing';
            caseObj.escalation__c=true;
            caseObj.Favourited__c = true;
            insert caseObj;  
            
            Test.startTest();
            PinnedCaseController.getCases();
            Test.stopTest();
        }    
}