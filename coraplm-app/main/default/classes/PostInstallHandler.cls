public class PostInstallHandler implements InstallHandler {
    public void onInstall(InstallContext context) {
        if (context.previousVersion() == null) {
            createDefaultAccount();
            createDefaultTATRule();
            createDefaultQCRule();
            createDefaultEmailToCaseRules();
            createDefaultReminderRule();
            scheduleReminder();
            createContentWorkspace();
        }
    }

    private void createDefaultAccount() {

        List<account> accList = [select id from Account where Name = :PLMConstants.DEFAULT_ACCOUNT_NAME];
        if (accList.isEmpty()) {
            Account acc = new Account();
            acc.Name = PLMConstants.DEFAULT_ACCOUNT_NAME;
            insert acc;
        }
    }
    private void createDefaultTATRule() {
        TATHelper.TATRule tatRuleObj = new TATHelper.TATRule();
        tatRuleObj.JSONValue = new TATHelper.JSONValue();
        List<BusinessHours> bhs = [select id from BusinessHours where IsDefault = true];

        tatRuleObj.JSONValue.criterias = new List<TATHelper.Criteria> ();
        tatRuleObj.JSONValue.tatTime = 1;
        tatRuleObj.JSONValue.calendar = bhs[0].id;
        tatRuleObj.JSONValue.tatTimeUnit = 'Days';
        tatRuleObj.JSONValue.notificationTime = 0;
        tatRuleObj.JSONValue.notificationTimeUnit = '';
        tatRuleObj.JSONValue.template = '';
        tatRuleObj.JSONValue.additionalEmails = '';
        tatRuleObj.JSONValue.sendNotification = false;

        TATHelper.Criteria cre = null;
        cre = new TATHelper.Criteria();
        cre.Field = ObjectUtil.getPackagedFieldName('Process__c');
        cre.Operator = 'equals';
        cre.Value = 'AP';
        cre.Type = 'PICKLIST';
        tatRuleObj.JSONValue.Criterias.add(cre);

        TAT_Rule_Config__c tatConfig = new TAT_Rule_Config__c();
        tatConfig.Rule_Name__c = 'AP Default';
        tatConfig.JSON__c = JSON.serialize(tatRuleObj.JSONValue);
        tatConfig.IsActive__c = true;
        tatConfig.Order__c = 1;

        insert tatConfig;
    }
    private void createDefaultQCRule() {
        QCHelper.QCRule qcRuleObj = new QCHelper.QCRule();
        qcRuleObj.JSONValue = new QCHelper.JSONValue();

        qcRuleObj.JSONValue.Criterias = new List<QCHelper.Criteria> ();
        qcRuleObj.JSONValue.AlwaysMoveToQC = true;
        qcRuleObj.JSONValue.QCPerecentage = 0;

        QCHelper.Criteria cre = null;
        cre = new QCHelper.Criteria();
        cre.Field = ObjectUtil.getPackagedFieldName('Process__c');
        cre.Operator = 'equals';
        cre.Value = 'AP';
        cre.Type = 'PICKLIST';
        cre.RefField = '';
        qcRuleObj.JSONValue.Criterias.add(cre);

        QC_Rule_Config__c qcConfig = new QC_Rule_Config__c();
        qcConfig.Rule_Name__c = 'AP Default';
        qcConfig.JSON__c = JSON.serialize(qcRuleObj.JSONValue);
        qcConfig.IsActive__c = true;
        qcConfig.Type__c = 'sampling';
        qcConfig.Order__c = 1;

        insert qcConfig;
    }
    private void createDefaultEmailToCaseRules() {

        EmailTemplate emailTemplate = [select id, name from EmailTemplate where Name = 'DefaultEmailtocasenotification'];

        EmailToCaseSettingHelper.JSONValue jsonObj = new EmailToCaseSettingHelper.JSONValue();
        jsonObj.Criterias = new List<EmailToCaseSettingHelper.Criteria> ();
        jsonObj.ActionFields = new List<EmailToCaseSettingHelper.ActionField> ();
        jsonObj.SendACKMail = true;
        jsonObj.SendACKMailToSender = true;
        jsonObj.IsNotifiationCriteria = false;
        jsonObj.TemplateId = emailTemplate.id;
        jsonObj.includeCCInACK = false;

        EmailToCaseSettingHelper.Criteria cre = null;
        cre = new EmailToCaseSettingHelper.Criteria();
        cre.Field = 'Salesforce Email';
        cre.Operator = 'contains';
        cre.Value = 'salesforce.com';
        cre.Type = 'TEXT';
        cre.Source = 'EMAIL';
        jsonObj.Criterias.add(cre);

        EmailToCaseSettingHelper.ActionField actionField = null;

        actionField = new EmailToCaseSettingHelper.ActionField();
        actionField.Field = ObjectUtil.getPackagedFieldName('Process__c');
        actionField.fieldLabel = 'Process';
        actionField.value = 'AP';
        actionField.UseEmailField = false;
        actionField.Type = 'PICKLIST';
        jsonObj.ActionFields.add(actionField);

        actionField = new EmailToCaseSettingHelper.ActionField();
        actionField.Field = ObjectUtil.getPackagedFieldName('WorkType__c');
        actionField.fieldLabel = 'Work Type';
        actionField.value = 'PO Invoices';
        actionField.UseEmailField = false;
        actionField.Type = 'PICKLIST';
        jsonObj.ActionFields.add(actionField);

        /*actionField = new EmailToCaseSettingHelper.ActionField();
          actionField.Field = ObjectUtil.getPackagedFieldName('Document_Type__c');
          actionField.value = 'Purchase Order';
          actionField.UseEmailField = false;
          actionField.Type = 'PICKLIST';
          jsonObj.ActionFields.add(actionField);*/

        actionField = new EmailToCaseSettingHelper.ActionField();
        actionField.Field = ObjectUtil.getPackagedFieldName('Status__c');
        actionField.value = 'Start';
        actionField.fieldLabel = 'Status';
        actionField.UseEmailField = false;
        actionField.Type = 'PICKLIST';
        jsonObj.ActionFields.add(actionField);

        actionField = new EmailToCaseSettingHelper.ActionField();
        actionField.Field = ObjectUtil.getPackagedFieldName('Requestor_Email__c');
        actionField.fieldLabel = 'Requestor Email';
        actionField.value = 'Sender Email';
        actionField.UseEmailField = true;
        actionField.Type = 'TEXT';
        jsonObj.ActionFields.add(actionField);

        actionField = new EmailToCaseSettingHelper.ActionField();
        actionField.Field = ObjectUtil.getPackagedFieldName('Subject__c');
        actionField.fieldLabel = 'Subject';
        actionField.value = 'Subject';
        actionField.UseEmailField = true;
        actionField.Type = 'TEXT';
        jsonObj.ActionFields.add(actionField);

        actionField = new EmailToCaseSettingHelper.ActionField();
        actionField.Field = ObjectUtil.getPackagedFieldName('Input_Source__c');
        actionField.fieldLabel = 'Input Source';
        actionField.value = 'Email';
        actionField.UseEmailField = false;
        actionField.Type = 'PICKLIST';
        jsonObj.ActionFields.add(actionField);

        Email_To_Case_Rule__c emailToCaseRule = new Email_To_Case_Rule__c();
        emailToCaseRule.IsActive__c = true;
        emailToCaseRule.JSON__c = JSON.serialize(jsonObj);
        emailToCaseRule.Rule_Name__c = 'AP Default';
        emailToCaseRule.Order__c = 1;
        emailToCaseRule.Type__c = 'Accept';

        insert emailToCaseRule;

    }
    private void createDefaultReminderRule() {
        ReminderAndEscalationHelper.ReminderRule reminderRuleObj = new ReminderAndEscalationHelper.ReminderRule();
        reminderRuleObj.JSONValue = new ReminderAndEscalationHelper.JSONValue();

        reminderRuleObj.JSONValue.criterias = new List<ReminderAndEscalationHelper.Criteria> ();
        reminderRuleObj.JSONValue.notifications = new List<ReminderAndEscalationHelper.Notification> ();
        reminderRuleObj.JSONValue.escalations = new List<ReminderAndEscalationHelper.Escalation> ();
        reminderRuleObj.JSONValue.isEscalationRequired = true;
        reminderRuleObj.JSONValue.isEnableEscalationMatrix = false;
        reminderRuleObj.JSONValue.escalationMatrixTemplate = '';

        ReminderAndEscalationHelper.Criteria cre = null;
        cre = new ReminderAndEscalationHelper.Criteria();
        cre.Field = ObjectUtil.getPackagedFieldName('Process__c');
        cre.Operator = 'equals';
        cre.Value = 'AP';
        cre.Type = 'PICKLIST';

        reminderRuleObj.JSONValue.Criterias.add(cre);

        EmailTemplate emailTemplate = [select id, name from EmailTemplate where Name = 'DefaultReminder'];
        ReminderAndEscalationHelper.Notification notification = null;
        notification = new ReminderAndEscalationHelper.Notification();
        notification.after = '10';
        notification.additionalRecipient = '';
        notification.afterUnit = 'Minutes';
        notification.includeIn = 'TO';
        notification.toAddressField = 'To_Addresses__c';
        notification.ccAddressField = 'CC_Addresses__c';
        notification.bccAddressField = 'BCC_Addresses__c';
        notification.mailTrailIn = 'In Mail';
        notification.emailTemplate = emailTemplate.id;
        reminderRuleObj.JSONValue.notifications.add(notification);


        EmailTemplate emailTemplateEsc = [select id, name from EmailTemplate where Name = 'DefaultEscalation'];
        ReminderAndEscalationHelper.Escalation esclation = null;
        esclation = new ReminderAndEscalationHelper.Escalation();
        esclation.after = '10';
        esclation.additionalRecipient = '';
        esclation.afterUnit = 'Minutes';
        esclation.includeIn = 'TO';
        esclation.toAddressField = 'CC_Addresses__c';
        esclation.ccAddressField = 'To_Addresses__c';
        esclation.bccAddressField = 'BCC_Addresses__c';
        esclation.mailTrailIn = 'In Mail';
        esclation.emailTemplate = emailTemplateEsc.id;
        reminderRuleObj.JSONValue.escalations.add(esclation);

        Reminder_Rule_Config__c ruleConfig = new Reminder_Rule_Config__c();
        ruleConfig.Rule_Name__c = 'AP Default';
        ruleConfig.JSON__c = JSON.serialize(reminderRuleObj.JSONValue);
        ruleConfig.IsActive__c = true;
        ruleConfig.Order__c = 1;

        insert ruleConfig;
    }
    private void scheduleReminder() {
        DateTime objDateTime = datetime.now().addMinutes(1);
        String cron = '';
        cron += objDateTime.second();
        cron += ' ' + objDateTime.minute();
        cron += ' ' + objDateTime.hour();
        cron += ' ' + objDateTime.day();
        cron += ' ' + objDateTime.month();
        cron += ' ' + '?';
        cron += ' ' + objDateTime.year();
        ReminderAndEscalationSchedulable reminder = new ReminderAndEscalationSchedulable();
        String jobID = System.schedule('Reminder And Esclation Job', cron, reminder);

    }

    private void createContentWorkspace() {
        /*
        List<ContentWorkspace> shareWorkspace = [select id from ContentWorkspace where name = :PLMConstants.WORKSPACE_NAME];
        if (shareWorkspace.isempty()) {
            ContentWorkspace c1 = new ContentWorkspace();
            c1.Name = PLMConstants.WORKSPACE_NAME;
            insert c1;
        }
        */
    }
}