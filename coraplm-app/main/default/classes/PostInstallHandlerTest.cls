@isTest
public class PostInstallHandlerTest {
    public static testMethod void unitTest01(){
        PostInstallHandler postinstall = new PostInstallHandler();
		Test.testInstall(postinstall, null);
		Test.testInstall(postinstall, new Version(1,0), true);
    }
}