/*
  Authors: Chandresh Koyani
  Date created: 01/11/2017
  Purpose: This class is used to calculate QC Logic, it contains only one public method its used from "ProcessBulider".
  Dependencies: Called from "ProcessBuilder".
  -------------------------------------------------

  Modifications: 1
  Date: 17/11/2017
  Purpose of modification: Added new methods to get QC Fieldset name
  Method/Code segment modified: added new methods getQCFormFieldInfo()
 
*/
public class QCCalculation {
    public static final double MIN_LIMIT = 1;
    public static final double MAX_LIMIT = 100;
    /*
      Authors: Chandresh Koyani
      Purpose: This method iterate all QC simpaling Rules and based on matching rule its decide next status of case.
      Dependencies: Called from "ProcessBuilder".
     */
    @InvocableMethod(label = 'Check For QC' description = 'Check for QC based on QC Rules definied')
    public static void checkForQC(List<CaseManager__c> caseTrackers) {
        try {
            List<QC_Rule_Config__c> qcRuleConfiges = [select id, Name, JSON__c, Order__c, IsActive__c from QC_Rule_Config__c where IsActive__c = true and Type__c = 'sampling' order by Order__c];
            Map<string, User> userMap = new Map<string, User> ([select id, Name, Email from User where isactive = true]);

            List<CaseManager__c> updateCaseList = new List<CaseManager__c> ();
            for (CaseManager__c caseTrackerObj : caseTrackers) {
                QCHelper.QCRule rule = findRule(caseTrackerObj, qcRuleConfiges, userMap);
                if (rule != null) {
                    CaseManager__c updateObj = new CaseManager__c();
                    updateObj.Id = caseTrackerObj.Id;
                    if (rule.JSONValue.AlwaysMoveToQC) {
                        updateObj.QC_Available__c = true;
                        updateObj.QC_Rule__c = rule.Id;
                        updateCaseList.add(updateObj);
                    }
                    else {
                        double perecentage = double.valueOf(rule.JSONValue.qcPerecentage);
                        if (perecentage > getRandomNumber(MIN_LIMIT, MAX_LIMIT)) {
                            updateObj.QC_Available__c = true;
                            updateObj.QC_Rule__c = rule.Id;
                            updateCaseList.add(updateObj);
                        }
                        else {
                            updateObj.QC_Available__c = false;
                            updateObj.QC_Rule__c = rule.Id;
                            updateCaseList.add(updateObj);
                        }
                    }
                }

            }

            if (updateCaseList.size() > 0) {
                update updateCaseList;
            }
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('QCCalculation', ex.getMessage(), ex.getStackTraceString());
        }
    }

    public static void checkForQC(List<CaseManager__c> caseTrackeListNew, Map<Id, CaseManager__c> caseTrackerOldMap) {
        List<QC_Rule_Config__c> qcRuleConfiges = [select id, Name, JSON__c, Order__c, IsActive__c from QC_Rule_Config__c where IsActive__c = true and Type__c = 'sampling' order by Order__c];
        Map<string, User> userMap = new Map<string, User> ([select id, Name, Email from User where isactive = true]);

        for (CaseManager__c caseTrackerNew : caseTrackeListNew) {
            CaseManager__c caseTrackerOld = caseTrackerOldMap.get(caseTrackerNew.Id);
            if (caseTrackerNew.UserAction__c != caseTrackerOld.UserAction__c && caseTrackerNew.UserAction__c == 'Process') {
                QCHelper.QCRule rule = findRule(caseTrackerNew, qcRuleConfiges, userMap);
                if (rule != null) {
                    if (rule.JSONValue.AlwaysMoveToQC) {
                        caseTrackerNew.QC_Available__c = true;
                        caseTrackerNew.QC_Rule__c = rule.Id;
                    }
                    else {
                        double perecentage = double.valueOf(rule.JSONValue.qcPerecentage);
                        if (perecentage > getRandomNumber(MIN_LIMIT, MAX_LIMIT)) {
                            caseTrackerNew.QC_Available__c = true;
                            caseTrackerNew.QC_Rule__c = rule.Id;
                        }
                        else {
                            caseTrackerNew.QC_Available__c = false;
                            caseTrackerNew.QC_Rule__c = rule.Id;
                        }
                    }
                }
            }
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: This method iterate all QC Form rules and based on matching rule its return fieldset name to display field in QC Block on UI.
      Dependencies: Called from UI App.
     */
    public static QCHelper.QCFormResponse getQCFormFieldInfo(string caseId) {
        List<CaseManager__c> caseTrackers = CaseService.getCaseById(caseId);
        QCHelper.QCFormResponse response = new QCHelper.QCFormResponse();

        if (caseTrackers != null && caseTrackers.size() > 0) {
            CaseManager__c caseTrackerObj = caseTrackers[0];
            try {
                List<QC_Rule_Config__c> qcRuleConfiges = [select id, Name, JSON__c, Order__c, IsActive__c from QC_Rule_Config__c where IsActive__c = true and Type__c = 'form' order by Order__c];
                Map<string, User> userMap = new Map<string, User> ([select id, Name, Email from User where isactive = true]);

                QCHelper.QCRule rule = findRule(caseTrackerObj, qcRuleConfiges, userMap);
                if (rule != null) {
                    response.qcFormFieldsetName = rule.JSONValue.qcFormFieldsetName;
                    response.qcFormHeader = rule.JSONValue.qcFormHeader;
                }
            }
            catch(Exception ex) {
                ExceptionHandlerUtility.writeException('QCCalculation', ex.getMessage(), ex.getStackTraceString());
            }
        }
        return response;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Find matching QC rule based on case fields.
      Dependencies: Called from "CheckForQC" method.
     */
    private static QCHelper.QCRule findRule(CaseManager__c caseTrackerObject, List<QC_Rule_Config__c> qcRuleConfiges, Map<string, User> userMap) {

        for (QC_Rule_Config__c qcRuleConfig : qcRuleConfiges) {

            QCHelper.QCRule qcRule = new QCHelper.QCRule();
            qcRule.id = qcRuleConfig.id;
            qcRule.JSONValue = (QCHelper.JSONValue) System.JSON.deserialize(qcRuleConfig.JSON__c, QCHelper.JSONValue.class);

            if (qcRule.JSONValue != null && qcRule.JSONValue.criterias != null) {
                boolean isMatch = false;
                for (QCHelper.Criteria cre : qcRule.JSONValue.criterias) {
                    if (cre.type.equalsIgnoreCase('TEXT') || cre.type.equalsIgnoreCase('PICKLIST') || cre.type.equalsIgnoreCase('EMAIL') || cre.type.equalsIgnoreCase('PHONE') || cre.type.equalsIgnoreCase('STRING') || cre.type.equalsIgnoreCase('TEXTAREA')) {
                        string fieldValue = string.valueOf(caseTrackerObject.get(cre.field));
                        isMatch = isMatch(cre, fieldValue);
                    }
                    else if (cre.type.equalsIgnoreCase('NUMBER') || cre.type.equalsIgnoreCase('CURRENCY') || cre.type.equalsIgnoreCase('DOUBLE')) {
                        double fieldValue = double.valueOf(caseTrackerObject.get(cre.field));
                        isMatch = isMatch(cre, fieldValue);
                    }
                    else if (cre.type.equalsIgnoreCase('BOOLEAN')) {
                        boolean fieldValue = boolean.valueOf(caseTrackerObject.get(cre.field));
                        isMatch = isMatch(cre, fieldValue);
                    }
                    else if (cre.type.equalsIgnoreCase('REFERENCE')) {
                        string userId = string.valueOf(caseTrackerObject.get(cre.field));

                        if (!string.isEmpty(userId)) {
                            User ur = userMap.get(userId);
                            if (ur != null) {
                                string fieldValue = string.valueOf(ur.get(cre.refField));
                                isMatch = isMatch(cre, fieldValue);
                            }
                        }
                    }
                    if (!isMatch) {
                        break;
                    }
                }

                if (isMatch) {
                    return qcRule;
                }
            }
        }
        return null;

    }

    /*
      Authors: Chandresh Koyani
      Purpose: Check value is match based on operator is provided, This method is use only for string value.
      Dependencies: Called from "findRule" method.
     */
    @testVisible
    private static boolean isMatch(QCHelper.Criteria car, string fieldValue) {
        if (car.operator.equalsIgnoreCase('equals')) {
            return fieldValue == car.Value;
        }
        else if (car.operator.equalsIgnoreCase('not equal to')) {
            return fieldValue != car.Value;
        }
        else if (car.operator.equalsIgnoreCase('starts with')) {
            return fieldValue.startsWithIgnoreCase(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('end with')) {
            return fieldValue.endsWithIgnoreCase(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('contains')) {
            return fieldValue.containsIgnoreCase(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('does not contain')) {
            return !fieldValue.containsIgnoreCase(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('contains in list')) {
            if (!string.isEmpty(car.Value)) {
                List<string> tempList = car.Value.split(',');
                for (string str : tempList) {
                    if (car.type.equalsIgnoreCase('PICKLIST')) {
                        if (fieldValue!=null && fieldValue.equalsIgnoreCase(str)) {
                            return true;
                        }
                    }
                    else {
                        if (fieldValue!=null && fieldValue.containsIgnoreCase(str)) {
                            return true;
                        }
                    }
                }
            }
        }
		else if (car.operator.equalsIgnoreCase('does not contains in list')) {
			if (!string.isEmpty(car.Value)) {
				List<string> tempList = car.Value.split(',');
				for (string str : tempList) {
					if (fieldValue!=null && fieldValue.equalsIgnoreCase(str)) {
							return false;
					}
				}
			}
			return true;
		}
        return false;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Check value is match based on operator is provided, This method is use only for integer value.
      Dependencies: Called from "findRule" method.
     */
    @testVisible
    private static boolean isMatch(QCHelper.Criteria car, double fieldValue) {
        if (car.operator.equalsIgnoreCase('equals')) {
            return fieldValue == double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('not equal to')) {
            return fieldValue != double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('less than')) {
            return fieldValue < double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('greater than')) {
            return fieldValue > double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('less or equal')) {
            return fieldValue <= double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('greater or equal')) {
            return fieldValue >= double.valueOf(car.Value);
        }
        return false;
    }
    /*
      Authors: Chandresh Koyani
      Purpose: Check value is match based on operator is provided, This method is use only for boolean value.
      Dependencies: Called from "findRule" method.
     */
    @testVisible
    private static boolean isMatch(QCHelper.Criteria car, boolean fieldValue) {
        if (car.operator.equalsIgnoreCase('equals')) {
            return fieldValue == boolean.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('not equal to')) {
            return fieldValue != boolean.valueOf(car.Value);
        }
        return false;
    }
    /*
      Authors: Chandresh Koyani
      Purpose: Generate random number based on start and end value provided.
      Dependencies: Called from "CheckForQC" method.
     */
    @testVisible
    private static double getRandomNumber(double lower, double upper) {
        return Math.round((Math.random() * (upper - lower)) + lower);
    }
}