/*
Authors: Chandresh Koyani
Date created: 22/11/2017
Purpose: This class contains all wrapper classes and common methods that use in QC System.
Dependencies: QCCalculation,QCRuleController
-------------------------------------------------
Modifications:
                Date: 
                Purpose of modification:
                Method/Code segment modified:
                
*/ 
public with sharing class QCHelper { 
    
    /*
      Authors: Chandresh Koyani
      Purpose: Wrapper class to store option like value,text.
      Dependencies: QCCalculation.cls,QCRuleController.cls.
    */
    public class Option {
        public string value { get; set; }
        public string text { get; set; }

        public Option() {
        }
        public Option(string value, string text) {
            this.value = value;
            this.text = text;
        }
    }
    /*
      Authors: Chandresh Koyani
      Purpose: This wrapper class is used to store Field information.
      Dependencies: QCCalculation.cls,QCRuleController.cls.
    */
    public class FieldInfo {
        public string label {
            get; set;
        }
        public string apiName {
            get; set;
        }
        public string type {
            get; set;
        }
        public list<Option> pickListValue {
            get; set;
        }
        public List<string> operator{
            get; set;
        }
    }
    /*
      Authors: Chandresh Koyani
      Purpose: This wrapper class is used to store Criteria information.
      Dependencies: QCCalculation.cls,QCRuleController.cls.
    */
    public class Criteria {
        public string field { get; set; }
        public string operator { get; set; }
        public string value { get; set; }
        public string type { get; set; }
        public string refField{get;set;}
    }

    /*
      Authors: Chandresh Koyani
      Purpose: This wrapper class is used to store JSONValue information.
      Dependencies: QCCalculation.cls,QCRuleController.cls.
    */
    public class JSONValue {
        Public List<Criteria> criterias { get; set; }
        public boolean alwaysMoveToQC { get; set; }
        public integer qcPerecentage { get; set; }
        public string qcFormFieldsetName{get;set;}
        public string qcFormHeader{get;set;}
    }

    /*
      Authors: Chandresh Koyani
      Purpose: This wrapper class is used to store QCRule information.
      Dependencies: QCCalculation.cls,QCRuleController.cls.
    */
    public class QCRule {
        public JSONValue JSONValue { get; set; }
        public string ruleName { get; set; }
        public integer order { get; set; }
        public string id { get; set; }
        public boolean isActive{get;set;}
        public string type{get;set;}
    }

    public class QCFormResponse{
        @AuraEnabled
        public string qcFormFieldsetName{get;set;}
        @AuraEnabled
        public string qcFormHeader{get;set;}
    }
}