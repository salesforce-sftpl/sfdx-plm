/*
  Authors: Chandresh Koyani
  Date created: 01/11/2017
  Purpose: Controller class for "QCRule.Page". Its contain static method related to insert,update and delete of QC Rule.
  Dependencies: Called from "QCRule.Page".
  -------------------------------------------------
  Modifications: 1
  Date: 17/11/2017
  Purpose of modification: Added new rule type "form"
  Method/Code segment modified: added new method getQualityCheckFieldset().
 
*/
public with sharing class QCRuleController {


    /*
      Authors: Chandresh Koyani
      Purpose: Get Case Tracker Fields to create criteria.
      Dependencies: Called from "QCRule.Page"
     */
    @RemoteAction
    public static List<QCHelper.FieldInfo> getCaseTrackerFields() {
        List<User> allActiveUser = [select id, Name from User];

        List<QCHelper.FieldInfo> fieldNames = new List<QCHelper.FieldInfo> ();

        Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(ObjectUtil.getPackagedFieldName('CaseManager__c')).getDescribe().fields.getMap();
        for (Schema.SObjectField sfield : fieldMap.Values())
        {
            schema.describefieldresult dfield = sfield.getDescribe();

            string type = String.valueOf(dfield.getType());
            if (dField.isCustom() && !dField.isCalculated()) {

                QCHelper.FieldInfo fieldInfo = new QCHelper.FieldInfo();
                fieldInfo.label = dfield.getLabel();
                fieldInfo.apiName = dfield.getName();
                fieldInfo.pickListValue = new list<QCHelper.option> ();
                fieldInfo.type = type;
                fieldInfo.operator = EmailToCaseSettingHelper.operatorMap.get(fieldInfo.type);

                if (fieldInfo.type == 'REFERENCE') {
                    string relatedObjectName = String.valueOf(dfield.getReferenceTo()).substringBetween('(', ')');
                    if (relatedObjectName.equalsIgnoreCase('USER')) {
                        fieldNames.add(fieldInfo);
                    }
                }
                else {
                    if (fieldInfo.type == 'PICKLIST') {
                        List<Schema.PicklistEntry> picklist = dfield.getPicklistValues();
                        for (Schema.PicklistEntry pickListVal : picklist) {
                            fieldInfo.pickListValue.add(new QCHelper.option(pickListVal.getValue(), pickListVal.getLabel()));
                        }
                    }
                    else if (fieldInfo.type == 'BOOLEAN') {
                        fieldInfo.pickListValue.add(new QCHelper.option('True', 'True'));
                        fieldInfo.pickListValue.add(new QCHelper.option('False', 'False'));
                    }

                    fieldNames.add(fieldInfo);
                }
            }
        }
        return fieldNames;
    }


    /*
      Authors: Chandresh Koyani
      Purpose: Save QC Rule entry if id is provided, it will create new rule if id is not provided.
      Dependencies: Called from "QCRule.Page"
     */
    @RemoteAction
    public static string saveRule(QCHelper.QCRule qcRuleObj) {
        try {
            QC_Rule_Config__c qcConfig = new QC_Rule_Config__c();
            qcConfig.Rule_Name__c = qcRuleObj.ruleName;
            qcConfig.JSON__c = JSON.serialize(qcRuleObj.JSONValue);
            qcConfig.IsActive__c = qcRuleObj.isActive;
            qcConfig.Type__c = qcRuleObj.type;
            if (!string.isEmpty(qcRuleObj.id)) {
                qcConfig.id = qcRuleObj.id;
            }
            else {
                AggregateResult[] groupedResults = [select MAX(Order__c) from QC_Rule_Config__c where Type__c = :qcRuleObj.type];
                integer nextOrder = 1;
                if (groupedResults != null && groupedResults.size() > 0) {
                    integer maxOrder = integer.valueOf(groupedResults[0].get('expr0'));
                    if (maxOrder != null) {
                        nextOrder = maxOrder + 1;
                    }
                }
                qcConfig.Order__c = nextOrder;
            }

            upsert qcConfig;
            return 'SUCCESS';
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('QCRuleController', ex.getMessage(), ex.getStackTraceString());
            return '';
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Get all QC Rule.
      Dependencies: Called from "QCRule.Page"
     */
    @RemoteAction
    public static List<QCHelper.QCRule> getAllRules() {
        List<QCHelper.QCRule> qcRules = new List<QCHelper.QCRule> ();

        List<QC_Rule_Config__c> qcConfiges = [select id, Rule_Name__c, JSON__c, Order__c, IsActive__c, Type__c from QC_Rule_Config__c order by Order__c limit 999];
        if (qcConfiges.size() > 0) {
            for (QC_Rule_Config__c qcConfigObj : qcConfiges) {
                QCHelper.JSONValue jsonField = (QCHelper.JSONValue) System.JSON.deserialize(qcConfigObj.JSON__c, QCHelper.JSONValue.class);
                QCHelper.QCRule qcRuleObj = new QCHelper.QCRule();
                qcRuleObj.JSONValue = jsonField;
                qcRuleObj.ruleName = qcConfigObj.Rule_Name__c;
                qcRuleObj.order = Integer.valueOf(qcConfigObj.Order__c);
                qcRuleObj.id = qcConfigObj.id;
                qcRuleObj.isActive = qcConfigObj.IsActive__c;
                qcRuleObj.type = qcConfigObj.Type__c;
                qcRules.add(qcRuleObj);
            }
        }
        return qcRules;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Remove QC Rule Config entry based on id provided..
      Dependencies: Called from "QCRule.Page"
     */
    @RemoteAction
    public static void removeRule(string id) {
        try {
            List<QC_Rule_Config__c> qcRule = [select id from QC_Rule_Config__c where id = :id];
            delete qcRule;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('QCRuleController', ex.getMessage(), ex.getStackTraceString());
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Change state of a rule(Active/Inactive).
      Dependencies: Called from "QCRule.Page"
     */
    @RemoteAction
    public static void changeState(string id, boolean isActive) {
        try {
            QC_Rule_Config__c rule = new QC_Rule_Config__c();
            rule.id = id;
            rule.IsActive__c = isActive;
            update rule;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('QCRuleController', ex.getMessage(), ex.getStackTraceString());
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Change order of a rule.
      Dependencies: Called from "QCRule.Page"
     */
    @RemoteAction
    public static void changeOrder(integer currentOrder, string orderType, string ruleType) {
        try {
            integer previousNextCount = 0;
            if (orderType == 'UP') {
                previousNextCount = currentOrder - 1;
            }
            else {
                previousNextCount = currentOrder + 1;
            }
            List<QC_Rule_Config__c> qcRules = [select id, Order__c from QC_Rule_Config__c where Type__c = :ruleType and(Order__c = :currentOrder OR Order__c = :previousNextCount)];
            if (qcRules.size() > 0) {
                for (QC_Rule_Config__c rule : qcRules) {
                    if (rule.Order__c == currentOrder) {
                        rule.Order__c = previousNextCount;
                    }
                    else {
                        rule.Order__c = currentOrder;
                    }
                }
            }
            update qcRules;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('QCRuleController', ex.getMessage(), ex.getStackTraceString());
        }
    }


    /*
      Authors: Chandresh Koyani
      Purpose: Get Quality Check fieldset to create form.
      Dependencies: Called from "QCRule.Page"
     */
    @RemoteAction
    public static List<QCHelper.Option> getQualityCheckFieldset() {
        List<QCHelper.Option> options = new List<QCHelper.Option> ();

        try {
            Map<String, Schema.FieldSet> fieldSetMap = Schema.getGlobalDescribe().get(ObjectUtil.getPackagedFieldName('QualityCheck__c')).getDescribe().fieldSets.getMap();

            for (Schema.FieldSet fieldsetObj : fieldSetMap.values()) {
                options.add(new QCHelper.Option(fieldsetObj.getLabel(), fieldsetObj.getName()));
            }
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('QCRuleController', ex.getMessage(), ex.getStackTraceString());
        }
        return options;
    }
}