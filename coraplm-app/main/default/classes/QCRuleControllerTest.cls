/**
  * Authors     : Chandresh Koyani
  * Date created : 17/11/2017 
  * Purpose      : Test Class for QCRuleTest 
  * Dependencies : QCHelper.cls,QCRuleController.cls,QCCalculation.cls
  * -------------------------------------------------
  *Modifications:
                Date:   
                Purpose of modification:  
                Method/Code segment modified: 

*/
@isTest
private class QCRuleControllerTest { 
    @testSetup 
    static void setupTestData() {
        
        CreateRuleAlwaysMoveToQC();
        CreateRuleQCPerecentage();

        CaseManager__c caseObj=new CaseManager__c();
        caseObj.Process__c='AP';
        caseObj.WorkType__c='PO Invoices';
        caseObj.Document_Type__c='Electricity';
        caseObj.Requestor_Email__c='chandresh.koyani@test123.com';
        caseObj.Amount__c=12000;
        caseObj.Status__c='Ready for processing';
        caseObj.escalation__c=true;
        insert caseObj;

        CaseManager__c caseObjNew=new CaseManager__c();
        caseObjNew.Process__c='AP';
        caseObjNew.WorkType__c='PO Invoices';
        caseObjNew.Document_Type__c='Electricity';
        caseObjNew.Requestor_Email__c='chandresh.koyani@test123.com';
        caseObjNew.Amount__c=2000;
        caseObjNew.Status__c='Ready for processing';
        caseObj.escalation__c=true;
        insert caseObjNew;
    }
    public static void CreateRuleAlwaysMoveToQC(){
        QCHelper.QCRule qcRuleObj=new QCHelper.QCRule();
        qcRuleObj.JSONValue=new QCHelper.JSONValue();

        qcRuleObj.JSONValue.Criterias=new List<QCHelper.Criteria>();
        qcRuleObj.JSONValue.AlwaysMoveToQC=true;
        qcRuleObj.JSONValue.QCPerecentage=0;

        QCHelper.Criteria cre=null;
        cre=new QCHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Process__c');
        cre.Operator='contains';
        cre.Value='AP';
        cre.Type='PICKLIST';
        cre.RefField='';
        qcRuleObj.JSONValue.Criterias.add(cre);
        

        cre=new QCHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Worktype__c');
        cre.Operator='contains';
        cre.Value='PO Invoices';
        cre.Type='PICKLIST';
        cre.RefField='';
        qcRuleObj.JSONValue.Criterias.add(cre);

        cre=new QCHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Amount__c');
        cre.Operator='greater or equal';
        cre.Value='10000';
        cre.Type='NUMBER';
        cre.RefField='';
        qcRuleObj.JSONValue.Criterias.add(cre);
        
        QC_Rule_Config__c qcConfig = new QC_Rule_Config__c();
        qcConfig.Rule_Name__c = 'Test';
        qcConfig.JSON__c = JSON.serialize(qcRuleObj.JSONValue);
        qcConfig.IsActive__c=true;
        qcConfig.Order__c = 1;
        
        insert qcConfig;
    }
    public static void CreateRuleQCPerecentage(){
        QCHelper.QCRule qcRuleObj=new QCHelper.QCRule();
        qcRuleObj.JSONValue=new QCHelper.JSONValue();

        qcRuleObj.JSONValue.Criterias=new List<QCHelper.Criteria>();
        qcRuleObj.JSONValue.AlwaysMoveToQC=false;
        qcRuleObj.JSONValue.QCPerecentage=50;

        QCHelper.Criteria cre=null;
        cre=new QCHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Process__c');
        cre.Operator='contains';
        cre.Value='AP';
        cre.Type='PICKLIST';
        cre.RefField='';
        qcRuleObj.JSONValue.Criterias.add(cre);
        

        cre=new QCHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Worktype__c');
        cre.Operator='contains';
        cre.Value='PO Invoices';
        cre.Type='PICKLIST';
        cre.RefField='';
        qcRuleObj.JSONValue.Criterias.add(cre);

        cre=new QCHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Amount__c');
        cre.Operator='greater or equal';
        cre.Value='1000';
        cre.Type='NUMBER';
        cre.RefField='';
        qcRuleObj.JSONValue.Criterias.add(cre);

        cre=new QCHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('escalation__c');
        cre.Operator='equals';
        cre.Value='True';
        cre.Type='BOOLEAN';
        cre.RefField='';
        qcRuleObj.JSONValue.Criterias.add(cre);
        
        QC_Rule_Config__c qcConfig = new QC_Rule_Config__c();
        qcConfig.Rule_Name__c = 'Test';
        qcConfig.JSON__c = JSON.serialize(qcRuleObj.JSONValue);
        qcConfig.IsActive__c=true;
        qcConfig.Order__c = 1;
        qcConfig.Type__c='sampling';
        insert qcConfig;
    }
    public static testMethod void testQCRuleController(){
        QCRuleController ruleController=new QCRuleController();
        QCRuleController.GetAllRules();
        List<QCHelper.FieldInfo> fieldList= QCRuleController.GetCaseTrackerFields();

        QC_Rule_Config__c qcRule = [select id,Name,JSON__c from QC_Rule_Config__c limit 1];
        
        QCRuleController.ChangeState(qcRule.id,true);
       
        QCRuleController.ChangeOrder(1,'UP','sampling');
        QCRuleController.ChangeOrder(4,'Down','sampling');
        
        QCRuleController.ChangeOrder(3,'UP','sampling');
        

        QCHelper.QCRule qcRuleObj=new QCHelper.QCRule();
        qcRuleObj.Id=qcRule.Id;
        qcRuleObj.RuleName='Test1';
        qcRuleObj.Order=1;
        qcRuleObj.IsActive=true;
        qcRuleObj.JSONValue=(QCHelper.JSONValue) System.JSON.deserialize(qcRule.JSON__c, QCHelper.JSONValue.class);

        QCRuleController.SaveRule(qcRuleObj);

        qcRuleObj.Id='';

        QCRuleController.SaveRule(qcRuleObj);

        QCRuleController.RemoveRule(qcRule.id);

    }
    public static testMethod void testQCCalculation(){
      List<CaseManager__c> caseObjList=[select id,Name,UserAction__c,Process__c,WorkType__c,Amount__c,escalation__c from CaseManager__c];
        QCCalculation.CheckForQC(caseObjList);
        
        Map<Id,CaseManager__c> caseTrackerMap=new Map<Id,CaseManager__c>();
        for(CaseManager__c caseObj : caseObjList){
            
            CaseManager__c caseObjNew=new CaseManager__c();
            caseObjNew.Process__c='AP';
            caseObjNew.WorkType__c='PO Invoices';
            caseObjNew.Document_Type__c='Electricity';
            caseObjNew.Requestor_Email__c='chandresh.koyani@test123.com';
            caseObjNew.Amount__c=20000;
            caseObjNew.Status__c='Ready for processing';
            caseObjNew.escalation__c=true;
            
            caseObjNew.Id=caseObj.id;
            
            caseTrackerMap.put(caseObj.id,caseObjNew);

            caseObj.UserAction__c='Process';
        }

        QCCalculation.CheckForQC(caseObjList,caseTrackerMap);


        QC_Rule_Config__c qcRule = [select id,Name,JSON__c from QC_Rule_Config__c limit 1];
        qcRule.Type__c='form';
        update qcRule;
        QCCalculation.getQCFormFieldInfo(caseObjList[0].id);
    }
    public static testMethod void testIsMatchMethod(){
        
        QCHelper.Criteria cre=new QCHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Status__c');
        cre.Operator='equals';
        cre.Value='True';
        cre.Type='BOOLEAN';

        //Boolean check
        
        QCCalculation.IsMatch(cre,true);
        cre.Operator='not equal to';
        QCCalculation.IsMatch(cre,true);

        //Integer Check
        cre.Value='5';

        cre.Operator='not equal to';
        QCCalculation.IsMatch(cre,1);

        cre.Operator='equals';
        QCCalculation.IsMatch(cre,1);

        cre.Operator='less than';
        QCCalculation.IsMatch(cre,2);

        cre.Operator='greater than';
        QCCalculation.IsMatch(cre,2);

        cre.Operator='less or equal';
        QCCalculation.IsMatch(cre,2);

        cre.Operator='greater or equal';
        QCCalculation.IsMatch(cre,2);

        //String Check

        cre.Value='TEst';

        cre.Operator='not equal to';
        QCCalculation.IsMatch(cre,'Test');

        cre.Operator='equals';
        QCCalculation.IsMatch(cre,'Test');

        cre.Operator='starts with';
        QCCalculation.IsMatch(cre,'Test');

        cre.Operator='end with';
        QCCalculation.IsMatch(cre,'Test');

        cre.Operator='contains';
        QCCalculation.IsMatch(cre,'Test');

        cre.Operator='does not contain';
        QCCalculation.IsMatch(cre,'Test');

        cre.Operator='contains in list';
        QCCalculation.IsMatch(cre,'Test');
    }
}