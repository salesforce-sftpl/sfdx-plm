/*
Authors: Niraj Prajapati
Date created: 01/11/2017
Purpose: This controller will get the fields as per field set configured on Quality Check Object.
Dependencies: 
-------------------------------------------------
Modifications: Niraj Prajapati
Date: 14/12/2017
Purpose of modification: Pass Fieldset Name Dynamically for PUX-512
Method/Code segment modified: getQualityControls

*/
public  with sharing class QualityControlController {
    /*
Authors: Niraj Prajapati
Purpose: This method will be used to get QualityControls records.
Dependencies: QualityControl.cmp


*/
    @AuraEnabled
    public static Map<String,Object> getQualityControls(String caseId,String fsName) {
        system.debug('fsName'+fsName);
        fsName=ObjectUtil.getValidFieldSetName(ObjectUtil.getWithNameSpace('QualityCheck__c'),fsName);
        Map<String,Object> resp=new Map<String,Object>();
        String errorMessage='The following exception has occurred: ';
        system.debug('fsName'+fsName);
        try{
            Map<String,Object> fieldMap=new  Map<String,Object>();
            resp.put('QualityControls',CaseService.getQualityControls(caseId,fsName));
            
            for(Schema.FieldSetMember fld :SObjectType.QualityCheck__c.FieldSets.getMap().get(fsName).getFields()) {
                fieldMap.put(fld.getFieldPath(), fld.getLabel());
                
            }
            resp.put('FieldMap',fieldMap);
        }catch (Exception e) {
            resp.put('error', errorMessage+e.getMessage());
            ExceptionHandlerUtility.writeException('QualityControlController', e.getMessage(), e.getStackTraceString());       
        } 
        return resp;
    }
}