/**
* Authors    : Niraj Prajapati
* Date created : 16/11/2017 
* Purpose      : Test Class for QualityControlController
* Dependencies : -
* -------------------------------------------------
*Modifications:
Date:   
Purpose of modification:  
Method/Code segment modified: 

*/
@isTest
public class QualityControlControllerTest {
    //Test 1
    public static testMethod void unitTest01(){
        CaseManager__c caseObj=TestDataGenerator.createCaseTracker();
        System.assertEquals(caseObj.Subject__c,'Invalid Invoice');
        
        QualityCheck__c qcObj=new QualityCheck__c();
        qcObj.CaseManager__c=caseObj.Id;
        // qcObj.Process__c='AP';
        // qcObj.VendorName__c='SFTPL';
        
        insert qcObj;
        System.assertEquals(qcObj.CaseManager__c,caseObj.Id);
        QualityCheck__c qcObjNew=new QualityCheck__c();
        qcObjNew.CaseManager__c=caseObj.Id;
        // qcObjNew.Process__c='CT';
        //   qcObjNew.VendorName__c='Sailfin';
        
        insert qcObjNew;
        
        QualityControlController.getQualityControls(caseObj.Id,'QC');
        QualityControlController.getQualityControls('invalidId','QC');
        QualityControlController.getQualityControls(null,'QC');
        System.runAs(TestDataGenerator.createUser()) {
            QualityControlController.getQualityControls(caseObj.Id,'QC');
            QualityControlController.getQualityControls('invalidId','QC');
            QualityControlController.getQualityControls(null,'QC');
        } 
    }
}