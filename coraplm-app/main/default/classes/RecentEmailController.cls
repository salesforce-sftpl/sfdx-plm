/*
Authors: Ashish Kumar
Date created: 24/10/2017
Purpose: Class for getting recent Interactions in PLM Assist
Dependencies: 
-------------------------------------------------
Modifications:
Date: 
Purpose of modification:
Method/Code segment modified:

*/
public class RecentEmailController {
    
    @AuraEnabled
    public static List<InteractionWrapper> getRecentInteraction(){        
        List<InteractionWrapper> interactionWrapperList = new List<InteractionWrapper>();
        for (CaseManager__c master : [Select Id,Subject__c,Name, (select id,TextBody,FromName,CreatedDate, HtmlBody from Emails Order By CreatedDate desc Limit 1) 
                                      from CaseManager__c Order By CreatedDate desc Limit 10]) {
                                          //where Ownerid=:uid limit 10
                                          String interactionBody = '';
                                          String fromName = '';
                                          String createdDate = '';
                                          if(master.Emails.size() >0){
                                              interactionBody = master.Emails[0].HTMLBody;
                                              fromName = master.Emails[0].FromName;
                                              createdDate = String.valueOf(master.Emails[0].CreatedDate);
                                          }
                                          interactionWrapperList.add(new InteractionWrapper(master.Name, interactionBody,fromName, master.Subject__c, createdDate));
                                          
                                      }
        return interactionWrapperList;
    }
    
 /*   class interactionWrapper{
        @AuraEnabled
        public String caseNumber{get;set;}
        @AuraEnabled
        public String intrBody{get;set;}
        @AuraEnabled
        public String fromName{get;set;}
        @AuraEnabled
        public String subject{get;set;}
        @AuraEnabled
        public String createdDate {get;set;}
        
        public interactionWrapper(String caseNumber, String intrBody,String fromName, String subject, String createdDate){
            this.caseNumber = caseNumber;
            this.intrBody = intrBody;
            this.fromName = fromName;
            this.subject = subject;
            this.createdDate = createdDate;
        }
    }
    */
    
}