/**
  * Authors		: Parth Lukhi
  * Date created : 10/11/2017 
  * Purpose      : Test Class for PinnedCaseController
  * Dependencies : PinnedCaseController.apxc
  * -------------------------------------------------
  * Modifications:
    Date:   
    Purpose of modification:  
    Method/Code segment modified: 

*/
@isTest
public class RecentEmailControllerTest {
    
    	/*
        Authors: Ashish Kumar
        Purpose: Code Coverage
        Dependencies : PinnedCaseController.cls
    	*/
        public static testMethod void testFirst(){
            
            CaseManager__c caseObj=new CaseManager__c();
            caseObj.Process__c='AP';
            caseObj.WorkType__c='PO Invoices';
            caseObj.Document_Type__c='Electricity';
            caseObj.Requestor_Email__c='testEmail@hello.com';
            caseObj.Amount__c=12000;
            caseObj.Status__c='Ready for processing';
            caseObj.escalation__c=true;
            caseObj.Favourited__c = true;
            insert caseObj;  
            
            EmailMessage emailMsg = new EmailMessage();
			emailMsg.fromAddress = 'sender@email.com';

			emailMsg.fromName = 'Sender name';
            emailMsg.ToAddress = 'test@email.com';
            emailMsg.CcAddress = 'testcc@email.com';
			
			emailMsg.subject = 'Email subject';
			emailMsg.TextBody = 'Email Text Body';
			emailMsg.htmlBody = 'Email Text Body';
			emailMsg.Incoming = true;

			emailMsg.RelatedToId = caseObj.Id;
			emailMsg.status = '3';
			emailMsg.Headers = 'Header';
			emailMsg.Email_To_Case_Rule__c = null;
			insert emailMsg;
            
            Test.startTest();
            RecentEmailController.getRecentInteraction();
            Test.stopTest();
        }

}