/*
  Authors: Chandresh Koyani
  Date created: 22/12/2017
  Purpose: Batchable class for processing reminder and escalation.
  Dependencies: ReminderAndEscalationSchedulable.cls
  -------------------------------------------------
  Modifications:
  Date: 
  Purpose of modification: 
  Method/Code segment modified: 
 
*/
global class ReminderAndEscalationBatch implements Database.Batchable < sObject > {

    
    public static Map<id, ReminderAndEscalationHelper.ReminderRule> reminderMap;
    global SchedulableContext schedulableContextObject;

    public ReminderAndEscalationBatch(){

    }
    public ReminderAndEscalationBatch(SchedulableContext schedulableContextObject){
        this.schedulableContextObject=schedulableContextObject;
    }
    

    /*
      Authors: Chandresh Koyani
      Purpose: Static constructor to initialize Reminder Map. 
      Dependencies: 
    */
    static {
        reminderMap = new Map<id, ReminderAndEscalationHelper.ReminderRule> ();
        List<Reminder_Rule_Config__c> reminderConfigList = [select id,Rule_Name__c, Name, JSON__c, Order__c, IsActive__c from Reminder_Rule_Config__c order by Order__c];
        for (Reminder_Rule_Config__c configObj : reminderConfigList) {

            ReminderAndEscalationHelper.JSONValue jsonField = (ReminderAndEscalationHelper.JSONValue) System.JSON.deserialize(configObj.JSON__c, ReminderAndEscalationHelper.JSONValue.class);
            ReminderAndEscalationHelper.ReminderRule remRuleObj = new ReminderAndEscalationHelper.ReminderRule();
            remRuleObj.JSONValue = jsonField;
            remRuleObj.ruleName = configObj.Rule_Name__c;
            remRuleObj.order = Integer.valueOf(configObj.Order__c);
            remRuleObj.id = configObj.id;
            remRuleObj.isActive = configObj.IsActive__c;
            reminderMap.put(remRuleObj.Id, remRuleObj);
        }

    }

    /*
      Authors: Chandresh Koyani
      Purpose: Start method to get all reminder and escalation object, based on Due date.
      Dependencies: 
    */
    global Database.QueryLocator start(Database.BatchableContext batchContect) {
        Datetime currentDate = Datetime.Now();
		List<string> fields=new List<string>();
		fields.add('id');
		fields.add(ObjectUtil.getPackagedFieldName('Case_Manager__c'));
		fields.add(ObjectUtil.getPackagedFieldName('Due_Date__c'));
		fields.add(ObjectUtil.getPackagedFieldName('Reminder_Rule__c'));
		fields.add(ObjectUtil.getPackagedFieldName('Additional_Recipient__c'));
		fields.add(ObjectUtil.getPackagedFieldName('Org_Wide_Id__c'));
		fields.add(ObjectUtil.getPackagedFieldName('Template__c'));
		fields.add(ObjectUtil.getPackagedFieldName('Include_In__c'));
		fields.add(ObjectUtil.getPackagedFieldName('To_Addresses__c'));
		fields.add(ObjectUtil.getPackagedFieldName('CC_Addresses__c'));
		fields.add(ObjectUtil.getPackagedFieldName('BCC_Addresses__c'));
		fields.add(ObjectUtil.getPackagedFieldName('Next_Reminder__c'));
		fields.add(ObjectUtil.getPackagedFieldName('Mail_Trail_In__c'));
		fields.add(ObjectUtil.getPackagedFieldName('Type__c'));
		fields.add(ObjectUtil.getPackagedFieldName('Case_Manager__r.OwnerId'));
		fields.add(ObjectUtil.getPackagedFieldName('Case_Manager__r.Owner.Email'));
		
		string query='SELECT '+string.join(fields,',')+' from '+ObjectUtil.getPackagedFieldName('Reminder_And_Escalation__c')+' where '+ObjectUtil.getPackagedFieldName('Is_Sent__c')+'=false and '+ObjectUtil.getPackagedFieldName('Is_Cancelled__c')+'=false and '+ObjectUtil.getPackagedFieldName('Due_Date__c')+'<=:currentDate';
        return Database.getQueryLocator(query);
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Process reminder and escalation object and set IsSent or IsCancelled flag.
      Dependencies: 
    */
    global void execute(Database.BatchableContext batchContect, List<sObject> scope) {
        ReminderAndEscalationBatchHelper reminderAndEsclationHelperObj=new ReminderAndEscalationBatchHelper();
        List<Reminder_And_Escalation__c> reminderList = scope;

        for (Reminder_And_Escalation__c remObj : reminderList) {
            boolean isSuccess=reminderAndEsclationHelperObj.ProcessNotification(remObj);
            if(isSuccess){
                remObj.Is_Sent__c=true;
                remObj.Sent_Date__c=Datetime.now();
            }
            else{
                remObj.Is_Cancelled__c=true;
            }
        }
        update reminderList;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Finish method to write finish logic.
      Dependencies: 
    */
    global void finish(Database.BatchableContext batchContect) {
        if (this.schedulableContextObject != null) {
             System.abortJob(this.schedulableContextObject.getTriggerId());
        }
        
        DateTime objDateTime = datetime.now().addMinutes(5);
        String cron = '';
        cron += objDateTime.second();
        cron += ' ' + objDateTime.minute();
        cron += ' ' + objDateTime.hour();
        cron += ' ' + objDateTime.day();
        cron += ' ' + objDateTime.month();
        cron += ' ' + '?';
        cron += ' ' + objDateTime.year();

        System.schedule('Reminder And Escalation', cron, new ReminderAndEscalationSchedulable());

    }
}