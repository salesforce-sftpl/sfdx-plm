/*
  Authors: Chandresh Koyani
  Date created: 22/11/2017
  Purpose: This class contains methods to send mail for Reminder and Escalation.
  Dependencies: ReminderAndEscalationBatch.cls
  -------------------------------------------------
  Modifications:
  Date: 
  Purpose of modification: 
  Method/Code segment modified: 
 
*/
public class ReminderAndEscalationBatchHelper {

    
    /*
      Authors: Chandresh Koyani
      Purpose: Process Reminder And Escalation, Send Mail.
      Dependencies: ReminderAndEscalationBatch.cls
    */
    public boolean ProcessNotification(Reminder_And_Escalation__c remObj) {
        return SendMail(remObj,remObj.Case_Manager__r);
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Send mail using Reminder and Escalation object.
      Dependencies: Called from ProcessNotification nethod.
    */
    @testvisible
    private boolean SendMail(Reminder_And_Escalation__c remObj, CaseManager__c caseTrackerObj) {
        boolean isSuccess = false;
        try {
            
            List<Attachment> attach=[select id,Body from Attachment where parentId=:remObj.Id];

            Messaging.SingleEmailMessage mailMsg = new Messaging.SingleEmailMessage();

            EmailToCaseTemplateResponse emailMergeResponse = mergeTemplate(remObj.Template__c, caseTrackerObj.id);
            
            string mailBody=emailMergeResponse.body;

            

            List<Messaging.EmailFileAttachment> emailAttachments = new List<Messaging.EmailFileAttachment> ();
            if(attach!=null && attach.size()>0){
                string mailTrail=attach[0].body.toString();
                mailBody=mailBody+'<br/><hr/><br/>'+mailTrail;

                Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                efa.setFileName('MailTrail.html');
                efa.setBody(attach[0].Body);
                emailAttachments.add(efa);
            }

            if(!string.isEmpty(remObj.Mail_Trail_In__c) && remObj.Mail_Trail_In__c=='In Mail'){
                mailMsg.setHtmlBody(mailBody);
            }
            else{
                mailMsg.setHtmlBody(emailMergeResponse.body);
            }
            
            if(!string.isEmpty(remObj.Mail_Trail_In__c) && remObj.Mail_Trail_In__c=='Attachment'){
                mailMsg.setFileAttachments(emailAttachments);
            }

            mailMsg.setSubject(emailMergeResponse.Subject);

            mailMsg.setWhatId(caseTrackerObj.Id);
            if (!string.isEmpty(remObj.Org_Wide_Id__c)) {
                mailMsg.setOrgWideEmailAddressId(remObj.Org_Wide_Id__c);
            }

            List<string> toAddresses=new List<string>();
            List<string> ccAddresses=new List<string>();
            List<string> bccAddresses=new List<string>();

            if(!string.isEmpty(remObj.To_Addresses__c)){
                toAddresses=remObj.To_Addresses__c.split(';');
            }
            if(!string.isEmpty(remObj.CC_Addresses__c)){
                ccAddresses=remObj.CC_Addresses__c.split(';');
            }
            if(!string.isEmpty(remObj.BCC_Addresses__c)){
                bccAddresses=remObj.BCC_Addresses__c.split(';');
            }

            if(!string.isEmpty(remObj.Additional_Recipient__c)){
                List<string> additionalList=remObj.Additional_Recipient__c.split(';');
                if(!string.isEmpty(remObj.Include_In__c)){
                    if(remObj.Include_In__c=='TO'){
                        toAddresses.addAll(additionalList);
                    }
                    else if(remObj.Include_In__c=='CC'){
                        ccAddresses.addAll(additionalList);
                    }
                    else{
                        bccAddresses.addAll(additionalList);
                    }
                }
                else{
                    toAddresses.addAll(additionalList);
                }
            }
            if(remObj.Type__c=='TAT'){
                toAddresses.add(caseTrackerObj.Owner.Email);
                mailMsg.setToAddresses(toAddresses);
                //mailMsg.setToAddresses(new List<string>{caseTrackerObj.Owner.Email});
            }
            else{
                mailMsg.setToAddresses(toAddresses);
                mailMsg.setCCAddresses(ccAddresses);
                mailMsg.setBCCAddresses(bccAddresses);
            }

            

            Messaging.sendEmail(new List<Messaging.SingleEmailMessage> { mailMsg });

            if(!string.isEmpty(remObj.Next_Reminder__c)){

                Attachment attchObj = new Attachment();
                attchObj.Name = 'PreviousMailTrail.html';
                attchObj.Body = blob.valueOf(mailBody);
                attchObj.ParentId = remObj.Next_Reminder__c;
                insert attchObj;
            }
            isSuccess = true;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('ReminderAndEscalationBatchHelper',ex.getMessage(),ex.getStackTraceString());
        }
        return isSuccess;
    }

    /*
      Wrapper class handle Merge Template response.
     */
    public class EmailToCaseTemplateResponse {
        public string body {
            get; set;
        }
        public string subject {
            get; set;
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: This method take templateId and case object id as parameter and returns subject and body with merged values.
      Dependencies: Use in "SendMail" method.
     */
    private EmailToCaseTemplateResponse mergeTemplate(string templateId, string caseTrackerId) {
        EmailToCaseTemplateResponse response = new EmailToCaseTemplateResponse();

        Messaging.SingleEmailMessage email = Messaging.renderStoredEmailTemplate(templateId, null, caseTrackerId);

        response.body = email.getHTMLBody();
        response.subject = email.getSubject();

        return response;
    }
}