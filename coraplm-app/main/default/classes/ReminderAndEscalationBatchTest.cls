/**
 * Authors     : Chandresh Koyani
 * Date created : 09/01/2018 
 * Purpose      : Test Class for ReminderAndEscalationBatch 
 * Dependencies : ReminderAndEscalationHelper.cls,ReminderAndEscalationBatch.cls
 * -------------------------------------------------
 *Modifications:
  Date:   
  Purpose of modification:  
  Method/Code segment modified: 

*/
@isTest
private class ReminderAndEscalationBatchTest {
    @testSetup
    static void setupTestData() {
        
        EmailTemplate validEmailTemplate = new EmailTemplate();

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            //Create Default template
            
            validEmailTemplate.isActive = true;
            validEmailTemplate.Name = 'name';
            validEmailTemplate.DeveloperName = 'testTemplate';
            validEmailTemplate.TemplateType = 'text';
            validEmailTemplate.FolderId = UserInfo.getUserId();

            insert validEmailTemplate;
        }
        CreateReminderRule();
        CreateReminderRuleNew();

        

        CaseManager__c caseObj = new CaseManager__c();
        caseObj.Process__c = 'AP';
        caseObj.WorkType__c = 'PO Invoices';
        caseObj.Document_Type__c = 'Electricity';
        caseObj.Requestor_Email__c = 'chandresh.koyani@test123.com';
        caseObj.Amount__c = 2000;
        caseObj.Status__c = 'Ready for processing';
        caseObj.escalation__c = true;
        caseObj.To_Addresses__c = 'chandresh.koyani@sftpl.com';
        caseObj.CC_Addresses__c = 'chandresh.koyani@sftpl.com';
        caseObj.BCC_Addresses__c = 'chandresh.koyani@sftpl.com';
        //caseObj.Esc_Addresses__c='chandresh.koyani@sftpl.com';
        insert caseObj;


        Reminder_And_Escalation__c remObj = new Reminder_And_Escalation__c();
        remObj.Case_Manager__c = caseObj.id;
        remobj.Due_Date__c = DateTime.now();
        //remobj.Reminder_Rule__c=0;
        remobj.Additional_Recipient__c = 'chandresh.koyani@sftpl.com';
        remobj.To_Addresses__c = 'chandresh.koyani@sftpl.com';
        remobj.CC_Addresses__c = 'chandresh.koyani@sftpl.com';
        remobj.BCC_Addresses__c = 'chandresh.koyani@sftpl.com';
        remobj.Template__c=validEmailTemplate.id;
        remobj.Include_In__c='TO';
        insert remObj;

        Reminder_And_Escalation__c remObj1 = new Reminder_And_Escalation__c();
        remObj1.Case_Manager__c = caseObj.id;
        remObj1.Due_Date__c = DateTime.now();
        //remobj.Reminder_Rule__c=0;
        remObj1.Additional_Recipient__c = 'chandresh.koyani@sftpl.com';
        remObj1.To_Addresses__c = 'chandresh.koyani@sftpl.com';
        remObj1.CC_Addresses__c = 'chandresh.koyani@sftpl.com';
        remObj1.BCC_Addresses__c = 'chandresh.koyani@sftpl.com';
        remObj1.Template__c=validEmailTemplate.id;
        remObj1.Include_In__c='CC';
        insert remObj1;

        Reminder_And_Escalation__c remObj2 = new Reminder_And_Escalation__c();
        remObj2.Case_Manager__c = caseObj.id;
        remObj2.Due_Date__c = DateTime.now();
        //remobj.Reminder_Rule__c=0;
        remObj2.Additional_Recipient__c = 'chandresh.koyani@sftpl.com';
        remObj2.To_Addresses__c = 'chandresh.koyani@sftpl.com';
        remObj2.CC_Addresses__c = 'chandresh.koyani@sftpl.com';
        remObj2.BCC_Addresses__c = 'chandresh.koyani@sftpl.com';
        remObj2.Template__c=validEmailTemplate.id;
        remObj2.Include_In__c='BCC';
        //remobj2.Next_Reminder__c=remObj1.id;
        insert remObj2;

        Reminder_And_Escalation__c remObjUpdate = new Reminder_And_Escalation__c();
        remObjUpdate.id=remObj1.id;
        remObjUpdate.Next_Reminder__c=remObj2.id;
        update remObjUpdate;

        Attachment attachmentObj=new Attachment();
        attachmentObj.Body=Blob.valueOf('This is simple text');
        attachmentObj.Name='test.pdf';
        attachmentObj.parentId=remObj2.id;
        insert attachmentObj;
    }
    public static void CreateReminderRule() {
        ReminderAndEscalationHelper.ReminderRule reminderRuleObj = new ReminderAndEscalationHelper.ReminderRule();
        reminderRuleObj.JSONValue = new ReminderAndEscalationHelper.JSONValue();

        reminderRuleObj.JSONValue.criterias = new List<ReminderAndEscalationHelper.Criteria> ();
        reminderRuleObj.JSONValue.notifications = new List<ReminderAndEscalationHelper.Notification> ();
        reminderRuleObj.JSONValue.escalations = new List<ReminderAndEscalationHelper.Escalation> ();
        reminderRuleObj.JSONValue.isEscalationRequired = true;
        reminderRuleObj.JSONValue.isEnableEscalationMatrix = true;
        reminderRuleObj.JSONValue.escalationMatrixTemplate = '';

        ReminderAndEscalationHelper.Criteria cre = null;
        cre = new ReminderAndEscalationHelper.Criteria();
        cre.Field = ObjectUtil.getPackagedFieldName('Process__c');
        cre.Operator = 'contains';
        cre.Value = 'AP';
        cre.Type = 'PICKLIST';

        reminderRuleObj.JSONValue.Criterias.add(cre);


        cre = new ReminderAndEscalationHelper.Criteria();
        cre.Field = ObjectUtil.getPackagedFieldName('Worktype__c');
        cre.Operator = 'contains';
        cre.Value = 'PO Invoices';
        cre.Type = 'PICKLIST';

        reminderRuleObj.JSONValue.Criterias.add(cre);
        /*
          cre=new ReminderAndEscalationHelper.Criteria();
          cre.Field='Amount__c';
          cre.Operator='greater or equal';
          cre.Value='10000';
          cre.Type='NUMBER';
         
          reminderRuleObj.JSONValue.Criterias.add(cre);
         
          cre=new ReminderAndEscalationHelper.Criteria();
          cre.Field='escalation__c';
          cre.Operator='equals';
          cre.Value='True';
          cre.Type='BOOLEAN';
         
          reminderRuleObj.JSONValue.Criterias.add(cre);*/

        ReminderAndEscalationHelper.Notification notification = null;
        notification = new ReminderAndEscalationHelper.Notification();
        notification.after = '30';
        notification.additionalRecipient = 'chandresh.koyani@sftpl.com';
        notification.afterUnit = 'Minutes';
        notification.includeIn = 'To';
        notification.toAddressField='To_Addresses__c';
        notification.ccAddressField='CC_Addresses__c';
        notification.bccAddressField='BCC_Addresses__c';
        reminderRuleObj.JSONValue.notifications.add(notification);



        ReminderAndEscalationHelper.Escalation esclation = null;
        esclation = new ReminderAndEscalationHelper.Escalation();
        esclation.after = '30';
        esclation.additionalRecipient = 'chandresh.koyani@sftpl.com';
        esclation.afterUnit = 'Minutes';
        esclation.includeIn = 'To';
        esclation.toAddressField='To_Addresses__c';
        esclation.ccAddressField='CC_Addresses__c';
        esclation.bccAddressField='BCC_Addresses__c';
        reminderRuleObj.JSONValue.escalations.add(esclation);

        Reminder_Rule_Config__c ruleConfig = new Reminder_Rule_Config__c();
        ruleConfig.Rule_Name__c = 'Test';
        ruleConfig.JSON__c = JSON.serialize(reminderRuleObj.JSONValue);
        ruleConfig.IsActive__c = true;
        ruleConfig.Order__c = 1;

        insert ruleConfig;
    }
    public static void CreateReminderRuleNew() {
        ReminderAndEscalationHelper.ReminderRule reminderRuleObj = new ReminderAndEscalationHelper.ReminderRule();
        reminderRuleObj.JSONValue = new ReminderAndEscalationHelper.JSONValue();

        reminderRuleObj.JSONValue.criterias = new List<ReminderAndEscalationHelper.Criteria> ();
        reminderRuleObj.JSONValue.notifications = new List<ReminderAndEscalationHelper.Notification> ();
        reminderRuleObj.JSONValue.escalations = new List<ReminderAndEscalationHelper.Escalation> ();
        reminderRuleObj.JSONValue.isEscalationRequired = true;
        reminderRuleObj.JSONValue.isEnableEscalationMatrix = true;
        reminderRuleObj.JSONValue.escalationMatrixTemplate = '';

        ReminderAndEscalationHelper.Criteria cre = null;
        cre = new ReminderAndEscalationHelper.Criteria();
        cre.Field =  ObjectUtil.getPackagedFieldName('Process__c');
        cre.Operator = 'contains';
        cre.Value = 'AP';
        cre.Type = 'PICKLIST';

        reminderRuleObj.JSONValue.Criterias.add(cre);


        cre = new ReminderAndEscalationHelper.Criteria();
        cre.Field = ObjectUtil.getPackagedFieldName('Worktype__c');
        cre.Operator = 'contains';
        cre.Value = 'PO Invoices';
        cre.Type = 'PICKLIST';

        reminderRuleObj.JSONValue.Criterias.add(cre);

        cre = new ReminderAndEscalationHelper.Criteria();
        cre.Field = ObjectUtil.getPackagedFieldName('Amount__c');
        cre.Operator = 'greater or equal';
        cre.Value = '10000';
        cre.Type = 'NUMBER';

        reminderRuleObj.JSONValue.Criterias.add(cre);

        cre = new ReminderAndEscalationHelper.Criteria();
        cre.Field = ObjectUtil.getPackagedFieldName('escalation__c');
        cre.Operator = 'equals';
        cre.Value = 'True';
        cre.Type = 'BOOLEAN';

        reminderRuleObj.JSONValue.Criterias.add(cre);

        ReminderAndEscalationHelper.Notification notification = null;
        notification = new ReminderAndEscalationHelper.Notification();
        notification.after = '30';
        notification.additionalRecipient = 'chandresh.koyani@sftpl.com';
        notification.afterUnit = 'Minutes';
        notification.includeIn = 'To';
        notification.toAddressField='To_Addresses__c';
        notification.ccAddressField='CC_Addresses__c';
        notification.bccAddressField='BCC_Addresses__c';
        reminderRuleObj.JSONValue.notifications.add(notification);



        ReminderAndEscalationHelper.Escalation esclation = null;
        esclation = new ReminderAndEscalationHelper.Escalation();
        esclation.after = '30';
        esclation.additionalRecipient = 'chandresh.koyani@sftpl.com';
        esclation.afterUnit = 'Minutes';
        esclation.includeIn = 'To';
        esclation.toAddressField='To_Addresses__c';
        esclation.ccAddressField='CC_Addresses__c';
        esclation.bccAddressField='BCC_Addresses__c';
        reminderRuleObj.JSONValue.escalations.add(esclation);

        Reminder_Rule_Config__c ruleConfig = new Reminder_Rule_Config__c();
        ruleConfig.Rule_Name__c = 'Test1';
        ruleConfig.JSON__c = JSON.serialize(reminderRuleObj.JSONValue);
        ruleConfig.IsActive__c = true;
        ruleConfig.Order__c = 2;

        insert ruleConfig;
    }
    public static testMethod void testBatch() {
        Test.startTest();

        ReminderAndEscalationBatch obj = new ReminderAndEscalationBatch();
        DataBase.executeBatch(obj);

        Test.stopTest();
    }
}