/*
  Authors: Chandresh Koyani
  Date created: 22/11/2017
  Purpose: This class contains all wrapper classes and common methods that use in Reminder and Escalation System.
  Dependencies: ReminderAndEscalationBatch.cls, ReminderAndEscalationRuleController.cls, CaseTrackerTrigger.trigger
  -------------------------------------------------
  Modifications:
  Date: 
  Purpose of modification:
  Method/Code segment modified:
 
*/
public class ReminderAndEscalationHelper {
    public static Boolean isFirstCall = true;
    /*
      Authors: Chandresh Koyani
      Purpose: Wrapper class to store option like value,text.
      Dependencies: ReminderAndEscalationRuleController.cls.
     */
    public class Option {
        public string value { get; set; }
        public string text { get; set; }

        public Option() {
        }
        public Option(string value, string text) {
            this.value = value;
            this.text = text;
        }
    }
    /*
      Authors: Chandresh Koyani
      Purpose: This wrapper class is used to store Field information.
      Dependencies: ReminderAndEscalationRuleController.cls.
     */
    public class FieldInfo {
        public string label {
            get; set;
        }
        public string apiName {
            get; set;
        }
        public string type {
            get; set;
        }
        public list<Option> pickListValue {
            get; set;
        }
        public List<string> operator {
            get; set;
        }
    }
    /*
      Authors: Chandresh Koyani
      Purpose: This wrapper class is used to store Criteria information.
      Dependencies: ReminderAndEscalationRuleController.cls.
     */
    public class Criteria {
        public string field { get; set; }
        public string operator { get; set; }
        public string value { get; set; }
        public string type { get; set; }
        public string refField { get; set; }
    }

    public class Notification {
        public string emailTemplate { get; set; }
        public string after { get; set; }
        public string additionalRecipient { get; set; }
        public string afterUnit { get; set; }
        public string includeIn { get; set; }
        public string toAddressField { get; set; }
        public string ccAddressField { get; set; }
        public string bccAddressField { get; set; }
        public string mailTrailIn { get; set; }
    }
    public class Escalation {
        public string emailTemplate { get; set; }
        public string after { get; set; }
        public string additionalRecipient { get; set; }
        public string afterUnit { get; set; }
        public string includeIn { get; set; }
        public string toAddressField { get; set; }
        public string ccAddressField { get; set; }
        public string bccAddressField { get; set; }
        public string mailTrailIn { get; set; }
    }


    /*
      Authors: Chandresh Koyani
      Purpose: This wrapper class is used to store JSONValue information.
      Dependencies: ReminderAndEscalationRuleController.cls.
     */
    public class JSONValue {
        public List<Criteria> criterias { get; set; }
        public List<Notification> notifications { get; set; }
        public List<Escalation> escalations { get; set; }
        public boolean isEscalationRequired { get; set; }
        public boolean isEnableEscalationMatrix { get; set; }
        public string escalationMatrixTemplate { get; set; }

    }

    /*
      Authors: Chandresh Koyani
      Purpose: This wrapper class is used to store ReminderRule information.
      Dependencies: ReminderAndEscalationRuleController.cls.
     */
    public class ReminderRule {
        public JSONValue JSONValue { get; set; }
        public string ruleName { get; set; }
        public integer order { get; set; }
        public string id { get; set; }
        public boolean isActive { get; set; }
    }
    /*
      Authors: Chandresh Koyani
      Purpose: This wrapper class is used to store Escalation Matrix information.
      Dependencies: ReminderAndEscalationRuleController.cls.
     */
    public class MatrixEscalation {
        public string after { get; set; }
        public string afterUnit { get; set; }
        public string toAddresses { get; set; }
        public string ccAddresses { get; set; }
        public string bccAddresses { get; set; }
        public string mailTrailIn { get; set; }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: This wrapper class is used to store Escalation Matrix information in JSON.
      Dependencies: ReminderAndEscalationRuleController.cls.
     */
    public class MatrixJSON {
        public List<MatrixEscalation> matrixEscalations { get; set; }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: This wrapper class is used to store Escalation Matrix information.
      Dependencies: ReminderAndEscalationRuleController.cls.
     */
    public class EscalationMatrix {
        public MatrixJSON matrixJSON { get; set; }
        public string matrixFor { get; set; }
        public string id { get; set; }
        public boolean isActive { get; set; }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: This method called from trigger it will remove all reminder and escalation object based on Case Tracker Id.
      Dependencies: CaseTrackerTrigger.trigger.
     */
    public static void removeReminderObject(List<CaseManager__c> caseTrackeListNew, Map<Id, CaseManager__c> caseTrackerOldMap) {
        try {
            List<id> caseIdsForStateChange = new List<id> ();
            for (CaseManager__c caseTrackerNew : caseTrackeListNew) {
                CaseManager__c caseTrackerOld = caseTrackerOldMap.get(caseTrackerNew.Id);
                if (caseTrackerNew.Status__c != caseTrackerOld.Status__c) {
                    caseIdsForStateChange.add(caseTrackerNew.Id);
                }
            }
            if (caseIdsForStateChange.size() > 0) {
                List<Reminder_And_Escalation__c> reminderList = [select id from Reminder_And_Escalation__c where Type__c IN('REM', 'ESC') and Case_Manager__C = :caseIdsForStateChange];
                delete reminderList;
            }
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('ReminderAndEscalationHelper', ex.getMessage(), ex.getStackTraceString());
        }

    }

    /*
      Authors: Chandresh Koyani
      Purpose: This method is called from Process Builder to set Reminder and escalation based on Rule.
      Dependencies: CaseTrackerTrigger.trigger.
     */
    @InvocableMethod(label = 'Set Reminder and Escalation' description = 'Set Reminder and Escalation based on Reminder Rules.')
    public static void setReminderAndEscalation(List<CaseManager__c> caseTrackers) {
        if (isFirstCall)
        {
            try {
                isFirstCall = false;
                List<Reminder_Rule_Config__c> remRuleConfiges = [select id, Name, JSON__c, Order__c, IsActive__c from Reminder_Rule_Config__c where IsActive__c = true order by Order__c];
                List<Reminder_And_Escalation__c> reminderList = new List<Reminder_And_Escalation__c> ();
                List<Escalation_Matrix__c> escalationMatrices = [select id, For__c, JSON__c from Escalation_Matrix__c where IsActive__c = true];
                Map<string, Escalation_Matrix__c> matrixMap = new Map<string, Escalation_Matrix__c> ();

                List<string> caseIds = new List<string> ();
                for (CaseManager__c caseObj : caseTrackers)
                {
                    caseIds.add(caseObj.Id);
                }

                List<ContentDocumentLink> contentDocumentLinks = [SELECT ContentDocument.Id, LinkedEntityId FROM ContentDocumentLink where LinkedEntityId = :caseIds and ContentDocument.Title like
                                                                  '%_OUTBOUND.html' order by ContentDocument.CreatedDate];
                Map<string, string> caseLinkMap = new Map<string, string> ();
                Map<string, string> caseLinkRevMap = new Map<string, string> ();
                for (ContentDocumentLink contentLink : contentDocumentLinks) {
                    if (!caseLinkMap.containsKey(contentLink.LinkedEntityId)) {
                        caseLinkMap.put(contentLink.LinkedEntityId, contentLink.ContentDocument.Id);
                        caseLinkRevMap.put(contentLink.ContentDocument.Id, contentLink.LinkedEntityId);
                    }
                }

                Map<string, ContentVersion> caseVersionMap = new Map<string, ContentVersion> ();

                if (caseLinkMap.size() > 0) {
                    List<ContentVersion> contentVersionList = [SELECT id, ContentDocumentId, VersionData, FileExtension FROM ContentVersion where ContentDocumentId = :caseLinkMap.values()];

                    for (ContentVersion contVersionObj : contentVersionList) {
                        string caseId = caseLinkRevMap.get(contVersionObj.ContentDocumentId);
                        caseVersionMap.put(caseId, contVersionObj);
                    }
                }

                for (Escalation_Matrix__c matrix : escalationMatrices) {
                    matrixMap.put(matrix.For__c, matrix);
                }

                for (CaseManager__c caseObj : caseTrackers)
                {
                    ReminderRule rule = findRule(caseObj, remRuleConfiges);
                    if (rule != null)
                    {

                        createReminderAndEscalation(rule, caseObj, reminderList, matrixMap);
                    }
                }
                insert reminderList;

                List<Attachment> attachList = new List<Attachment> ();
                Map<string, Attachment> caseFirstReminder = new Map<string, Attachment> ();
                List<Reminder_And_Escalation__c> remObjList = [select id, Case_Manager__c from Reminder_And_Escalation__c where Id = :reminderList and Type__c in('REM', 'ESC') order by Due_Date__c];
                for (Reminder_And_Escalation__c remObj : remObjList) {
                    if (!caseFirstReminder.containsKey(remObj.Case_Manager__c)) {
                        ContentVersion contentVersionObj = caseVersionMap.get(remObj.Case_Manager__c);
                        Attachment attchObj = new Attachment();
                        attchObj.Name = 'PreviousMailTrail.html';
                        attchObj.Body = contentVersionObj.VersionData;
                        attchObj.ParentId = remObj.id;
                        caseFirstReminder.put(remObj.Case_Manager__c, attchObj);
                    }
                }
                insert caseFirstReminder.values();

                Map<string, List<Reminder_And_Escalation__c>> caseReminderMap = new Map<string, List<Reminder_And_Escalation__c>> ();
                for (Reminder_And_Escalation__c remObj : remObjList) {
                    List<Reminder_And_Escalation__c> tempMap = caseReminderMap.get(remObj.Case_Manager__c);
                    if (tempMap == null) {
                        tempMap = new List<Reminder_And_Escalation__c> ();
                    }
                    tempMap.add(remObj);
                    caseReminderMap.put(remObj.Case_Manager__c, tempMap);
                }

                List<Reminder_And_Escalation__c> updateReminderList = new List<Reminder_And_Escalation__c> ();
                for (string key : caseReminderMap.keySet()) {
                    List<Reminder_And_Escalation__c> tempMap = caseReminderMap.get(key);
                    for (integer index = 0; index < tempMap.size(); index++) {
                        Reminder_And_Escalation__c nextReminder = null;
                        if (index != (tempMap.size() - 1)) {
                            nextReminder = tempMap.get(index + 1);
                        }
                        Reminder_And_Escalation__c reminder = tempMap.get(index);
                        if (nextReminder != null) {
                            Reminder_And_Escalation__c updateReminder = new Reminder_And_Escalation__c();
                            updateReminder.id = reminder.id;
                            updateReminder.Next_Reminder__c = nextReminder.id;
                            updateReminderList.add(updateReminder);
                        }
                    }
                }
                update updateReminderList;
            }
            catch(Exception ex) {
                ExceptionHandlerUtility.writeException('ReminderAndEscalationHelper', ex.getMessage(), ex.getStackTraceString());
            }
        }

    }

    public static void setReminderAndEscalation(List<CaseManager__c> caseTrackeListNew, Map<Id, CaseManager__c> caseTrackerOldMap) {
        List<CaseManager__c> updateList = new List<CaseManager__c> ();

        for (CaseManager__c caseTrackerNew : caseTrackeListNew) {
            CaseManager__c caseTrackerOld = caseTrackerOldMap.get(caseTrackerNew.Id);
            if (caseTrackerNew.Status__c != caseTrackerOld.Status__c && caseTrackerNew.Status__c == 'Awaiting Email Response') {
                updateList.add(caseTrackerNew);
            }
        }

        if (updateList.size() > 0) {
            setReminderAndEscalation(updateList);
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Create Reminder and Escalation object based on Rule.
      Dependencies: Called from "setReminderAndEscalation" method.
     */
    public static void createReminderAndEscalation(ReminderRule rule, CaseManager__c caseObj, List<Reminder_And_Escalation__c> reminderList, Map<string, Escalation_Matrix__c> matrixMap) {

        DateTime dueDate = Datetime.now();
        for (Notification notificationObj : rule.JSONValue.notifications)
        {

            dueDate = addUnit(dueDate, notificationObj.afterUnit, Integer.valueOf(notificationObj.after));

            Reminder_And_Escalation__c reminderObj = new Reminder_And_Escalation__c();
            reminderObj.Case_Manager__c = caseObj.id;
            reminderObj.Reminder_Rule__c = rule.id;
            reminderObj.Due_Date__c = dueDate;
            reminderObj.Org_Wide_Id__c = ''; // notificationObj.fromAddress;
            reminderObj.Template__c = notificationObj.emailTemplate;
            reminderObj.Additional_Recipient__c = notificationObj.additionalRecipient;
            reminderObj.Include_In__c = notificationObj.includeIn;

            if (!string.isEmpty(notificationObj.toAddressField)) {
                reminderObj.To_Addresses__c = string.valueof(caseObj.get(ObjectUtil.getPackagedFieldName(notificationObj.toAddressField)));
            }
            if (!string.isEmpty(notificationObj.ccAddressField)) {
                reminderObj.CC_Addresses__c = string.valueof(caseObj.get(ObjectUtil.getPackagedFieldName(notificationObj.ccAddressField)));
            }
            if (!string.isEmpty(notificationObj.bccAddressField)) {
                reminderObj.BCC_Addresses__c = string.valueof(caseObj.get(ObjectUtil.getPackagedFieldName(notificationObj.bccAddressField)));
            }

            if (!string.isEmpty(notificationObj.mailTrailIn) && notificationObj.mailTrailIn != 'None') {
                reminderObj.Mail_Trail_In__c = notificationObj.mailTrailIn;
            }
            reminderObj.Type__c = 'REM';
            reminderList.add(reminderObj);
        }

        //Check for matrix.
        Escalation_Matrix__c matrix = null;
        string toAddress = caseObj.To_Addresses__c;
        if (!string.isEmpty(toAddress)) {
            List<string> addressList = toAddress.split(';');
            if (addressList.size() > 0) {
                matrix = matrixMap.get(addressList[0]);
            }
        }
        if (rule.JSONValue.isEscalationRequired) {
            if (rule.JSONValue.isEnableEscalationMatrix && matrix != null) {
                //Create Esclation object based on matrix.
                ReminderAndEscalationHelper.MatrixJSON jsonField = (ReminderAndEscalationHelper.MatrixJSON) System.JSON.deserialize(matrix.JSON__c, ReminderAndEscalationHelper.MatrixJSON.class);

                for (MatrixEscalation matrixObj : jsonField.matrixEscalations) {
                    dueDate = addUnit(dueDate, matrixObj.afterUnit, Integer.valueOf(matrixObj.after));
                    Reminder_And_Escalation__c reminderObj = new Reminder_And_Escalation__c();
                    reminderObj.Case_Manager__c = caseObj.id;
                    reminderObj.Reminder_Rule__c = rule.id;
                    reminderObj.Due_Date__c = dueDate;
                    reminderObj.Org_Wide_Id__c = '';
                    reminderObj.Template__c = rule.JSONValue.escalationMatrixTemplate;
                    if (!string.isEmpty(matrixObj.toAddresses)) {
                        reminderObj.To_Addresses__c = matrixObj.toAddresses;
                    }
                    if (!string.isEmpty(matrixObj.ccAddresses)) {
                        reminderObj.CC_Addresses__c = matrixObj.ccAddresses;
                    }
                    if (!string.isEmpty(matrixObj.bccAddresses)) {
                        reminderObj.BCC_Addresses__c = matrixObj.bccAddresses;
                    }
                    if (!string.isEmpty(matrixObj.mailTrailIn) && matrixObj.mailTrailIn != 'None') {
                        reminderObj.Mail_Trail_In__c = matrixObj.mailTrailIn;
                    }
                    reminderObj.Type__c = 'ESC';
                    reminderList.add(reminderObj);
                }
            }
            else {
                //Create Esclation objects
                if (rule.JSONValue.escalations != null) {
                    for (Escalation esclationObj : rule.JSONValue.escalations) {
                        dueDate = addUnit(dueDate, esclationObj.afterUnit, Integer.valueOf(esclationObj.after));
                        Reminder_And_Escalation__c reminderObj = new Reminder_And_Escalation__c();
                        reminderObj.Case_Manager__c = caseObj.id;
                        reminderObj.Reminder_Rule__c = rule.id;
                        reminderObj.Due_Date__c = dueDate;
                        reminderObj.Org_Wide_Id__c = ''; // notificationObj.fromAddress;
                        reminderObj.Template__c = esclationObj.emailTemplate;
                        reminderObj.Additional_Recipient__c = esclationObj.additionalRecipient;
                        reminderObj.Include_In__c = esclationObj.includeIn;

                        if (!string.isEmpty(esclationObj.toAddressField)) {
                            reminderObj.To_Addresses__c = string.valueof(caseObj.get(ObjectUtil.getPackagedFieldName(esclationObj.toAddressField)));
                        }
                        if (!string.isEmpty(esclationObj.ccAddressField)) {
                            reminderObj.CC_Addresses__c = string.valueof(caseObj.get(ObjectUtil.getPackagedFieldName(esclationObj.ccAddressField)));
                        }
                        if (!string.isEmpty(esclationObj.bccAddressField)) {
                            reminderObj.BCC_Addresses__c = string.valueof(caseObj.get(ObjectUtil.getPackagedFieldName(esclationObj.bccAddressField)));
                        }
                        if (!string.isEmpty(esclationObj.mailTrailIn) && esclationObj.mailTrailIn != 'None') {
                            reminderObj.Mail_Trail_In__c = esclationObj.mailTrailIn;
                        }
                        reminderObj.Type__c = 'ESC';
                        reminderList.add(reminderObj);
                    }
                }
            }
        }
    }


    /*
      Authors: Chandresh Koyani
      Purpose: Add Minutes,Hours or Days on given datetime.
      Dependencies: Called from "createReminderAndEscalation" method.
     */
    @testvisible
    private static DateTime addUnit(DateTime dateTimeValue, string unit, integer value) {

        if (unit.equalsIgnoreCase('Minutes')) {
            dateTimeValue = dateTimeValue.addMinutes(value);
        }
        else if (unit.equalsIgnoreCase('Hours')) {
            dateTimeValue = dateTimeValue.addHours(value);
        }
        else if (unit.equalsIgnoreCase('Days')) {
            dateTimeValue = dateTimeValue.addDays(value);
        }
        return dateTimeValue;
    }
    /*
      Authors: Chandresh Koyani
      Purpose: Find matching Reminder rule based on case fields.
      Dependencies: Called from "setReminderAndEscalation" method.
     */
    @testvisible
    private static ReminderRule findRule(CaseManager__c caseTrackerObject, List<Reminder_Rule_Config__c> remRuleConfiges) {

        Map<string, User> userMap = new Map<string, User> ([select id, Name, Email from User where isActive = true]);

        for (Reminder_Rule_Config__c qcReminderConfig : remRuleConfiges) {

            ReminderRule qcReminderRule = new ReminderRule();
            qcReminderRule.id = qcReminderConfig.id;
            qcReminderRule.JSONValue = (JSONValue) System.JSON.deserialize(qcReminderConfig.JSON__c, JSONValue.class);

            if (qcReminderRule.JSONValue != null && qcReminderRule.JSONValue.criterias != null) {
                boolean isMatch = false;
                for (Criteria cre : qcReminderRule.JSONValue.criterias) {
                    if (cre.type.equalsIgnoreCase('TEXT') || cre.type.equalsIgnoreCase('PICKLIST') || cre.type.equalsIgnoreCase('EMAIL') || cre.type.equalsIgnoreCase('PHONE') || cre.type.equalsIgnoreCase('STRING') || cre.type.equalsIgnoreCase('TEXTAREA')) {
                        string fieldValue = string.valueOf(caseTrackerObject.get(cre.field));
                        isMatch = isMatch(cre.operator, cre.value, fieldValue, cre.type);
                    }
                    else if (cre.type.equalsIgnoreCase('NUMBER') || cre.type.equalsIgnoreCase('CURRENCY') || cre.type.equalsIgnoreCase('DOUBLE')) {
                        integer fieldValue = integer.valueOf(caseTrackerObject.get(cre.field));
                        isMatch = isMatch(cre.operator, cre.value, fieldValue);
                    }
                    else if (cre.type.equalsIgnoreCase('BOOLEAN')) {
                        boolean fieldValue = boolean.valueOf(caseTrackerObject.get(cre.field));
                        isMatch = isMatch(cre.operator, cre.value, fieldValue);
                    }
                    else if (cre.type.equalsIgnoreCase('REFERENCE')) {
                        string userId = string.valueOf(caseTrackerObject.get(cre.field));

                        if (!string.isEmpty(userId)) {
                            User ur = userMap.get(userId);
                            if (ur != null) {
                                string fieldValue = string.valueOf(ur.get(cre.refField));
                                isMatch = isMatch(cre.operator, cre.value, fieldValue, cre.type);
                            }
                        }
                    }
                    if (!isMatch) {
                        break;
                    }
                }

                if (isMatch) {
                    return qcReminderRule;
                }
            }
        }
        return null;
    }
    /*
      Authors: Chandresh Koyani
      Purpose: Check value is match based on operator is provided, This method is use only for string value.
      Dependencies: Called from "findRule" method.
     */

    @testVisible
    private static boolean isMatch(string operator, string value, string fieldValue, string type) {
        if (operator.equalsIgnoreCase('equals')) {
            return fieldValue == value;
        }
        else if (operator.equalsIgnoreCase('not equal to')) {
            return fieldValue != value;
        }
        else if (operator.equalsIgnoreCase('starts with')) {
            return fieldValue.startsWithIgnoreCase(value);
        }
        else if (operator.equalsIgnoreCase('end with')) {
            return fieldValue.endsWithIgnoreCase(value);
        }
        else if (operator.equalsIgnoreCase('contains')) {
            return fieldValue.containsIgnoreCase(value);
        }
        else if (operator.equalsIgnoreCase('does not contain')) {
            return !fieldValue.containsIgnoreCase(value);
        }
        else if (operator.equalsIgnoreCase('contains in list')) {
            if (!string.isEmpty(value)) {
                List<string> tempList = value.split(',');
                for (string str : tempList) {
                    if (type.equalsIgnoreCase('PICKLIST')) {
                        if (fieldValue.equalsIgnoreCase(str)) {
                            return true;
                        }
                    }
                    else {
                        if (fieldValue.containsIgnoreCase(str)) {
                            return true;
                        }
                    }
                }
            }
        }
		else if (operator.equalsIgnoreCase('does not contains in list')) {
			if (!string.isEmpty(value)) {
				List<string> tempList = value.split(',');
				for (string str : tempList) {
					if (fieldValue!=null && fieldValue.equalsIgnoreCase(str)) {
							return false;
					}
				}
			}
			return true;
		}
        return false;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Check value is match based on operator is provided, This method is use only for integer value.
      Dependencies: Called from "findRule" method.
     */
    @testVisible
    private static boolean isMatch(string operator, string value, integer fieldValue) {
        if (operator.equalsIgnoreCase('equals')) {
            return fieldValue == integer.valueOf(value);
        }
        else if (operator.equalsIgnoreCase('not equal to')) {
            return fieldValue != integer.valueOf(value);
        }
        else if (operator.equalsIgnoreCase('less than')) {
            return fieldValue < integer.valueOf(value);
        }
        else if (operator.equalsIgnoreCase('greater than')) {
            return fieldValue > integer.valueOf(value);
        }
        else if (operator.equalsIgnoreCase('less or equal')) {
            return fieldValue <= integer.valueOf(value);
        }
        else if (operator.equalsIgnoreCase('greater or equal')) {
            return fieldValue >= integer.valueOf(value);
        }
        return false;
    }
    /*
      Authors: Chandresh Koyani
      Purpose: Check value is match based on operator is provided, This method is use only for boolean value.
      Dependencies: Called from "findRule" method.
     */
    @testVisible
    private static boolean isMatch(string operator, string value, boolean fieldValue) {
        if (operator.equalsIgnoreCase('equals')) {
            return fieldValue == boolean.valueOf(value);
        }
        else if (operator.equalsIgnoreCase('not equal to')) {
            return fieldValue != boolean.valueOf(value);
        }
        return false;
    }

}