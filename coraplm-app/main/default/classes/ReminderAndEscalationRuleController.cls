/*
  Authors: Chandresh Koyani
  Date created: 20/11/2017
  Purpose: This calss contains static method for Reminder and Escalation page.
  Dependencies: ReminderAndEscalationRules.page
  -------------------------------------------------
  Modifications:
  Date: 
  Purpose of modification: 
  Method/Code segment modified: 
 
*/
public class ReminderAndEscalationRuleController { 
	
    /*
        Wrapper calass for Rule Response.
    */
    public class RuleResponse {
        
        public List<ReminderAndEscalationHelper.FieldInfo> caseTrackerFields { get; set; }
        public List<EmailTemplate> emailTemplates { get; set; }
        public List<OrgWideEmailAddress> orgWideAddresses { get; set; }
        public List<EmailTemplate> escalationEmailTemplates { get; set; }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Get Case Tracker Fields to create criteria.
      Dependencies: Called from "ReminderAndEscalationRules.Page"
     */
    @RemoteAction
    public static List<ReminderAndEscalationHelper.FieldInfo> getCaseTrackerFields() {
        List<ReminderAndEscalationHelper.FieldInfo> fieldNames = new List<ReminderAndEscalationHelper.FieldInfo> ();

        Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(ObjectUtil.getPackagedFieldName('CaseManager__c')).getDescribe().fields.getMap();
        for (Schema.SObjectField sfield : fieldMap.Values())
        {
            schema.describefieldresult dfield = sfield.getDescribe();

            string type = String.valueOf(dfield.getType());
            if (dField.isCustom() && !dField.isCalculated() && type != 'REFERENCE') {

                ReminderAndEscalationHelper.FieldInfo fieldInfo = new ReminderAndEscalationHelper.FieldInfo();
                fieldInfo.label = dfield.getLabel();
                fieldInfo.apiName = dfield.getName();
                fieldInfo.pickListValue = new list<ReminderAndEscalationHelper.Option> ();
                fieldInfo.type = type;
                fieldInfo.operator=EmailToCaseSettingHelper.OperatorMap.get(fieldInfo.type);

                if (fieldInfo.type == 'PICKLIST') {
                    List<Schema.PicklistEntry> picklist = dfield.getPicklistValues();
                    for (Schema.PicklistEntry pickListVal : picklist) {
                        fieldInfo.pickListValue.add(new ReminderAndEscalationHelper.Option(pickListVal.getValue(), pickListVal.getLabel()));
                    }
                }
                else if (fieldInfo.type == 'BOOLEAN') {
                        fieldInfo.pickListValue.add(new ReminderAndEscalationHelper.Option('True', 'True'));
                        fieldInfo.pickListValue.add(new ReminderAndEscalationHelper.Option('False', 'False'));
                }
                fieldNames.add(fieldInfo);
            }
        }
        return fieldNames;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Get Data to initialize Rule Page .
      Dependencies: Called from "ReminderAndEscalationRules.Page"
     */
    @RemoteAction
    public static RuleResponse getInitData() {
        RuleResponse response = new RuleResponse();
        response.caseTrackerFields = GetCaseTrackerFields();
        response.emailTemplates = GetAllEmailTemplate(PLMConstants.REMINDER_TEMPLATE_FOLDER);
        response.orgWideAddresses = GetAllOrgWideEmailAddress();
        response.escalationEmailTemplates=GetAllEmailTemplate(PLMConstants.ESCALATION_TEMPLATE_FOLDER);
        return response;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Geat all templates to send notfication.
      Dependencies: Use in "GetInitData" method.
     */
    
    public static List<EmailTemplate> getAllEmailTemplate(string folder) {
        List<EmailTemplate> emailTemplates = [Select Id, Name from EmailTemplate where Folder.Name=:folder];
        return emailTemplates;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Geat all OrgWide email addresses to send notfication.
      Dependencies: Use in "GetInitData" method.
     */
    @RemoteAction
    public static List<OrgWideEmailAddress> getAllOrgWideEmailAddress() {
        return[SELECT Address, Id FROM OrgWideEmailAddress ];
    }
    
    /*
      Authors: Chandresh Koyani
      Purpose: Save Reminder Rule entry if id is provided, it will create new rule if id is not provided.
      Dependencies: Called from "ReminderAndEscalationRules.Page"
     */
    @RemoteAction
    public static string saveRule(ReminderAndEscalationHelper.ReminderRule remRuleObj) {
        try {
            Reminder_Rule_Config__c remConfig = new Reminder_Rule_Config__c();
            remConfig.Rule_Name__c = remRuleObj.ruleName;
            remConfig.JSON__c = JSON.serialize(remRuleObj.JSONValue);
            remConfig.IsActive__c = remRuleObj.isActive;
            if (!string.isEmpty(remRuleObj.id)) {
                remConfig.id = remRuleObj.id;
            }
            else {
                AggregateResult[] groupedResults = [select MAX(Order__c) from Reminder_Rule_Config__c];
                integer nextOrder = 1;
                if (groupedResults != null && groupedResults.size() > 0) {
                    integer maxOrder = integer.valueOf(groupedResults[0].get('expr0'));
                    if (maxOrder != null) {
                        nextOrder = maxOrder + 1;
                    }
                }
                remConfig.Order__c = nextOrder;
            }

            upsert remConfig;
            return 'SUCCESS';
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('ReminderAndEscalationRuleController', ex.getMessage(), ex.getStackTraceString());
            return '';
        }

    }

    /*
      Authors: Chandresh Koyani
      Purpose: Get all Reminder And Escalation Rule.
      Dependencies: Called from "ReminderAndEscalationRules.Page"
     */
    @RemoteAction
    public static List<ReminderAndEscalationHelper.ReminderRule> getAllRules() {
        try {
            List<ReminderAndEscalationHelper.ReminderRule> qcRules = new List<ReminderAndEscalationHelper.ReminderRule> ();

            List<Reminder_Rule_Config__c> tatConfiges = [select id, Rule_Name__c, JSON__c, Order__c, IsActive__c from Reminder_Rule_Config__c order by Order__c limit 999];

            for (Reminder_Rule_Config__c tatConfigObj : tatConfiges) {
                ReminderAndEscalationHelper.JSONValue jsonField = (ReminderAndEscalationHelper.JSONValue) System.JSON.deserialize(tatConfigObj.JSON__c, ReminderAndEscalationHelper.JSONValue.class);
                ReminderAndEscalationHelper.ReminderRule remRuleObj = new ReminderAndEscalationHelper.ReminderRule();
                remRuleObj.JSONValue = jsonField;
                remRuleObj.ruleName = tatConfigObj.Rule_Name__c;
                remRuleObj.order = Integer.valueOf(tatConfigObj.Order__c);
                remRuleObj.id = tatConfigObj.id;
                
                remRuleObj.isActive = tatConfigObj.IsActive__c;
                qcRules.add(remRuleObj);
            }
            return qcRules;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('ReminderAndEscalationRuleController', ex.getMessage(), ex.getStackTraceString());
            return null;
        }

    }

    /*
      Authors: Chandresh Koyani
      Purpose: Remove Reminder And Escalation Rule Config entry based on id provided..
      Dependencies: Called from "ReminderAndEscalationRules.Page"
     */
    @RemoteAction
    public static void removeRule(string id) {
        try {
            List<Reminder_Rule_Config__c> qcRule = [select id from Reminder_Rule_Config__c where id = :id];
            delete qcRule;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('ReminderAndEscalationRuleController', ex.getMessage(), ex.getStackTraceString());
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Change state of a rule(Active/Inactive).
      Dependencies: Called from "ReminderAndEscalationRules.Page"
     */
    @RemoteAction
    public static void changeState(string id, boolean isActive) {
        try {
            Reminder_Rule_Config__c rule = new Reminder_Rule_Config__c();
            rule.id = id;
            rule.IsActive__c = isActive;
            update rule;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('ReminderAndEscalationRuleController', ex.getMessage(), ex.getStackTraceString());
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Change order of a rule.
      Dependencies: Called from "ReminderAndEscalationRules.Page"
     */
    @RemoteAction
    public static void changeOrder(integer currentOrder, string orderType) {
        try {
            integer previousNextCount = 0;
            if (orderType == 'UP') {
                previousNextCount = currentOrder - 1;
            }
            else {
                previousNextCount = currentOrder + 1;
            }
            List<Reminder_Rule_Config__c> qcRules = [select id, Order__c from Reminder_Rule_Config__c where(Order__c = :currentOrder OR Order__c = :previousNextCount)];
            for (Reminder_Rule_Config__c rule : qcRules) {
                if (rule.Order__c == currentOrder) {
                    rule.Order__c = previousNextCount;
                }
                else {
                    rule.Order__c = currentOrder;
                }
            }
            update qcRules;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('ReminderAndEscalationRuleController', ex.getMessage(), ex.getStackTraceString());
        }

    }

    /*
      Authors: Chandresh Koyani
      Purpose: Save Escalation Matrix.
      Dependencies: Called from "ReminderAndEscalationRules.Page"
     */
    @RemoteAction
    public static string saveEscalationMatrix(ReminderAndEscalationHelper.EscalationMatrix matrixObj) {
        try {
            Escalation_Matrix__c escMatrix = new Escalation_Matrix__c();
            escMatrix.For__c = matrixObj.matrixFor;
            escMatrix.JSON__c = JSON.serialize(matrixObj.matrixJSON);
            escMatrix.IsActive__c = matrixObj.isActive;
            if (!string.isEmpty(matrixObj.id)) {
                escMatrix.id = matrixObj.id;
            }

            upsert escMatrix;
            return 'SUCCESS';
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('ReminderAndEscalationRuleController', ex.getMessage(), ex.getStackTraceString());
            return '';
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Get All Escalation matrix.
      Dependencies: Called from "ReminderAndEscalationRules.Page"
     */
    @RemoteAction
    public static List<ReminderAndEscalationHelper.EscalationMatrix> getAllEscalationMatrix() {
        try {
            List<ReminderAndEscalationHelper.EscalationMatrix> escalationMatricesList = new List<ReminderAndEscalationHelper.EscalationMatrix> ();

            List<Escalation_Matrix__c> escalationMatrices= [select id, For__c, JSON__c, IsActive__c from Escalation_Matrix__c order by createddate];

            for (Escalation_Matrix__c  escalationMatrixObj : escalationMatrices) {
                ReminderAndEscalationHelper.MatrixJSON jsonField = (ReminderAndEscalationHelper.MatrixJSON) System.JSON.deserialize(escalationMatrixObj.JSON__c, ReminderAndEscalationHelper.MatrixJSON.class);
                ReminderAndEscalationHelper.EscalationMatrix escRuleObj = new ReminderAndEscalationHelper.EscalationMatrix();
                escRuleObj.matrixJSON = jsonField;
                escRuleObj.matrixFor = escalationMatrixObj.For__c;
                escRuleObj.id = escalationMatrixObj.id;
                escRuleObj.isActive = escalationMatrixObj.IsActive__c;
                escalationMatricesList.add(escRuleObj);
            }
            return escalationMatricesList;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('ReminderAndEscalationRuleController', ex.getMessage(), ex.getStackTraceString());
            return null;
        }

    }
    /*
      Authors: Chandresh Koyani
      Purpose: Change State of Escalation Matrix.
      Dependencies: Called from "ReminderAndEscalationRules.Page"
     */
    @RemoteAction
    public static void changeStateMatrix(string id, boolean isActive) {
        try {
            Escalation_Matrix__c rule = new Escalation_Matrix__c();
            rule.id = id;
            rule.IsActive__c = isActive;
            update rule;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('ReminderAndEscalationRuleController', ex.getMessage(), ex.getStackTraceString());
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Remove Escalation Matrix object based on ID.
      Dependencies: Called from "ReminderAndEscalationRules.Page"
     */
    @RemoteAction
    public static void removeMatrix(string id) {
        try {
            List<Escalation_Matrix__c> qcRule = [select id from Escalation_Matrix__c where id = :id];
            delete qcRule;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('ReminderAndEscalationRuleController', ex.getMessage(), ex.getStackTraceString());
        }
    }
}