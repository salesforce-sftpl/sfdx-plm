/**
  * Authors     : Chandresh Koyani
  * Date created : 04/12/2017 
  * Purpose      : Test Class for ReminderAndEscalationRuleControllerTest 
  * Dependencies : ReminderAndEscalationHelper.cls,ReminderAndEscalationRuleController.cls
  * -------------------------------------------------
  *Modifications:
                Date:   
                Purpose of modification:  
                Method/Code segment modified: 

*/
@isTest
public class ReminderAndEscalationTest { 
    @testSetup 
    static void setupTestData() {
        
        CreateReminderRule();
        CreateReminderRuleNew();
        CreateEscalationMatrixRule();

        CaseManager__c caseObj=new CaseManager__c();
        caseObj.Process__c='AP';
        caseObj.WorkType__c='PO Invoices';
        caseObj.Document_Type__c='Electricity';
        caseObj.Requestor_Email__c='chandresh.koyani@test123.com';
        caseObj.Amount__c=2000;
        caseObj.Status__c='Ready for processing';
        caseObj.escalation__c=true;
        caseObj.To_Addresses__c='chandresh.koyani@sftpl.com';
        caseObj.CC_Addresses__c='chandresh.koyani@sftpl.com';
        caseObj.BCC_Addresses__c='chandresh.koyani@sftpl.com';
        //caseObj.Esc_Addresses__c='chandresh.koyani@sftpl.com';
        insert caseObj;

        CaseManager__c caseObjNew=new CaseManager__c();
        caseObjNew.Process__c='AP';
        caseObjNew.WorkType__c='PO Invoices';
        caseObjNew.Document_Type__c='Electricity';
        caseObjNew.Requestor_Email__c='chandresh.koyani@test123.com';
        caseObjNew.Amount__c=2000;
        caseObjNew.Status__c='Ready for processing';
        caseObjNew.To_Addresses__c='chandresh.koyani1@sftpl.com';
        caseObjNew.CC_Addresses__c='chandresh.koyani@sftpl.com';
        caseObjNew.BCC_Addresses__c='chandresh.koyani@sftpl.com';
        caseObjNew.escalation__c=true;
        //caseObjNew.Esc_Addresses__c='chandresh.koyani@sftpl.com';
        insert caseObjNew;
    }

    public static void CreateReminderRule(){
        ReminderAndEscalationHelper.ReminderRule reminderRuleObj=new ReminderAndEscalationHelper.ReminderRule();
        reminderRuleObj.JSONValue=new ReminderAndEscalationHelper.JSONValue();

        reminderRuleObj.JSONValue.criterias=new List<ReminderAndEscalationHelper.Criteria>();
        reminderRuleObj.JSONValue.notifications=new List<ReminderAndEscalationHelper.Notification>();
        reminderRuleObj.JSONValue.escalations=new List<ReminderAndEscalationHelper.Escalation>();
        reminderRuleObj.JSONValue.isEscalationRequired=true;
        reminderRuleObj.JSONValue.isEnableEscalationMatrix=true;
        reminderRuleObj.JSONValue.escalationMatrixTemplate='';

        ReminderAndEscalationHelper.Criteria cre=null;
        cre=new ReminderAndEscalationHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Process__c');
        cre.Operator='contains';
        cre.Value='AP';
        cre.Type='PICKLIST';
        
        reminderRuleObj.JSONValue.Criterias.add(cre);
        

        cre=new ReminderAndEscalationHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Worktype__c');
        cre.Operator='contains';
        cre.Value='PO Invoices';
        cre.Type='PICKLIST';
        
        reminderRuleObj.JSONValue.Criterias.add(cre);
        /*
        cre=new ReminderAndEscalationHelper.Criteria();
        cre.Field='Amount__c';
        cre.Operator='greater or equal';
        cre.Value='10000';
        cre.Type='NUMBER';
        
        reminderRuleObj.JSONValue.Criterias.add(cre);
        
        cre=new ReminderAndEscalationHelper.Criteria();
        cre.Field='escalation__c';
        cre.Operator='equals';
        cre.Value='True';
        cre.Type='BOOLEAN';
        
        reminderRuleObj.JSONValue.Criterias.add(cre);*/
        
        ReminderAndEscalationHelper.Notification notification=null;
        notification=new ReminderAndEscalationHelper.Notification();
        notification.after='30';
        notification.additionalRecipient='chandresh.koyani@sftpl.com';
        notification.afterUnit='Minutes';
        notification.includeIn='To';
        notification.toAddressField='To_Addresses__c';
        notification.ccAddressField='CC_Addresses__c';
        notification.bccAddressField='BCC_Addresses__c';
        reminderRuleObj.JSONValue.notifications.add(notification);

        

        ReminderAndEscalationHelper.Escalation esclation=null;
        esclation=new ReminderAndEscalationHelper.Escalation();
        esclation.after='30';
        esclation.additionalRecipient='chandresh.koyani@sftpl.com';
        esclation.afterUnit='Minutes';
        esclation.includeIn='To';
        esclation.toAddressField='To_Addresses__c';
        esclation.ccAddressField='CC_Addresses__c';
        esclation.bccAddressField='BCC_Addresses__c';
        reminderRuleObj.JSONValue.escalations.add(esclation);

        Reminder_Rule_Config__c ruleConfig = new Reminder_Rule_Config__c();
        ruleConfig.Rule_Name__c = 'Test';
        ruleConfig.JSON__c = JSON.serialize(reminderRuleObj.JSONValue);
        ruleConfig.IsActive__c=true;
        ruleConfig.Order__c = 1;
        
        insert ruleConfig;
    }
    public static void CreateReminderRuleNew(){
        ReminderAndEscalationHelper.ReminderRule reminderRuleObj=new ReminderAndEscalationHelper.ReminderRule();
        reminderRuleObj.JSONValue=new ReminderAndEscalationHelper.JSONValue();

        reminderRuleObj.JSONValue.criterias=new List<ReminderAndEscalationHelper.Criteria>();
        reminderRuleObj.JSONValue.notifications=new List<ReminderAndEscalationHelper.Notification>();
        reminderRuleObj.JSONValue.escalations=new List<ReminderAndEscalationHelper.Escalation>();
        reminderRuleObj.JSONValue.isEscalationRequired=true;
        reminderRuleObj.JSONValue.isEnableEscalationMatrix=true;
        reminderRuleObj.JSONValue.escalationMatrixTemplate='';

        ReminderAndEscalationHelper.Criteria cre=null;
        cre=new ReminderAndEscalationHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Process__c');
        cre.Operator='contains';
        cre.Value='AP';
        cre.Type='PICKLIST';
        
        reminderRuleObj.JSONValue.Criterias.add(cre);
        

        cre=new ReminderAndEscalationHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Worktype__c');
        cre.Operator='contains';
        cre.Value='PO Invoices';
        cre.Type='PICKLIST';
        
        reminderRuleObj.JSONValue.Criterias.add(cre);
        
        cre=new ReminderAndEscalationHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Amount__c');
        cre.Operator='greater or equal';
        cre.Value='10000';
        cre.Type='NUMBER';
        
        reminderRuleObj.JSONValue.Criterias.add(cre);
        
        cre=new ReminderAndEscalationHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('escalation__c');
        cre.Operator='equals';
        cre.Value='True';
        cre.Type='BOOLEAN';
        
        reminderRuleObj.JSONValue.Criterias.add(cre);
        
        ReminderAndEscalationHelper.Notification notification=null;
        notification=new ReminderAndEscalationHelper.Notification();
        notification.after='30';
        notification.additionalRecipient='chandresh.koyani@sftpl.com';
        notification.afterUnit='Minutes';
        notification.includeIn='To';
		notification.toAddressField='To_Addresses__c';
        notification.ccAddressField='CC_Addresses__c';
        notification.bccAddressField='BCC_Addresses__c';
        reminderRuleObj.JSONValue.notifications.add(notification);

        

        ReminderAndEscalationHelper.Escalation esclation=null;
        esclation=new ReminderAndEscalationHelper.Escalation();
        esclation.after='30';
        esclation.additionalRecipient='chandresh.koyani@sftpl.com';
        esclation.afterUnit='Minutes';
        esclation.includeIn='To';
		esclation.toAddressField='To_Addresses__c';
        esclation.ccAddressField='CC_Addresses__c';
        esclation.bccAddressField='BCC_Addresses__c';
      
        reminderRuleObj.JSONValue.escalations.add(esclation);

        Reminder_Rule_Config__c ruleConfig = new Reminder_Rule_Config__c();
        ruleConfig.Rule_Name__c = 'Test1';
        ruleConfig.JSON__c = JSON.serialize(reminderRuleObj.JSONValue);
        ruleConfig.IsActive__c=true;
        ruleConfig.Order__c = 2;
        
        insert ruleConfig;
    }
    public static void CreateEscalationMatrixRule(){
        ReminderAndEscalationHelper.EscalationMatrix esclationMatrix=new ReminderAndEscalationHelper.EscalationMatrix();
        esclationMatrix.matrixJSON=new ReminderAndEscalationHelper.MatrixJSON();
        esclationMatrix.matrixJSON.matrixEscalations=new List<ReminderAndEscalationHelper.MatrixEscalation>();
        
        ReminderAndEscalationHelper.MatrixEscalation matrix=new ReminderAndEscalationHelper.MatrixEscalation();
        matrix.after='30';
        matrix.afterUnit='Minutes';
        matrix.toAddresses='chandresh.koyani@sftpl.com';
        matrix.ccAddresses='chandresh.koyani@sftpl.com';
        matrix.bccAddresses='chandresh.koyani@sftpl.com';
        esclationMatrix.matrixJSON.matrixEscalations.add(matrix);

        Escalation_Matrix__c ruleConfig = new Escalation_Matrix__c();
        ruleConfig.For__c = 'chandresh.koyani@sftpl.com';
        ruleConfig.JSON__c = JSON.serialize(esclationMatrix.matrixJSON);
        ruleConfig.IsActive__c=true;
        insert ruleConfig;


    }
    public static testMethod void testReminderRuleController(){
        ReminderAndEscalationRuleController ruleController=new ReminderAndEscalationRuleController();
        ReminderAndEscalationRuleController.getAllRules();
        ReminderAndEscalationRuleController.getInitData();
        List<ReminderAndEscalationHelper.FieldInfo> fieldList= ReminderAndEscalationRuleController.GetCaseTrackerFields();

        Reminder_Rule_Config__c reminderRule = [select id,Name,JSON__c from Reminder_Rule_Config__c limit 1];
        
        ReminderAndEscalationRuleController.ChangeState(reminderRule.id,true);
        ReminderAndEscalationRuleController.ChangeOrder(1,'UP');
        ReminderAndEscalationRuleController.ChangeOrder(4,'Down');
        
        ReminderAndEscalationRuleController.ChangeOrder(3,'UP');
        

        ReminderAndEscalationHelper.ReminderRule reminderRuleObj=new ReminderAndEscalationHelper.ReminderRule();
        reminderRuleObj.Id=reminderRule.Id;
        reminderRuleObj.RuleName='Test1';
        reminderRuleObj.Order=1;
        reminderRuleObj.IsActive=true;
        reminderRuleObj.JSONValue=(ReminderAndEscalationHelper.JSONValue) System.JSON.deserialize(reminderRule.JSON__c, ReminderAndEscalationHelper.JSONValue.class);

        ReminderAndEscalationRuleController.SaveRule(reminderRuleObj);

        reminderRuleObj.Id='';

        ReminderAndEscalationRuleController.SaveRule(reminderRuleObj);

        ReminderAndEscalationRuleController.RemoveRule(reminderRuleObj.id);

        
        Escalation_Matrix__c escalationMatrix = [select id,Name,JSON__c from Escalation_Matrix__c limit 1];

        ReminderAndEscalationRuleController.getAllEscalationMatrix();
        ReminderAndEscalationRuleController.changeStateMatrix(escalationMatrix.id,true);


        ReminderAndEscalationHelper.EscalationMatrix matrixObj=new ReminderAndEscalationHelper.EscalationMatrix();
        matrixObj.Id=reminderRule.Id;
        matrixObj.matrixFor='chandresh.koyani@sftpl.com';
        matrixObj.IsActive=true;
        matrixObj.MatrixJSON=(ReminderAndEscalationHelper.MatrixJSON) System.JSON.deserialize(reminderRule.JSON__c, ReminderAndEscalationHelper.MatrixJSON.class);

        ReminderAndEscalationRuleController.saveEscalationMatrix(matrixObj);

        ReminderAndEscalationRuleController.removeMatrix(escalationMatrix.id);

        
    }
    public static testmethod void testReminderHelper(){
        List<CaseManager__c> caseObjList=[select id,Name,Process__c,Status__c,WorkType__c,Amount__c,escalation__c,To_Addresses__c,CC_Addresses__c,BCC_Addresses__c from CaseManager__c];
        ReminderAndEscalationHelper.setReminderAndEscalation(caseObjList);

        Map<Id,CaseManager__c> caseTrackerMap=new Map<Id,CaseManager__c>();

        for(CaseManager__c caseObj : caseObjList){
            
            CaseManager__c caseObjNew=new CaseManager__c();
            caseObjNew.Process__c='AP';
            caseObjNew.WorkType__c='PO Invoices';
            caseObjNew.Document_Type__c='Electricity';
            caseObjNew.Requestor_Email__c='chandresh.koyani@test123.com';
            caseObjNew.Amount__c=2000;
            caseObjNew.Status__c='Ready for QC';
            caseObjNew.escalation__c=true;
            caseObjNew.Id=caseObj.id;
            
            caseTrackerMap.put(caseObj.id,caseObjNew);
        }
        ReminderAndEscalationHelper.setReminderAndEscalation(caseObjList,caseTrackerMap);

        ReminderAndEscalationHelper.removeReminderObject(caseObjList,caseTrackerMap);

        List<Reminder_Rule_Config__c> remRuleConfiges = [select id, Name, JSON__c, Order__c, IsActive__c from Reminder_Rule_Config__c where IsActive__c = true and Order__c=2];

        ReminderAndEscalationHelper.findRule(caseObjList[0],remRuleConfiges);
        
    }
    public static testMethod void testIsMatchMethod(){
        
        ReminderAndEscalationHelper.Criteria cre=new ReminderAndEscalationHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Status__c');
        cre.Operator='equals';
        cre.Value='True';
        cre.Type='BOOLEAN';

        //Boolean check
        
        ReminderAndEscalationHelper.IsMatch('equals','True',true);
        ReminderAndEscalationHelper.IsMatch('not equal to','True',true);


        ReminderAndEscalationHelper.IsMatch('not equal to','5',1);
        ReminderAndEscalationHelper.IsMatch('equals','5',1);
        ReminderAndEscalationHelper.IsMatch('less than','5',2);
        ReminderAndEscalationHelper.IsMatch('greater than','5',2);
        ReminderAndEscalationHelper.IsMatch('less or equal','5',2);
        ReminderAndEscalationHelper.IsMatch('greater or equal','5',2);

        ReminderAndEscalationHelper.addUnit(Datetime.now(),'Minutes',1);
        ReminderAndEscalationHelper.addUnit(Datetime.now(),'Hours',1);
        ReminderAndEscalationHelper.addUnit(Datetime.now(),'Days',1);

        ReminderAndEscalationHelper.IsMatch('not equal to','Test','Test','text');
        ReminderAndEscalationHelper.IsMatch('equals','Test','Test','text');
        ReminderAndEscalationHelper.IsMatch('end with','Test','Test','text');
        ReminderAndEscalationHelper.IsMatch('contains','Test','Test','text');
        ReminderAndEscalationHelper.IsMatch('does not contain','Test','Test','text');
        ReminderAndEscalationHelper.IsMatch('contains in list','Test,ABC','text','text');
        ReminderAndEscalationHelper.IsMatch('contains in list','Test,ABC','text','PICKLIST');

    }
}