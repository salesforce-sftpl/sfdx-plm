global class RequestDataFromAWS implements RequestAWSDataInterface{
    @future (callout = true)
    global static void sendRequest(String CaseId) {
        EmailMessage caseEmailData = [Select Id,ToAddress,FromAddress,HtmlBody,TextBody,Incoming,Status,Subject,CreatedDate from EmailMessage where RelatedToId = :CaseId Order By CreatedDate ASC LIMIT 1];
        MLPrediction__mdt index = [SELECT EndPointURL__c,AuthorizationToken__c,Method__c,Active__c,CaseFields__c FROM MLPrediction__mdt where DeveloperName = 'RequestCaseDetails'];     
        String caseFields = index.CaseFields__c;
        List<String> caseFieldsList = caseFields.split(',');
        //System.debug('index : ' +index);
        if(index != null && index.Active__c == true){
            CaseManager__c caseData = Database.query('Select '+caseFields+' from CaseManager__c where id = \''+CaseId+'\'');
          if(caseEmailData != null){
        String objType='CaseManager__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType CaseSchema = schemaMap.get(objType);
        Map<String, Schema.SObjectField> fieldMap = CaseSchema.getDescribe().fields.getMap();           
        System.debug('schemaMap : ' +schemaMap);
        System.debug('fieldMap : ' +fieldMap);
            List<String> toAddressList = new List<String>();
            toAddressList.add(caseEmailData.ToAddress);
           //Creating RequestJson through JsonGenerator
            JSONGenerator gen = JSON.createGenerator(true); 
            gen.writeStartObject(); 
            // Adding Email Fields to RequestJson
            gen.writeStringField('Id', caseEmailData.Id);
            gen.writeObjectField('ToAddress', toAddressList);
            gen.writeStringField('FromAddress', caseEmailData.FromAddress);
            gen.writeStringField('HtmlBody', caseEmailData.HtmlBody);
            gen.writeStringField('TextBody', caseEmailData.TextBody);
            gen.writeBooleanField('Incoming', caseEmailData.Incoming);
            gen.writeStringField('CaseTracker', CaseId);
            gen.writeStringField('Status', caseEmailData.Status);
            gen.writeStringField('Subject', caseEmailData.Subject);
            gen.writeDateTimeField('CreatedDate', caseEmailData.CreatedDate);
            // Adding CaseFields to RequestJson
            for (Integer i=0;i<caseFieldsList.size();i++){
                // System.debug('Field Name : ' + caseFieldsList[i]);
                Schema.DisplayType fielddataType = fieldMap.get(caseFieldsList[i]).getDescribe().getType();
                //System.debug('Field Type : ' + fielddataType);
                if(fielddataType == Schema.DisplayType.Integer || fielddataType == Schema.DisplayType.Double) {
                        gen.writeNumberField(String.ValueOf(caseFieldsList[i]), caseData.get(caseFieldsList[i]) == null?0:Integer.valueOf(caseData.get(caseFieldsList[i])));
                }
                else {
                        gen.writeStringField(String.ValueOf(caseFieldsList[i]), caseData.get(caseFieldsList[i]) == null?'':String.ValueOf(caseData.get(caseFieldsList[i])));
                }
                
            }
            gen.writeEndObject();  
            String jsonS = gen.getAsString();
          //  System.debug('jsonMaterials'+jsonS);
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            req.setEndpoint(String.ValueOf(index.EndPointURL__c));
            req.setMethod(String.valueOf(index.Method__c));
            req.setHeader('Authorization', String.ValueOf(index.AuthorizationToken__c));
            req.setHeader('Content-Type', 'application/json');
            req.setBody(jsonS);
             // System.debug('req : ' +req);
            try {
                res = http.send(req);
                if (res.getStatusCode() == 200) {
                    System.debug('Success!');
                    //parsing ResponseJson
                    Map<String,Object> jsonMap = (Map<String,Object>)JSON.deserializeUntyped(res.getBody());
                    String JsonMapSLA = String.ValueOf(jsonMap.get('SlaHours'));
                    //Fetching SLAHours from ResponseJson to Predicting Case Closure Date
                    Decimal SLAHours = Decimal.valueOf(JsonMapSLA);
                    System.debug('SLAHours ' +SLAHours);
                    SLAHours = SLAHours.setScale(2);
                    System.debug('SLAHours rounded ' +SLAHours);
                    Integer SLAInMs = Integer.ValueOf(SLAHours*3600000);
                    System.Debug(SLAInMs);
                    //Updating Case with the latest ResponseJson
                    List<BusinessHours> bhs = [SELECT Id FROM BusinessHours where IsDefault=true];
                    CaseManager__c caseUpdate = [Select id,CreatedDate,AWSResponseJson__c,PredictedClosureDate__c from CaseManager__c where Id = :CaseId];
                    caseUpdate.AWSResponseJson__c = res.getBody();
                    caseUpdate.PredictedClosureDate__c = BusinessHours.add(bhs[0].id, caseUpdate.CreatedDate, SLAInMs);
                    update caseUpdate;
                } else {
                    System.debug('HTTP error: ' + res.getStatusCode());
                }
                System.debug(res.getBody());
            } 
            catch(System.CalloutException e) {
                System.debug('Callout error: '+ e);
            }
          }
         }else{System.debug('ML prediction module not Active');}
     } 
}