@isTest
public class RequestDataFromAWSMock implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"Status": "3", "SlaHours": "104.0848892246141"}');
        res.setStatusCode(200);
        return res;
    }
}