@isTest(seeAllData=false)
public class RequestDataFromAWSTest{
    public static testMethod void unitTest01(){
        CaseManager__c testCase = TestDataGenerator.createCaseTracker();
        EmailMessage emailMsg=new EmailMessage();
        emailMsg.FromName='Mr. John Green';
        emailMsg.ToAddress='ToAbc@Domain.com';
        emailMsg.CcAddress='ToAbc@Domain.com';
        emailMsg.BccAddress='ToAbc@Domain.com';
        emailMsg.FromAddress='FromAbc@Domain.com';
        emailMsg.HtmlBody='test';
        emailMsg.TextBody='test';
        emailMsg.Incoming=true;
        emailMsg.Status='1';
        emailMsg.Subject='Subject';
        emailMsg.RelatedToId=testCase.Id;
        insert emailMsg;
        
        Test.setMock(HttpCalloutMock.class, new RequestDataFromAWSMock());
        
        RequestDataFromAWS.sendRequest(testCase.Id);
    }
}