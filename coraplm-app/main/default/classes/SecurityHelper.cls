public with sharing class SecurityHelper {  
    public static void CheckObjectAccess(string objectname, Set<string> accessList) {
        List<string> fieldNames = new List<string> ();
        DescribeSObjectResult drResult = Schema.getGlobalDescribe().get(objectname).getDescribe();
        if (accessList.contains('Read') && !drResult.isAccessible()) {
            fieldNames.add('Read');
        }
        if (accessList.contains('Edit') && !drResult.isUpdateable()) {
            fieldNames.add('Edit');
        }
        if (accessList.contains('Create') && !drResult.isCreateable()) {
            fieldNames.add('Create');
        }
        if (accessList.contains('Delete') && !drResult.isDeletable()) {
            fieldNames.add('Delete');
        }
        if (!fieldNames.isEmpty()) {
            throw new SecurityException('Insufficient access of ' + string.join(fieldNames, ', ') + ' to ' + objectname);
        }
    }
	
	public static void CheckFieldReadAccess(List<Schema.FieldSetMember> fieldSetMembers, string objectname) {
        List<string> fieldNames = new List<string> ();
        Map<String, Schema.SObjectField> fieldsMap = Schema.getGlobalDescribe().get(objectname).getDescribe().fields.getMap();
        for (Schema.FieldSetMember fieldMember : fieldSetMembers) {
            if (!fieldsMap.get(fieldMember.getFieldPath()).getDescribe().isAccessible()) {
                fieldNames.add(fieldMember.getFieldPath());
            }
        }
        if (!fieldNames.isEmpty()) {
            throw new SecurityException('Insufficient access to ' + string.join(fieldNames, ', '));
        }
    }
    public static void CheckFieldEditAccess(List<Schema.FieldSetMember> fieldSetMembers, string objectname) {
        List<string> fieldNames = new List<string> ();
        Map<String, Schema.SObjectField> fieldsMap = Schema.getGlobalDescribe().get(objectname).getDescribe().fields.getMap();
        for (Schema.FieldSetMember fieldMember : fieldSetMembers) {
            if (!fieldsMap.get(fieldMember.getFieldPath()).getDescribe().isUpdateable()) {
                fieldNames.add(fieldMember.getFieldPath());
            }
        }
        if (!fieldNames.isEmpty()) {
            throw new SecurityException('Insufficient access to ' + string.join(fieldNames, ', '));
        }
    }
    
    public static void CheckFieldReadAccess(List<string> fieldList, string objectname) {
        List<string> fieldNames = new List<string> ();
        Map<String, Schema.SObjectField> fieldsMap = Schema.getGlobalDescribe().get(objectname).getDescribe().fields.getMap();
        for (string fieldMember : fieldList)
        {
                if (fieldsMap.get(fieldMember)!=null && !fieldsMap.get(fieldMember).getDescribe().isAccessible())
                {
                    fieldNames.add(fieldMember);
                }

        }

        if (!fieldNames.isEmpty()) {
            throw new SecurityException('Insufficient access to ' + string.join(fieldNames, ', '));
        }
    }
    public static void CheckFieldEditAccess(List<string> fieldList, string objectname) {
        List<string> fieldNames = new List<string> ();
        Map<String, Schema.SObjectField> fieldsMap = Schema.getGlobalDescribe().get(objectname).getDescribe().fields.getMap();


        for (string fieldMember : fieldList) {
            if (!fieldsMap.get(fieldMember).getDescribe().isUpdateable()) {
                fieldNames.add(fieldMember);
            }
        }
        if (!fieldNames.isEmpty()) {
            throw new SecurityException('Insufficient access to ' + string.join(fieldNames, ', '));
        }
    }
	
    public static void CheckAllFieldCreateAccess(string objectname) {
        SObjectType schemaType = null;
        schemaType = Schema.getGlobalDescribe().get(objectname);
        if (!schemaType.getDescribe().isCreateable()) {
            throw new SecurityException('Insufficient access of Create to ' + objectname);
        }
        Map<String, SObjectField> fieldMap = schemaType.getDescribe().fields.getMap();
        List<string> fieldNames = new List<string> ();
        for (String field : fieldMap.keyset()) {
            if (fieldMap.get(field).getDescribe().isCustom() && !fieldMap.get(field).getDescribe().isCalculated()) {
                if (!fieldMap.get(field).getDescribe().isCreateable()) {
                    fieldNames.add(field);
                }
            }
        }
        if (!fieldNames.isEmpty()) {
            throw new SecurityException('Insufficient access of Create to ' + string.join(fieldNames, ', '));
        }
    }
    public static void CheckAllFieldEditAccess(string objectname) {
        SObjectType schemaType = null;
        schemaType = Schema.getGlobalDescribe().get(objectname);
        if (!schemaType.getDescribe().isUpdateable()) {
            throw new SecurityException('Insufficient access of Edit to ' + objectname);
        }
        Map<String, SObjectField> fieldMap = schemaType.getDescribe().fields.getMap();
        List<string> fieldNames = new List<string> ();
        for (String field : fieldMap.keyset()) {
            if (fieldMap.get(field).getDescribe().isCustom() && !fieldMap.get(field).getDescribe().isCalculated()) {
                if (!fieldMap.get(field).getDescribe().isUpdateable()) {
                    fieldNames.add(field);
                }
            }
        }
        if (!fieldNames.isEmpty()) {
            throw new SecurityException('Insufficient access of Edit to ' + string.join(fieldNames, ', '));
        }
    }
}