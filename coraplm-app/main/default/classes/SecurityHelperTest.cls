/**
* Authors    : Atul Hinge
* Date created : 16/11/2017 
* Purpose      : Test Class for EmailController
* Dependencies : -
* -------------------------------------------------
*Modifications:
Date:   
Purpose of modification:  
Method/Code segment modified: 

*/
@isTest
public class SecurityHelperTest {
    public static testMethod void unitTest01(){
        CaseManager__c ct =TestDataGenerator.createCaseTracker();
        CaseSelector cs =new CaseSelector();
        cs.addRelationShip(new Set<String>{'Emails'});
        cs.selectCasesByIds(new Set<Id>{ct.Id});
        cs.selectCasesByClause(' (Id != null) ');
        cs.selectSharedCases('');
        cs.selectClosedCases('');
        cs.selectMyTaskCases('');
        cs.selectUnassignedCases('');
        CaseSelector cs1 =new CaseSelector(new Set<String>{''},new Set<String>{'Emails'});
        
        Test.startTest();
	        List<Schema.FieldSetMember> availablefields = Schema.SObjectType.CaseManager__c.fieldSets.DefaultFieldset.getFields();
	        SecurityHelper.CheckObjectAccess(ObjectUtil.getWithNameSpace('CaseManager__c'), new set<String>{'Edit','Create','Read','Delete'});
	        
	        SecurityHelper.CheckFieldReadAccess(availablefields, ObjectUtil.getWithNameSpace('CaseManager__c'));
	        SecurityHelper.CheckFieldEditAccess(availablefields, ObjectUtil.getWithNameSpace('CaseManager__c'));
	        
	        SecurityHelper.CheckFieldReadAccess(new List<String>{ObjectUtil.getWithNameSpace('UserAction__c')}, ObjectUtil.getWithNameSpace('CaseManager__c'));
	        SecurityHelper.CheckFieldEditAccess(new List<String>{ObjectUtil.getWithNameSpace('UserAction__c')}, ObjectUtil.getWithNameSpace('CaseManager__c'));
	                
	        SecurityHelper.CheckAllFieldCreateAccess(ObjectUtil.getWithNameSpace('CaseManager__c'));
	        SecurityHelper.CheckAllFieldEditAccess(ObjectUtil.getWithNameSpace('CaseManager__c'));
        Test.stopTest();
    }
    
    public static testMethod void unitTest02(){
        Profile p = [SELECT Id FROM Profile WHERE Name = 'Read Only'];
        
        User u = new User(Alias = 'admplm', Country='United Kingdom',Email='admplm@package.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='admplm@package.com');
        insert u;
        
        Test.startTest();
        	list<Schema.FieldSetMember> availablefields = Schema.SObjectType.CaseManager__c.fieldSets.DefaultFieldset.getFields();
        	
        	System.runAs(u){
        		try{
		        	SecurityHelper.CheckObjectAccess(ObjectUtil.getWithNameSpace('CaseManager__c'), new set<String>{'Edit','Create','Read','Delete'});
		        }catch(Exception ex){
		            System.debug(' Insufficient access');
		        }
		        
		        try{
		            SecurityHelper.CheckFieldReadAccess(availablefields, ObjectUtil.getWithNameSpace('CaseManager__c'));
		        }catch(Exception ex){
		            System.debug(' Insufficient access');
		        }
		        
		        try{
		            SecurityHelper.CheckFieldEditAccess(availablefields, ObjectUtil.getWithNameSpace('CaseManager__c'));
		        }catch(Exception ex){
		            System.debug(' Insufficient access');
		        }
		        
		        try{
		            SecurityHelper.CheckFieldReadAccess(new List<String>{ObjectUtil.getWithNameSpace('UserAction__c')}, ObjectUtil.getWithNameSpace('CaseManager__c'));
		        }catch(Exception ex){
		            System.debug(' Insufficient access');
		        }
		        
		        try{
		            SecurityHelper.CheckFieldEditAccess(new List<String>{ObjectUtil.getWithNameSpace('UserAction__c')}, ObjectUtil.getWithNameSpace('CaseManager__c'));
		        }catch(Exception ex){
		            System.debug(' Insufficient access');
		        }
		        
		        try{
		            SecurityHelper.CheckAllFieldCreateAccess(ObjectUtil.getWithNameSpace('CaseManager__c'));
		        }catch(Exception ex){
		            System.debug(' Insufficient access');
		        }
		        
		        try{
		            SecurityHelper.CheckAllFieldEditAccess(ObjectUtil.getWithNameSpace('CaseManager__c'));
		        }catch(Exception ex){
		            System.debug(' Insufficient access');
		        }
        	}
        Test.stopTest();
    }
}