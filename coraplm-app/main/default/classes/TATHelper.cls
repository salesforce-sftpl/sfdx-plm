/*
  Authors: Parth Lukhi
  Date created: 06/11/2017
  Purpose: This class contains all wrapper classes and common methods that use in TAT System.
  Dependencies: TATCalculation,TATRuleController
  -------------------------------------------------
  Modifications:
  Date: 
  Purpose of modification:
  Method/Code segment modified:
 
*/
public with sharing class TATHelper {

    /*
      Authors: Chandresh Koyani
      Purpose: Wrapper class to store option like value,text.
      Dependencies: TATRuleController.cls,TATTriggerHelper.cls.
    */
    public class Option {
        public string value { get; set; }
        public string text { get; set; }

        public Option() {
        }
        public Option(string value, string text) {
            this.value = value;
            this.text = text;
        }
    }


    /*
      Authors: Chandresh Koyani
      Purpose: Wrapper class to store field information.
      Dependencies: TATRuleController.cls,TATTriggerHelper.cls.
    */
    public class FieldInfo {
        public string label {
            get; set;
        }
        public string apiName {
            get; set;
        }
        public string type {
            get; set;
        }
        public list<Option> pickListValue {
            get; set;
        }
        public List<string> operator {
            get; set;
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Wrapper class to store Criteria values.
      Dependencies: TATRuleController.cls,TATTriggerHelper.cls.
    */
    public class Criteria {
        public string field { get; set; }
        public string operator { get; set; }
        public string value { get; set; }
        public string type { get; set; }
    }


    /*
      Authors: Chandresh Koyani
      Purpose: Wrapper class to store Criteria and other TAT values.
      Dependencies: TATRuleController.cls,TATTriggerHelper.cls.
    */
    public class JSONValue {
        Public List<Criteria> criterias { get; set; }
        public integer tatTime { get; set; }
        public string tatTimeUnit{get; set;}
        public string calendar { get; set; }
        public integer notificationTime{get;set;}
        public string notificationTimeUnit{get;set;}
        public string template{get;set;}
        public boolean sendNotification{get;set;}
        public string additionalEmails{get;set;}
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Wrapper class to store TAT Rule values.
      Dependencies: TATRuleController.cls,TATTriggerHelper.cls.
    */
    public class TATRule {
        public JSONValue JSONValue { get; set; }
        public string ruleName { get; set; }
        public integer order { get; set; }
        public string id { get; set; }
        public boolean isActive { get; set; }
    }
}