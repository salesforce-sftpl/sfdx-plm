/**
  * Authors     : Chandresh Koyani
  * Date created : 17/11/2017 
  * Purpose      : Test Class for TATRuleTest 
  * Dependencies : TATHelper.cls,TATTriggerHelper.cls,TATRuleController.cls
  * -------------------------------------------------
  *Modifications:
                Date:   
                Purpose of modification:  
                Method/Code segment modified: 

*/
@isTest
private class TATRuleControllerTest { 
    @testSetup 
    static  void  setupTestData() {
        
        CreateRuleTat();

        CaseManager__c caseObj=new CaseManager__c();
        caseObj.Process__c='AP';
        caseObj.WorkType__c='PO Invoices';
        caseObj.Document_Type__c='Electricity';
        caseObj.Requestor_Email__c='chandresh.koyani@test123.com';
        caseObj.Amount__c=12000;
        caseObj.Status__c='Ready for processing';
        caseObj.escalation__c=true;
        caseObj.UserAction__c='Process';
        insert caseObj;

        CaseManager__c caseObjNew=new CaseManager__c();
        caseObjNew.Process__c='AP';
        caseObjNew.WorkType__c='PO Invoices';
        caseObjNew.Document_Type__c='Electricity';
        caseObjNew.Requestor_Email__c='chandresh.koyani@test123.com';
        caseObjNew.Amount__c=2000;
        caseObjNew.Status__c='Ready for processing';
        caseObj.escalation__c=true;
        caseObj.UserAction__c='Process';
        insert caseObjNew;
    }
    @isTest
    public static void CreateRuleTat(){
        TATHelper.TATRule tatRuleObj=new TATHelper.TATRule();
        tatRuleObj.JSONValue=new TATHelper.JSONValue();
        List<BusinessHours> bhs=[select id from BusinessHours where IsDefault=true];

        tatRuleObj.JSONValue.criterias=new List<TATHelper.Criteria>();
        tatRuleObj.JSONValue.tatTime=100;
        tatRuleObj.JSONValue.calendar=bhs[0].id;
        tatRuleObj.JSONValue.tatTimeUnit='Minutes';
        tatRuleObj.JSONValue.notificationTime=20;
        tatRuleObj.JSONValue.notificationTimeUnit='Minutes';
        tatRuleObj.JSONValue.template='';
        tatRuleObj.JSONValue.additionalEmails='chandreshkoyani@sftpl.com';
        tatRuleObj.JSONValue.sendNotification=true;

        TATHelper.Criteria cre=null;
        cre=new TATHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Process__c');
        cre.Operator='contains';
        cre.Value='AP';
        cre.Type='PICKLIST';
        
        tatRuleObj.JSONValue.Criterias.add(cre);
        

        cre=new TATHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Worktype__c');
        cre.Operator='contains';
        cre.Value='PO Invoices';
        cre.Type='PICKLIST';
        
        tatRuleObj.JSONValue.Criterias.add(cre);

        cre=new TATHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Amount__c');
        cre.Operator='greater or equal';
        cre.Value='10000';
        cre.Type='NUMBER';
        
        tatRuleObj.JSONValue.Criterias.add(cre);
        
        TAT_Rule_Config__c tatConfig = new TAT_Rule_Config__c();
        tatConfig.Rule_Name__c = 'Test';
        tatConfig.JSON__c = JSON.serialize(tatRuleObj.JSONValue);
        tatConfig.IsActive__c=true;
        tatConfig.Order__c = 1;
        
        insert tatConfig;
    }
    @isTest
    public static void CreateRuleQCPerecentage(){
        TATHelper.TATRule tatRuleObj=new TATHelper.TATRule();
        tatRuleObj.JSONValue=new TATHelper.JSONValue();
        List<BusinessHours> bhs=[select id from BusinessHours where IsDefault=true];

        tatRuleObj.JSONValue.criterias=new List<TATHelper.Criteria>();
        tatRuleObj.JSONValue.tatTime=100;
        tatRuleObj.JSONValue.calendar=bhs[0].id;
        tatRuleObj.JSONValue.tatTimeUnit='Minutes';
        tatRuleObj.JSONValue.notificationTime=20;
        tatRuleObj.JSONValue.notificationTimeUnit='Minutes';
        tatRuleObj.JSONValue.template='';
        tatRuleObj.JSONValue.additionalEmails='chandreshkoyani@sftpl.com';
        tatRuleObj.JSONValue.sendNotification=true;

        TATHelper.Criteria cre=null;
        cre=new TATHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Process__c');
        cre.Operator='contains';
        cre.Value='AP';
        cre.Type='PICKLIST';
        
        tatRuleObj.JSONValue.Criterias.add(cre);
        

        cre=new TATHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Worktype__c');
        cre.Operator='contains';
        cre.Value='PO Invoices';
        cre.Type='PICKLIST';
        
        tatRuleObj.JSONValue.Criterias.add(cre);

        cre=new TATHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Amount__c');
        cre.Operator='greater or equal';
        cre.Value='1000';
        cre.Type='NUMBER';
        
        tatRuleObj.JSONValue.Criterias.add(cre);

        cre=new TATHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('escalation__c');
        cre.Operator='equals';
        cre.Value='True';
        cre.Type='BOOLEAN';
        
        tatRuleObj.JSONValue.Criterias.add(cre);
        
        TAT_Rule_Config__c tatConfig = new TAT_Rule_Config__c();
        tatConfig.Rule_Name__c = 'Test';
        tatConfig.JSON__c = JSON.serialize(tatRuleObj.JSONValue);
        tatConfig.IsActive__c=true;
        tatConfig.Order__c = 1;
        
        insert tatConfig;
    }
    public static testMethod void testTATCalculation(){
        List<CaseManager__c> caseObjList=[select id,Name,OwnerId,Process__c,WorkType__c,Amount__c,escalation__c,Tat_Rule__c,Target_TAT_Time__c from CaseManager__c];
        TATTriggerHelper.calculateTAT(caseObjList);

        Map<Id,CaseManager__c> caseTrackerMap=new Map<Id,CaseManager__c>();

        for(CaseManager__c caseObj : caseObjList){
            
            CaseManager__c caseObjNew=new CaseManager__c();
            caseObjNew.Process__c='AP';
            caseObjNew.WorkType__c='PO Invoices';
            caseObjNew.Document_Type__c='Electricity';
            caseObjNew.Requestor_Email__c='chandresh.koyani@test123.com';
            caseObjNew.Amount__c=2000;
            caseObjNew.Status__c='Ready for QC';
            caseObjNew.escalation__c=true;
            caseObjNew.Id=caseObj.id;
            
            caseTrackerMap.put(caseObj.id,caseObjNew);
        }

        TATTriggerHelper.calculateStateChange(caseObjList,caseTrackerMap);

        TATTriggerHelper.setActualTatTime(caseObjList);
    }
    public static testMethod void testIsMatchMethod(){
        
        TATHelper.Criteria cre=new TATHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Status__c');
        cre.Operator='equals';
        cre.Value='True';
        cre.Type='BOOLEAN';

        //Boolean check
        
        TATTriggerHelper.IsMatch(cre,true);
        cre.Operator='not equal to';
        TATTriggerHelper.IsMatch(cre,true);

        //Integer Check
        cre.Value='5';

        cre.Operator='not equal to';
        TATTriggerHelper.IsMatch(cre,1);

        cre.Operator='equals';
        TATTriggerHelper.IsMatch(cre,1);

        cre.Operator='less than';
        TATTriggerHelper.IsMatch(cre,2);

        cre.Operator='greater than';
        TATTriggerHelper.IsMatch(cre,2);

        cre.Operator='less or equal';
        TATTriggerHelper.IsMatch(cre,2);

        cre.Operator='greater or equal';
        TATTriggerHelper.IsMatch(cre,2);

        //String Check

        cre.Value='TEst';

        cre.Operator='not equal to';
        TATTriggerHelper.IsMatch(cre,'Test');

        cre.Operator='equals';
        TATTriggerHelper.IsMatch(cre,'Test');

        cre.Operator='starts with';
        TATTriggerHelper.IsMatch(cre,'Test');

        cre.Operator='end with';
        TATTriggerHelper.IsMatch(cre,'Test');

        cre.Operator='contains';
        TATTriggerHelper.IsMatch(cre,'Test');

        cre.Operator='does not contain';
        TATTriggerHelper.IsMatch(cre,'Test');

        cre.Operator='contains in list';
        TATTriggerHelper.IsMatch(cre,'Test');

        List<BusinessHours> bhs=[select id from BusinessHours where IsDefault=true];
        TATTriggerHelper.getDateDiffByBusinessHours(bhs[0].id,Datetime.Now(),Datetime.Now());
    }
    public static testMethod void testQCRuleController(){
        TATRuleController ruleController=new TATRuleController();
        TATRuleController.getAllRules();
        List<TATHelper.FieldInfo> fieldList= TATRuleController.GetCaseTrackerFields();

        TAT_Rule_Config__c tatRule = [select id,Name,JSON__c from TAT_Rule_Config__c limit 1];
        
        TATRuleController.ChangeState(tatRule.id,true);
        TATRuleController.ChangeOrder(1,'UP');
        TATRuleController.ChangeOrder(4,'Down');
        
        TATRuleController.ChangeOrder(3,'UP');
        

        TATHelper.TATRule tatRuleObj=new TATHelper.TATRule();
        tatRuleObj.Id=tatRule.Id;
        tatRuleObj.RuleName='Test1';
        tatRuleObj.Order=1;
        tatRuleObj.IsActive=true;
        tatRuleObj.JSONValue=(TATHelper.JSONValue) System.JSON.deserialize(tatRule.JSON__c, TATHelper.JSONValue.class);

        TATRuleController.SaveRule(tatRuleObj);

        tatRuleObj.Id='';

        TATRuleController.SaveRule(tatRuleObj);

        TATRuleController.RemoveRule(tatRule.id);

        TATRuleController.getBusinessHours();

        TATRuleController.getAllEmailTemplate();
    }
}