/*
  Authors: Chandresh Koyani
  Date created: 08/11/2017
  Purpose: This class contains static method to find TAT Rule and calculate Status change date.
  Dependencies: Called from "Case Tracker Trigger".
  -------------------------------------------------
  Modifications:
  Date: 28/12/2017
  Purpose of modification: Added User Score logic
  Method/Code segment modified:
 
*/
public class TATTriggerHelper {

    @InvocableMethod(label = 'Set Actual TAT Time' description = 'Set Actual Tat Time based on ')
    public static void setActualTatTime(List<CaseManager__c> caseList) {
        MAP<ID, Tat_Rule_Config__c> tatRuleConfiges = new MAP<ID, Tat_Rule_Config__c> ([select id, Name, JSON__c, Order__c, IsActive__c from Tat_Rule_Config__c limit 999]);
        Map<Id, User> userMap = new Map<Id, User> ([select id,Score__c, Current_Week_Score__c, Current_Month_Score__c, Last_Week_Score__c, Last_Month_Score__c, Last_Updated_Date__c from user]);
        
        List<CaseManager__c> updateList = new List<CaseManager__c> ();
        List<user> updateUserList = new List<user> ();
		List<BusinessHours> defaultBusinessHrs = [select id from BusinessHours where IsDefault = true];

        for (CaseManager__c caseTrackerObj : caseList) {
            CaseManager__c caseObjNew = new CaseManager__c();
            caseObjNew.id = caseTrackerObj.id;
            string businessHrsId = '';
            if (!string.isEmpty(caseTrackerObj.Tat_Rule__c)) {
                Tat_Rule_Config__c rule = tatRuleConfiges.get(caseTrackerObj.Tat_Rule__c);
                if (rule != null) {
                    TATHelper.JSONValue jsonVal = (TATHelper.JSONValue) System.JSON.deserialize(rule.JSON__c, TATHelper.JSONValue.class);
                    businessHrsId = jsonVal.calendar;
                }
				else{
					businessHrsId=defaultBusinessHrs[0].id;
				}
            }
            Datetime actualTatTime = datetime.now();
            if (!string.isEmpty(businessHrsId)) {
                actualTatTime = BusinessHours.addGmt(businessHrsId, datetime.now(), 0);

            }
            caseObjNew.Actual_TAT_Time__c = actualTatTime;
			if(caseTrackerObj.PreviousPerformer__c!=null && string.valueOf(caseTrackerObj.PreviousPerformer__c).startsWith('005')){
				 if (caseTrackerObj.Target_TAT_Time__c >= actualTatTime) {
                    User ur = userMap.get(caseTrackerObj.PreviousPerformer__c);
                    setUserScore(ur, Date.today());
                    updateUserList.add(ur);
                }
			}
            /*if (string.valueOf(caseTrackerObj.OwnerId).startsWith('005')) {
                if (caseTrackerObj.Target_TAT_Time__c >= actualTatTime) {
                    User ur = userMap.get(caseTrackerObj.OwnerId);
                    setUserScore(ur, Date.today());
                    updateUserList.add(ur);
                }
            }*/
            updateList.add(caseObjNew);

        }
        update updateUserList;
        update updateList;
    }
    /*
      Authors: Chandresh Koyani
      Purpose: Set actual TAT time based on calender.
      Dependencies: Called from "Case Tracker Trigger"
    */
    public static void setActualTatTime(List<CaseManager__c> caseTrackeListNew, Map<Id, CaseManager__c> caseTrackerOldMap) {
        List<CaseManager__c> updateList = new List<CaseManager__c> ();

        for (CaseManager__c caseTrackerNew : caseTrackeListNew) {
            CaseManager__c caseTrackerOld = caseTrackerOldMap.get(caseTrackerNew.Id);
            if (caseTrackerNew.Status__c != caseTrackerOld.Status__c && caseTrackerNew.Status__c == 'Pending For Archival') {
                updateList.add(caseTrackerNew);
            }
        }

        if (updateList.size() > 0) {
            setActualTatTime(updateList);
        }
    }
    /*
      Authors: Chandresh Koyani
      Purpose: Set User Score when case is complted withing TAT time.
      Dependencies: Called from "setActualTatTime"
    */
    private static void setUserScore(User ur, Date currentDate) {
        if (ur.Current_Month_Score__c == null) {
            ur.Current_Month_Score__c = 0;
        }
        if (ur.Current_Week_Score__c == null) {
            ur.Current_Week_Score__c = 0;
        }
        if (ur.Score__c == null) {
            ur.Score__c = 0;
        }
        if (ur.Last_Updated_Date__c == null || isSameMonth(ur.Last_Updated_Date__c, currentDate)) {
            ur.Current_Month_Score__c = ur.Current_Month_Score__c + 10;
        }
        else {
            ur.Last_Month_Score__c = ur.Current_Month_Score__c;
            ur.Current_Month_Score__c = 10;
        }

        if (ur.Last_Updated_Date__c == null || isSameWeek(ur.Last_Updated_Date__c, currentDate)) {
            ur.Current_Week_Score__c = ur.Current_Week_Score__c + 10;
        }
        else {
            ur.Last_Week_Score__c = ur.Current_Week_Score__c;
            ur.Current_Week_Score__c = 10;
        }
        ur.Score__c = ur.Score__c + 10;
        ur.Last_Updated_Date__c = currentDate;
        
    }
    /*
      Authors: Chandresh Koyani
      Purpose: Check both date are in same month.
      Dependencies: Called from "setUserScore"
    */
    public static boolean isSameMonth(Date firstDate, Date secondDate) {
        return(firstDate.month() == secondDate.month() && firstDate.Year() == secondDate.Year());
    }
    /*
      Authors: Chandresh Koyani
      Purpose: Check both date are in same week.
      Dependencies: Called from "setUserScore"
    */
    public static boolean isSameWeek(Date firstDate, Date secondDate) {
        Date weekStart = firstDate.toStartofWeek();
        return weekStart.daysBetween(secondDate) < 7;
    }
    /*
      Authors: Chandresh Koyani
      Purpose: Find matching TAT Rule based on criteria and fill in Case Tracker Object.
      Dependencies: Called from "Case Tracker Trigger"
     */
    public static void calculateTAT(List<CaseManager__c> caseTrackerList) {
        try {
            List<Tat_Rule_Config__c> tatRuleConfiges = [select id, Name, JSON__c, Order__c, IsActive__c from Tat_Rule_Config__c where IsActive__c = true order by Order__c];
			List<BusinessHours> defaultBusinessHrs = [select id from BusinessHours where IsDefault = true];

            for (CaseManager__c caseTracker : caseTrackerList) {
                TATHelper.TATRule rule = findRule(caseTracker, tatRuleConfiges);
                if (rule != null) {
                    caseTracker.Tat_Rule__c = rule.id;
                    if (!string.isEmpty(rule.JSONValue.calendar)) {
                        caseTracker.Target_TAT_Time__c = (BusinessHours.addGmt(rule.JSONValue.calendar, datetime.now(), getMiliSecondByUnit(rule.JSONValue.tatTimeUnit, rule.JSONValue.tatTime)));
                    }
					else{
						caseTracker.Target_TAT_Time__c = (BusinessHours.addGmt(defaultBusinessHrs[0].id, datetime.now(), getMiliSecondByUnit(rule.JSONValue.tatTimeUnit, rule.JSONValue.tatTime)));
					}
                }
				else{
					caseTracker.Target_TAT_Time__c = (BusinessHours.addGmt(defaultBusinessHrs[0].id, datetime.now(), getMiliSecondByUnit('Days', 1)));
				}

            }


        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('TATTriggerHelper', ex.getMessage(), ex.getStackTraceString());
        }

    }



    /*
      Authors: Chandresh Koyani
      Purpose: Find matching TAT rule based on case fields.
      Dependencies: Called from "CalculateTAT" method.
     */
    private static TATHelper.TATRule findRule(CaseManager__c caseTrackerObject, List<Tat_Rule_Config__c> tatRuleConfiges) {

        for (Tat_Rule_Config__c TATRuleConfig : tatRuleConfiges) {

            TATHelper.TATRule TATRule = new TATHelper.TATRule();
            TATRule.id = TATRuleConfig.id;
            TATRule.JSONValue = (TATHelper.JSONValue) System.JSON.deserialize(TATRuleConfig.JSON__c, TATHelper.JSONValue.class);

            if (TATRule.JSONValue != null && TATRule.JSONValue.criterias != null) {
                boolean isMatch = false;
                for (TATHelper.Criteria cre : TATRule.JSONValue.criterias) {
                    if (cre.type.equalsIgnoreCase('TEXT') || cre.type.equalsIgnoreCase('PICKLIST') || cre.type.equalsIgnoreCase('EMAIL') || cre.type.equalsIgnoreCase('PHONE') || cre.type.equalsIgnoreCase('STRING') || cre.type.equalsIgnoreCase('TEXTAREA')) {
                        string fieldValue = string.valueOf(caseTrackerObject.get(cre.Field));
                        isMatch = isMatch(cre, fieldValue);
                    }
                    else if (cre.type.equalsIgnoreCase('NUMBER') || cre.type.equalsIgnoreCase('CURRENCY') || cre.type.equalsIgnoreCase('DOUBLE')) {
                        double fieldValue = double.valueOf(caseTrackerObject.get(cre.Field));
                        isMatch = isMatch(cre, fieldValue);
                    }
                    else if (cre.type.equalsIgnoreCase('BOOLEAN')) {
                        boolean fieldValue = boolean.valueOf(caseTrackerObject.get(cre.Field));
                        isMatch = isMatch(cre, fieldValue);
                    }
                    if (!isMatch) {
                        break;
                    }
                }

                if (isMatch) {
                    return TATRule;
                }
            }
        }
        return null;

    }

    /*
      Authors: Chandresh Koyani
      Purpose: Check value is match based on operator is provided, This method is use only for string value.
      Dependencies: Called from "FindRule" method.
     */
    @testVisible
    private static boolean isMatch(TATHelper.Criteria car, string fieldValue) {
        if (car.operator.equalsIgnoreCase('equals')) {
            return fieldValue == car.Value;
        }
        else if (car.operator.equalsIgnoreCase('not equal to')) {
            return fieldValue != car.Value;
        }
        else if (car.operator.equalsIgnoreCase('starts with')) {
            return fieldValue.startsWithIgnoreCase(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('end with')) {
            return fieldValue.endsWithIgnoreCase(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('contains')) {
            return fieldValue.containsIgnoreCase(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('does not contain')) {
            return !fieldValue.containsIgnoreCase(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('contains in list')) {
            if (!string.isEmpty(car.Value)) {
                List<string> tempList = car.Value.split(',');
                for (string str : tempList) {
                    if (car.type.equalsIgnoreCase('PICKLIST')) {
                        if (fieldValue!=null && fieldValue.equalsIgnoreCase(str)) {
                            return true;
                        }
                    }
                    else {
                        if (fieldValue!=null && fieldValue.containsIgnoreCase(str)) {
                            return true;
                        }
                    }
                }
            }
        }
		else if (car.operator.equalsIgnoreCase('does not contains in list')) {
			if (!string.isEmpty(car.Value)) {
				List<string> tempList = car.Value.split(',');
				for (string str : tempList) {
					if (fieldValue!=null && fieldValue.equalsIgnoreCase(str)) {
							return false;
					}
				}
			}
			return true;
		}
        return false;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Check value is match based on operator is provided, This method is use only for double value.
      Dependencies: Called from "FindRule" method.
     */
    @testVisible
    private static boolean isMatch(TATHelper.Criteria car, double fieldValue) {
        if (car.operator.equalsIgnoreCase('equals')) {
            return fieldValue == double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('not equal to')) {
            return fieldValue != double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('less than')) {
            return fieldValue < double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('greater than')) {
            return fieldValue > double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('less or equal')) {
            return fieldValue <= double.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('greater or equal')) {
            return fieldValue >= double.valueOf(car.Value);
        }
        return false;
    }
    /*
      Authors: Chandresh Koyani
      Purpose: Check value is match based on operator is provided, This method is use only for boolean value.
      Dependencies: Called from "FindRule" method.
     */
    @testVisible
    private static boolean isMatch(TATHelper.Criteria car, boolean fieldValue) {
        if (car.operator.equalsIgnoreCase('equals')) {
            return fieldValue == boolean.valueOf(car.Value);
        }
        else if (car.operator.equalsIgnoreCase('not equal to')) {
            return fieldValue != boolean.valueOf(car.Value);
        }
        return false;
    }

    /*
      Authors: Chandresh Koyani
      Purpose: This method is called from trigger when status is changed it will create new object for that status.
      Dependencies: Called from "CaseTrackerTrigger"
     */
    public static void calculateStateChange(List<CaseManager__c> caseTrackeListNew, Map<Id, CaseManager__c> caseTrackerOldMap) {
        try {
            MAP<ID, Tat_Rule_Config__c> tatRuleConfiges = new MAP<ID, Tat_Rule_Config__c> ([select id, Name, JSON__c, Order__c, IsActive__c from Tat_Rule_Config__c limit 999]);

            List<MileStone__c> caseTrasitionList = [select id, Current_State__c, Start_Date__c, End_Date__c, CaseManager__c from MileStone__c where CaseManager__c = :caseTrackerOldMap.keySet() and End_Date__c = null];
			List<BusinessHours> defaultBusinessHrs = [select id from BusinessHours where IsDefault = true];

            Map<id, List<MileStone__c>> caseManagerTransitionMap = new Map<id, List<MileStone__c>> ();

            for (MileStone__c caseTransition : caseTrasitionList) {
                List<MileStone__c> tempList = caseManagerTransitionMap.get(caseTransition.CaseManager__c);
                if (tempList == null) {
                    tempList = new List<MileStone__c> ();
                }
                tempList.add(caseTransition);
                caseManagerTransitionMap.put(caseTransition.CaseManager__c, tempList);
            }

            List<MileStone__c> caseTransitionInsertList = new List<MileStone__c> ();
            List<MileStone__c> caseTransitionUpdateList = new List<MileStone__c> ();

            for (CaseManager__c caseTrackerNew : caseTrackeListNew) {
                CaseManager__c caseTrackerOld = caseTrackerOldMap.get(caseTrackerNew.Id);

                if (caseTrackerNew.Status__c != caseTrackerOld.Status__c) {
                    MileStone__c currentTransition = null;

                    List<MileStone__c> transList = caseManagerTransitionMap.get(caseTrackerOld.id);
                    if (transList != null) {
                        for (MileStone__c trans : transList) {
                            if (trans.Current_State__c == caseTrackerOld.Status__c) {
                                currentTransition = trans;
                                break;
                            }
                        }
                    }
                    string businessHrsId = defaultBusinessHrs[0].id;
                    if (!string.isEmpty(caseTrackerOld.Tat_Rule__c)) {
                        Tat_Rule_Config__c rule = tatRuleConfiges.get(caseTrackerOld.Tat_Rule__c);
                        if (rule != null) {
                            TATHelper.JSONValue jsonVal = (TATHelper.JSONValue) System.JSON.deserialize(rule.JSON__c, TATHelper.JSONValue.class);
                            businessHrsId = jsonVal.calendar;
                        }
                    }

                    if (currentTransition != null) {
                        currentTransition.End_Date__c = System.now();
                        if (!string.isEmpty(businessHrsId)) {
                            currentTransition.Time_Taken_In_Mins__c = getDateDiffByBusinessHours(businessHrsId, currentTransition.Start_Date__c, currentTransition.End_Date__c);
                        }
                        caseTransitionUpdateList.add(currentTransition);
                    }

                    MileStone__c currentTransitionNew = new MileStone__c();
                    currentTransitionNew.CaseManager__c = caseTrackerNew.Id;
                    currentTransitionNew.Current_State__c = caseTrackerNew.Status__c;
                    currentTransitionNew.Start_Date__c = System.now();
                    caseTransitionInsertList.add(currentTransitionNew);

                }

            }
            if (caseTransitionInsertList.size() > 0) {
                insert caseTransitionInsertList;
            }
            if (caseTransitionUpdateList.size() > 0) {
                update caseTransitionUpdateList;
            }
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('TATTriggerHelper', ex.getMessage(), ex.getStackTraceString());
        }
    }
    /*
      Authors: Chandresh Koyani
      Purpose: This method is called from Trigger when Case Tracker is inserted, it will create new Case Transition object.
      Dependencies: Called from "CaseTrackerTrigger"
     */
    public static void createCaseTransition(List<CaseManager__c> caseTrackeListNew) {
        try {
            List<MileStone__c> caseTransitionUpsertList = new List<MileStone__c> ();


            for (CaseManager__c caseTrackerNew : caseTrackeListNew) {
                if (!string.isEmpty(caseTrackerNew.Status__c)) {
                    MileStone__c currentTransition = new MileStone__c();
                    currentTransition.CaseManager__c = caseTrackerNew.Id;
                    currentTransition.Current_State__c = caseTrackerNew.Status__c;
                    currentTransition.Start_Date__c = System.now();
                    caseTransitionUpsertList.add(currentTransition);
                }
            }

            if (caseTransitionUpsertList.size() > 0) {
                insert caseTransitionUpsertList;
            }
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('TATTriggerHelper', ex.getMessage(), ex.getStackTraceString());
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Add new TAT Notification entry when case is created.
      Dependencies: Called from "CaseTrackerTrigger"
     */
    public static void createTATNotification(List<CaseManager__c> caseTrackeListNew) {
        MAP<ID, Tat_Rule_Config__c> tatRuleConfiges = new MAP<ID, Tat_Rule_Config__c> ([select id, Name, JSON__c, Order__c, IsActive__c from Tat_Rule_Config__c limit 999]);

        List<Reminder_And_Escalation__c> reminderAndEscalationList = new List<Reminder_And_Escalation__c> ();

        for (CaseManager__c caseTrackerNew : caseTrackeListNew) {
            if (!string.isEmpty(caseTrackerNew.Tat_Rule__c)) {
                Tat_Rule_Config__c rule = tatRuleConfiges.get(caseTrackerNew.Tat_Rule__c);
                if (rule != null) {
                    TATHelper.JSONValue jsonVal = (TATHelper.JSONValue) System.JSON.deserialize(rule.JSON__c, TATHelper.JSONValue.class);
                    if (jsonVal.sendNotification != null && jsonVal.sendNotification) {
                        Reminder_And_Escalation__c reminderObj = new Reminder_And_Escalation__c();
                        reminderObj.Case_Manager__c = caseTrackerNew.id;
                        reminderObj.Due_Date__c = (BusinessHours.addGmt(jsonVal.calendar, caseTrackerNew.Target_TAT_Time__c, getMiliSecondByUnit(jsonVal.notificationTimeUnit, jsonVal.notificationTime) * - 1));
                        reminderObj.Org_Wide_Id__c = ''; // notificationObj.fromAddress;
                        reminderObj.Template__c = jsonVal.template;
                        reminderObj.Type__c = 'TAT';

                        if (!string.isEmpty(jsonVal.additionalEmails)) {
                            reminderObj.Additional_Recipient__c = jsonVal.additionalEmails;
                            reminderObj.Include_In__c = 'TO';
                        }

                        reminderAndEscalationList.add(reminderObj);
                    }
                }
            }
        }
        insert reminderAndEscalationList;
    }

    public static void removeTatNotification(List<CaseManager__c> caseTrackeListNew, Map<Id, CaseManager__c> caseTrackerOldMap) {
        try {
            Set<string> tatRemoveStateList = new Set<string> ();
            tatRemoveStateList.add('Rejected');
            tatRemoveStateList.add('Completed');
            tatRemoveStateList.add('Pending For Archival');

            List<id> caseIdsForStateChange = new List<id> ();
            for (CaseManager__c caseTrackerNew : caseTrackeListNew) {
                CaseManager__c caseTrackerOld = caseTrackerOldMap.get(caseTrackerNew.Id);
                if (caseTrackerNew.Status__c != caseTrackerOld.Status__c) {
                    if (caseTrackerNew.Status__c == 'Rejected' || caseTrackerNew.Status__c == 'Completed' || caseTrackerNew.Status__c == 'Pending For Archival') {
                        caseIdsForStateChange.add(caseTrackerNew.Id);
                    }
                }
            }
            if (caseIdsForStateChange.size() > 0) {
                List<Reminder_And_Escalation__c> reminderList = [select id from Reminder_And_Escalation__c where Type__c IN('TAT') and Case_Manager__C = :caseIdsForStateChange];
                delete reminderList;
            }
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('ReminderAndEscalationHelper', ex.getMessage(), ex.getStackTraceString());
        }
    }

    /*
      Authors: Chandresh Koyani
      Purpose: Calculate Date Diff based on bussiness hours.
      Dependencies: Called from "CaseTrackerTrigger"
     */
    @testVisible
    private static Decimal getDateDiffByBusinessHours(string businessHrsId, Datetime dt1, Datetime dt2) {
        long Diff1 = BusinessHours.diff(businessHrsId, dt1, dt2);
        Decimal dateDiff = Diff1 / 1000.0 / 60.0;
        return dateDiff;
    }

    private static long getMiliSecondByUnit(string unit, integer tatTime) {
        long returnVal;
        if (unit.endsWithIgnoreCase('Days')) {
            returnVal = tatTime * 24 * 60 * 60 * 1000L;
        }
        else if (unit.endsWithIgnoreCase('Hours')) {
            returnVal = tatTime * 60 * 60 * 1000L;
        }
        else {
            returnVal = tatTime * 60 * 1000L;
        }
        return returnVal;
    }
}