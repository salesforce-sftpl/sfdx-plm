/**
    * Authors    : Atul Hinge
    * Date created : 16/11/2017 
    * Purpose      : Test Data Generator for All Test Classes
    * Dependencies : -
    * -------------------------------------------------
    *Modifications:
        Date:   
        Purpose of modification:  
        Method/Code segment modified: 

*/
public class TestDataGenerator {
    
    public static List<CaseManager__c> createCases(integer count){
        List<CaseManager__c> ctList=new List<CaseManager__c>(); 
        for(integer i=0;i<count;i++){
            ctList.add(getCase());
        }
        insert ctList;
        return ctList;
    }
     public static CaseManager__c getCase(){
        CaseManager__c ct= new CaseManager__c();
        ct.Subject__c='Invalid Invoice';
        ct.Status__c='Ready For Processing';
        return ct;
    }
    public static CaseManager__c createCaseTracker(){
        CaseManager__c ct= new CaseManager__c();
        ct.Subject__c='Invalid Invoice';
        ct.Status__c='Ready For Processing';
        insert ct;
        return ct;
    }
    public static EmailMessage createEmailMessage(CaseManager__c ct){
        EmailMessage emailMsg=new EmailMessage();
        emailMsg.FromName='Mr. John Green';
        emailMsg.ToAddress='ToAbc@Domain.com';
        emailMsg.CcAddress='ToAbc@Domain.com';
        emailMsg.BccAddress='ToAbc@Domain.com';
        emailMsg.FromAddress='FromAbc@Domain.com';
        emailMsg.HtmlBody='test';
        emailMsg.TextBody='test';
        emailMsg.Incoming=true;
        emailMsg.Status='1';
        emailMsg.Subject='Subject';
        emailMsg.RelatedToId=ct.Id;
        insert emailMsg;
        return emailMsg;
    }
    public static void createAppConfig()
    {
         AppConfig__c appConf = new AppConfig__c();
          appConf.Allow_Image_Size__c = 20;
          appConf.Content_Version_Extension__c = 'pdf,txt';
          insert appConf;
    }
    public static ID createContent(CaseManager__c ct){ 
         
          ContentVersion cont = new ContentVersion();
            cont.Title = 'file.png';
            cont.PathOnClient = 'file.png';
            cont.VersionData = EncodingUtil.base64Decode('Test Blob');
            //cont.AttachmentType__c='Related'; 
            cont.Origin = 'H';
            cont.IsMajorVersion =true;
            cont.Title ='Google.com'; 
        insert cont; 
        
        ContentVersion testContent = [SELECT ContentDocumentId FROM ContentVersion where Id = :cont.Id]; 
        ContentDocumentLink cntent=new ContentDocumentLink ();
        cntent.ContentDocumentId=testContent.ContentDocumentId;
        cntent.LinkedEntityId=ct.Id;
        cntent.ShareType= 'V';
        cntent.Visibility='AllUsers';
       
        insert cntent;
               
        return testContent.ContentDocumentId; 
    }
    public static User createUser() {
        List<CaseManager__c> ctList= TestDataGenerator.createCases(1);
        TestDataGenerator.createAppConfig();
        String attachId= TestDataGenerator.createContent(ctList[0]);
        
        // Setup test data
        // Create a unique UserName
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Chatter Only User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName=uniqueUserName);

        return u;
    }
   
    public static EmailTemplate createEmailTemplate(){
        
        List<Folder> lstFolder = [Select Id From Folder LIMIT 3];
        System.debug('lstFolder' + lstFolder);
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        EmailTemplate validEmailTemplate = new EmailTemplate();
        System.runAs ( thisUser ) {
                validEmailTemplate.isActive = true;
                validEmailTemplate.Name = 'TemplteTemplate1';
                validEmailTemplate.DeveloperName = 'unique_name11';
                validEmailTemplate.TemplateType = 'text';
                validEmailTemplate.FolderId = lstFolder[2].Id;
                validEmailTemplate.Subject = 'Email template Subject }{';
                validEmailTemplate.Body = 'Email template body][';
                insert validEmailTemplate;
        }
        
        return validEmailTemplate;
    }
    
    
}