/**
  * @author rahul.pastagiya@newplm.dev
  * @description To displayed cases in shared tab. 
  * To update field "PreviousQueueName__c" by Queue API Name when Owner assiged to case.
  *
  *
  * Updates: Tej Pal | Need to rewrite trigger using salesforce 
*/
trigger CaseManagerTrigger on CaseManager__c(before insert, before update, after update, after insert) {
    if (Trigger.isBefore && Trigger.isUpdate) {        
        
        //To update pervious queue when any user is assigned to case 
        CaseManagerHandler.updatePrevQueue(trigger.new, trigger.old);
        //To populate report related fields
        CaseManagerHandler.populateReportRelatedFields(trigger.new, trigger.old);        
               
        QCCalculation.checkForQC(Trigger.New, Trigger.OldMap);

    }
    if (Trigger.isAfter && Trigger.isUpdate) {
        
        TATTriggerHelper.calculateStateChange(Trigger.New, Trigger.OldMap);
        ReminderAndEscalationHelper.removeReminderObject(Trigger.New, Trigger.OldMap);
        TATTriggerHelper.removeTatNotification(Trigger.New, Trigger.OldMap);
        TATTriggerHelper.setActualTatTime(Trigger.New, Trigger.OldMap);
        ReminderAndEscalationHelper.setReminderAndEscalation(Trigger.New, Trigger.OldMap);
    }
    if (Trigger.isAfter && Trigger.isInsert) {
        TATTriggerHelper.CreateCaseTransition(trigger.new);
        TATTriggerHelper.createTATNotification(trigger.new);
        //Start - Chandresh - below code is not needed any more than please remove it
        for (CaseManager__c caseTracker : trigger.new) {
            if (caseTracker.Status__c != 'Rejected' && !Test.isRunningTest()) {
                System.debug('SendNotfication Start...');
                /* Start : Harshil Added on 02/02/2018 For Popup Notifications For PUX-299*/
                PLMUtilitiesController.sendNotification(trigger.new);
                /* End : Harshil Added on 02/02/2018 For Popup Notifications For PUX-299*/
                //MessageHelper.SendMessage('NewCaseNotfication', caseTracker.Subject__c, 'New case created');
            }
            //Start-Mohit - below piece of code is used for prediction of Indexing fields & SLA using ML for Email to Case
            if (caseTracker.Input_Source__c != 'Manual'){
                try{
                    Type result = Type.forName('RequestDataFromAWS');
                    if(result != null){
                        if(!Test.isRunningTest()){
                            RequestDataFromAWS.sendRequest(caseTracker.Id);
                        }
                    }
                }catch (exception e){
                    System.debug('Class not found ' +e);
                } 
            }
            //End - by Mohit 
        }
        //End - Chandresh 
    }
    if (Trigger.isbefore && Trigger.isInsert) {
        TATTriggerHelper.calculateTAT(trigger.new);
    }

}