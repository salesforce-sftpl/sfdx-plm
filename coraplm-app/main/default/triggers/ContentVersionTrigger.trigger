trigger ContentVersionTrigger on ContentVersion (before insert) {
    
    if ((Trigger.isBefore && Trigger.isInsert)) { 
       // To update flag of attachment
       ContentVerionHandler.updateFlag(trigger.new, trigger.old);
    }

}