/*
* Authors      :  Rahul Pastagiya
* Date created :  24/08/2017
* Purpose      :  Trigger on After INSERT/UPDATE/DELETE/UNDELETE event to set unread email count in on case object
* Dependencies :  None
*   __________________________________________________________________________
*   Author : Rahu Pastagiya
*   Modifications: Delegates to Apex class EmailMessageUtil
*   Date:  09/01/2018
*   Purpose of modification: Modularise code 
*   Method/Code segment modified: 
*/
trigger EmailMessageTrigger on EmailMessage(before insert, after insert, after update, after delete, after undelete) {
    /*
    Authors: Rahul Pastagiya
    Purpose: PUX-333
    */
    if (Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate || Trigger.isDelete || Trigger.isUndelete)) {
        EmailMessageHandler.calUnreadEmailCntAndUpdateCase(trigger.new);
    }
    //PUX-333
    if(Trigger.isbefore && Trigger.isInsert){
        EmailMessageHandler.markOutboundMessageAsRead(trigger.new);
    } 
}